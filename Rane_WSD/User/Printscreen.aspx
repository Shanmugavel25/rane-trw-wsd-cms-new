﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Printscreen.aspx.cs" Inherits="Rane_WSD.User.Printscreen" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<head id="Head1" runat="server">
    <title></title>
    <style type="text/css" media="print">

</style>
    <style type="text/css">
            

*{font-family:Arial, Helvetica, sans-serif; font-size:100%;
	color: #003399;
	font-weight: 600;
}
        .table-bordered {
            border:1px solid;
        }
        .tr {
            border:1px solid #003399;
        }
          .td {
            border:1px solid #003399 !important;
            border-bottom:1px solid #003399 !important;
        }
    p.MsoNormal
	{margin-top:0in;
	margin-right:0in;
	margin-bottom:10.0pt;
	margin-left:0in;
	line-height:115%;
	font-size:11.0pt;
	font-family:"Calibri","sans-serif";
	}
        .style3
        {
            width: 800px;
        }
        .style4
        {
          width: 780px;
        }
        .style5
        {
            height: 100px;
        }
        .style8
        {
            height: 70px;
        }
        .style9
        {
            height: 35px;
        }
        </style>
</head>
<body onload="window.print()">
    <form id="form1" runat="server">
    <div>
    <table class="style3" cellspacing="0">
            <tr>
                <td>
                    <table class="table table-bordered  style4" cellspacing="0">
                        <tr>
                           
                        </tr>
                        <tr>
                            <td>
                               
                                <img src="../assets/images/logo.jpg" style="width:180px;" />
                                </td><td>
                                <div style="text-align:center"><h3>Rane TRW Spares Claim Report</h3></div>
                                    
                           
                                <div style="text-align:center"><h6>(Failure Information Report)</h6></div>
                               
                            
</td>                            
                           
                        </tr>
                        <tr>
                                <td valign="center" style="height:50px;border:1px solid #003399" align="center">
                                    <asp:Label ID="Label3"  runat="server" Font-Bold="true"  Text=" FIR No "></asp:Label>
                                </td>
                                <td valign="center" style="height:50px;border:1px solid #003399" align="center">
                                    <asp:Label ID="Label4" runat="server"  Text=" WSD Name "></asp:Label>
                                   
                                </td>
                               <td valign="center" style="height:50px;border:1px solid #003399" align="center">
                                    <asp:Label ID="Label5" runat="server"  Text="WSD Branch"></asp:Label>
                                   
                                </td>
                            </tr>
                          <tr>
                                <td valign="center" style="height:50px;border:1px solid #003399" align="center">
                                     <asp:Label ID="lblFIRNo" style="color:gray;font-weight:400" runat="server" Text=""></asp:Label>
                                </td>
                                <td valign="center" style="height:50px;border:1px solid #003399" align="center">
                                    <asp:Label ID="lblWSDName" style="color:gray;font-weight:400" runat="server" Text=""></asp:Label>
                                </td>
                              <td valign="center" style="height:50px;border:1px solid #003399" align="center">
                                    <asp:Label ID="lblWSDBranch" runat="server" style="color:gray;font-weight:400" Text=""></asp:Label>
                                </td>
                            </tr>
                         <tr>
                                <td valign="center" style="height:50px;border:1px solid #003399" align="center">
                                    <asp:Label ID="Label7" runat="server" Text=" Date of Complaint "></asp:Label>
                                </td>
                                <td valign="center" style="height:50px;border:1px solid #003399" align="center">
                                    <asp:Label ID="Label8" runat="server" Text=" Retailer Name "></asp:Label>
                                   
                                </td>
                               <td valign="center" style="height:50px;border:1px solid #003399" align="center">
                                    <asp:Label ID="Label9" runat="server" Text="Retailer Location"></asp:Label>
                                   
                                </td>
                            </tr>
                          <tr>
                                <td valign="center" style="height:50px;border:1px solid #003399" align="center">
                                     <asp:Label ID="lblDateofComplaint" style="color:gray;font-weight:400" runat="server" Text=""></asp:Label>
                                </td>
                                <td valign="center" style="height:50px;border:1px solid #003399" align="center">
                                    <asp:Label ID="lblRetailerName" style="color:gray;font-weight:400" runat="server" Text=""></asp:Label>
                                </td>
                              <td valign="center" style="height:50px;border:1px solid #003399" align="center">
                                    <asp:Label ID="lblRetailerLocation" style="color:gray;font-weight:400" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                        <tr>
                                <td  style="height:50px;border:1px solid #003399" >
                                    &nbsp;<asp:Label ID="Label10" runat="server" Text=" WSD Invoice No :"></asp:Label>
                                    <asp:Label ID="lblWSDInvoiceNo" style="color:gray;font-weight:400" runat="server" Text=""></asp:Label>
                                </td>
                                <td  style="height:50px;border:1px solid #003399" >
                                    &nbsp;<asp:Label ID="Label11" runat="server" Text=" Invoice Date :"></asp:Label>
                                     <asp:Label ID="lblInvoiceDate" style="color:gray;font-weight:400" runat="server" Text=""></asp:Label>
                                   
                                </td>
                               <td  style="height:50px;border:1px solid #003399" >
                                    &nbsp;<asp:Label ID="Label12" runat="server" Text="Part No :"></asp:Label>
                                   <asp:Label ID="lblPartNo" runat="server" style="color:gray;font-weight:400"  Text=""></asp:Label>
                                   
                                </td>
                            </tr>
                        <%--  <tr>
                                <td valign="center" style="height:50px;border:1px solid #003399" align="center">
                                     
                                </td>
                                <td valign="center" style="height:50px;border:1px solid #003399" align="center">
                                   
                                </td>
                              <td valign="center" style="height:50px;border:1px solid #003399" align="center">
                                    
                                </td>
                            </tr>--%>
                        <%--<tr style="border:1px solid #003399">
                          
                                <td valign="center" halign="center" style="height:50px;border:1px solid #003399" align="left">
                                  <div id="Div1" class="t" style="margin-bottom:10px"><asp:Label ID="lblFIRNo" runat="server" Text="FIR No: "></asp:Label><asp:Label ID="lblFIRNo1" runat="server" Text=""></asp:Label></div>
                                <div id="Div4" class="t" style="border-top:1px solid #003399;margin-top:10px"><asp:Label ID="lblWSDName" runat="server" Text="WSD Name: "></asp:Label><asp:Label ID="lblWSDName1" runat="server" Text=""></asp:Label></div> 
                                <div id="Div5" class="t" style="border-top:1px solid #003399;margin-top:10px"><asp:Label ID="lblWSDBranch" runat="server" Text="WSD Branch: "></asp:Label><asp:Label ID="lblWSDBranch1" runat="server" Text=""></asp:Label></div>
                                    
                                    <div id="Div9" class="t" style="border-top:1px solid #003399;margin-top:10px">
                                        <br /><asp:Label ID="lblDateofComplaint" runat="server" Text="Date of Complaint: "></asp:Label><asp:Label ID="lblDateofComplaint1" runat="server" Text=""></asp:Label></div> 
                                </td>
                               <%-- <td valign="center" style="height:50px;border:1px solid #003399" align="center">
                                   <asp:ImageButton ID="ImageButton1" runat="server" style="height:120px;width:120px;" />
                                </td>
                             <td valign="center" halign="center" style="height:50px;border:1px solid #003399" align="left">
                                     <div id="Div6" class="t" style="margin-bottom:10px"><asp:Label ID="lblretailername" runat="server" Text="Retailer Name: "></asp:Label><asp:Label ID="lblretailername1" runat="server" Text=""></asp:Label></div>
                                <div id="Div7" class="t" style="border-top:1px solid #003399;margin-top:10px"><asp:Label ID="lblRetailerlocation" runat="server" Text="Retailer Location: "></asp:Label><asp:Label ID="lblRetailerlocation1" runat="server" Text=""></asp:Label></div> 
                                <div id="Div8" class="t" style="border-top:1px solid #003399;margin-top:10px"><asp:Label ID="lblWSDInvoiceNo" runat="server" Text="WSD Invoice No: "></asp:Label><asp:Label ID="lblWSDInvoiceNo1" runat="server" Text=""></asp:Label></div>
                                
                                 <div id="Div10" class="t" style="border-top:1px solid #003399;margin-top:10px">
                                      <br /><asp:Label ID="lblInvoiceDate" runat="server" Text="Invoice Date: "></asp:Label><asp:Label ID="lblInvoiceDate1" runat="server" Text=""></asp:Label></div> 
                                <td valign="center" halign="center" style="height:50px;border:1px solid #003399" align="left">
                                    
                                <div id="span1" class="t" style="margin-top:10px"><asp:Label ID="lblPartNo" runat="server" Text="Part No: "></asp:Label><asp:Label ID="lblPartNo1" runat="server" Text=""></asp:Label></div> 
                                <div id="Div2" class="t" style="border-top:1px solid #003399;margin-top:10px"><asp:Label ID="lblParttype" runat="server" Text="Part Type: "></asp:Label><asp:Label ID="lblParttype1" runat="server" Text=""></asp:Label></div>
                                </td>
                            
                           
                          
                            </tr>--%>
                         <tr >
                                <td valign="center" style="height:50px;border:1px solid #003399" align="center">
                                    <asp:Label ID="Label2" runat="server" Text="Aggregate Serial No  "></asp:Label>
                                </td>
                              <td valign="center" style="height:50px;border:1px solid #003399" align="center">
                                    <asp:Label ID="Label14" runat="server" Text="Aggregate Model  "></asp:Label>
                                </td>
                              <td valign="center" style="height:50px;border:1px solid #003399" align="center">
                                    <asp:Label ID="Label13" runat="server" Text="Status  "></asp:Label>
                                </td>
                                
                             
                                
                             
                               
                            </tr>
                        <tr>
                            <%-- <td valign="center" style="height:50px;border:1px solid #003399" align="center">
                                    <asp:Label ID="Label6" runat="server" Text="Status : "></asp:Label>
                                </td>--%>
                            <td valign="center" style="height:50px;border:1px solid #003399" align="center" >
                                    <asp:Label ID="lblAggregateSerialNo1" style="color:gray;font-weight:400" runat="server" Text=""></asp:Label>
                                </td>
                            <td valign="center" style="height:50px;border:1px solid #003399" align="center" >
                                    <asp:Label ID="lblAggregateModel" style="color:gray;font-weight:400" runat="server" Text=""></asp:Label>
                                </td>
                                <td valign="center" style="height:50px;border:1px solid #003399" align="center" >
                                    <asp:Label ID="lblStatus1" style="color:gray;font-weight:400" runat="server" Text=""></asp:Label>
                                </td>
                        </tr>
                           

                          <tr>
                                <td valign="center" style="height:50px;border:1px solid #003399" align="center">
                                    <asp:Label ID="lblallocatedServiceEng" runat="server" Text=" Service Engineer "></asp:Label>
                                </td>
                                <td valign="center" style="height:50px;border:1px solid #003399" align="center">
                                    <asp:Label ID="lblAllocatedASC" runat="server" Text="Allocated ASC "></asp:Label>
                                   
                                </td>
                               <td valign="center" style="height:50px;border:1px solid #003399" align="center">
                                    <asp:Label ID="lblASCRecommendation" runat="server" Text="ASC Recommendation"></asp:Label>
                                   
                                </td>
                            </tr>
                          <tr>
                                <td valign="center" style="height:50px;border:1px solid #003399" align="center">
                                     <asp:Label ID="lblallocatedServiceEng1" style="color:gray;font-weight:400" runat="server" Text=""></asp:Label>
                                </td>
                                <td valign="center" style="height:50px;border:1px solid #003399" align="center">
                                    <asp:Label ID="lblAllocatedASC1" style="color:gray;font-weight:400" runat="server" Text=""></asp:Label>
                                </td>
                              <td valign="center" style="height:50px;border:1px solid #003399" align="center">
                                    <asp:Label ID="lblASCRecommendation1" style="color:gray;font-weight:400" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                         
                         <tr>
                                <td valign="center" style="height:50px;border:1px solid #003399" align="center">
                                    <asp:Label ID="Label16" runat="server" Text="ASC Reported Date "></asp:Label>
                                </td>
                                <td valign="center" style="height:50px;border:1px solid #003399" align="center">
                                    <asp:Label ID="Label17" runat="server" Text=" Service Eng FeedBack "></asp:Label>
                                </td>
                                <td valign="center" style="height:50px;border:1px solid #003399" align="center">
                                    <asp:Label ID="Label18" runat="server" Text="Service Recommendation"></asp:Label>
                                </td>
                            
                            </tr>
                                  <tr>
                                <td valign="center" style="height:50px;border:1px solid #003399" align="center">
                                    <asp:Label ID="lblASCRepotedDate1" runat="server" style="color:gray;font-weight:400" Text="" Dateformat="dd/MM/yyyy"></asp:Label>
                                </td>
                                <td valign="center" style="height:50px;border:1px solid #003399" align="center">
                                   <asp:Label ID="lblServiceApproveReject" style="color:gray;font-weight:400" runat="server" Text=""></asp:Label>
                                </td>
                                <td valign="center" style="height:50px;border:1px solid #003399" align="center">
                                    <asp:Label ID="lblServiceEngRecommendation1" style="color:gray;font-weight:400" runat="server" Text=""></asp:Label>
                                </td>
                            
                            </tr>
                          <tr>
                                <td valign="center" style="height:50px;border:1px solid #003399" align="center">
                                    <asp:Label ID="Label1" runat="server"  Text="Customer Complaint: "></asp:Label>
                                </td>
                                <td valign="center" style="height:50px;border:1px solid #003399" align="center" colspan="2">
                                    <asp:Label ID="lblCustomerComplaint" style="color:gray;font-weight:400" runat="server" Text=" "></asp:Label>
                                </td>
                            </tr>
                          <tr>
                                <td valign="center" style="height:50px;border:1px solid #003399" align="center">
                                    <asp:Label ID="Label22" runat="server"  Text="RTSS ASC FeedBack: "></asp:Label>
                                </td>
                                <td valign="center" style="height:50px;border:1px solid #003399" align="center" colspan="2">
                                   <asp:Label ID="lblObservationofProblem1" style="color:gray;font-weight:400" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                          <tr>
                                <td valign="center" style="height:50px;border:1px solid #003399" align="center">
                                    <asp:Label ID="Label23" runat="server" Text="Action Plan : "></asp:Label>
                                </td>
                                <td valign="center" style="height:50px;border:1px solid #003399" align="center" colspan="2">
                                    
                                </td>
                            </tr>
                       
                         
                        <tr>
                                <td valign="center" style="height:50px;border:1px solid #003399" align="center" colspan="3">
                                    <asp:Label ID="Label25" runat="server" Text="FOR OFFICE USE ONLY"></asp:Label>
                                </td>
                                
                            </tr>
                       
                        <tr>
                                <td valign="center" style="height:50px;width:140px;border:1px solid #003399" align="center">
                                    <asp:Label ID="Label32" runat="server" Text="Signature of Customer"></asp:Label>
                                </td>
                                <td valign="center" style="height:50px;width:140px;border:1px solid #003399" align="center">
                                    <asp:Label ID="Label33" runat="server" Text="RTSS ASC Name"></asp:Label>
                                </td>
                                <td valign="center" style="height:50px;width:140px;border:1px solid #003399" align="center">
                                    <asp:Label ID="Label34" runat="server" Text="Signature of ASC / Service Engineer"></asp:Label>
                                </td>
                            
                            </tr>
                        <tr>
                                <td valign="center" style="height:50px;width:140px;border:1px solid #003399" align="center">
                                    <asp:Label ID="Label35" runat="server" Text=""></asp:Label>
                                </td>
                                <td valign="center" style="height:50px;width:140px;border:1px solid #003399" align="center">
                                    <asp:Label ID="lblRTSSASCName" style="color:gray;font-weight:400" runat="server" Text=""></asp:Label>
                                </td>
                                <td valign="center" style="height:50px;width:140px;border:1px solid #003399" align="center">
                                    <asp:Label ID="Label37" runat="server" Text=""></asp:Label>
                                </td>
                            
                            </tr>
                         </table>
                    </td>
                </tr>
        </table>
        <table>
                       
                       </table>
    </div>
    </form>
</body>
</html>