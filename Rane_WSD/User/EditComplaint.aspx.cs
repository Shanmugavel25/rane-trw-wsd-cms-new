﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Rane_WSD.User
{
    public partial class EditComplaint : System.Web.UI.Page
    {
        string cs = ConfigurationManager.ConnectionStrings["conn"].ConnectionString;
        public string pdfName = "";
        public string pdfName1 = "";
        public string pdfName2 = "";

        public string pdfName3 = "";
        public bool noError = true;
        public string str = "";
        public string MDTWSD = "";
        public string APASCF = "";
        public string Status1 = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                pnlPDF.Visible = false;
                pnlPDF1.Visible = false;
                pnlPDF2.Visible = false;
                pnlPDF3.Visible = false;

                string editacc = MyCrypto.GetDecryptedQueryString(Request.QueryString["Id"].ToString());

                SqlConnection con = new SqlConnection(cs);
                SqlCommand cmd = new SqlCommand("[sp_b_FIRRegister]", con);

                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                cmd.Parameters.AddWithValue("@Id", editacc);
                cmd.Parameters.AddWithValue("@qtype", "viewFIR");
                SqlDataReader sdr = cmd.ExecuteReader();
                if (sdr.Read())
                {
                    lblFirno.Text = sdr["FIRNo"].ToString();
                    lblAggregateSerialNo.Text = sdr["AggregateSerialNo"].ToString();
                    lblDateofComplaint.Text = String.Format("{0:dd MMM yyyy }", sdr["DateofComplaint"]);
                    lblInvoiceDate.Text = String.Format("{0:dd MMM yyyy }", sdr["InvoiceDate"]);
                    lblInvoiceno.Text = sdr["WSDInvoiceNo"].ToString();
                    lblpartno.Text = sdr["PartNo"].ToString();
                    lblparttype.Text = sdr["PartType"].ToString();
                    lblQuantity.Text = sdr["Quantity"].ToString();
                    lblretailerlocation.Text = sdr["RetailerLocation"].ToString();
                    lblretailername.Text = sdr["RetailerName"].ToString();
                    lblStatus.Text = sdr["StatusName"].ToString();
                    MDTWSD = sdr["StatusName"].ToString();
                    APASCF = sdr["StatusName"].ToString();
                    Status1=sdr["StatusName"].ToString();
                    //Session["Filename"] = sdr["UploadFile"].ToString();
                    lblWSDBranch.Text = sdr["WSDBranch"].ToString();
                    lblWSDName.Text = sdr["WSDName"].ToString();
                    lblUploadfile.Text = sdr["UploadFile"].ToString();
                   
                    string User = sdr["UploadFile"].ToString();
                    if (!string.IsNullOrEmpty(User))
                    {
                        if (User.Split(',').Count() > 0)
                        {
                            string[] path=User.Split(',');
                            for (int i = 0; i < path.Count(); i++ )
                            {
                                if (path[i].ToString() == "")
                                {

                                }
                                else
                                {
                                    if (i == 0) 
                                    {
                                        pnlPDF.Visible = true;
                                        pdfName = path[i].ToString();
                                        Label2.Text = path[i].ToString();
                                    }
                                    else  if (i == 1)
                                    {
                                        pnlPDF1.Visible = true;
                                        pdfName1 = path[i].ToString();
                                        Label3.Text = path[i].ToString();
                                    }
                                    else if (i == 2)
                                    {
                                        pnlPDF2.Visible = true;
                                        pdfName2 = path[i].ToString();
                                        Label4.Text = path[i].ToString();
                                    }
                                    else if (i == 3)
                                    {
                                        pnlPDF3.Visible = true;
                                        pdfName3 = path[i].ToString();
                                        Label5.Text = path[i].ToString();
                                    }

                                }




                                //ddl_user.Items.Add(ct);
                            }
                        }
                    }
                    if (pdfName == "")
                    {
                        pnlPDF.Visible = false;
                    }
                    else
                    {
                        pnlPDF.Visible = true;
                    }

                }
                con.Close();
            }
           
            if (!IsPostBack)
            {
                getStatus();
                getserviceEngname();
                getASCName();

                if (Status1 == "Material Received From WSD/ASC Observation")
                {
                    pnlASCObservation.Visible = true;
                    ASCObservation();
                }
                else
                {
                    pnlASCObservation.Visible = false;
                }
               
            }
            //if (pnlStatus.Visible == true)
            //{
            //    getStatus();
            //}
            //else if(pnlStatus.Visible==false)
            //{
            //     getStatusASCUpadte();
            //}
            pnlServiceEng.Visible = false;
            pnlReason.Visible = false;
            txtRemarks.Visible = false;
            lblRemarks.Visible = false;
            pnlASC.Visible = false;
            pnlASCUpadte.Visible = false;
            pnlMethod.Visible = false;
            pnlDocu.Visible = false;
            pnlStatus.Visible = true;
            pnlReportedDate.Visible = false;
            pnldateASC.Visible = false;
            pnlObservation.Visible = false;
            pnlLRNo.Visible = false;
            pnlLRDate.Visible = false;
            pnlWarranty.Visible = false;
            pnlWSDupdate1.Visible = false;
            pnlWarrehouse.Visible = false;
            pnlWSDSend.Visible = false;
            pnlInwardNo.Visible = false;
            pnlQuantity.Visible = false;
            pnlPlantNo.Visible = false;
            pnlCreditNote.Visible = false;
            pnlCreditValue.Visible = false;
        }
        protected void ASCObservation()
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_b_FIRDetails", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@qtype", "upApproveService");
            cmd.Parameters.AddWithValue("@FIRNo", lblFirno.Text);
            SqlDataReader sdr = cmd.ExecuteReader();
            if (sdr.Read())
            {
                lblFeedback.Text = sdr["ObservationofProblem"].ToString();
                lblRecommendation.Text = sdr["ASCRecommendation"].ToString();
                lblReported.Text =  String.Format("{0:dd MMM yyyy }", sdr["ASCReportedDate"]);
            }
            else
            {
                lblError.Text = "Error Occur Please Contact Adminstrator";
            }
            con.Close();
        }
        protected void getStatus()
        {
            SqlConnection conn = new SqlConnection(cs);
            conn.Open();
            if ("Sales Engineer"== Session["UserType"].ToString())
            {
                string com = "Select distinct [Id],FIRStatus,FIRStatusName from [tbl_FIRstatus] where Id in (2,3)";
                SqlDataAdapter adpt = new SqlDataAdapter(com, conn);
                DataTable dt = new DataTable();
                adpt.Fill(dt);
                ddlStatus.DataSource = dt;
                ddlStatus.DataTextField = "FIRStatusName";
                ddlStatus.DataValueField = "Id";
                ddlStatus.DataBind();
                ddlStatus.Items.Insert(0, new ListItem("-- Select --", ""));
            }
            else if (Session["UserType"].ToString() == "Service Engineer")
            {
                if (APASCF == "Material Received From WSD/ASC Observation")
                {
                    string cmd = "Select distinct [Id],FIRStatus,FIRStatusName from [tbl_FIRstatus] where Id in (9,10)";
                    SqlDataAdapter adpt1 = new SqlDataAdapter(cmd, conn);
                    DataTable dt = new DataTable();
                    adpt1.Fill(dt);
                    ddlStatus.DataSource = dt;
                    ddlStatus.DataTextField = "FIRStatusName";
                    ddlStatus.DataValueField = "Id";
                    ddlStatus.DataBind();
                    ddlStatus.Items.Insert(0, new ListItem("-- Select--", ""));
                }
                else
                {
                    string cmd = "Select distinct [Id],FIRStatus,FIRStatusName from [tbl_FIRstatus] where Id in (5)";
                    SqlDataAdapter adpt1 = new SqlDataAdapter(cmd, conn);
                    DataTable dt = new DataTable();
                    adpt1.Fill(dt);
                    ddlStatus.DataSource = dt;
                    ddlStatus.DataTextField = "FIRStatusName";
                    ddlStatus.DataValueField = "Id";
                    ddlStatus.DataBind();
                    ddlStatus.Items.Insert(0, new ListItem("-- Select--", ""));
                }
            }
            else if (Session["UserType"].ToString() == "WSD")
            {


                if (MDTWSD == "Material Dispatched to WSD")
                {
                    string cmd = "Select distinct [Id],FIRStatus,FIRStatusName from [tbl_FIRstatus] where Id in (11)";
                    SqlDataAdapter adpt1 = new SqlDataAdapter(cmd, conn);
                    DataTable dt = new DataTable();
                    adpt1.Fill(dt);
                    ddlStatus.DataSource = dt;
                    ddlStatus.DataTextField = "FIRStatusName";
                    ddlStatus.DataValueField = "Id";
                    ddlStatus.DataBind();
                    ddlStatus.Items.Insert(0, new ListItem("-- Select--", ""));
                }

                else if (MDTWSD == "Credit Note Updated" || MDTWSD == "Material Sent Back To Retailer")
                {
                    string cmd = "Select distinct [Id],FIRStatus,FIRStatusName from [tbl_FIRstatus] where Id in (15)";
                    SqlDataAdapter adpt1 = new SqlDataAdapter(cmd, conn);
                    DataTable dt = new DataTable();
                    adpt1.Fill(dt);
                    ddlStatus.DataSource = dt;
                    ddlStatus.DataTextField = "FIRStatusName";
                    ddlStatus.DataValueField = "Id";
                    ddlStatus.DataBind();
                    ddlStatus.Items.Insert(0, new ListItem("-- Select --", ""));
                }
                else if (MDTWSD == "Rejected By Sales Engineer")
                {
                    string cmd = "Select distinct [Id],FIRStatus,FIRStatusName from [tbl_FIRstatus] where Id in (18)";
                    SqlDataAdapter adpt1 = new SqlDataAdapter(cmd, conn);
                    DataTable dt = new DataTable();
                    adpt1.Fill(dt);
                    ddlStatus.DataSource = dt;
                    ddlStatus.DataTextField = "FIRStatusName";
                    ddlStatus.DataValueField = "Id";
                    ddlStatus.DataBind();
                    ddlStatus.Items.Insert(0, new ListItem("-- Select --", ""));
                }
                else if (MDTWSD == "Material Received From ASC")
                {
                    string cmd = "select count(*) from tbl_FIRDetails where FIRNo='" + lblFirno.Text + "' and Status=10";
                    SqlDataAdapter adpt1 = new SqlDataAdapter(cmd, conn);
                    DataTable dt = new DataTable();
                    adpt1.Fill(dt);

                    if (dt.Rows[0][0].ToString() != "0")
                    {
                        string cmd1 = "Select distinct [Id],FIRStatus,FIRStatusName from [tbl_FIRstatus] where Id in (16)";
                        SqlDataAdapter adpt11 = new SqlDataAdapter(cmd1, conn);
                        DataTable dt1 = new DataTable();
                        adpt11.Fill(dt1);
                        ddlStatus.DataSource = dt1;
                        ddlStatus.DataTextField = "FIRStatusName";
                        ddlStatus.DataValueField = "Id";
                        ddlStatus.DataBind();
                        ddlStatus.Items.Insert(0, new ListItem("-- Select--", ""));
                    }
                    else
                    {
                        string cmd2 = "Select distinct [Id],FIRStatus,FIRStatusName from [tbl_FIRstatus] where Id in (12)";
                        SqlDataAdapter adpt2 = new SqlDataAdapter(cmd2, conn);
                        DataTable dt2 = new DataTable();
                        adpt2.Fill(dt2);
                        ddlStatus.DataSource = dt2;
                        ddlStatus.DataTextField = "FIRStatusName";
                        ddlStatus.DataValueField = "Id";
                        ddlStatus.DataBind();
                        ddlStatus.Items.Insert(0, new ListItem("-- Select--", ""));
                    }

                }
                else
                {
                    string cmd = "Select distinct [Id],FIRStatus,FIRStatusName from [tbl_FIRstatus] where Id in (6)";
                    SqlDataAdapter adpt1 = new SqlDataAdapter(cmd, conn);
                    DataTable dt = new DataTable();
                    adpt1.Fill(dt);
                    ddlStatus.DataSource = dt;
                    ddlStatus.DataTextField = "FIRStatusName";
                    ddlStatus.DataValueField = "Id";
                    ddlStatus.DataBind();
                    ddlStatus.Items.Insert(0, new ListItem("-- Select--", ""));
                }
            }
            else if (Session["UserType"].ToString() == "ASC")
            {
                    if (MDTWSD == "Approved ASC FeedBack")
                    {
                        string cmd1 = "Select distinct [Id],FIRStatus,FIRStatusName from [tbl_FIRstatus] where Id in (8)";
                       
                        SqlDataAdapter adpt2 = new SqlDataAdapter(cmd1, conn);
                        DataTable dt = new DataTable();
                       
                        adpt2.Fill(dt);
                        ddlStatus.DataSource = dt;
                        ddlStatus.DataTextField = "FIRStatusName";
                        ddlStatus.DataValueField = "Id";
                        ddlStatus.DataBind();
                        ddlStatus.Items.Insert(0, new ListItem("-- Select --", ""));

                    }
                    else if (MDTWSD == "Rejected ASC FeedBack")
                    {
                        string cmd = "Select distinct [Id],FIRStatus,FIRStatusName from [tbl_FIRstatus] where Id in (8)";
                        SqlDataAdapter adpt1 = new SqlDataAdapter(cmd, conn);
                        DataTable dt = new DataTable();
                        adpt1.Fill(dt);
                        ddlStatus.DataSource = dt;
                        ddlStatus.DataTextField = "FIRStatusName";
                        ddlStatus.DataValueField = "Id";
                        ddlStatus.DataBind();
                        ddlStatus.Items.Insert(0, new ListItem("-- Select --", ""));
                    }
                    else
                    {
                        string cmd2 = "Select distinct [Id],FIRStatus,FIRStatusName from [tbl_FIRstatus] where Id in (7)";
                        SqlDataAdapter adpt1 = new SqlDataAdapter(cmd2, conn);
                        //SqlDataAdapter adpt2 = new SqlDataAdapter(cmd1, conn);
                        DataTable dt = new DataTable();
                        adpt1.Fill(dt);
                        //adpt2.Fill(dt);
                        ddlStatus.DataSource = dt;
                        ddlStatus.DataTextField = "FIRStatusName";
                        ddlStatus.DataValueField = "Id";
                        ddlStatus.DataBind();
                        ddlStatus.Items.Insert(0, new ListItem("-- Select --", ""));
                    }


               
            }
            else if (Session["UserType"].ToString() == "Warehouse")
            {
                if (APASCF == "Material Received From WSD by Warehouse")
                {
                    string cmd = "Select distinct [Id],FIRStatus,FIRStatusName from [tbl_FIRstatus] where Id in (14)";
                    SqlDataAdapter adpt1 = new SqlDataAdapter(cmd, conn);
                    DataTable dt = new DataTable();
                    adpt1.Fill(dt);
                    ddlStatus.DataSource = dt;
                    ddlStatus.DataTextField = "FIRStatusName";
                    ddlStatus.DataValueField = "Id";
                    ddlStatus.DataBind();
                    ddlStatus.Items.Insert(0, new ListItem("-- Select --", ""));
                }
                else
                {
                    string cmd = "Select distinct [Id],FIRStatus,FIRStatusName from [tbl_FIRstatus] where Id in (13)";
                    SqlDataAdapter adpt1 = new SqlDataAdapter(cmd, conn);
                    DataTable dt = new DataTable();
                    adpt1.Fill(dt);
                    ddlStatus.DataSource = dt;
                    ddlStatus.DataTextField = "FIRStatusName";
                    ddlStatus.DataValueField = "Id";
                    ddlStatus.DataBind();
                    ddlStatus.Items.Insert(0, new ListItem("-- Select--", ""));
                }
            }
            
        }
       
       
        protected void getserviceEngname()
        {
            SqlConnection conn = new SqlConnection(cs);
            conn.Open();
            string com = "Select distinct [SEID],Name,secode from [ServiceEng] where Name !=''";
            SqlDataAdapter adpt = new SqlDataAdapter(com, conn);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            ddlserviceEng.DataSource = dt;
            ddlserviceEng.DataTextField = "Name";
            ddlserviceEng.DataValueField = "Name";
            ddlserviceEng.DataBind();
            ddlserviceEng.Items.Insert(0, new ListItem("-- Select ServiceEng --", ""));
        }
        protected void getASCName()
        {
            SqlConnection conn = new SqlConnection(cs);
            conn.Open();
            string com = "Select distinct Name,ASCCode from [Authorized] where Name !='' and serviceEng ='" + Session["Code"].ToString() + "' ";
            SqlDataAdapter adpt = new SqlDataAdapter(com, conn);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            ddlASC.DataSource = dt;
            ddlASC.DataTextField = "Name";
            ddlASC.DataValueField = "Name";
            ddlASC.DataBind();
            ddlASC.Items.Insert(0, new ListItem("-- Select ASC --", ""));
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            UpdateFIR();

            if (noError)
            {
                Response.Write("<script language='javascript'>alert('Update Record Successfully');window.location=('ListComplaint.aspx')</script>");
            }
            else
            {
                //pnlerror.Visible = true;
                lblError.Text = "Error Occured please Contact Administrator";
            }
        }
        protected void UpdateFIR()
        {
            string editacc = MyCrypto.GetDecryptedQueryString(Request.QueryString["Id"].ToString());

            SqlConnection con = new SqlConnection(cs);
            try
            {
                SqlCommand cmd = new SqlCommand("sp_b_FIRDetails", con);
                SqlCommand cmd1 = new SqlCommand("sp_b_FIRRegister", con);
               
                cmd.CommandType = CommandType.StoredProcedure;
                cmd1.CommandType = CommandType.StoredProcedure;
             
                con.Open();
                cmd.Parameters.AddWithValue("@FIRId", editacc);
                cmd1.Parameters.AddWithValue("@Id", editacc);
                if ("Sales Engineer" == Session["UserType"].ToString())
                {
                    cmd.Parameters.AddWithValue("@qtype", "UpdateSaleFIR");
                    cmd1.Parameters.AddWithValue("@qtype", "UpdateSalesEng");

                    cmd.Parameters.AddWithValue("@FIRNo", lblFirno.Text);
                    //cmd.Parameters.AddWithValue("@FIRNo", lblFirno.Text);
                    cmd.Parameters.AddWithValue("@Remarks", txtRemarks.Text.Trim());
                    //cmd.Parameters.AddWithValue("@Remarks", txtRemarks.Text.Trim());
                    cmd.Parameters.AddWithValue("@ReasonForRejection", txtReason.Text.Trim());
                    cmd.Parameters.AddWithValue("@AllocateServiceEngg", ddlserviceEng.SelectedValue);
                    //cmd.Parameters.AddWithValue("@AllocateAsc", ddlASC.SelectedValue);
                    cmd.Parameters.AddWithValue("@Status", ddlStatus.SelectedValue);
                    cmd1.Parameters.AddWithValue("@Status", ddlStatus.SelectedValue);
                    cmd.Parameters.AddWithValue("@UpdatedBy", Session["UserName"].ToString());
                    cmd1.Parameters.AddWithValue("@UpdatedBy", Session["UserName"].ToString());
                }
                else if (Session["UserType"].ToString() == "Service Engineer")
                {
                    if (ddlStatus.SelectedValue == "9")
                    {
                        cmd.Parameters.AddWithValue("@qtype", "ServiceApproved");
                        cmd1.Parameters.AddWithValue("@qtype", "UpdateSalesEng");
                        cmd.Parameters.AddWithValue("@FIRNo", lblFirno.Text);
                        cmd.Parameters.AddWithValue("@ASCReportedDate", lblReported.Text);
                        cmd.Parameters.AddWithValue("@ObservationofProblem", lblFeedback.Text);
                        cmd.Parameters.AddWithValue("@ASCRecommendation", ddlWarranty.SelectedValue);
                        cmd.Parameters.AddWithValue("@Remarks", txtRemarks.Text.Trim());
                        cmd.Parameters.AddWithValue("@Status", ddlStatus.SelectedValue);
                        cmd.Parameters.AddWithValue("@ServiceEnggApproval", ddlStatus.SelectedValue);
                        cmd1.Parameters.AddWithValue("@Status", ddlStatus.SelectedValue);
                        cmd.Parameters.AddWithValue("@UpdatedBy", Session["UserName"].ToString());
                        cmd1.Parameters.AddWithValue("@UpdatedBy", Session["UserName"].ToString());
                    }
                    else if (ddlStatus.SelectedValue == "10")
                    {
                        cmd.Parameters.AddWithValue("@qtype", "RejectFeedBackToWSD");
                        cmd1.Parameters.AddWithValue("@qtype", "UpdateSalesEng");
                        cmd.Parameters.AddWithValue("@FIRNo", lblFirno.Text);
                        cmd.Parameters.AddWithValue("@Status", ddlStatus.SelectedValue);
                        cmd.Parameters.AddWithValue("@ASCRecommendation", ddlWarranty.SelectedValue);
                        cmd.Parameters.AddWithValue("@ServiceEnggApproval", ddlStatus.SelectedValue);
                        cmd1.Parameters.AddWithValue("@Status", ddlStatus.SelectedValue);
                        cmd.Parameters.AddWithValue("@Remarks", txtRemarks.Text.Trim());
                        cmd.Parameters.AddWithValue("@UpdatedBy", Session["UserName"].ToString());
                        cmd1.Parameters.AddWithValue("@UpdatedBy", Session["UserName"].ToString());
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@qtype", "UpdateServiceEngFIR");
                        cmd1.Parameters.AddWithValue("@qtype", "UpdateSalesEng");
                        cmd.Parameters.AddWithValue("@FIRNo", lblFirno.Text);
                        cmd.Parameters.AddWithValue("@AllocateAsc", ddlASC.SelectedValue);
                        cmd.Parameters.AddWithValue("@Remarks", txtRemarks.Text.Trim());
                        cmd.Parameters.AddWithValue("@Status", ddlStatus.SelectedValue);
                        cmd1.Parameters.AddWithValue("@Status", ddlStatus.SelectedValue);
                        cmd.Parameters.AddWithValue("@UpdatedBy", Session["UserName"].ToString());
                        cmd1.Parameters.AddWithValue("@UpdatedBy", Session["UserName"].ToString());
                    }
                }
                else if (Session["UserType"].ToString() == "WSD")
                {
                    if (ddlStatus.SelectedValue == "11")
                    {
                        cmd.Parameters.AddWithValue("@qtype", "UpdateWSDReceived");
                        cmd1.Parameters.AddWithValue("@qtype", "UpdateSalesEng");
                        cmd.Parameters.AddWithValue("@FIRNo", lblFirno.Text);
                        cmd.Parameters.AddWithValue("@Status", ddlStatus.SelectedValue);
                        cmd1.Parameters.AddWithValue("@Status", ddlStatus.SelectedValue);
                        if (FileUpload2.HasFiles)
                        {
                            foreach (HttpPostedFile uploadedFile in FileUpload2.PostedFiles)
                            {
                                uploadedFile.SaveAs(System.IO.Path.Combine(Server.MapPath("~/Upload/"), uploadedFile.FileName));
                                listofuploadedfiles1.Text += String.Format("{0},", uploadedFile.FileName);

                            }
                            cmd.Parameters.AddWithValue("@FileUpload", listofuploadedfiles1.Text);
                        }
                        //cmd.Parameters.AddWithValue("@DispatchDate",txtdate.Text.Trim());
                        cmd.Parameters.AddWithValue("@UpdatedBy", Session["UserName"].ToString());
                        cmd1.Parameters.AddWithValue("@UpdatedBy", Session["UserName"].ToString());
                    }
                    else if (ddlStatus.SelectedValue == "12")
                    {
                        cmd.Parameters.AddWithValue("@qtype", "MateDispatchWarehouse");
                        cmd1.Parameters.AddWithValue("@qtype", "UpdateSalesEng");
                        cmd.Parameters.AddWithValue("@FIRNo", lblFirno.Text);
                        cmd.Parameters.AddWithValue("@LRNumber",textLRNO.Text.Trim() );
                        cmd.Parameters.AddWithValue("@Status", ddlStatus.SelectedValue);
                        cmd1.Parameters.AddWithValue("@Status", ddlStatus.SelectedValue);
                        cmd.Parameters.AddWithValue("@LRDate", textLRDate.Text.Trim());
                        cmd.Parameters.AddWithValue("@Remarks", txtRemarks.Text.Trim());
                        cmd.Parameters.AddWithValue("@DeliveryNoteNo", txtDeliveryNote.Text.Trim());
                        cmd.Parameters.AddWithValue("@TransportationName", txttransportation.Text.Trim());
                        cmd.Parameters.AddWithValue("@UpdatedBy", Session["UserName"].ToString());
                        cmd1.Parameters.AddWithValue("@UpdatedBy", Session["UserName"].ToString());
                    }
                    else if (ddlStatus.SelectedValue == "15")
                    {
                        cmd.Parameters.AddWithValue("@qtype", "WSDClosed");
                        cmd1.Parameters.AddWithValue("@qtype", "UpdateSalesEng");
                        cmd.Parameters.AddWithValue("@FIRNo", lblFirno.Text);
                        cmd.Parameters.AddWithValue("@Status", ddlStatus.SelectedValue);
                        cmd1.Parameters.AddWithValue("@Status", ddlStatus.SelectedValue);
                        cmd.Parameters.AddWithValue("@Remarks", txtRemarks.Text.Trim());
                        cmd.Parameters.AddWithValue("@UpdatedBy", Session["UserName"].ToString());
                        cmd1.Parameters.AddWithValue("@UpdatedBy", Session["UserName"].ToString());
                    }
                    else if (ddlStatus.SelectedValue == "16")
                    {
                        cmd.Parameters.AddWithValue("@qtype", "SendToRetailer");
                        cmd1.Parameters.AddWithValue("@qtype", "UpdateSalesEng");
                        cmd.Parameters.AddWithValue("@FIRNo", lblFirno.Text);
                        cmd.Parameters.AddWithValue("@Status", ddlStatus.SelectedValue);
                        cmd1.Parameters.AddWithValue("@Status", ddlStatus.SelectedValue);
                        cmd.Parameters.AddWithValue("@Remarks", txtRemarks.Text.Trim());
                        cmd.Parameters.AddWithValue("@UpdatedBy", Session["UserName"].ToString());
                        cmd1.Parameters.AddWithValue("@UpdatedBy", Session["UserName"].ToString());
                    }
                    else if (ddlStatus.SelectedValue == "17")
                    {
                        cmd.Parameters.AddWithValue("@qtype", "RejectFeedBackToWSD");
                        cmd1.Parameters.AddWithValue("@qtype", "UpdateSalesEng");
                        cmd.Parameters.AddWithValue("@FIRNo", lblFirno.Text);
                        cmd.Parameters.AddWithValue("@Status", ddlStatus.SelectedValue);
                        cmd1.Parameters.AddWithValue("@Status", ddlStatus.SelectedValue);
                        cmd.Parameters.AddWithValue("@Remarks", txtRemarks.Text.Trim());
                        cmd.Parameters.AddWithValue("@UpdatedBy", Session["UserName"].ToString());
                        cmd1.Parameters.AddWithValue("@UpdatedBy", Session["UserName"].ToString());
                    }
                    else if (ddlStatus.SelectedValue == "18")
                    {
                        cmd.Parameters.AddWithValue("@qtype", "WSDClosed");
                        cmd1.Parameters.AddWithValue("@qtype", "UpdateSalesEng");
                        cmd.Parameters.AddWithValue("@FIRNo", lblFirno.Text);
                        cmd.Parameters.AddWithValue("@Status", ddlStatus.SelectedValue);
                        cmd1.Parameters.AddWithValue("@Status", ddlStatus.SelectedValue);
                        cmd.Parameters.AddWithValue("@Remarks", txtRemarks.Text.Trim());
                        cmd.Parameters.AddWithValue("@UpdatedBy", Session["UserName"].ToString());
                        cmd1.Parameters.AddWithValue("@UpdatedBy", Session["UserName"].ToString());
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@qtype", "updDispatchASC");
                        cmd1.Parameters.AddWithValue("@qtype", "UpdateSalesEng");
                        cmd.Parameters.AddWithValue("@FIRNo", lblFirno.Text);
                        cmd.Parameters.AddWithValue("@Remarks", txtRemarks.Text.Trim());
                        cmd.Parameters.AddWithValue("@Status", ddlStatus.SelectedValue);
                        cmd1.Parameters.AddWithValue("@Status", ddlStatus.SelectedValue);
                        cmd.Parameters.AddWithValue("@AllocateAsc", txtASCName.Text.Trim());
                        cmd.Parameters.AddWithValue("@DispatchMethod", ddlmethodtype.SelectedValue);
                        cmd.Parameters.AddWithValue("@DocumentNo", txtdocno.Text.Trim());
                        //cmd.Parameters.AddWithValue("@DispatchDate",txtdate.Text.Trim());
                        cmd.Parameters.AddWithValue("@UpdatedBy", Session["UserName"].ToString());
                        cmd1.Parameters.AddWithValue("@UpdatedBy", Session["UserName"].ToString());
                    }
                }
                else if (Session["UserType"].ToString() == "ASC")
                {
                    if (ddlStatus.SelectedValue == "8")
                    {
                        cmd.Parameters.AddWithValue("@qtype", "MateDispatchWSD");
                        cmd1.Parameters.AddWithValue("@qtype", "UpdateSalesEng");
                        cmd.Parameters.AddWithValue("@FIRNo", lblFirno.Text);
                        cmd.Parameters.AddWithValue("@LRNumber",txtLRNo.Text.Trim());
                        cmd.Parameters.AddWithValue("@Status", ddlStatus.SelectedValue);
                        cmd1.Parameters.AddWithValue("@Status", ddlStatus.SelectedValue);
                        cmd.Parameters.AddWithValue("@LRDate",txtLRDate.Text.Trim() );
                        cmd.Parameters.AddWithValue("@Remarks", txtRemarks.Text.Trim());
                        cmd.Parameters.AddWithValue("@DCNo", txtDcno.Text.Trim());
                        cmd.Parameters.AddWithValue("@UpdatedBy", Session["UserName"].ToString());
                        cmd1.Parameters.AddWithValue("@UpdatedBy", Session["UserName"].ToString());
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@qtype", "MateReceiveWSD");
                        cmd1.Parameters.AddWithValue("@qtype", "UpdateSalesEng");
                        cmd.Parameters.AddWithValue("@FIRNo", lblFirno.Text);
                        cmd.Parameters.AddWithValue("@ASCReportedDate", txtreporteddate.Text.Trim());
                        cmd.Parameters.AddWithValue("@Status", ddlStatus.SelectedValue);
                        cmd1.Parameters.AddWithValue("@Status", ddlStatus.SelectedValue);
                        cmd.Parameters.AddWithValue("@ObservationofProblem", txtObservation.Text.Trim());
                        cmd.Parameters.AddWithValue("@Remarks", txtObservation.Text.Trim());
                        cmd.Parameters.AddWithValue("@ASCRecommendation", ddlObservation.SelectedValue);

                        if (FileUpload1.HasFiles)
                        {
                            foreach (HttpPostedFile uploadedFile in FileUpload1.PostedFiles)
                            {
                                uploadedFile.SaveAs(System.IO.Path.Combine(Server.MapPath("~/Upload/"), uploadedFile.FileName));
                                listofuploadedfiles.Text += String.Format("{0},", uploadedFile.FileName);

                            }
                            cmd.Parameters.AddWithValue("@FileUpload", listofuploadedfiles.Text);
                        }
                        //cmd.Parameters.AddWithValue("@FileUpload", );
                        //cmd.Parameters.AddWithValue("@DispatchDate",txtdate.Text.Trim());
                        cmd.Parameters.AddWithValue("@UpdatedBy", Session["UserName"].ToString());
                        cmd1.Parameters.AddWithValue("@UpdatedBy", Session["UserName"].ToString());
                    }

                }
                else if (Session["UserType"].ToString() == "Warehouse")
                {
                    if (ddlStatus.SelectedValue == "14")
                    {
                        cmd.Parameters.AddWithValue("@qtype", "CreditNoteNo");
                        cmd1.Parameters.AddWithValue("@qtype", "UpdateSalesEng");
                        cmd.Parameters.AddWithValue("@FIRNo", lblFirno.Text);
                        cmd.Parameters.AddWithValue("@PlantNotificationNo",txtplantnotify.Text.Trim());
                        cmd.Parameters.AddWithValue("@Status", ddlStatus.SelectedValue);
                        cmd1.Parameters.AddWithValue("@Status", ddlStatus.SelectedValue);
                        cmd.Parameters.AddWithValue("@CreditNoteNo",txtCreditNote.Text.Trim());
                        cmd.Parameters.AddWithValue("@Remarks", txtRemarks.Text.Trim());
                        cmd.Parameters.AddWithValue("@CreditNoteDate",txtCreditDate.Text.Trim());
                        cmd.Parameters.AddWithValue("@Value",txtValue.Text.Trim());
                        cmd.Parameters.AddWithValue("@PlantDocNo", txtplantDocuno.Text.Trim());
                        cmd.Parameters.AddWithValue("@PlantDocDate",txtplantdocudate.Text.Trim());
                        cmd.Parameters.AddWithValue("@UpdatedBy", Session["UserName"].ToString());
                        cmd1.Parameters.AddWithValue("@UpdatedBy", Session["UserName"].ToString());
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@qtype", "MateReceiveFromWSD");
                        cmd1.Parameters.AddWithValue("@qtype", "UpdateSalesEng");
                        cmd.Parameters.AddWithValue("@FIRNo", lblFirno.Text);
                        cmd.Parameters.AddWithValue("@InwardNumber", txtinwardnumber.Text.Trim());
                        cmd.Parameters.AddWithValue("@Status", ddlStatus.SelectedValue);
                        cmd1.Parameters.AddWithValue("@Status", ddlStatus.SelectedValue);
                        cmd.Parameters.AddWithValue("@InwardDate", txtinwarddate.Text.Trim());
                        cmd.Parameters.AddWithValue("@Remarks", txtRemarks.Text.Trim());
                        cmd.Parameters.AddWithValue("@InwardQuantity", txtQuantity.Text.Trim());
                        cmd.Parameters.AddWithValue("@UpdatedBy", Session["UserName"].ToString());
                        cmd1.Parameters.AddWithValue("@UpdatedBy", Session["UserName"].ToString());
                    }
                }

                cmd.ExecuteNonQuery();
                cmd1.ExecuteNonQuery();

                


            }
            catch (Exception ex)
            {
                Response.Write(ex);
                lblError.Text = "Error Occured, Please Contact Administrator";
                noError = false;
            }
            finally
            {
                con.Close();


            }
        }

        protected void ddlStatus_SelectedIndexChanged(object sender, EventArgs e)
        {

           

            if (ddlStatus.SelectedValue == "2")
            {
                pnlServiceEng.Visible = true;
                pnlReason.Visible = false;
                txtRemarks.Visible = true;
                lblRemarks.Visible = true;
                pnlASC.Visible = false;
                pnlASCUpadte.Visible = false;
                pnlMethod.Visible = false;
                pnlDocu.Visible = false;
                pnlReportedDate.Visible = false;
                pnldateASC.Visible = false;
                pnlStatus.Visible = true;
                pnlObservation.Visible = false;
                pnlLRNo.Visible = false;
                pnlLRDate.Visible = false;
                pnlWarranty.Visible = false;
                pnlWSDupdate1.Visible = false;
                pnlWarrehouse.Visible = false;
                pnlWSDSend.Visible = false;
                lblRemarks.Text = "Remarks";
                pnlInwardNo.Visible = false;
                pnlQuantity.Visible = false;
                pnlPlantNo.Visible = false;
                pnlCreditNote.Visible = false;
                pnlCreditValue.Visible = false;
            }
            else if (ddlStatus.SelectedValue == "3")
            {
                pnlServiceEng.Visible = false;
                pnlReason.Visible = true;
                pnlASC.Visible = false;
                pnlMethod.Visible = false;
                pnlASCUpadte.Visible = false;
                pnlDocu.Visible = false;
                pnlStatus.Visible = true;
                pnldateASC.Visible = false;
                pnlReportedDate.Visible = false;
                pnlObservation.Visible = false;
                pnlLRNo.Visible = false;
                pnlLRDate.Visible = false;
                pnlWarranty.Visible = false;
                pnlWSDupdate1.Visible = false;
                pnlWarrehouse.Visible = false;
                pnlWSDSend.Visible = false;
                pnlInwardNo.Visible = false;
                pnlQuantity.Visible = false;
                pnlPlantNo.Visible = false;
                pnlCreditNote.Visible = false;
                pnlCreditValue.Visible = false;
            }
            else if (ddlStatus.SelectedValue == "5")
            {
                pnlASC.Visible = true;
                txtRemarks.Visible = true;
                lblRemarks.Visible = true;
                lblRemarks.Text = "Remarks";
                pnlASCUpadte.Visible = false;
                pnlDocu.Visible = false;
                pnlStatus.Visible = true;
                pnlMethod.Visible = false;
                pnldateASC.Visible = false;
                pnlReportedDate.Visible = false;
                pnlObservation.Visible = false;
                pnlLRNo.Visible = false;
                pnlLRDate.Visible = false;
                pnlWarranty.Visible = false;
                pnlWSDupdate1.Visible = false;
                pnlWarrehouse.Visible = false;
                pnlWSDSend.Visible = false;
                pnlInwardNo.Visible = false;
                pnlQuantity.Visible = false;
                pnlPlantNo.Visible = false;
                pnlCreditNote.Visible = false;
                pnlCreditValue.Visible = false;
            }
            else if (ddlStatus.SelectedValue == "6")
            {
                pnlASCUpadte.Visible = true;
                pnlMethod.Visible = true;
                pnlDocu.Visible = false;
                lblRemarks.Visible = true;
                txtRemarks.Visible = true;
                pnlStatus.Visible = true;
                pnldateASC.Visible = false;
                pnlReportedDate.Visible = false;
                pnlObservation.Visible = false;
                lblRemarks.Text = "Remarks";
                pnlLRNo.Visible = false;
                pnlLRDate.Visible = false;
                pnlWarranty.Visible = false;
                pnlWSDupdate1.Visible = false;
                pnlWarrehouse.Visible = false;
                pnlWSDSend.Visible = false;
                pnlInwardNo.Visible = false;
                pnlQuantity.Visible = false;
                pnlPlantNo.Visible = false;
                pnlCreditNote.Visible = false;
                pnlCreditValue.Visible = false;
               

                SqlConnection con = new SqlConnection(cs);
                con.Open();
                SqlCommand cmd = new SqlCommand("sp_b_FIRDetails", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@qtype", "updASCName");
                cmd.Parameters.AddWithValue("@FIRNo", lblFirno.Text);
                SqlDataReader sdr = cmd.ExecuteReader();
                if (sdr.Read())
                {
                    txtASCName.Text = sdr["AllocateASC"].ToString();
                }
                else
                {
                    lblError.Text = "Error Occur Please Contact Adminstrator";
                }
                con.Close();

            }
            else if (ddlStatus.SelectedValue == "7")
            {
                pnlReportedDate.Visible = true;
                pnlObservation.Visible = true;
                pnlStatus.Visible = true;
                pnldateASC.Visible = true;
                pnlLRNo.Visible = false;
                pnlLRDate.Visible = false;
                pnlWarranty.Visible = false;
                pnlWSDupdate1.Visible = false;
                pnlWarrehouse.Visible = false;
                pnlWSDSend.Visible = false;
                pnlInwardNo.Visible = false;
                pnlQuantity.Visible = false;
                pnlPlantNo.Visible = false;
                pnlCreditNote.Visible = false;
                pnlCreditValue.Visible = false;
            }
            else if (ddlStatus.SelectedValue == "8")
            {
                pnlLRNo.Visible = true;
                lblRemarks.Text = "Remarks";
                lblRemarks.Visible = true;
                txtRemarks.Visible = true;
                pnlLRDate.Visible = true;
                pnlWSDupdate1.Visible = false;
                pnlWarranty.Visible = false;
                pnlWarrehouse.Visible = false;
                pnlWSDSend.Visible = false;
                pnlInwardNo.Visible = false;
                pnlQuantity.Visible = false;
                pnlPlantNo.Visible = false;
                pnlCreditNote.Visible = false;
                pnlCreditValue.Visible = false;
            }
            else if (ddlStatus.SelectedValue == "9")
            {
                pnlWarranty.Visible = true;
                lblRemarks.Visible = true;
                lblRemarks.Text = "Remarks*";
                txtRemarks.Visible = true;
                pnlWSDupdate1.Visible = false;
                pnlWarrehouse.Visible = false;
                pnlWSDSend.Visible = false;
                pnlInwardNo.Visible = false;
                pnlQuantity.Visible = false;
                pnlPlantNo.Visible = false;
                pnlCreditNote.Visible = false;
                pnlCreditValue.Visible = false;
            }
            else if (ddlStatus.SelectedValue == "11")
            {
                pnlWSDupdate1.Visible = true;
                pnlWarrehouse.Visible = false;
                pnlWSDSend.Visible = false;
                pnlInwardNo.Visible = false;
                pnlQuantity.Visible = false;
                pnlPlantNo.Visible = false;
                pnlCreditNote.Visible = false;
                pnlCreditValue.Visible = false;
            }
            else if (ddlStatus.SelectedValue == "12")
            {
                pnlWarrehouse.Visible = true;
                pnlWSDSend.Visible = true;
                lblRemarks.Visible = true;
                lblRemarks.Text = "Remarks*";
                txtRemarks.Visible = true;
                pnlInwardNo.Visible = false;
                pnlQuantity.Visible = false;
                pnlPlantNo.Visible = false;
                pnlCreditNote.Visible = false;
                pnlCreditValue.Visible = false;
            }
            else if (ddlStatus.SelectedValue == "13")
            {
                pnlInwardNo.Visible = true;
                pnlQuantity.Visible = true;
                lblRemarks.Visible = true;
                lblRemarks.Text = "Remarks*";
                txtRemarks.Visible = true;
                pnlPlantNo.Visible = false;
                pnlCreditNote.Visible = false;
                pnlCreditValue.Visible = false;
            }
            else if (ddlStatus.SelectedValue == "14")
            {
                pnlPlantNo.Visible = true;
                pnlCreditNote.Visible = true;
                lblRemarks.Visible = true;
                lblRemarks.Text = "Remarks*";
                txtRemarks.Visible = true;
                pnlCreditValue.Visible = true;
            }
            else if (ddlStatus.SelectedValue == "15")
            {
                lblRemarks.Visible = true;
                lblRemarks.Text = "Remarks*";
                txtRemarks.Visible = true;
            }
            else if (ddlStatus.SelectedValue == "10")
            {
                pnlWarranty.Visible = true;
                lblRemarks.Visible = true;
                lblRemarks.Text = "Remarks*";
                txtRemarks.Visible = true;

            }
            else if (ddlStatus.SelectedValue == "16")
            {
                lblRemarks.Visible = true;
                lblRemarks.Text = "Remarks*";
                txtRemarks.Visible = true;

            }
            else if (ddlStatus.SelectedValue == "18")
            {
                lblRemarks.Visible = true;
                lblRemarks.Text = "Remarks*";
                txtRemarks.Visible = true;
            }
            else
            {
                lblError.Text = "Choose Status";
            }

        }

        protected void txtASC_TextChanged(object sender, EventArgs e)
        {

        }

        protected void ddlmethodtype_SelectedIndexChanged(object sender, EventArgs e)
        {
            pnlASCUpadte.Visible = true;
            pnlMethod.Visible = true;
            pnlDocu.Visible = false;
            txtRemarks.Visible = true;
            lblRemarks.Visible = true;
            pnlStatus.Visible = true;
            pnlObservation.Visible = false;
            pnlReportedDate.Visible = false;
            pnldateASC.Visible = false;
            pnlLRNo.Visible = false;
            pnlLRDate.Visible = false;
            pnlWSDupdate1.Visible = false;
            pnlWarranty.Visible = false;
            lblRemarks.Text = "Remarks";

            if (ddlmethodtype.SelectedValue == "Courier")
            {
                pnlDocu.Visible = true;
            }

            SqlConnection con = new SqlConnection(cs);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_b_FIRDetails", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@qtype", "updASCName");
            cmd.Parameters.AddWithValue("@FIRNo", lblFirno.Text);
            SqlDataReader sdr = cmd.ExecuteReader();
            if (sdr.Read())
            {
                txtASCName.Text = sdr["AllocateASC"].ToString();
            }
            else
            {
                lblError.Text = "Error Occur Please Contact Adminstrator";
            }
            con.Close();
            
        }

        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

       

       
    }
}