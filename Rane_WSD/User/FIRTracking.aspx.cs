﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Rane_WSD.User
{
    public partial class FIRTracking : System.Web.UI.Page
    {
        string cs = ConfigurationManager.ConnectionStrings["conn"].ConnectionString;
        public common db = new common();
        public bool noError = true;
        public bool IsUpdated;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {                
                dataget();

            }
        }
        protected void dataget()
        {
            SqlConnection con = new SqlConnection(cs);

            SqlCommand cmd = new SqlCommand("sp_b_Dashboard", con);
            cmd.CommandType = CommandType.StoredProcedure;
            con.Open();    
            cmd.Parameters.AddWithValue("@qtype", "Tracking");
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable ds = new DataTable();
            sda.Fill(ds);
            lstv_tracking.DataSource = ds;
            lstv_tracking.DataBind();
            con.Close();
        }
    }
}