﻿<%@ Page Title="" Language="C#" MasterPageFile="~/User/UserLayout.Master" AutoEventWireup="true" CodeBehind="FIRTracking.aspx.cs" Inherits="Rane_WSD.User.FIRTracking" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .table {
    width: 240% !important;
    margin-bottom: 1rem;
    color: #505d69;
}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
      <div class="page-title-box">
        <div class="container-fluid">
            <div class="page-title dflex-between-center">
                <h3 class="mb-1 font-weight-bold">List of FIR</h3>
                <ol class="breadcrumb mb-0 mt-1">
                    <li class="breadcrumb-item">
                        <a href="#">
                            <i class="bx bx-home fs-xs"></i>
                        </a>
                    </li>
                    <li class="breadcrumb-item active">FIR Tracking</li>
                </ol>
            </div>
        </div>
    </div>

    <!-- page content -->
    <div class="page-content-wrapper mt--45">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-title">FIR Tracking</h5>
                            <h6 class="card-subtitle"></h6>
                        </div>
                        <div class="card-body">
                            <asp:Label ID="lblError" ForeColor="Red" runat="server" Text=""></asp:Label>


                               <%-- <div class="col-md-4">
                                    <asp:HyperLink ID="hplCreate" Visible="false" class="btn btn-success btn-sm" NavigateUrl="~/User/CreateFIR.aspx" runat="server">Create New</asp:HyperLink>
                                    
                                </div>--%>

                             
                      
                            <div class="table-responsive">
                                <asp:ListView ID="lstv_tracking" runat="server" DataKeyNames="Id" ItemPlaceholderID="itmPlaceholder"  ConvertEmptyStringToNull="true" >
                                    <LayoutTemplate>
                                        <table class="table table-bordered table-hover">
                                            <thead class="thead-light">
                                                <tr>
                                                    <th class="text-center">FIR Number</th>
                                                    <th class="text-center">FIR Created Date</th>                                               
                                                    <th class="text-center">WSD Name</th>
                                                    <th class="text-center">WSD Location</th>
                                                    <th class="text-center">Delivery Note No.</th>                                              
                                                    <th class="text-center">Retailer Name</th>
                                                    <th class="text-center">Retailer Location</th>
                                                    <th class="text-center">LR Number</th>
                                                    <th class="text-center">LR Date</th>
                                                    <th class="text-center">Rane Invoice Number</th>
                                                    <th class="text-center">Rane Invoice Date</th>
                                                    <th class="text-center">Part Number</th>
                                                    <th class="text-center">Quantity</th>
                                                    <th class="text-center">Service Engg Decision</th>
                                                    <th class="text-center">Material Send to Warehouse</th>
                                                    <th class="text-center">Material Inward Number</th>
                                                    <th class="text-center">Material Inward Date</th>
                                                    <th class="text-center">Plant DC Number</th>
                                                    <th class="text-center">Plant DC Date</th>
                                                    <th class="text-center">Plant Notification Number</th>
                                                    <th class="text-center">Credit Note Ref No</th>
                                                    <th class="text-center">Credit Note Date</th>
                                                    <th class="text-center">Completed Date</th>
                                                    <th class="text-center">FIR Status</th>
                                                </tr>
                                                <tr>
                                                    <asp:PlaceHolder ID="itmPlaceholder" runat="server"></asp:PlaceHolder>
                                                </tr>
                                            </thead>
                                            <tr>
                                                <td colspan="5">
                                                    <asp:DataPager ID="dataPagerNumeric" runat="server" PageSize="10">
                                                        <Fields>
                                                            <asp:NextPreviousPagerField FirstPageText="<<" RenderDisabledButtonsAsLabels="true"
                                                                ButtonType="Button" ShowFirstPageButton="True" ButtonCssClass="btn btn-default"
                                                                ShowNextPageButton="False" ShowPreviousPageButton="True" Visible="true" />
                                                            <asp:NumericPagerField ButtonCount="5" NumericButtonCssClass="btn btn-default" CurrentPageLabelCssClass="btn btn-success active" />
                                                            <asp:NextPreviousPagerField LastPageText=">>" RenderDisabledButtonsAsLabels="true"
                                                                ButtonType="Button" ShowLastPageButton="True" ButtonCssClass="btn btn-default"
                                                                ShowNextPageButton="True" ShowPreviousPageButton="False" Visible="true" />
                                                        </Fields>
                                                    </asp:DataPager>
                                                </td>
                                                <td colspan="19" class="number_of_record" style="text-align: right">
                                                    <asp:DataPager ID="dataPageDisplayNumberOfPages" runat="server" PageSize="10">
                                                        <Fields>
                                                            <asp:TemplatePagerField>
                                                                <PagerTemplate>
                                                                    <span style="color: Black;">Records:
                                                                <%# Container.StartRowIndex >= 0 ? (Container.StartRowIndex + 1) : 0 %>
                                                                -
                                                                <%# (Container.StartRowIndex + Container.PageSize) > Container.TotalRowCount ? Container.TotalRowCount : (Container.StartRowIndex + Container.PageSize)%>
                                                                of
                                                                <%# Container.TotalRowCount %>
                                                                    </span>
                                                                </PagerTemplate>
                                                            </asp:TemplatePagerField>
                                                        </Fields>
                                                    </asp:DataPager>
                                                </td>
                                            </tr>

                                        </table>

                                    </LayoutTemplate>
                                    <ItemTemplate>
                                        <tbody id="mytable">
                                            <tr>
                                                
                                                <td class="text-center">
                                                   <a href="ViewComplaint.aspx?Id=<%#  Rane_WSD.MyCrypto.GetEncryptedQueryString( DataBinder.Eval(Container.DataItem,"Id").ToString()) %>" title="View FIR" data-toggle="tooltip" data-placement="top" class="text-primary"><u><%#Eval("FIRNo")%></u></a>
                                                </td>
                                                <td class="text-center">
                                                 <%#Eval("CreatedOn" ,"{0: dd MMM yyyy}")%>
                                                </td>
                                                <td class="text-center">
                                                  <%#Eval("WSDName")%>
                                                </td>
                                                <td class="text-center">
                                                  <%#Eval("WSDBranch")%>
                                                </td>
                                                <td class="text-center">
                                                  <%#Eval("DeliveryNoteNo")%>
                                                </td>
                                                 <td class="text-center">
                                                  <%#Eval("RetailerName")%>
                                                </td>
                                                 <td class="text-center">
                                                  <%#Eval("RetailerLocation")%>
                                                </td>
                                                  <td class="text-center">
                                                  <%#Eval("LRNo")%>
                                                </td>
                                                 <td class="text-center">
                                                  <%#Eval("LRDate")%>
                                                </td>
                                                  <td class="text-center">
                                                  <%#Eval("WSDInvoiceNo")%>
                                                </td>
                                                 <td class="text-center">
                                                  <%#Eval("InvoiceDate" ,"{0: dd MMM yyyy}")%>
                                                </td>
                                                <td class="text-center">
                                                  <%#Eval("AggregatePartNo")%>
                                                </td>
                                                 <td class="text-center">
                                                  <%#Eval("Quantity")%>
                                                </td>
                                                 <td class="text-center">
                                                  <%#Eval("ServiceEnggRecommendation")%>
                                                </td>
                                                 <td class="text-center">
                                                  <%#Eval("DispatchedtowhDate")%>
                                                </td>
                                                <td class="text-center">
                                                  <%#Eval("InwardNumber")%>
                                                </td>
                                                 <td class="text-center">
                                                  <%#Eval("InwardDate")%>
                                                </td>
                                                <td class="text-center">
                                                  <%#Eval("PlantDocNo")%>
                                                </td>
                                                 <td class="text-center">
                                                  <%#Eval("PlantDocDate")%>
                                                </td>
                                                <td class="text-center">
                                                  <%#Eval("PlantNotificationNo")%>
                                                </td>
                                                 <td class="text-center">
                                                  <%#Eval("CreditNoteNo")%>
                                                </td>
                                                 <td class="text-center">
                                                  <%#Eval("CreditNoteDate" ,"{0: dd MMM yyyy}")%>
                                                </td>                                                
                                                <td class="text-center">
                                                  <%#Eval("CompletedDate" ,"{0: dd MMM yyyy}")%>
                                                </td>
                                                 <td class="text-center">
                                                  <%# db.IsActive(Eval("StatusName").ToString())%>
                                                </td>
                                               
                                            </tr>
                                        </tbody>
                                    </ItemTemplate>
                                    <EmptyDataTemplate>
                                        <div class="widget-content">
                                            <div class="table-responsive">
                                                <table class="table table-hover table-bordered">
                                                    <th>

                                                        <tr>
                                                    <th class="text-center">FIR Number</th>
                                                    <th class="text-center">FIR Created Date</th>                                               
                                                    <th class="text-center">WSD Name</th>
                                                    <th class="text-center">WSD Location</th>
                                                    <th class="text-center">Delivery Note No.</th>                                              
                                                    <th class="text-center">Retailer Name</th>
                                                    <th class="text-center">Retailer Location</th>
                                                    <th class="text-center">LR Number</th>
                                                    <th class="text-center">LR Date</th>
                                                    <th class="text-center">Rane Invoice Number</th>
                                                    <th class="text-center">Rane Invoice Date</th>
                                                    <th class="text-center">Part Number</th>
                                                    <th class="text-center">Quantity</th>
                                                    <th class="text-center">Service Engg Decision</th>
                                                    <th class="text-center">Material Send to Warehouse</th>
                                                    <th class="text-center">Material Inward Number</th>
                                                    <th class="text-center">Material Inward Date</th>
                                                    <th class="text-center">Plant DC Number</th>
                                                    <th class="text-center">Plant DC Date</th>
                                                    <th class="text-center">Plant Notification Number</th>
                                                    <th class="text-center">Credit Note Ref No</th>
                                                    <th class="text-center">Credit Note Date</th>
                                                    <th class="text-center">Completed Date</th>
                                                    <th class="text-center">FIR Status</th>
                                                        </tr>
                                                        <tr class="text-center">
                                                            <td colspan="24" style="background-color: white; color: black;">No Records Found</td>
                                                        </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                        </div>
                                    </EmptyDataTemplate>
                                </asp:ListView>

                            </div>
                        </div>
                        <!-- end card-box-->
                    </div>
                    <!-- end col-->
                </div>
            </div>
        </div>
    </div>
    <!-- end row -->
    </asp:Content>
