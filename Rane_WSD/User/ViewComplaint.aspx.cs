﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Rane_WSD.User
{
    public partial class ViewComplaint : System.Web.UI.Page
    {
        string cs = ConfigurationManager.ConnectionStrings["conn"].ConnectionString;
        public string pdfName = "";
        public string pdfName1 = "";
        public string pdfName2 = "";
        public string pdfName3 = "";
        public string pdfName4 = "";
        public string pdfName5 = "";
        public string pdfName6 = "";
        public string pdfName7 = "";
        public string pdfName9 = "";
        public string pdfName10 = "";
        public string pdfName11 = "";
        public string pdfName12 = "";
        public string pdfName13 = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Panel1.Visible = false;
                Panel2.Visible = false;
                Panel3.Visible = false;
                Panel4.Visible = false;
                Panel5.Visible = false;
                Panel6.Visible = false;
                Panel7.Visible = false;
                Panel8.Visible = false;
                string editacc = MyCrypto.GetDecryptedQueryString(Request.QueryString["Id"].ToString());

                SqlConnection con = new SqlConnection(cs);
                SqlCommand cmd = new SqlCommand("sp_b_FIRRegister", con);

                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                cmd.Parameters.AddWithValue("@Id", editacc);
                cmd.Parameters.AddWithValue("@qtype", "viewFIR");
                SqlDataReader sdr = cmd.ExecuteReader();
                if (sdr.Read())
                {
                    

                    lblFirno.Text=sdr["FIRNo"].ToString();
                        lblAggregateSerialNo.Text=sdr["AggregateSerialNo"].ToString();
                        lblDateofComplaint.Text=String.Format("{0:dd MMM yyyy }",sdr["DateofComplaint"]);
                        lblInvoiceDate.Text= String.Format("{0:dd MMM yyyy }",sdr["InvoiceDate"]);
                        lblInvoiceNo.Text=sdr["WSDInvoiceNo"].ToString();
                        lblPartno.Text=sdr["PartNo"].ToString();
                        lblPartType.Text=sdr["PartType"].ToString();
                        lblQuantity.Text=sdr["Quantity"].ToString();
                        lblRetailerLocation.Text=sdr["RetailerLocation"].ToString();
                        lblRetailername.Text=sdr["RetailerName"].ToString();
                        lblStatus.Text=sdr["StatusName"].ToString();
                        lblUploadFile.Text=sdr["UploadFile"].ToString();
                        lblNotes.Text = sdr["Notes"].ToString();
                        string User = sdr["UploadFile"].ToString();
                        if (!string.IsNullOrEmpty(User))
                        {
                            if (User.Split(',').Count() > 0)
                            {
                                string[] path = User.Split(',');
                                for (int i = 0; i < path.Count(); i++)
                                {
                                    if (path[i].ToString() == "")
                                    {

                                    }
                                    else
                                    {
                                        if (i == 0)
                                        {
                                            Panel1.Visible = true;
                                            pdfName4 = path[i].ToString();
                                            Label4.Text = path[i].ToString();
                                        }
                                        else if (i == 1)
                                        {
                                            Panel2.Visible = true;
                                            pdfName5 = path[i].ToString();
                                            Label5.Text = path[i].ToString();
                                        }
                                        else if (i == 2)
                                        {
                                            Panel3.Visible = true;
                                            pdfName6 = path[i].ToString();
                                            Label6.Text = path[i].ToString();
                                        }
                                        else if (i == 3)
                                        {
                                            Panel4.Visible = true;
                                            pdfName7 = path[i].ToString();
                                            Label7.Text = path[i].ToString();
                                        }

                                    }




                                    //ddl_user.Items.Add(ct);
                                }
                            }
                        }
                        

                        lblWSDbranch.Text=sdr["WSDBranch"].ToString();
                        lblWSDname.Text=sdr["WSDName"].ToString();
                        
                   
                   
                }
                con.Close();
            }
            if (!IsPostBack)
            {
                pnlPDF.Visible = false;
                pnlPDF1.Visible = false;
                pnlPDF2.Visible = false;
                pnlPDF3.Visible = false;

                ServiceApproval();
                AllocateServiceEng();
                AllocateASC();
                ASCData();
                ASCRecommendation();
                ASCDocumentNo();
                WSDReceived();
                WSDSendLR();
                WarehouseReceive();
                CreditNoteUpdate();
                WSDFIRClosed();
                RejecetedASCFeedBack();
                WSDSendToRetailer();
                WSDFIRReject();
                SalesRejection();
              

                if (lblStatus.Text == "Approved By Sales Engineer")
                {
                    
                    pnlApproved.Visible = true;
                }
                else if (lblStatus.Text == "Rejected By Sales Engineer")
                {
                    pnlRejected.Visible = true;
                }
                else if (lblStatus.Text == "Allocated to ASC")
                {
                    pnlApproved.Visible = true;
                    pnlAllocateASC.Visible = true;
                }
                else if (lblStatus.Text == "Material Dispatched to ASC")
                {
                    pnlApproved.Visible = true;
                    pnlAllocateASC.Visible = true;
                    pnlDispatchASC.Visible = true;
                }
                else if (lblStatus.Text == "Material Received From WSD/ASC Observation")
                {
                    pnlApproved.Visible = true;
                    pnlAllocateASC.Visible = true;
                    pnlDispatchASC.Visible = true;
                    pnlReceivedWSD.Visible = true;
                }
                else if (lblStatus.Text == "Approved ASC FeedBack" || lblStatus.Text == "Rejected ASC FeedBack")
                {
                    pnlApproved.Visible = true;
                    pnlAllocateASC.Visible = true;
                    pnlDispatchASC.Visible = true;
                    pnlReceivedWSD.Visible = true;
                    pnlServiceEngApprove.Visible = true;
                }
                else if (lblStatus.Text == "Material Dispatched to WSD")
                {
                    pnlApproved.Visible = true;
                    pnlAllocateASC.Visible = true;
                    pnlDispatchASC.Visible = true;
                    pnlReceivedWSD.Visible = true;
                    pnlDispatchWSD.Visible = true;
                    pnlServiceEngApprove.Visible = true;
                }
                else if (lblStatus.Text == "Material Received From ASC")
                {
                    pnlApproved.Visible = true;
                    pnlAllocateASC.Visible = true;
                    pnlDispatchASC.Visible = true;
                    pnlReceivedWSD.Visible = true;
                    pnlDispatchWSD.Visible = true;
                    pnlServiceEngApprove.Visible = true;
                    pnlReceiApprove.Visible = true;
                }
                else if (lblStatus.Text == "Material Dispatched to Warehouse")
                {
                    pnlApproved.Visible = true;
                    pnlAllocateASC.Visible = true;
                    pnlDispatchASC.Visible = true;
                    pnlReceivedWSD.Visible = true;
                    pnlDispatchWSD.Visible = true;
                    pnlServiceEngApprove.Visible = true;
                    pnlReceiApprove.Visible = true;
                    pnlSendLRNo.Visible = true;
                }
                else if (lblStatus.Text == "Material Received From WSD by Warehouse")
                {
                    pnlApproved.Visible = true;
                    pnlAllocateASC.Visible = true;
                    pnlDispatchASC.Visible = true;
                    pnlReceivedWSD.Visible = true;
                    pnlDispatchWSD.Visible = true;
                    pnlServiceEngApprove.Visible = true;
                    pnlReceiApprove.Visible = true;
                    pnlSendLRNo.Visible = true;
                    pnlReceiveWarehouse.Visible = true;
                }
                else if (lblStatus.Text == "Credit Note Updated")
                {
                    pnlApproved.Visible = true;
                    pnlAllocateASC.Visible = true;
                    pnlDispatchASC.Visible = true;
                    pnlReceivedWSD.Visible = true;
                    pnlDispatchWSD.Visible = true;
                    pnlServiceEngApprove.Visible = true;
                    pnlReceiApprove.Visible = true;
                    pnlSendLRNo.Visible = true;
                    pnlReceiveWarehouse.Visible = true;
                    pnlCreditNoteUpdation.Visible = true;
                }
                else if (lblStatus.Text == "FIR Closed")
                {
                    SqlConnection conn = new SqlConnection(cs);
                    string cmd = "select count(*) from tbl_FIRDetails where FIRNo='" + lblFirno.Text + "' and Status=10";
                    SqlDataAdapter adpt1 = new SqlDataAdapter(cmd, conn);
                    DataTable dt = new DataTable();
                    adpt1.Fill(dt);

                    if (dt.Rows[0][0].ToString() != "0")
                    {
                        pnlRejectedASCFeedBack.Visible = true;
                        pnlSendRetailer.Visible = true;
                        pnlRejectedASCFeedBack.Visible = true;
                        pnlSendRetailer.Visible = true;
                        pnlApproved.Visible = true;
                        pnlAllocateASC.Visible = true;
                        pnlDispatchASC.Visible = true;
                        pnlReceivedWSD.Visible = true;
                        pnlWSDClosed.Visible = true;
                    }
                    else
                    {
                        pnlWSDClosed.Visible = true;
                        pnlApproved.Visible = true;
                        pnlAllocateASC.Visible = true;
                        pnlDispatchASC.Visible = true;
                        pnlReceivedWSD.Visible = true;
                        pnlDispatchWSD.Visible = true;
                        pnlServiceEngApprove.Visible = true;
                        pnlReceiApprove.Visible = true;
                        pnlSendLRNo.Visible = true;
                        pnlReceiveWarehouse.Visible = true;
                        pnlCreditNoteUpdation.Visible = true;
                    }

                }
                else if (lblStatus.Text == "Rejected ASC FeedBack")
                {
                    pnlApproved.Visible = true;
                    pnlAllocateASC.Visible = true;
                    pnlDispatchASC.Visible = true;
                    pnlReceivedWSD.Visible = true;
                    pnlRejectedASCFeedBack.Visible = true;

                }
                else if (lblStatus.Text == "Material Sent Back To Retailer")
                {
                    pnlSendRetailer.Visible = true;
                    pnlApproved.Visible = true;
                    pnlAllocateASC.Visible = true;
                    pnlDispatchASC.Visible = true;
                    pnlReceivedWSD.Visible = true;

                    pnlRejectedASCFeedBack.Visible = true;
                }
                else
                {
                }

               
               
                
                
               
            }
        }
        protected void AllocateServiceEng()
        {
            string editacc = MyCrypto.GetDecryptedQueryString(Request.QueryString["Id"].ToString());

                SqlConnection con = new SqlConnection(cs);
                SqlCommand cmd = new SqlCommand("sp_b_FIRDetails", con);

                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                cmd.Parameters.AddWithValue("@FIRId", editacc);
                cmd.Parameters.AddWithValue("@qtype", "ViewFIRDetails");
                SqlDataReader sdr = cmd.ExecuteReader();
                if (sdr.Read())
                {
                    lblAllocateServiceEng.Text = sdr["AllocateServiceEngg"].ToString();
                    lblRemarks.Text = sdr["Remarks"].ToString();
                    lblUpdatedBy.Text = sdr["UpdatedBy"].ToString();
                    lblUpdatedOn.Text = String.Format("{0:dd MMM yyyy h:mm tt }", sdr["UpdatedOn"]);
                    lblReasonForRejection.Text = sdr["ReasonForRejection"].ToString();
                }
                con.Close();

                    
        }
        protected void AllocateASC()
        {
            string editacc = MyCrypto.GetDecryptedQueryString(Request.QueryString["Id"].ToString());

            SqlConnection con = new SqlConnection(cs);
            SqlCommand cmd = new SqlCommand("sp_b_FIRDetails", con);

            cmd.CommandType = CommandType.StoredProcedure;
            con.Open();
            cmd.Parameters.AddWithValue("@FIRId", editacc);
            cmd.Parameters.AddWithValue("@qtype", "ViewDetails");
            SqlDataReader sdr = cmd.ExecuteReader();
            if (sdr.Read())
            {
                lblAllocateASC.Text = sdr["AllocateAsc"].ToString();
                lblremark1.Text = sdr["Remarks"].ToString();
                lblupda.Text = sdr["UpdatedBy"].ToString();
                lblupdOn.Text = String.Format("{0:dd MMM yyyy h:mm tt }", sdr["UpdatedOn"]);
                
            }
            con.Close();
        }
        protected void ASCData()
        {
            string editacc = MyCrypto.GetDecryptedQueryString(Request.QueryString["Id"].ToString());

            SqlConnection con = new SqlConnection(cs);
            SqlCommand cmd = new SqlCommand("sp_b_FIRDetails", con);

            cmd.CommandType = CommandType.StoredProcedure;
            con.Open();
            cmd.Parameters.AddWithValue("@FIRId", editacc);
            cmd.Parameters.AddWithValue("@qtype", "ViewfirDet");
            SqlDataReader sdr = cmd.ExecuteReader();
            if (sdr.Read())
            {
                lblAlloASC.Text = sdr["AllocateAsc"].ToString();
                lblDispatchMethod.Text = sdr["DispatchMethod"].ToString();
                lblrem.Text = sdr["Remarks"].ToString();
                lblDispatchMethod.Text = sdr["DispatchMethod"].ToString();
                lblDocumentNo.Text=sdr["DocumentNo"].ToString();
                lblDispatchdate.Text = String.Format("{0:dd MMM yyyy h:mm tt }", sdr["DispatchDate"]);
                lblupdby.Text = sdr["UpdatedBy"].ToString();
                lblupd.Text = String.Format("{0:dd MMM yyyy h:mm tt }", sdr["UpdatedOn"]);

            }
            con.Close();
        }
        protected void ASCRecommendation()
        {
            string editacc = MyCrypto.GetDecryptedQueryString(Request.QueryString["Id"].ToString());

            SqlConnection con = new SqlConnection(cs);
            SqlCommand cmd = new SqlCommand("sp_b_FIRDetails", con);

            cmd.CommandType = CommandType.StoredProcedure;
            con.Open();
            cmd.Parameters.AddWithValue("@FIRId", editacc);
            cmd.Parameters.AddWithValue("@qtype", "ViewDet");
            SqlDataReader sdr = cmd.ExecuteReader();
            if (sdr.Read())
            {
                lblRecommendation.Text = sdr["ASCRecommendation"].ToString();
                lblASCReporteddate.Text = String.Format("{0:dd MMM yyyy h:mm tt }", sdr["ASCReportedDate"]); 
                lblObservationofProblem.Text = sdr["ObservationofProblem"].ToString();
                lblReceivedDate.Text = String.Format("{0:dd MMM yyyy h:mm tt }", sdr["ReceivedDate"]);
                lblUploadedFiles.Text = sdr["FileUpload"].ToString();
                string User = sdr["FileUpload"].ToString();
                if (!string.IsNullOrEmpty(User))
                {
                    if (User.Split(',').Count() > 0)
                    {
                        string[] path = User.Split(',');
                        for (int i = 0; i < path.Count(); i++)
                        {
                            if (path[i].ToString() == "")
                            {

                            }
                            else
                            {
                                if (i == 0)
                                {
                                    pnlPDF.Visible = true;
                                    pdfName = path[i].ToString();
                                    Label8.Text = path[i].ToString();
                                }
                                else if (i == 1)
                                {
                                    pnlPDF1.Visible = true;
                                    pdfName1 = path[i].ToString();
                                    Label9.Text = path[i].ToString();
                                }
                                else if (i == 2)
                                {
                                    pnlPDF2.Visible = true;
                                    pdfName2 = path[i].ToString();
                                    Label10.Text = path[i].ToString();
                                }
                                else if (i == 3)
                                {
                                    pnlPDF3.Visible = true;
                                    pdfName3 = path[i].ToString();
                                    Label11.Text = path[i].ToString();
                                }

                            }




                            //ddl_user.Items.Add(ct);
                        }
                    }
                }
                if (pdfName == "")
                {
                    pnlPDF.Visible = false;
                }
                else
                {
                    pnlPDF.Visible = true;
                }

                lblupdat.Text = sdr["UpdatedBy"].ToString();
                lblupdatOn.Text = String.Format("{0:dd MMM yyyy h:mm tt }", sdr["UpdatedOn"]);

            }
            con.Close();
        }
        protected void ASCDocumentNo()
        {
            string editacc = MyCrypto.GetDecryptedQueryString(Request.QueryString["Id"].ToString());

            SqlConnection con = new SqlConnection(cs);
            SqlCommand cmd = new SqlCommand("sp_b_FIRDetails", con);

            cmd.CommandType = CommandType.StoredProcedure;
            con.Open();
            cmd.Parameters.AddWithValue("@FIRId", editacc);
            cmd.Parameters.AddWithValue("@qtype", "ViewDetailsFIR");
            SqlDataReader sdr = cmd.ExecuteReader();
            if (sdr.Read())
            {
                lbllrno.Text = sdr["LRNumber"].ToString();
                lblDcNo.Text = sdr["DCNo"].ToString();
                lblre.Text = sdr["Remarks"].ToString();
                lblLrdate.Text = String.Format("{0:dd MMM yyyy h:mm tt }", sdr["LRDate"]);
                //lblUploadedFiles.Text = sdr["FileUpload"].ToString();
                lblupdatedby1.Text = sdr["UpdatedBy"].ToString();
                lblupdatedOn1.Text = String.Format("{0:dd MMM yyyy h:mm tt }", sdr["UpdatedOn"]);

            }
            con.Close();
        }
        protected void ServiceApproval()
        {
            string editacc = MyCrypto.GetDecryptedQueryString(Request.QueryString["Id"].ToString());

            SqlConnection con = new SqlConnection(cs);
            SqlCommand cmd = new SqlCommand("sp_b_FIRDetails", con);

            cmd.CommandType = CommandType.StoredProcedure;
            con.Open();
            cmd.Parameters.AddWithValue("@FIRId", editacc);
            cmd.Parameters.AddWithValue("@qtype", "ViewServiceApproval");
            SqlDataReader sdr = cmd.ExecuteReader();
            if (sdr.Read())
            {
                lblServiceRecommended.Text = sdr["ASCRecommendation"].ToString();
              
                lblremarks3.Text = sdr["Remarks"].ToString();
               
                //lblUploadedFiles.Text = sdr["FileUpload"].ToString();
                UpdatedBy.Text = sdr["UpdatedBy"].ToString();
                UpdatedOn.Text = String.Format("{0:dd MMM yyyy h:mm tt }", sdr["UpdatedOn"]);

            }
            con.Close();
        }
        protected void WSDReceived()
        {
            string editacc = MyCrypto.GetDecryptedQueryString(Request.QueryString["Id"].ToString());

            SqlConnection con = new SqlConnection(cs);
            SqlCommand cmd = new SqlCommand("sp_b_FIRDetails", con);

            cmd.CommandType = CommandType.StoredProcedure;
            con.Open();
            cmd.Parameters.AddWithValue("@FIRId", editacc);
            cmd.Parameters.AddWithValue("@qtype", "ViewReceiveWSD");
            SqlDataReader sdr = cmd.ExecuteReader();
            if (sdr.Read())
            {

                lblupload20.Text = sdr["FileUpload"].ToString();
                string User3 = sdr["FileUpload"].ToString();
                if (!string.IsNullOrEmpty(User3))
                {
                    if (User3.Split(',').Count() > 0)
                    {
                        string[] path = User3.Split(',');
                        for (int i = 0; i < path.Count(); i++)
                        {
                            if (path[i].ToString() == "")
                            {

                            }
                            else
                            {
                                if (i == 0)
                                {
                                    Panel5.Visible = true;
                                    pdfName9 = path[i].ToString();
                                    Label12.Text = path[i].ToString();
                                }
                                else if (i == 1)
                                {
                                    Panel6.Visible = true;
                                    pdfName10 = path[i].ToString();
                                    Label13.Text = path[i].ToString();
                                }
                                else if (i == 2)
                                {
                                    Panel7.Visible = true;
                                    pdfName11 = path[i].ToString();
                                    Label14.Text = path[i].ToString();
                                }
                                else if (i == 3)
                                {
                                    Panel8.Visible = true;
                                    pdfName13 = path[i].ToString();
                                    Label15.Text = path[i].ToString();
                                }

                            }




                            //ddl_user.Items.Add(ct);
                        }
                    }
                }
                if (pdfName == "")
                {
                    pnlPDF.Visible = false;
                }
                else
                {
                    pnlPDF.Visible = true;
                }

                lblupdated20.Text = sdr["UpdatedBy"].ToString();
                lblupdate21.Text = String.Format("{0:dd MMM yyyy h:mm tt }", sdr["UpdatedOn"]);

            }
            con.Close();
        }
        protected void WSDSendLR()
        {
            string editacc = MyCrypto.GetDecryptedQueryString(Request.QueryString["Id"].ToString());

            SqlConnection con = new SqlConnection(cs);
            SqlCommand cmd = new SqlCommand("sp_b_FIRDetails", con);

            cmd.CommandType = CommandType.StoredProcedure;
            con.Open();
            cmd.Parameters.AddWithValue("@FIRId", editacc);
            cmd.Parameters.AddWithValue("@qtype", "SendMaterialWarehouse");
            SqlDataReader sdr = cmd.ExecuteReader();
            if (sdr.Read())
            {
                lblLRNoteno.Text = sdr["LRNumber"].ToString();
                lblLRNoteDate.Text = String.Format("{0:dd MMM yyyy h:mm tt }", sdr["LRDate"]);
                lblDeliveryNote.Text = sdr["DeliveryNoteNo"].ToString();
                lblTransportation.Text = sdr["TransportationName"].ToString();


                lblreman.Text = sdr["Remarks"].ToString();

                //lblUploadedFiles.Text = sdr["FileUpload"].ToString();
                lblupdated13.Text = sdr["UpdatedBy"].ToString();
                lblupdated14.Text = String.Format("{0:dd MMM yyyy h:mm tt }", sdr["UpdatedOn"]);

            }
            else
            {
                pnlSendLRNo.Visible = false;
            }
            con.Close();
        }

        protected void WarehouseReceive()
        {
            string editacc = MyCrypto.GetDecryptedQueryString(Request.QueryString["Id"].ToString());

            SqlConnection con = new SqlConnection(cs);
            SqlCommand cmd = new SqlCommand("sp_b_FIRDetails", con);

            cmd.CommandType = CommandType.StoredProcedure;
            con.Open();
            cmd.Parameters.AddWithValue("@FIRId", editacc);
            cmd.Parameters.AddWithValue("@qtype", "ViewWarehouse");
            SqlDataReader sdr = cmd.ExecuteReader();
            if (sdr.Read())
            {
                lblInwardNumber.Text = sdr["InwardNumber"].ToString();
                lblQuantity20.Text = sdr["InwardQuantity"].ToString();
                lblRemarks20.Text = sdr["Remarks"].ToString();
                lblInwardDate.Text = String.Format("{0:dd MMM yyyy h:mm tt }", sdr["InwardDate"]);
                //lblUploadedFiles.Text = sdr["FileUpload"].ToString();
                lblupdate15.Text = sdr["UpdatedBy"].ToString();
                lblupdate16.Text = String.Format("{0:dd MMM yyyy h:mm tt }", sdr["UpdatedOn"]);

            }
            con.Close();
        }
        protected void CreditNoteUpdate()
        {
            string editacc = MyCrypto.GetDecryptedQueryString(Request.QueryString["Id"].ToString());

            SqlConnection con = new SqlConnection(cs);
            SqlCommand cmd = new SqlCommand("sp_b_FIRDetails", con);

            cmd.CommandType = CommandType.StoredProcedure;
            con.Open();
            cmd.Parameters.AddWithValue("@FIRId", editacc);
            cmd.Parameters.AddWithValue("@qtype", "SendWarehouse");
            SqlDataReader sdr = cmd.ExecuteReader();
            if (sdr.Read())
            {
                lblCreditNoteNo.Text = sdr["CreditNoteNo"].ToString();
                lblCreditNotedate.Text = sdr["CreditNoteDate"].ToString();
                lblremaks25.Text = sdr["Remarks"].ToString();
                lblValue.Text = sdr["Value"].ToString();
                lblPlantDocumentNo.Text = sdr["PlantDocNo"].ToString();
                lblPlantDocumentDate.Text = sdr["PlantDocDate"].ToString();
                lblPlantNotifyNo.Text = sdr["PlantNotificationNo"].ToString();
                //lblInwardDate.Text = String.Format("{0:dd MMM yyyy h:mm tt }", sdr["InwardDate"]);
                //lblUploadedFiles.Text = sdr["FileUpload"].ToString();
                lblupdated18.Text = sdr["UpdatedBy"].ToString();
                lblupdated19.Text = String.Format("{0:dd MMM yyyy h:mm tt }", sdr["UpdatedOn"]);

            }
            con.Close();

        }
        protected void WSDFIRClosed()
        {
            string editacc = MyCrypto.GetDecryptedQueryString(Request.QueryString["Id"].ToString());

            SqlConnection con = new SqlConnection(cs);
            SqlCommand cmd = new SqlCommand("sp_b_FIRDetails", con);

            cmd.CommandType = CommandType.StoredProcedure;
            con.Open();
            cmd.Parameters.AddWithValue("@FIRId", editacc);
            cmd.Parameters.AddWithValue("@qtype", "ClosedWSD");
            SqlDataReader sdr = cmd.ExecuteReader();
            if (sdr.Read())
            {

                lblremarks21.Text = sdr["Remarks"].ToString();

                lblUpdated25.Text = sdr["UpdatedBy"].ToString();
                lblUpdated21.Text = String.Format("{0:dd MMM yyyy h:mm tt }", sdr["UpdatedOn"]);

            }
            con.Close();
        }
        protected void RejecetedASCFeedBack()
        {
            string editacc = MyCrypto.GetDecryptedQueryString(Request.QueryString["Id"].ToString());

            SqlConnection con = new SqlConnection(cs);
            SqlCommand cmd = new SqlCommand("sp_b_FIRDetails", con);

            cmd.CommandType = CommandType.StoredProcedure;
            con.Open();
            cmd.Parameters.AddWithValue("@FIRId", editacc);
            cmd.Parameters.AddWithValue("@qtype", "MateSendBackRetailer");
            SqlDataReader sdr = cmd.ExecuteReader();
            if (sdr.Read())
            {

                lblremarks26.Text = sdr["Remarks"].ToString();

                lblupdated27.Text = sdr["UpdatedBy"].ToString();
                lblupdated28.Text = String.Format("{0:dd MMM yyyy h:mm tt }", sdr["UpdatedOn"]);

            }
            con.Close();
        }
        protected void WSDSendToRetailer()
        {
            string editacc = MyCrypto.GetDecryptedQueryString(Request.QueryString["Id"].ToString());

            SqlConnection con = new SqlConnection(cs);
            SqlCommand cmd = new SqlCommand("sp_b_FIRDetails", con);

            cmd.CommandType = CommandType.StoredProcedure;
            con.Open();
            cmd.Parameters.AddWithValue("@FIRId", editacc);
            cmd.Parameters.AddWithValue("@qtype", "WSDSendToRetailer");
            SqlDataReader sdr = cmd.ExecuteReader();
            if (sdr.Read())
            {

                Label1.Text = sdr["Remarks"].ToString();

                Label2.Text = sdr["UpdatedBy"].ToString();
                Label3.Text = String.Format("{0:dd MMM yyyy h:mm tt }", sdr["UpdatedOn"]);

            }
            con.Close();
        }
        protected void WSDFIRReject()
        {
            string editacc = MyCrypto.GetDecryptedQueryString(Request.QueryString["Id"].ToString());

            SqlConnection con = new SqlConnection(cs);
            SqlCommand cmd = new SqlCommand("sp_b_FIRDetails", con);

            cmd.CommandType = CommandType.StoredProcedure;
            con.Open();
            cmd.Parameters.AddWithValue("@FIRId", editacc);
            cmd.Parameters.AddWithValue("@qtype", "SalesRejected");
            SqlDataReader sdr = cmd.ExecuteReader();
            if (sdr.Read())
            {

                Label16.Text = sdr["ReasonForRejection"].ToString();

                Label17.Text = sdr["UpdatedBy"].ToString();
                Label18.Text = String.Format("{0:dd MMM yyyy h:mm tt }", sdr["UpdatedOn"]);

            }
            con.Close();
        }
        protected void SalesRejection()
        {

        }

    }
}