﻿<%@ Page Title="" Language="C#" MasterPageFile="~/User/UserLayout.Master" AutoEventWireup="true" CodeBehind="CreateFIR.aspx.cs" MaintainScrollPositionOnPostback="true" Inherits="Rane_WSD.User.CreateFIR" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="page-title-box">
        <div class="container-fluid">
            <div class="page-title dflex-between-center">
                <h3 class="mb-1 font-weight-bold">Create FIR</h3>
                <ol class="breadcrumb mb-0 mt-1">
                    <li class="breadcrumb-item">
                        <a href="#">
                            <i class="bx bx-home fs-xs"></i>
                        </a>
                    </li>
                    <li class="breadcrumb-item active">Create FIR</li>
                </ol>
            </div>
        </div>
    </div>

    <!-- page content -->
    <div class="page-content-wrapper mt--45">
        <div class="container-fluid">



            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-title">Create FIR</h5>
                            <%--<div class="media-body" style="background-color: rgb(30 33 57 / 15%);
    border-radius: 8px;">
                <b>Note :</b><p>Aggregate means assemblies like pump,steering gears and reservoirs and also include Worm & Rack Assy,Worm& Valve Assy,Catridge Kit Assy and Sector Shaft Assy.</p>
            </div>--%>
                        </div>
                        <div class="card-body">
                            <asp:Label ID="lblError" runat="server" Text=""></asp:Label>

                            <div class="row">
                                <div class="col-lg-4">

                                    <div class="form-group" style="display: none">
                                        <label>FIR No <span class="text-danger">*</span></label>
                                        <asp:TextBox ID="txtFirno" ForeColor="Red" ReadOnly="true" class="form-control" runat="server" placeholder="Enter FIRNo "></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ForeColor="Red" ControlToValidate="txtFirno" runat="server" ErrorMessage="Enter FIRNo"></asp:RequiredFieldValidator>
                                    </div>
                                    <div class="form-group">
                                        <label>WSD Name<span class="text-danger">*</span></label>
                                        <asp:TextBox ID="txtWSDname" ReadOnly="true" class="form-control" runat="server" ForeColor="Blue" placeholder="Enter WSDName"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ForeColor="Red" ControlToValidate="txtWSDname" runat="server" ErrorMessage="Enter WSDName"></asp:RequiredFieldValidator>
                                    </div>

                                    <div class="form-group">
                                        <label>WSD Branch <span class="text-danger">*</span></label>
                                        <asp:TextBox ID="txtWSDBranch" ReadOnly="true" class="form-control" runat="server" ForeColor="Blue" placeholder="Enter WSDBranch"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ForeColor="Red" ControlToValidate="txtWSDBranch" runat="server" ErrorMessage="Enter WSDBranch"></asp:RequiredFieldValidator>
                                    </div>
                                    <div class="form-group">
                                        <label>Retailer Name <span class="text-danger">*</span></label>
                                        <asp:TextBox ID="txtRetailerName" class="form-control" runat="server" placeholder="Enter Retailer Name"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator8" ForeColor="Red" ControlToValidate="txtRetailerName" runat="server" ErrorMessage="Enter Retailer Name"></asp:RequiredFieldValidator>
                                    </div>
                                    <div class="form-group">
                                        <label>Retailer Location <span class="text-danger">*</span></label>
                                        <asp:TextBox ID="txtRetailerLocation" class="form-control" runat="server" placeholder="Enter Retailer Location"></asp:TextBox>


                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ForeColor="Red" ControlToValidate="txtRetailerLocation" runat="server" ErrorMessage="Enter Retailer Location"></asp:RequiredFieldValidator>
                                    </div>





                                </div>
                                <!-- end col -->

                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Part No <span class="text-danger">*</span></label>
                                        <asp:TextBox ID="txtpartno" class="form-control" runat="server" placeholder="Enter Part No"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ForeColor="Red" runat="server" ControlToValidate="txtpartno" ErrorMessage="Enter Part No"></asp:RequiredFieldValidator>
                                    </div>
                                    <div class="form-group">
                                        <label>WSD Invoice No<span class="text-danger">*</span></label>
                                        <asp:TextBox ID="txtInvoiceNo" class="form-control" runat="server" placeholder="Enter WSD Invoice No"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator9" ForeColor="Red" ControlToValidate="txtInvoiceNo" runat="server" ErrorMessage="Enter WSDInvoice No"></asp:RequiredFieldValidator>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label>Invoice Date <span class="text-danger">*</span></label>
                                        <asp:TextBox ID="txtInvoicedate" TextMode="Date" placeholder="Enter Invoice Date" CssClass="form-control" runat="server" AutoPostBack="true"  OnTextChanged="txtInvoicedate_TextChanged"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" ForeColor="Red" Display="Dynamic" runat="server" ControlToValidate="txtInvoicedate" ErrorMessage="Enter Invoice Date"></asp:RequiredFieldValidator>
                                                                      <asp:Label ID="txtdeviation" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>

                                    </div>
<asp:Label ID="lblwarranty" runat="server" style="display:none;"></asp:Label>
                                    <div class="form-group" style="display: none;">
                                        <label>Status</label>
                                        <asp:DropDownList ID="ddlStatus" class="form-control" runat="server">
                                            <%--<asp:ListItem Value="Active">Active</asp:ListItem>
                                            <asp:ListItem Value="Deactive">Deactive</asp:ListItem>--%>
                                        </asp:DropDownList>

                                    </div>
                                    <div class="form-group">
                                        <label>Notes <span class="text-danger">*</span></label>
                                        <asp:TextBox ID="txtNotes" TextMode="MultiLine" placeholder="Enter Comments" CssClass="form-control" runat="server"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator11" ForeColor="Red" runat="server" ControlToValidate="txtNotes" ErrorMessage="Enter Comments"></asp:RequiredFieldValidator>
                                    </div>

                                    <%--  <div class="form-group">
                                        <label>Date of Complaint <span class="text-danger">*</span></label>
                                         <asp:TextBox ID="txtDateofComplaint" class="form-control" runat="server" TextMode="Date" placeholder="Enter Date "></asp:TextBox>
                                          
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator11" ForeColor="Red" runat="server" ControlToValidate="txtDateofComplaint" ErrorMessage="Enter Date"></asp:RequiredFieldValidator>
                                    </div>--%>
                                </div>
                                <!-- end col -->

                                <div class="col-lg-4">

                                    <%--                                    <div class="form-group">

                                        <label>Part Type <span class="text-danger">*</span></label>
                                        <asp:DropDownList ID="ddlParttype" AutoPostBack="true" OnSelectedIndexChanged="ddlParttype_SelectedIndexChanged" CssClass="form-control" runat="server">
                                            <asp:ListItem Value="">--Select--</asp:ListItem>
                                            <asp:ListItem Value="Aggregate">Aggregate</asp:ListItem>
                                            <asp:ListItem Value="Child Part">Child Part</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="ReqParttype" ForeColor="Red" ControlToValidate="ddlParttype" runat="server" ErrorMessage="Choose PartType"></asp:RequiredFieldValidator>
                                    </div>--%>

                                    <div class="form-group">

                                        <label>Part Type <span class="text-danger">*</span></label>
                                        <asp:DropDownList ID="ddlParttype" CssClass="form-control" runat="server" onchange="return JsFunction(this.value)">
                                            <asp:ListItem Value="">--Select--</asp:ListItem>
                                            <asp:ListItem Value="Aggregate">Aggregate</asp:ListItem>
                                            <asp:ListItem Value="Child Part">Child Part</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="ReqParttype" ForeColor="Red" ControlToValidate="ddlParttype" runat="server" ErrorMessage="Choose PartType"></asp:RequiredFieldValidator>
                                    </div>
                                    <asp:Panel ID="SerialNo" runat="server">

                                        <div class="form-group panel1">
                                            <label>Aggregate Serial No <span class="text-danger">*</span></label>
                                            <asp:TextBox ID="txtSerialno" placeholder="Enter Aggregate Serial No" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                       
                                        <div class="form-group panel1">
                                            <label>Aggregate Model <span class="text-danger">*</span></label>
                                            <asp:TextBox ID="txtAggregateModel" placeholder="Enter Aggregate Model" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                    </asp:Panel>
                                    <div class="form-group">
                                        <label>Quantity <span class="text-danger">*</span></label>
                                        <asp:TextBox ID="txtQuantity" TextMode="Number" class="form-control" runat="server" placeholder="Enter Quantity"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator10" ForeColor="Red" ControlToValidate="txtQuantity" runat="server" ErrorMessage="Enter WSDBranch"></asp:RequiredFieldValidator>
                                    </div>
                                    <div class="form-group">
                                        <label>Upload Invoice<span class="text-danger">*</span></label>
                                        <asp:FileUpload ID="FileUpload1" AllowMultiple="true" class="form-control" runat="server" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator12" ForeColor="Red" ControlToValidate="FileUpload1" runat="server" ErrorMessage="Choose File"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="UploadFile" runat="server"
                                            ControlToValidate="FileUpload1" Display="Dynamic"
                                            ErrorMessage="Only pdf,doc,img file are allowed"
                                            ValidationExpression="^.+(.pdf|.PDF|.jpg|.JPG|.png|.PNG|.doc|.DOC|.docx|.DOCX|.jpeg|.JPEG)$"
                                            Font-Bold="True" Font-Italic="True" ForeColor="#CC3300" SetFocusOnError="True"></asp:RegularExpressionValidator>
                                        <asp:Label ID="listofuploadedfiles" runat="server" Text=""></asp:Label>
                                        <asp:Label ID="lblmsg" runat="server" ForeColor="Red" Font-Bold="true" Text="Upload Maximum 4 Files"></asp:Label>
                                    </div>



                                </div>
                                <div class="pl-3">
                                    <asp:Button ID="btnsubmit" runat="server" OnClick="btnsubmit_Click" CssClass="btn btn-primary" Text="Submit" />
                                    <a href="ListComplaint.aspx" class=" btn btn-dark ">Back</a>
                                </div>

                                <br />
                                <br />

                                <button type="button" class="btn btn-info" style="display: none" data-toggle="modal" id="InfoAlert"
                                    data-target="#info-alert-modal">
                                    Info Alert</button>


                                <div id="info-alert-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
                                    <div class="modal-dialog modal-sm">
                                        <div class="modal-content">
                                            <div class="modal-body p-4">
                                                <div class="text-center">
                                                    <i class="bx bx-info-circle fs-xl text-info"></i>
                                                    <h4 class="mt-2">Notes !</h4>
                                                    <p class="mt-3">
                                                        Aggregate means assemblies like pump,steering gears and reservoirs and also include Worm & Rack Assy,Worm& Valve Assy,Catridge Kit Assy and Sector Shaft Assy.
                                                    </p>
                                                    <button type="button" id="btnDismiss" class="btn btn-info my-2" data-dismiss="modal">Continue</button>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>
                                <!-- /.modal -->


                                <%--  <div id="general2" class="custom-accordion">

                                    <div class="card shadow-none mb-3">
                                        <a class="collapsed" data-toggle="collapse" href="#collapseFour" aria-expanded="true"
                                            aria-controls="collapseFour">
                                            <div class="card-header py-3" id="headingFour">
                                                <h6 class="mb-0">
                                                    <i class="mdi mdi-alarm-note text-primary h5 mr-3"></i>Note
                          <i class="mdi mdi-chevron-up float-right toggle-icon fs-sm"></i>
                                                </h6>
                                            </div>
                                        </a>

                                        <div id="collapseFour" class="collapse show" aria-labelledby="headingFour" data-parent="#general2">
                                            <div class="card-body">
                                                <p><b style="color: grey;">Aggregate means assemblies like pump,steering gears and reservoirs and also include Worm & Rack Assy,Worm& Valve Assy,Catridge Kit Assy and Sector Shaft Assy.</b></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>--%>
                                <!-- end col -->
                            </div>
                            <!-- end row-->

                        </div>
                        <!-- end card-body -->
                    </div>
                    <!-- end card -->
                </div>
                <!-- end col -->
            </div>
        </div>
    </div>
    <!-- end row -->
    <script src="../assets/js/jquery.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            var value = document.getElementById("ddlParttype").value;
            if (value == "Aggregate") {
                $('#InfoAlert').click();
                $('.panel1').show();
                $('#txtQuantity').val(1);
                $('#txtQuantity').attr('readonly', true);
            }
            else {
                $('.panel1').hide();
                $('#txtQuantity').attr('readonly', false);
            }

        });
        function JsFunction(value) {
            if (value == "Aggregate") {
                $('#InfoAlert').click();
                $('.panel1').show();
                $('#txtQuantity').val(1);
                $('#txtQuantity').attr('readonly', true);
            }
            else {
                $('.panel1').hide();
                $('#txtQuantity').attr('readonly', false);
            }
        }
    </script>

</asp:Content>
