﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Rane_WSD.User
{
    public partial class UserLayout : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //if (Session["UserType"].ToString() == "Sales Engineer" || Session["UserType"].ToString() == "WSD" || Session["UserType"].ToString() == "Service Engineer" || Session["UserType"].ToString() == "ASC")
            //{
            //    lblFIR.Text = "View FIR";
            //}

            if (Session["UserName"] == null || Session["UserType"] == null)
            {
                Response.Redirect("~/login.aspx");
            }

            txtusername.Text = Session["UserName"].ToString();
            lblUserType.Text = Session["UserType"].ToString();

            if (Session["UserType"].ToString() == "ASC")
            {
                txtusername.Text = Session["InchargeName"].ToString();
                lblUserType.Text = Session["UserType"].ToString();
            }
           
        }
    }
 }