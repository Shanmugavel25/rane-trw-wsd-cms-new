﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Rane_WSD.User
{
    public partial class Dashboard : System.Web.UI.Page
    {
        string cs = ConfigurationManager.ConnectionStrings["conn"].ConnectionString;
        public string chart = "";
        public string chartproduct = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindData();
                BindValue();                              
            }
        }

        protected void BindData()
        {
            SqlConnection con = new SqlConnection(cs);
            SqlCommand cmd = new SqlCommand("sp_b_Dashboard", con);

            cmd.CommandType = CommandType.StoredProcedure;
            con.Open();
            if (Session["UserType"].ToString() == "WSD")
            {
                cmd.Parameters.AddWithValue("@NAME", Session["BranchName"]);
                cmd.Parameters.AddWithValue("@qtype", Session["UserType"]);
                SqlDataReader sdr = cmd.ExecuteReader();
                if (sdr.Read())
                {
                
                    lbl_New.Text = sdr["New"].ToString();
                    lbl_pending.Text = sdr["Pending"].ToString();
                    lbl_rejected.Text = sdr["Rejected"].ToString();
                    lbl_Completed.Text = sdr["Completed"].ToString();
                    lbl_WSDDispatchWarehouse.Text = sdr["MaterialDisptachedtoWH"].ToString();                     
                    lbl_WSDReceivedWarehouse.Text = sdr["MaterialReceivedfromWSDbyWH"].ToString();
                    lbl_WSDServiceApproved.Text = sdr["ServiceEnggApprove"].ToString();
                    lbl_WSDServiceRejected.Text = sdr["ServiceEnggRejected"].ToString();
               
                }
            }
            if (Session["UserType"].ToString() == "Sales Engineer")
            {
                cmd.Parameters.AddWithValue("@NAME", Session["UserName"]);
                cmd.Parameters.AddWithValue("@qtype", Session["UserType"]);
                SqlDataReader sdr = cmd.ExecuteReader();
                if (sdr.Read())
                {

                    lbl_New.Text = sdr["New"].ToString();
                    lbl_pending.Text = sdr["Pending"].ToString();
                    lbl_rejected.Text = sdr["Rejected"].ToString();
                    lbl_Completed.Text = sdr["Completed"].ToString();
                    lbl_SEApproved.Text = sdr["SalesEnggApprove"].ToString();
                    lbl_SERejected.Text = sdr["SalesEnggRejected"].ToString();
                    lbl_SEPendingWithAdminApproval.Text = sdr["PendingWithAdminApproval"].ToString();
                    lbl_SEsentback.Text = sdr["MaterialSendBacktoRetailer"].ToString();

                }
            }
            if (Session["UserType"].ToString() == "Service Engineer")
            {
                cmd.Parameters.AddWithValue("@NAME", Session["UserName"]);
                cmd.Parameters.AddWithValue("@qtype", Session["UserType"]);
                SqlDataReader sdr = cmd.ExecuteReader();
                if (sdr.Read())
                {
                   
                    lbl_ServEnggApproved.Text = sdr["ASCFeedbackApproved"].ToString();
                    lbl_ServEnggRejected.Text = sdr["ASCFeedbackRejected"].ToString();
                    lbl_servenggcompletedFIR.Text = sdr["Completed"].ToString();
                    lbl_servenggallocatefir.Text = sdr["AllocatedFIR"].ToString();

                }
            }
            if (Session["UserType"].ToString() == "ASC")
            {
                cmd.Parameters.AddWithValue("@NAME", Session["UserName"]);
                cmd.Parameters.AddWithValue("@qtype", Session["UserType"]);
                SqlDataReader sdr = cmd.ExecuteReader();
                if (sdr.Read())
                {

                    lbl_AScallocateFIR.Text = sdr["AllocatedFIR"].ToString();
                    lbl_ASCobserv.Text = sdr["MaterialReceivedFromWSDorASCObservation"].ToString();
                    lbl_ASCfeedbackapproved.Text = sdr["ASCFeedbackApproved"].ToString();
                    lbl_ASCfeedbackrejected.Text = sdr["ASCFeedbackRejected"].ToString();
                    lbl_ASCdispatchedtoWSD.Text = sdr["MaterialDispatchedtoWSD"].ToString();
                    lbl_ASCmaterialDispatchedtoASC.Text = sdr["MaterialDispatchedtoASC"].ToString();

                }
            }
            if (Session["UserType"].ToString() == "Warehouse")
            {
                cmd.Parameters.AddWithValue("@NAME", Session["UserName"]);
                cmd.Parameters.AddWithValue("@qtype", Session["UserType"]);
                SqlDataReader sdr = cmd.ExecuteReader();
                if (sdr.Read())
                {

                    lbl_WHmaterialreceivedfromwsd.Text = sdr["MaterialReceivedFromWSDbyWarehouse"].ToString();
                    lbl_WSDDispatchWarehouse.Text = sdr["MaterialDispatchedToWarehouse"].ToString();
                    lbl_whcreditnoteupdated.Text = sdr["PendingwithWarehouseorCreditnoteupdated"].ToString();
                    lbl_whcompleted.Text = sdr["Completed"].ToString();                   

                }
            }
        }
            protected void BindValue()
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand("select (Select FIRStatus from tbl_FIRstatus b where b.id=a.Status) as StatusName,count(Id) as Total from tbl_FIRRegister a Group by Status", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            dt.Columns.ToString();

            string[] x = new string[dt.Rows.Count];
            int[] y = new int[dt.Rows.Count];


            chart = "[";
            for (int i = 0; i < dt.Rows.Count; i++)
            {

                x[i] = dt.Rows[i][0].ToString();
                // Console.WriteLine(dt.Rows[i].ItemArray[i] + "");
                y[i] = Convert.ToInt32(dt.Rows[i][1]);

                chart += "{ y: " + y[i] + ", label : '" + x[i] + "'},";

                //chartContainer += string.Concat(x, y);

                //.Points.DataBindXY(x, y);

                //Chart1.Series[0].Points.DataBindXY(x, y);
                //Chart1.Series[0].ChartType = SeriesChartType.Pie;
                //Chart1.Series["Series1"].Label = "#PERCENT{P2}";
                //Chart1.Series["Series1"].LegendText = "#VALX";
                //Chart1.Legends[0].LegendStyle = LegendStyle.Column;
                //Chart1.Legends[0].Docking = Docking.Right;
                //Chart1.Legends[0].Alignment = System.Drawing.StringAlignment.Center;

                //Chart1.ChartAreas[0].AxisX.Title = "Registered Details";
                //Chart1.ChartAreas[0].AxisY.Title = "User Status";
                //Chart1.ChartAreas["ChartArea1"].AxisX.IsLabelAutoFit = true;

                //Chart1.ChartAreas["ChartArea1"].AxisX.Interval = 1;
                //Chart1.ChartAreas["ChartArea1"].AxisY.IsLabelAutoFit = true;

                //Chart1.ChartAreas["ChartArea1"].AxisY.Interval = 1;
                //Chart1.ChartAreas[0].AxisX.LabelStyle.Format = "Itemname";
            }
            chart = chart.TrimEnd(',') + "]";
        }
    }
}