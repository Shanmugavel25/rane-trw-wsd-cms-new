﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Rane_WSD.User
{
    public partial class ListComplaint : System.Web.UI.Page
    {
        string cs = ConfigurationManager.ConnectionStrings["conn"].ConnectionString;
        public common db = new common();
        public bool noError = true;
        public bool IsUpdated;

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                BindValue();
                dataget();
                
            }
        }
        protected void BindValue()
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();
            SqlCommand cmd = new SqlCommand("Select distinct FIRStatus,Id from [tbl_FIRstatus] where FIRStatus !=''", con);
            //string com = "Select distinct [Id],FIRStatus from [tbl_FIRstatus] where Id in (2,3)";
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            DropDownList1.DataSource = dt;
            DropDownList1.DataTextField = "FIRStatus";
            DropDownList1.DataValueField = "Id";
            DropDownList1.DataBind();
            DropDownList1.Items.Insert(0, new ListItem("-- Select FIRStatus --", ""));
        }
        protected void dataget()
        {
            SqlConnection con = new SqlConnection(cs);

            SqlCommand cmd = new SqlCommand("sp_b_FIRRegister", con);
            cmd.CommandType = CommandType.StoredProcedure;
            con.Open();
            if (Session["UserType"].ToString() == "WSD")
            {
                cmd.Parameters.AddWithValue("@qtype", "selectlist");
            }
            else if (Session["UserType"].ToString() == "Sales Engineer")
            {
                cmd.Parameters.AddWithValue("@qtype", "selectsales");
            }
            else if (Session["UserType"].ToString() == "Service Engineer")
            {
                cmd.Parameters.AddWithValue("@qtype", "selectservice");
                cmd.Parameters.AddWithValue("@AllocateServiceEngg", Session["Name"].ToString());
            }
            else if (Session["UserType"].ToString() == "ASC")
            {
                cmd.Parameters.AddWithValue("@qtype", "selectasc");
                cmd.Parameters.AddWithValue("@AllocateAsc", Session["Name"].ToString());
            }
            else if (Session["UserType"].ToString() == "Warehouse")
            {
                cmd.Parameters.AddWithValue("@qtype", "selectwarehouse");
            }
            cmd.Parameters.AddWithValue("@FIRNo", txtFIRNO.Text.Trim());
            cmd.Parameters.AddWithValue("@CreatedBy", Session["UserName"].ToString());
            cmd.Parameters.AddWithValue("@WSDName", txtWSDName.Text.Trim());
            cmd.Parameters.AddWithValue("@Status", DropDownList1.SelectedValue);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable ds = new DataTable();
            sda.Fill(ds);
            ListView1.DataSource = ds;

            ListView1.DataBind();
            con.Close();
        }
        public string Id;
        protected void ListView1_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            if (e.CommandName == "delete")
            {
                Id = e.CommandArgument.ToString();

                deleteFn(Id);
            }
            if (noError)
            {
                Response.Redirect("ListComplaint.aspx");
            }
            else
            {

                lblError.Text = "Error Occured please Contact Administrator";
                noError = false;
            }

        }

        protected void ListView1_ItemDeleting(object sender, ListViewDeleteEventArgs e)
        {




        }
        protected void OnPagePropertiesChanging(object sender, PagePropertiesChangingEventArgs e)
        {
            (ListView1.FindControl("dataPagerNumeric") as DataPager).SetPageProperties(e.StartRowIndex, e.MaximumRows, false);
            this.dataget(); ;
        }
        protected string deleteFn(string Id)
        {
            SqlConnection con = new SqlConnection(cs);
            string rStatus = "";
            try
            {

                SqlCommand cmd = new SqlCommand("sp_b_FIRRegister", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                cmd.Parameters.AddWithValue("@Id", Id);
                cmd.Parameters.AddWithValue("@qtype", "deleteFIR");
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Response.Write(ex);
                lblError.Text = "Error Occured please Contact Administrator";
                noError = false;
            }
            finally
            {
                con.Close();

            }


            return rStatus;
        }

        protected void ListView1_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            ListViewDataItem dataItem = (ListViewDataItem)e.Item;


            
            if (e.Item.ItemType == ListViewItemType.DataItem)
            {

                Panel pnlEdit = e.Item.FindControl("pnlEdit") as Panel;
                HiddenField hfStatus = e.Item.FindControl("hfStatus") as HiddenField;

                if (hfStatus.Value == "5")
                {
                    if (Session["UserType"].ToString() == "Service Engineer")
                    {
                    }
                    else
                    {
                        if (Session["UserType"].ToString() == "ASC")
                        {
                        }
                        else
                        {
                            if (Session["UserType"].ToString() == "Sales Engineer" || Session["UserType"].ToString() == "Warehouse" || Session["UserType"].ToString() == "Admin")
                            {
                            }
                            else
                            {
                                HiddenField hfName = e.Item.FindControl("hfName") as HiddenField;

                                if (Session["UserName"].ToString() == hfName.Value && Session["UserType"].ToString() == "WSD" && hfStatus.Value == "5")
                                {

                                    pnlEdit.Visible = true;
                                }
                                else
                                {
                                    pnlEdit.Visible = false;
                                }
                            }
                        }
                    }
                }
                else if (hfStatus.Value == "2")
                {
                    if (Session["UserType"].ToString() == "ASC")
                    {
                       
                    }
                    else
                    {
                        if (Session["UserType"].ToString() == "WSD")
                        {
                            
                        }
                        else
                        {

                            if (Session["UserType"].ToString() == "Sales Engineer" || Session["UserType"].ToString() == "Warehouse" || Session["UserType"].ToString() == "Admin") { }
                            else
                            {

                                HiddenField hfServiceEng = e.Item.FindControl("hfServiceEng") as HiddenField;
                                if (Session["Name"].ToString() == hfServiceEng.Value && Session["UserType"].ToString() == "Service Engineer" && hfStatus.Value == "2")
                                {
                                    pnlEdit.Visible = true;
                                }
                                else
                                {
                                    pnlEdit.Visible = false;
                                }
                            }
                        }
                    }
                }
                else if(hfStatus.Value =="1")
                {
                    if (Session["UserType"].ToString() == "Service Engineer" || Session["UserType"].ToString() == "ASC" || Session["UserType"].ToString() == "Warehouse" || Session["UserType"].ToString() == "WSD" || Session["UserType"].ToString() == "Admin")
                    {

                    }
                    else
                    {

                        HiddenField hfInvoiceDate = e.Item.FindControl("hfInvoiceDate") as HiddenField;
                        var today = DateTime.Now;
                        var invoicedate = Convert.ToDateTime(hfInvoiceDate.Value);

                        int result = (today.Year * 12 + today.Month) - (invoicedate.Year * 12 + invoicedate.Month);

                        if (result < 9)
                        {
                            if (Session["UserType"].ToString() == "Sales Engineer" && hfStatus.Value == "1")
                            {
                                pnlEdit.Visible = true;
                            }
                            else
                            {
                                pnlEdit.Visible = false;
                            }
                        }
                    }

                   
                }
                else if (hfStatus.Value == "6")
                {

                    if (Session["UserType"].ToString() == "WSD" || Session["UserType"].ToString() == "Sales Engineer" || Session["UserType"].ToString() == "Service Engineer" || Session["UserType"].ToString() == "Warehouse" || Session["UserType"].ToString() == "Admin")
                    {
                    }
                    else
                    {
                        HiddenField hfASC = e.Item.FindControl("hfASC") as HiddenField;

                        if (Session["Name"].ToString() == hfASC.Value && Session["UserType"].ToString() == "ASC" && hfStatus.Value == "6")
                        {
                            pnlEdit.Visible = true;

                        }
                        else
                        {
                            pnlEdit.Visible = false;
                        }
                    }
                     
                    

                }
                else if (hfStatus.Value == "9")
                {
                    if (Session["UserType"].ToString() == "Service Engineer" || Session["UserType"].ToString() == "WSD" || Session["UserType"].ToString() == "Sales Engineer" || Session["UserType"].ToString() == "Warehouse" || Session["UserType"].ToString() == "Admin")
                    {

                    }
                    else
                    {
                        HiddenField hfASC = e.Item.FindControl("hfASC") as HiddenField;

                       
                            if (Session["Name"].ToString() == hfASC.Value && Session["UserType"].ToString() == "ASC" && hfStatus.Value == "9")
                            {
                                pnlEdit.Visible = true;
                            }
                            else
                            {
                                pnlEdit.Visible = false;
                            }

                      
                    }
                }
                else if (hfStatus.Value == "7")
                {
                    if (Session["UserType"].ToString() == "ASC")
                    {

                    }
                    else
                    {
                        if (Session["UserType"].ToString() == "WSD")
                        {

                        }
                        else
                        {

                            if (Session["UserType"].ToString() == "Sales Engineer" || Session["UserType"].ToString() == "Warehouse" || Session["UserType"].ToString() == "Admin") { }
                            else
                            {


                                HiddenField hfServiceEng = e.Item.FindControl("hfServiceEng") as HiddenField;

                                 if (Session["Name"].ToString() == hfServiceEng.Value && Session["UserType"].ToString() == "Service Engineer" && hfStatus.Value == "7")
                                    {
                                        pnlEdit.Visible = true;
                                    }
                                    else
                                    {
                                        pnlEdit.Visible = false;
                                    }
                               
                            }
                        }
                    }
                }
                else if (hfStatus.Value == "8")
                {
                    if (Session["UserType"].ToString() == "Service Engineer" || Session["UserType"].ToString() == "ASC" || Session["UserType"].ToString() == "Sales Engineer" || Session["UserType"].ToString() == "Warehouse" || Session["UserType"].ToString() == "Admin")
                    {

                    }
                    else
                    {
                        HiddenField hfWSDName = e.Item.FindControl("hfWSDName") as HiddenField;

                        if (Session["UserName"].ToString() == hfWSDName.Value && Session["UserType"].ToString() == "WSD" && hfStatus.Value == "8")
                        {
                            pnlEdit.Visible = true;
                        }
                        else
                        {
                            pnlEdit.Visible = false;
                        }
                    }
                }
                else if (hfStatus.Value == "12")
                {
                    if (Session["UserType"].ToString() == "Service Engineer" || Session["UserType"].ToString() == "ASC" || Session["UserType"].ToString() == "Sales Engineer" || Session["UserType"].ToString() == "WSD" || Session["UserType"].ToString() == "Admin")
                    {

                    }
                    else
                    {
                        if (Session["UserType"].ToString() == "Warehouse" && hfStatus.Value == "12")
                        {
                            pnlEdit.Visible = true;
                        }
                        else
                        {
                            pnlEdit.Visible = false;
                        }
                    }
                }
                else if(hfStatus.Value=="11")
                {
                    if (Session["UserType"].ToString() == "Service Engineer" || Session["UserType"].ToString() == "ASC" || Session["UserType"].ToString() == "Sales Engineer" || Session["UserType"].ToString() == "Warehouse" || Session["UserType"].ToString() == "Admin")
                    {

                    }
                    else
                    {
                        HiddenField hfWSDName = e.Item.FindControl("hfWSDName") as HiddenField;

                        if (Session["UserName"].ToString() == hfWSDName.Value && Session["UserType"].ToString() == "WSD" && hfStatus.Value == "11")
                        {
                            pnlEdit.Visible = true;
                        }
                        else
                        {
                            pnlEdit.Visible = false;
                        }
                    }
                }
                else if (hfStatus.Value == "13")
                {
                    if (Session["UserType"].ToString() == "Service Engineer" || Session["UserType"].ToString() == "ASC" || Session["UserType"].ToString() == "Sales Engineer" || Session["UserType"].ToString() == "WSD" || Session["UserType"].ToString() == "Admin")
                    {

                    }
                    else
                    {
                        if (Session["UserType"].ToString() == "Warehouse" && hfStatus.Value == "13")
                        {
                            pnlEdit.Visible = true;
                        }
                        else
                        {
                            pnlEdit.Visible = false;
                        }
                    }
                }
                else if (hfStatus.Value == "14" )
                {
                    if (Session["UserType"].ToString() == "Service Engineer" || Session["UserType"].ToString() == "ASC" || Session["UserType"].ToString() == "Sales Engineer" || Session["UserType"].ToString() == "Warehouse" || Session["UserType"].ToString() == "Admin")
                    {

                    }
                    else
                    {
                        HiddenField hfWSDName = e.Item.FindControl("hfWSDName") as HiddenField;

                        if (Session["UserName"].ToString() == hfWSDName.Value && Session["UserType"].ToString() == "WSD" && hfStatus.Value == "14")
                        {
                            pnlEdit.Visible = true;
                        }
                        else
                        {
                            pnlEdit.Visible = false;
                        }
                    }
                }
                else if (hfStatus.Value == "10")
                {
                    if (Session["UserType"].ToString() == "Service Engineer" || Session["UserType"].ToString() == "WSD" || Session["UserType"].ToString() == "Sales Engineer" || Session["UserType"].ToString() == "Warehouse" || Session["UserType"].ToString() == "Admin")
                    {

                    }
                    else
                    {
                        HiddenField hfASC = e.Item.FindControl("hfASC") as HiddenField;


                        if (Session["Name"].ToString() == hfASC.Value && Session["UserType"].ToString() == "ASC" && hfStatus.Value == "10")
                        {
                            pnlEdit.Visible = true;
                        }
                        else
                        {
                            pnlEdit.Visible = false;
                        }

                    }
                }
                else if (hfStatus.Value == "16")
                {
                    if (Session["UserType"].ToString() == "Service Engineer" || Session["UserType"].ToString() == "ASC" || Session["UserType"].ToString() == "Sales Engineer" || Session["UserType"].ToString() == "Warehouse" || Session["UserType"].ToString() == "Admin")
                    {

                    }
                    else
                    {
                        HiddenField hfWSDName = e.Item.FindControl("hfWSDName") as HiddenField;

                        if (Session["UserName"].ToString() == hfWSDName.Value && Session["UserType"].ToString() == "WSD" && hfStatus.Value == "16")
                        {
                            pnlEdit.Visible = true;
                        }
                        else
                        {
                            pnlEdit.Visible = false;
                        }
                    }
                }
                else if (hfStatus.Value == "3")
                {
                    if (Session["UserType"].ToString() == "Service Engineer" || Session["UserType"].ToString() == "ASC" || Session["UserType"].ToString() == "Sales Engineer" || Session["UserType"].ToString() == "Warehouse" || Session["UserType"].ToString() == "Admin")
                    {

                    }
                    else
                    {
                        HiddenField hfWSDName = e.Item.FindControl("hfWSDName") as HiddenField;

                        if (Session["UserName"].ToString() == hfWSDName.Value && Session["UserType"].ToString() == "WSD" && hfStatus.Value == "3")
                        {
                            pnlEdit.Visible = true;
                        }
                        else
                        {
                            pnlEdit.Visible = false;
                        }
                    }
                }
                else if (hfStatus.Value == "4")
                {
                    if (Session["UserType"].ToString() == "Service Engineer" || Session["UserType"].ToString() == "ASC" || Session["UserType"].ToString() == "Warehouse" || Session["UserType"].ToString() == "WSD" || Session["UserType"].ToString() == "Admin")
                    {

                    }
                    else
                    {

                        if (Session["UserType"].ToString() == "Sales Engineer" && hfStatus.Value == "4")
                        {
                            pnlEdit.Visible = true;
                        }
                        else
                        {
                            pnlEdit.Visible = false;
                        }
                    }
                }

                

                
               
                // Visible='<%#(Session["UserType"].ToString()=="WSD" && Eval("Status").ToString()=="5" )%>'
            }

        }
        protected void btn_submit_Click(object sender, EventArgs e)
        {
            dataget();
        }


    }
}