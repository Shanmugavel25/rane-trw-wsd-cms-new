﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Rane_WSD.Admins
{
    public partial class EditComplaint : System.Web.UI.Page
    {
        string cs = ConfigurationManager.ConnectionStrings["conn"].ConnectionString;
        public string pdfName = "";
        public string pdfName1 = "";
        public string pdfName2 = "";

        public string pdfName3 = "";
        public bool noError = true;
        public string str = "";
        public string MDTWSD = "";
        public string APASCF = "";
        public string Status1 = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                pnlPDF.Visible = false;
                pnlPDF1.Visible = false;
                pnlPDF2.Visible = false;
                pnlPDF3.Visible = false;

                string editacc = MyCrypto.GetDecryptedQueryString(Request.QueryString["Id"].ToString());

                SqlConnection con = new SqlConnection(cs);
                SqlCommand cmd = new SqlCommand("[sp_b_FIRRegister]", con);

                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                cmd.Parameters.AddWithValue("@Id", editacc);
                cmd.Parameters.AddWithValue("@qtype", "viewFIR");
                SqlDataReader sdr = cmd.ExecuteReader();
                if (sdr.Read())
                {
                    lblFirno.Text = sdr["FIRNo"].ToString();
                    lblAggregateSerialNo.Text = sdr["AggregateSerialNo"].ToString();
                    lblDateofComplaint.Text = String.Format("{0:dd MMM yyyy }", sdr["DateofComplaint"]);
                    lblInvoiceDate.Text = String.Format("{0:dd MMM yyyy }", sdr["InvoiceDate"]);
                    lblInvoiceno.Text = sdr["WSDInvoiceNo"].ToString();
                    lblpartno.Text = sdr["PartNo"].ToString();
                    lblparttype.Text = sdr["PartType"].ToString();
                    lblQuantity.Text = sdr["Quantity"].ToString();
                    lblretailerlocation.Text = sdr["RetailerLocation"].ToString();
                    lblretailername.Text = sdr["RetailerName"].ToString();
                    lblStatus.Text = sdr["StatusName"].ToString();
                    MDTWSD = sdr["StatusName"].ToString();
                    APASCF = sdr["StatusName"].ToString();
                    Status1 = sdr["StatusName"].ToString();
                    //Session["Filename"] = sdr["UploadFile"].ToString();
                    lblWSDBranch.Text = sdr["WSDBranch"].ToString();
                    lblWSDName.Text = sdr["WSDName"].ToString();
                    lblUploadfile.Text = sdr["UploadFile"].ToString();

                    string User = sdr["UploadFile"].ToString();
                    if (!string.IsNullOrEmpty(User))
                    {
                        if (User.Split(',').Count() > 0)
                        {
                            string[] path = User.Split(',');
                            for (int i = 0; i < path.Count(); i++)
                            {
                                if (path[i].ToString() == "")
                                {

                                }
                                else
                                {
                                    if (i == 0)
                                    {
                                        pnlPDF.Visible = true;
                                        pdfName = path[i].ToString();
                                        Label2.Text = path[i].ToString();
                                    }
                                    else if (i == 1)
                                    {
                                        pnlPDF1.Visible = true;
                                        pdfName1 = path[i].ToString();
                                        Label3.Text = path[i].ToString();
                                    }
                                    else if (i == 2)
                                    {
                                        pnlPDF2.Visible = true;
                                        pdfName2 = path[i].ToString();
                                        Label4.Text = path[i].ToString();
                                    }
                                    else if (i == 3)
                                    {
                                        pnlPDF3.Visible = true;
                                        pdfName3 = path[i].ToString();
                                        Label5.Text = path[i].ToString();
                                    }

                                }




                                //ddl_user.Items.Add(ct);
                            }
                        }
                    }
                    if (pdfName == "")
                    {
                        pnlPDF.Visible = false;
                    }
                    else
                    {
                        pnlPDF.Visible = true;
                    }

                }
                con.Close();
            }
            lblRemarks.Visible = false;
            lblRemarks.Text = "Remarks*";
            txtRemarks.Visible = false;

            if (!IsPostBack)
            {
                getStatus();
            }
        }

        protected void getStatus()
        {
            SqlConnection conn = new SqlConnection(cs);
            conn.Open();
            if ("Admin" == Session["UserType"].ToString())
            {
                string com = "Select distinct [Id],FIRStatus,FIRStatusName from [tbl_FIRstatus] where Id in (4)";
                SqlDataAdapter adpt = new SqlDataAdapter(com, conn);
                DataTable dt = new DataTable();
                adpt.Fill(dt);
                ddlStatus.DataSource = dt;
                ddlStatus.DataTextField = "FIRStatusName";
                ddlStatus.DataValueField = "Id";
                ddlStatus.DataBind();
                ddlStatus.Items.Insert(0, new ListItem("-- Select --", ""));
            }
        }
        protected void ddlStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlStatus.SelectedValue == "4")
            {
                lblRemarks.Visible = true;
                lblRemarks.Text = "Remarks*";
                txtRemarks.Visible = true;

            }

        }
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            UpdateFIR();

            if (noError)
            {
                Response.Write("<script language='javascript'>alert('Update Record Successfully');window.location=('ListComplaint.aspx')</script>");
            }
            else
            {
                //pnlerror.Visible = true;
                lblError.Text = "Error Occured please Contact Administrator";
            }
        }
        protected void UpdateFIR()
        {
            string editacc = MyCrypto.GetDecryptedQueryString(Request.QueryString["Id"].ToString());

            SqlConnection con = new SqlConnection(cs);
            try
            {
                SqlCommand cmd = new SqlCommand("sp_b_FIRDetails", con);
                SqlCommand cmd1 = new SqlCommand("sp_b_FIRRegister", con);
                SqlCommand cmd2 = new SqlCommand("sp_b_AdminApproved", con);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd1.CommandType = CommandType.StoredProcedure;
                cmd2.CommandType = CommandType.StoredProcedure;

                con.Open();
                cmd.Parameters.AddWithValue("@FIRId", editacc);
                cmd1.Parameters.AddWithValue("@Id", editacc);
                cmd2.Parameters.AddWithValue("@FIRId", editacc);

                if (Session["UserType"].ToString() == "Admin")
                {
                    cmd.Parameters.AddWithValue("@qtype", "WSDClosed");
                    cmd1.Parameters.AddWithValue("@qtype", "UpdateAdmin");
                    cmd2.Parameters.AddWithValue("@qtype", "insertFIR");
                    cmd.Parameters.AddWithValue("@FIRNo", lblFirno.Text);
                    cmd2.Parameters.AddWithValue("@FIRNo", lblFirno.Text);
                    cmd2.Parameters.AddWithValue("@WSDName",lblWSDName.Text.Trim());
                    cmd2.Parameters.AddWithValue("@WSDBranch", lblWSDBranch.Text.Trim());
                    cmd2.Parameters.AddWithValue("@RetailerName",lblretailername.Text.Trim());
                    cmd2.Parameters.AddWithValue("@RetailerLocation", lblretailerlocation.Text.Trim());
                    cmd2.Parameters.AddWithValue("@PartNo", lblpartno.Text.Trim());
                    cmd2.Parameters.AddWithValue("@PartType",lblparttype.Text.Trim() );
                    cmd2.Parameters.AddWithValue("@WSDInvoiceNo", lblInvoiceno.Text.Trim());
                    cmd2.Parameters.AddWithValue("@AggregateSerialNo", lblAggregateSerialNo.Text.Trim());
                    cmd2.Parameters.AddWithValue("@InvoiceDate", lblInvoiceDate.Text.Trim());
                    //cmd2.Parameters.AddWithValue("@AggregateModel", );
                    cmd2.Parameters.AddWithValue("@Quantity", lblQuantity.Text.Trim());
                    cmd2.Parameters.AddWithValue("@DateofComplaint", lblDateofComplaint.Text.Trim());
                    cmd2.Parameters.AddWithValue("@UploadFile", lblUploadfile.Text.Trim());
                    //cmd2.Parameters.AddWithValue("@CreatedOn",);
                    //cmd2.Parameters.AddWithValue("@CreatedBy",);
                    cmd.Parameters.AddWithValue("@Status", ddlStatus.SelectedValue);
                    cmd1.Parameters.AddWithValue("@Status", ddlStatus.SelectedValue);
                    cmd.Parameters.AddWithValue("@Remarks", txtRemarks.Text.Trim());
                    cmd1.Parameters.AddWithValue("@Iswarranty", 0);
                    cmd.Parameters.AddWithValue("@UpdatedBy", Session["UserName"].ToString());
                    cmd1.Parameters.AddWithValue("@UpdatedBy", Session["UserName"].ToString());
                }
                cmd.ExecuteNonQuery();
                cmd1.ExecuteNonQuery();
                cmd2.ExecuteNonQuery();



            }
            catch (Exception ex)
            {
                Response.Write(ex);
                lblError.Text = "Error Occured, Please Contact Administrator";
                noError = false;
            }
            finally
            {
                con.Close();


            }
        }


    }
}