﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Rane_WSD
{
    public partial class AccountCreation : System.Web.UI.Page
    {
        string cs = ConfigurationManager.ConnectionStrings["conn"].ConnectionString;
        public string usertype = "";
        public bool noError = true;

        protected void Page_Load(object sender, EventArgs e)
        {
            usertype = Request.QueryString["usertype"].ToString();

            txtUserType.Text = usertype;
            if (!IsPostBack)
            {
                GetData();
                GetState();
            }
        }
        protected void GetData()
        {
            SqlConnection conn = new SqlConnection(cs);
            conn.Open();
            string com = "Select distinct UserType,ShortName from [tbl_usertype] order by UserType asc";
            SqlDataAdapter adpt = new SqlDataAdapter(com, conn);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            ddlUsertype.DataSource = dt;
            ddlUsertype.DataTextField = "UserType";
            ddlUsertype.DataValueField = "ShortName";
            ddlUsertype.DataBind();
            ddlUsertype.Items.Insert(0, new ListItem("-- Select UserType --", ""));
            ddlUsertype.SelectedValue = usertype;
            // ddlUsertype.Enabled = false;
        }
        protected void GetState()
        {
            SqlConnection conn = new SqlConnection(cs);
            conn.Open();
            string com = "Select distinct id,state from tbl_state order by state asc";
            SqlDataAdapter adpt = new SqlDataAdapter(com, conn);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            ddlstate.DataSource = dt;
            ddlstate.DataTextField = "state";
            ddlstate.DataValueField = "state";
            ddlstate.DataBind();
            ddlstate.Items.Insert(0, new ListItem("-- Select State --", ""));
        }

        protected void btnsubmit_Click(object sender, EventArgs e)
        {
            CreateAccount();

            if (noError)
            {
                Response.Write("<script language='javascript'>alert('Account Creation Added Successfully');window.location=('listaccount.aspx?usertype=" + usertype + "')</script>");
            }
            else
            {
                //pnlerror.Visible = true;
                lblError.Text = "Error Occured please Contact Administrator";
            }
            
        }
        protected void CreateAccount()
        {
            SqlConnection con = new SqlConnection(cs);
            try
            {
                SqlCommand cmd = new SqlCommand("sp_b_user", con);

                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                cmd.Parameters.AddWithValue("@qtype", "insertuser");
                cmd.Parameters.AddWithValue("@Name",txtusername.Text.Trim() );
                cmd.Parameters.AddWithValue("@Mobile", txtmobile.Text.Trim());
                cmd.Parameters.AddWithValue("@EmailId", txtemail.Text.Trim());
                cmd.Parameters.AddWithValue("@State", ddlstate.SelectedValue);
                cmd.Parameters.AddWithValue("@City", ddlcity.SelectedValue);
                cmd.Parameters.AddWithValue("@BranchorLocation", txtlocation.Text.Trim());
                cmd.Parameters.AddWithValue("@UserType", usertype);
                cmd.Parameters.AddWithValue("@UserName", txtusername.Text.Trim());
                cmd.Parameters.AddWithValue("@Password", txtmobile.Text.Trim());
                cmd.Parameters.AddWithValue("@BranchName", txtBranchName.Text.Trim());
                cmd.Parameters.AddWithValue("@Code", txtcode.Text.Trim());
                cmd.Parameters.AddWithValue("@Status",ddlStatus.SelectedValue);
                cmd.Parameters.AddWithValue("@CreatedBy", Session["UserName"].ToString());

                Session["Empname"] = txtusername.Text;
                Session["Emppwd"] = txtmobile.Text;
                Session["email"] = txtemail.Text;

                cmd.ExecuteNonQuery();

                string Uname = Session["Empname"].ToString();
                string pwd = Session["Emppwd"].ToString();
                string email = Session["email"].ToString();

              //  mail1(Uname, email, pwd);


            }
            catch (Exception ex)
            {
                Response.Write(ex);
                lblError.Text = "Error Occured, Please Contact Administrator";
                noError = false;
            }
            finally
            {
                con.Close();

               
            }
        }

        protected void ddlstate_SelectedIndexChanged(object sender, EventArgs e)
        {
            string State = ddlstate.SelectedValue;

            SqlConnection conn = new SqlConnection(cs);
            conn.Open();
            string com = "Select distinct city_name from cities  where city_state='" + State + "' order by city_name asc";
            SqlDataAdapter adpt = new SqlDataAdapter(com, conn);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            ddlcity.DataSource = dt;
            ddlcity.DataTextField = "city_name";
            ddlcity.DataValueField = "city_name";
            ddlcity.DataBind();
            ddlcity.Items.Insert(0, new ListItem("-- Select City --", ""));
        }
        public int mail1(string user, string mailpass, string pswd)
        {
            int i = 0;


            try
            {
                DateTime dte = DateTime.Today;
                var dt = dte.ToString("dd/MM/yyyy");
                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient();
                mail.From = new MailAddress("RaneAlert@brainmagic.info");
                //mail.To.Add("n.panneerselvam@ranegroup.com");
                //mail.To.Add("d.dhanunjayarao@ranegroup.com");
                //mail.Bcc.Add("gayathri@brainmagic.info");
                mail.To.Add(mailpass);
                mail.Subject = "Rane Complaint Tracking | WSD"; // Mail Subject
                string body;
                //body = "Dear " + user + ", <br/>";
                body = "Dear Sir ,";
                body += "<br/>Your Ticket Accepted Successfully";
                body += "<br/> Please find below Login details";
                body += "<br/>Your username - " + user + " , password -" + pswd;
                body += "<br/>Thanks & Regards,<br>Turbo Energy<br>https://www.turboenergy.co.in/";
                mail.Body = body;

                mail.IsBodyHtml = true;
                //System.Net.Mail.Attachment attachment;
                //attachment = new System.Net.Mail.Attachment(file); //Attaching File to Mail
                //mail.Attachments.Add(attachment);
                SmtpServer.Host = "webmail.brainmagic.info"; //PORT
                SmtpServer.Port = Convert.ToInt32(25); //PORT

                SmtpServer.DeliveryMethod = SmtpDeliveryMethod.Network;
                SmtpServer.UseDefaultCredentials = false;
                SmtpServer.Credentials = new NetworkCredential("RaneAlert@brainmagic.info", "lRvq230*");
                SmtpServer.Send(mail);
            }
            catch (Exception ex)
            {
                i = 0;
            }
            return i;


        }
    }
}