﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admins/AdminLayout.Master" AutoEventWireup="true" CodeBehind="ViewServiceEng.aspx.cs" Inherits="Rane_WSD.Admins.ViewServiceEng" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="page-title-box">
        <div class="container-fluid">
            <div class="page-title dflex-between-center">
                <h3 class="mb-1 font-weight-bold">View Service Engineer </h3>
                <ol class="breadcrumb mb-0 mt-1">
                    <li class="breadcrumb-item">
                        <a href="#">
                            <i class="bx bx-home fs-xs"></i>
                        </a>
                    </li>
                    <li class="breadcrumb-item active">View Service Engineer </li>
                </ol>
            </div>
        </div>
    </div>

    <!-- page content -->
    <div class="page-content-wrapper mt--45">
        <div class="container-fluid">

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-title"></h5>

                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-4">

                                    <div class="form-group">
                                        <label class="text-primary"><b>Name </b><span class="text-danger">:</span></label>
                                  <asp:Label ID="lblname" runat="server" Text=""></asp:Label>
                                      
                                    </div>
                                    <div class="form-group">
                                        <label class="text-primary"><b>Address</b><span class="text-danger">:</span></label>
                                       <asp:Label ID="lbladdress" runat="server" Text=""></asp:Label>
                                      
                                    </div>

                                   
                                     <div class="form-group">
                                        <label class="text-primary"><b>ContactNo </b> <span class="text-danger">:</span></label>
                                      <asp:Label ID="lblcontact" runat="server" Text=""></asp:Label>
                                      
                                    </div>
                                    <div class="form-group">
                                        <label class="text-primary"><b>AuthorizedServiceCenter </b> <span class="text-danger">:</span></label>
                                      <asp:Label ID="lblauthorizedServiceCenter" runat="server" Text=""></asp:Label>
                                      
                                    </div>
                                   
                                </div>
                                <!-- end col -->

                                <div class="col-lg-4">

                                    <div class="form-group">
                                        <label class="text-primary"><b>State </b><span class="text-danger">:</span></label>
                                        <asp:Label ID="lblstate" runat="server" Text=""></asp:Label>
                                       
                                       
                                    </div>
                                    
                                     <div class="form-group">
                                        <label class="text-primary"><b>Mobile No </b> <span class="text-danger">:</span></label>
                                      <asp:Label ID="lblmobile" runat="server" Text=""></asp:Label>
                                      
                                    </div>
                                     <div class="form-group">
                                        <label class="text-primary"><b>SE Code </b> <span class="text-danger">:</span></label>
                                      <asp:Label ID="lblsecode" runat="server" Text=""></asp:Label>
                                      
                                    </div>
                                   
                                     <div class="form-group">
                                        <label class="text-primary"><b>Reg Code</b> <span class="text-danger">:</span></label>
                                      <asp:Label ID="lblDestination" runat="server" Text=""></asp:Label>
                                        
                                    </div>
                                </div>
                                <!-- end col -->

                                <div class="col-lg-4">

                                    <div class="form-group">
                                        <label class="text-primary"><b>Email ID</b> <span class="text-danger">:</span></label>
                                      <asp:Label ID="lblemail" runat="server" Text=""></asp:Label>
                                        
                                    </div>
                                    <div class="form-group">
                                        <label class="text-primary"><b>Region</b> <span class="text-danger">:</span></label>
                                      <asp:Label ID="Region" runat="server" Text=""></asp:Label>
                                        
                                    </div>
                                     
                                    <div class="form-group">
                                        <label class="text-primary"><b>Flag </b> <span class="text-danger">:</span></label>
                                      <asp:Label ID="lblflag" runat="server" Text=""></asp:Label>
                                      
                                    </div>
                                   
                                   
                                </div>
                                <div class="pl-3">
                                    
                                    <a href="listServiceEngineer.aspx" class=" btn btn-dark btn-sm" >Back</a>
                                </div>
                                <!-- end col -->
                            </div>
                            <!-- end row-->

                        </div>
                        <!-- end card-body -->
                    </div>
                    <!-- end card -->
                </div>
                <!-- end col -->
            </div>
        </div>
    </div>
    <!-- end row -->
</asp:Content>
