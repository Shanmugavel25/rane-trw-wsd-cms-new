﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Rane_WSD.Admins
{
    public partial class FIRView : System.Web.UI.Page
    {
        string cs = ConfigurationManager.ConnectionStrings["conn"].ConnectionString;
        public string pdfName = "";
        public string pdfName1 = "";
        public string pdfName2 = "";

        public string pdfName3 = "";
        public bool noError = true;

        protected void Page_Load(object sender, EventArgs e)
        {
         if (!IsPostBack)
            {
                pnlPDF.Visible = false;
                pnlPDF1.Visible = false;
                pnlPDF2.Visible = false;
                pnlPDF3.Visible = false;

                string editacc = MyCrypto.GetDecryptedQueryString(Request.QueryString["Id"].ToString());

                SqlConnection con = new SqlConnection(cs);
                SqlCommand cmd = new SqlCommand("sp_b_AdminApproved", con);

                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                cmd.Parameters.AddWithValue("@Id", editacc);
                cmd.Parameters.AddWithValue("@qtype", "ViewFIR");
                SqlDataReader sdr = cmd.ExecuteReader();
                if (sdr.Read())
                {
                    lblFirno.Text = sdr["FIRNo"].ToString();
                    lblAggregateSerialNo.Text = sdr["AggregateSerialNo"].ToString();
                    lblDateofComplaint.Text = String.Format("{0:dd MMM yyyy }", sdr["DateofComplaint"]);
                    lblInvoiceDate.Text = String.Format("{0:dd MMM yyyy }", sdr["InvoiceDate"]);
                    lblInvoiceno.Text = sdr["WSDInvoiceNo"].ToString();
                    lblpartno.Text = sdr["PartNo"].ToString();
                    lblparttype.Text = sdr["PartType"].ToString();
                    lblQuantity.Text = sdr["Quantity"].ToString();
                    lblretailerlocation.Text = sdr["RetailerLocation"].ToString();
                    lblretailername.Text = sdr["RetailerName"].ToString();
                    //lblStatus.Text = sdr["StatusName"].ToString();
                    //Session["Filename"] = sdr["UploadFile"].ToString();
                    lblWSDBranch.Text = sdr["WSDBranch"].ToString();
                    lblWSDName.Text = sdr["WSDName"].ToString();
                    lblUploadfile.Text = sdr["UploadFile"].ToString();

                    string User = sdr["UploadFile"].ToString();
                    if (!string.IsNullOrEmpty(User))
                    {
                        if (User.Split(',').Count() > 0)
                        {
                            string[] path = User.Split(',');
                            for (int i = 0; i < path.Count(); i++)
                            {
                                if (path[i].ToString() == "")
                                {

                                }
                                else
                                {
                                    if (i == 0)
                                    {
                                        pnlPDF.Visible = true;
                                        pdfName = path[i].ToString();
                                        Label2.Text = path[i].ToString();
                                    }
                                    else if (i == 1)
                                    {
                                        pnlPDF1.Visible = true;
                                        pdfName1 = path[i].ToString();
                                        Label3.Text = path[i].ToString();
                                    }
                                    else if (i == 2)
                                    {
                                        pnlPDF2.Visible = true;
                                        pdfName2 = path[i].ToString();
                                        Label4.Text = path[i].ToString();
                                    }
                                    else if (i == 3)
                                    {
                                        pnlPDF3.Visible = true;
                                        pdfName3 = path[i].ToString();
                                        Label5.Text = path[i].ToString();
                                    }

                                }




                                //ddl_user.Items.Add(ct);
                            }
                        }
                    }
                    

                }
                con.Close();
            }
           

            
        }

    }
}