﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admins/AdminLayout.Master" AutoEventWireup="true" CodeBehind="ViewAccount.aspx.cs" Inherits="Rane_WSD.ViewAccount" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div class="page-title-box">
        <div class="container-fluid">
            <div class="page-title dflex-between-center">
                <h3 class="mb-1 font-weight-bold">View Account </h3>
                <ol class="breadcrumb mb-0 mt-1">
                    <li class="breadcrumb-item">
                        <a href="#">
                            <i class="bx bx-home fs-xs"></i>
                        </a>
                    </li>
                    <li class="breadcrumb-item active">View Account </li>
                </ol>
            </div>
        </div>
    </div>

    <!-- page content -->
    <div class="page-content-wrapper mt--45">
        <div class="container-fluid">

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-title"></h5>

                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-4">

                                    <div class="form-group">
                                        <label class="text-primary"><b>Name </b><span class="text-danger">:</span></label>
                                  <asp:Label ID="lblname" runat="server" Text=""></asp:Label>
                                       <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ForeColor="Red" ControlToValidate="txtusername" runat="server" ErrorMessage="Enter Username"></asp:RequiredFieldValidator>--%>
                                    </div>
                                    <div class="form-group">
                                        <label class="text-primary"><b>Email ID</b><span class="text-danger">:</span></label>
                                       <asp:Label ID="lblemail" runat="server" Text=""></asp:Label>
                                       <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ForeColor="Red" ControlToValidate="txtemail" runat="server" ErrorMessage="Enter Email"></asp:RequiredFieldValidator>--%>
                                    </div>

                                    <div class="form-group">
                                        <label class="text-primary"><b>Mobile No</b> <span class="text-danger">:</span></label>
                                      <asp:Label ID="lblmobile" runat="server" Text=""></asp:Label>
                                       <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ForeColor="Red" ControlToValidate="txtmobile" runat="server" ErrorMessage="Enter Mobile"></asp:RequiredFieldValidator>--%>
                                    </div>
                                    <div class="form-group">
                                        <label class="text-primary"><b>Code</b> <span class="text-danger">:</span></label>
                                      <asp:Label ID="lblcode" runat="server" Text=""></asp:Label>
                                       <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ForeColor="Red" ControlToValidate="txtmobile" runat="server" ErrorMessage="Enter Mobile"></asp:RequiredFieldValidator>--%>
                                    </div>
                                </div>
                                <!-- end col -->

                                <div class="col-lg-4">

                                    <div class="form-group">
                                        <label class="text-primary"><b>State </b><span class="text-danger">:</span></label>
                                        <asp:Label ID="lblstate" runat="server" Text=""></asp:Label>
                                       
                                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator4" ForeColor="Red" ControlToValidate="ddlstate" runat="server" ErrorMessage="Choose State"></asp:RequiredFieldValidator>--%>
                                    </div>
                                    <div class="form-group">
                                        <label class="text-primary"><b>City </b><span class="text-danger">:</span></label>
                                      <asp:Label ID="lblcity" runat="server" Text=""></asp:Label>  
                                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator5" ForeColor="Red" runat="server" ControlToValidate="ddlcity" ErrorMessage="Enter City"></asp:RequiredFieldValidator>--%>
                                    </div>
                                    <div class="form-group">
                                        <label class="text-primary"><b> Location </b><span class="text-danger">:</span></label>
                                      <asp:Label ID="lbllocation" runat="server" Text=""></asp:Label> 
                                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator6" ForeColor="Red" runat="server" ControlToValidate="txtlocation" ErrorMessage="Enter Branch or Location"></asp:RequiredFieldValidator>--%>
                                    </div>
                                </div>
                                <!-- end col -->

                                <div class="col-lg-4">

                                    <div class="form-group">
                                        <label class="text-primary"><b>UserType</b> <span class="text-danger">:</span></label>
                                      <asp:Label ID="lblusertype" runat="server" Text=""></asp:Label>
                                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator7" ForeColor="Red" ControlToValidate="ddlUsertype" runat="server" ErrorMessage="Choose Usertype"></asp:RequiredFieldValidator>--%>
                                    </div>
                                    <div class="form-group">
                                        <label class="text-primary"><b>BranchName :</b></label>
                                       <asp:Label ID="lblbranch" runat="server" Text=""></asp:Label>
                                    </div>
                                    <div class="form-group">
                                        <label class="text-primary"><b>Status :</b></label>
                                       <asp:Label ID="lblstatus" runat="server" Text=""></asp:Label>
                                    </div>
                                </div>
                                <div class="pl-3">
                                    <%--<asp:Button ID="btnsubmit" runat="server" OnClick="btnsubmit_Click" CssClass="btn btn-primary" Text="Submit" />--%>
                                    <a href="listaccount.aspx?usertype=<%Response.Write(usertype); %>" class=" btn btn-dark btn-sm" >Back</a>
                                </div>
                                <!-- end col -->
                            </div>
                            <!-- end row-->

                        </div>
                        <!-- end card-body -->
                    </div>
                    <!-- end card -->
                </div>
                <!-- end col -->
            </div>
        </div>
    </div>
    <!-- end row -->
</asp:Content>
