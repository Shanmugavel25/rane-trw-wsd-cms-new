﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Rane_WSD
{
    public partial class AdminLayout : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (Session["UserName"] == null)
            {
                Response.Redirect("~/login.aspx");
            }
            txtusername.Text = Session["UserName"].ToString();
        }
    }
}