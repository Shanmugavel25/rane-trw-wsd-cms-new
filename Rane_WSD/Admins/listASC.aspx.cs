﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Rane_WSD.Admins
{
    public partial class listASC : System.Web.UI.Page
    {
        string cs = ConfigurationManager.ConnectionStrings["conn"].ConnectionString;
        public common db = new common();
        public bool noError = true;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                dataget();
                GetData();
            }
        }
        protected void GetData()
        {
            SqlConnection conn = new SqlConnection(cs);
            conn.Open();
            string com = "Select distinct [serviceEng] from [Authorized] where [serviceEng]!=''";
            SqlDataAdapter adpt = new SqlDataAdapter(com, conn);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            ddlsecode.DataSource = dt;
            ddlsecode.DataTextField = "serviceEng";
            ddlsecode.DataValueField = "serviceEng";
            ddlsecode.DataBind();
            ddlsecode.Items.Insert(0, new ListItem("-- Select Code --", ""));
            //ddlUsertype.SelectedValue = usertype;
            // ddlUsertype.Enabled = false;
        }
        protected void dataget()
        {
            SqlConnection con = new SqlConnection(cs);
            SqlCommand cmd = new SqlCommand("sp_b_authorized", con);
            cmd.CommandType = CommandType.StoredProcedure;
            con.Open();
            cmd.Parameters.AddWithValue("@qtype", "Selectlist");
            cmd.Parameters.AddWithValue("@Name", txtName.Text.Trim());
            cmd.Parameters.AddWithValue("@InchargeName", txtInchargeName.Text.Trim());
            cmd.Parameters.AddWithValue("@searchcode", ddlsecode.SelectedValue);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable ds = new DataTable();
            sda.Fill(ds);
            ListView1.DataSource = ds;
            ListView1.DataBind();
            con.Close();
        }
        public string Id;
        protected void ListView1_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            if (e.CommandName == "delete")
            {
                Id = e.CommandArgument.ToString();
                deleteFn(Id);
                if (noError)
                {
                    Response.Redirect("listASC.aspx");
                }
                else
                {
                   
                    //lblError.Text = "Error Occured please Contact Administrator";
                    noError = false;
                }
            }

            
        }

        protected void ListView1_ItemDeleting(object sender, ListViewDeleteEventArgs e)
        {
        }
        protected void OnPagePropertiesChanging(object sender, PagePropertiesChangingEventArgs e)
        {
            (ListView1.FindControl("dataPagerNumeric") as DataPager).SetPageProperties(e.StartRowIndex, e.MaximumRows, false);
            this.dataget(); ;
        }
        protected string deleteFn(string Id)
        {
            SqlConnection con = new SqlConnection(cs);
            string rStatus = "";
            try
            {

                SqlCommand cmd = new SqlCommand("sp_b_user", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                cmd.Parameters.AddWithValue("@Id", Id);
                cmd.Parameters.AddWithValue("@qtype", "deleteuser");
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Response.Write(ex);
            }
            finally
            {
                con.Close();
               
            }
            return rStatus;
        }


        protected void btn_submit_Click(object sender, EventArgs e)
        {
            dataget();
        }
        
    }
}