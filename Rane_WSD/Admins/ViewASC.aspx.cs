﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Rane_WSD.Admins
{
    public partial class ViewASC : System.Web.UI.Page
    {
        string cs = ConfigurationManager.ConnectionStrings["conn"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string editacc = MyCrypto.GetDecryptedQueryString(Request.QueryString["ASCID"].ToString());

                SqlConnection con = new SqlConnection(cs);
                SqlCommand cmd = new SqlCommand("sp_b_authorized", con);

                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                cmd.Parameters.AddWithValue("@ASCID", editacc);
                cmd.Parameters.AddWithValue("@qtype", "viewasc");
                SqlDataReader sdr = cmd.ExecuteReader();
                if (sdr.Read())
                {
                    lblname.Text = sdr["Name"].ToString();
                    lbladdress.Text = sdr["Address"].ToString();
                    lblinchargename.Text = sdr["InchargeName"].ToString();
                    lblcontact.Text = sdr["contactNo"].ToString();
                    lblmobile.Text = sdr["mobileNo"].ToString();
                    lblemail.Text = sdr["emailId"].ToString();
                    lblstate.Text = sdr["State"].ToString();
                    lblplace.Text = sdr["Place"].ToString();
                    Region.Text = sdr["Region"].ToString();
                    lblcssino.Text = sdr["CsiNo"].ToString();
                    lblgstno.Text = sdr["GSTNo"].ToString();
                    lbltinno.Text = sdr["TinNo"].ToString();
                    lblCertificateExpirydate.Text = String.Format("{0:dd MMM yyyy }", sdr["CertificateExpirydate"]);
                    lblReminderRenewalDate.Text = String.Format("{0:dd MMM yyyy }", sdr["ReminderRenewalDate"]); 
                    lblserviceEng.Text = sdr["serviceEng"].ToString();
                    lblflag.Text = sdr["flag"].ToString();
                    lblDestination.Text = sdr["Destination"].ToString();
                }
                con.Close();
            }
        }
    }
}