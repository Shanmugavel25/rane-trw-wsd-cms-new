﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admins/AdminLayout.Master" AutoEventWireup="true" CodeBehind="dashboard.aspx.cs" Inherits="Rane_WSD.dashboard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
      <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
    <script>
        window.onload = function () {

            var chart = new CanvasJS.Chart("chartContainer", {
                animationEnabled: true,
                theme: "dark",
                backgroundColor: "white",
                title: {
                    //text: "Total Registered Users",
                    fontcolor:"black"
                    
                },
                axisY: {
                    //title: "Registered Users",
                    fontcolor:"black"

                },
                Lengend:
        {
            fontcolor:"black"
        },
                data: [{
                    type: "pie",
                    indexLabelFontSize: 10,
                    //showInLegend: true, 
                    //legendMarkerColor: "grey",
                    //legendText: "Registered Users Status",
                    fontColor: "White",
                    dataPoints: <%= chart %> 
                    //    [
                    //    { y: 450 },
                    //    { y: 414 },
                    //    { y: 520, indexLabel: "\u2191 highest", markerColor: "red", markerType: "triangle" },
                    //    { y: 460 },
                    //    { y: 450 },
                    //    { y: 500 },
                    //    { y: 480 },
                    //    { y: 480 },
                    //    { y: 410, indexLabel: "\u2193 lowest", markerColor: "DarkSlateGrey", markerType: "cross" },
                    //    { y: 500 },
                    //    { y: 480 },
                    //    { y: 510 }
                    //]
                    }]
            });
            chart.render();

        }
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   <div class="page-title-box">
        <div class="container-fluid">
            <div class="page-title dflex-between-center">
                <h3 class="mb-1 font-weight-bold">Dashboard</h3>
                <ol class="breadcrumb mb-0 mt-1">
                    <li class="breadcrumb-item">
                        <a href="#">
                            <i class="bx bx-home fs-xs"></i>
                        </a>
                    </li>
                    <li class="breadcrumb-item active">Dashboard</li>
                </ol>
            </div>
        </div>
    </div>
    <div class="page-content-wrapper mt--45">
        <div class="container-fluid">
            <!-- Widget  -->
            <div class="row">
                <div class="col-md-6 col-xl-3">
                    <div class="card">
                        <div class="card-body">
                            <div class="media align-items-center">
                                <div class="media-body">
                                    <span class="text-muted text-uppercase font-size-12 font-weight-bold">New Complaint</span>
                                    <br />
                                    <asp:Label runat="server" class="mb-0 mt-1" style="font-size:25px"  ID="lblNewComplaint"></asp:Label>
                                </div>
                                <div class="text-center">
                                    <div id="t-rev"></div>
                                    <span class="text-success font-weight-bold font-size-13">
                                        <i class="bx bx-up-arrow-alt"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-xl-3">
                    <div class="card">
                        <div class="card-body">
                            <div class="media align-items-center">
                                <div class="media-body">
                                    <span class="text-muted text-uppercase font-size-12 font-weight-bold">Pending For Admin Approval</span>
                                    <br />
                                    <asp:Label runat="server" class="mb-0 mt-1" style="font-size:25px"  ID="lblAdminApproval"></asp:Label>
                                </div>
                                <div class="text-center">
                                    <div id="t-order"></div>
                                    <span class="text-danger font-weight-bold font-size-13">
                                        <i class="bx bx-down-arrow-alt"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-xl-3">
                    <div class="card">
                        <div class="card-body">
                            <div class="media align-items-center">
                                <div class="media-body">
                                    <span class="text-muted text-uppercase font-size-12 font-weight-bold">Credit Note Updated</span>
                                    <br />
                                    <asp:Label runat="server" class="mb-0 mt-1" style="font-size:25px"  ID="lblCreditNote"></asp:Label>
                                </div>
                                <div class="text-center">
                                    <div id="t-user"></div>
                                    <span class="text-success font-weight-bold font-size-13">
                                        <i class="bx bx-up-arrow-alt"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-xl-3">
                    <div class="card">
                        <div class="card-body">
                            <div class="media align-items-center">
                                <div class="media-body">
                                    <span class="text-muted text-uppercase font-size-12 font-weight-bold">Completed Complaint</span>
                                    <br />
                                    <asp:Label runat="server" class="mb-0 mt-1" style="font-size:25px"  ID="lblComplaintCompleted"></asp:Label>
                                </div>
                                <div class="text-center">
                                    <div id="t-visitor"></div>
                                    <span class="text-danger font-weight-bold font-size-13">
                                        <i class="bx bx-down-arrow-alt"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Row 2-->
            <div class="row align-items-stretch">
                <div class="col-md-4 col-lg-3">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-title">Service Engineer</h5>
                        </div>
                        <div class="card-body p-0">
                            <ul class="list-group list-group-flush">
                                <li class="list-group-item py-4">
                                    <div class="media">
                                        <div class="media-body">
                                            <p class="text-muted mb-2">Service Engineer Approved Complaint</p>
                                            <asp:Label runat="server" class="mb-0 mt-1" style="font-size:25px"  ID="lblServiceApproved"></asp:Label>
                                        </div>
                                        <div class="avatar avatar-md bg-info mr-0 align-self-center">
                                            <i class="bx bx-layer fs-lg"></i>
                                        </div>
                                    </div>
                                </li>
                                <li class="list-group-item py-4">
                                    <div class="media">
                                        <div class="media-body">
                                            <p class="text-muted mb-2">Service Engineer Rejected Complaint </p>
                                            <asp:Label runat="server" class="mb-0 mt-1" style="font-size:25px"  ID="lblServiceRejected"></asp:Label>
                                        </div>
                                        <div class="avatar avatar-md bg-primary mr-0 align-self-center">
                                            <i class="bx bx-bar-chart-alt fs-lg"></i>
                                        </div>
                                    </div>
                                </li>

                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-lg-3">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">Warehouse</h5>
                    </div>
                    <div class="card-body p-0">
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item py-4">
                                <div class="media">
                                    <div class="media-body">
                                        <p class="text-muted mb-2">WSD Dispatch to Warehouse</p>
                                         <asp:Label runat="server" class="mb-0 mt-1" style="font-size:25px" ID="lblDispatchWarehouse"></asp:Label>
                                    </div>
                                    <div class="avatar avatar-md bg-info mr-0 align-self-center">
                                        <i class="bx bx-layer fs-lg"></i>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item py-4">
                                <div class="media">
                                    <div class="media-body">
                                        <p class="text-muted mb-2">Received From Warehouse</p>
                                        <asp:Label runat="server" class="mb-0 mt-1" style="font-size:25px" ID="lblReceivedWarehouse"></asp:Label>
                                    </div>
                                    <div class="avatar avatar-md bg-success mr-0 align-self-center">
                                        <i class="bx bx-bar-chart-alt fs-lg"></i>
                                    </div>
                                </div>
                            </li>

                        </ul>
                    </div>
                </div>
            </div>
                <div class="col-md-6 col-lg-6">
                    
            <div class="card">
               
                    
             <%--<div id="chartContainer"></div>--%>
                   
            </div>
            <!-- End total revenue chart -->
        </div>
            </div>


            
        </div>

        <!-- Begin total revenue chart -->
        
    </div>
   
                  <!-- Row 3-->

</asp:Content>
