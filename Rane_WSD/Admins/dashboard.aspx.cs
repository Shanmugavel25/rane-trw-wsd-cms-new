﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Rane_WSD
{
    public partial class dashboard : System.Web.UI.Page
    {
        string cs = ConfigurationManager.ConnectionStrings["conn"].ConnectionString;
        public string chart = "";
        public string chartproduct = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Binddata1();
                Binddata2();
                Binddata3();
                Binddata4();
                Binddata5();
                Binddata6();
                Binddata7();
                Binddata8();
                BindValue();

            }
        }
        protected void Binddata1()
        {
            SqlConnection conn = new SqlConnection(cs);
            SqlCommand Cmd = new SqlCommand();
            conn.Open();
            Cmd = new SqlCommand(" select count(*) from tbl_FIRDetails where Status=1", conn);
            Int32 count = (Int32)Cmd.ExecuteScalar();

            conn.Close();

            lblNewComplaint.Text = count.ToString();
        }
        protected void Binddata2()
        {
            SqlConnection conn = new SqlConnection(cs);
            SqlCommand Cmd = new SqlCommand();
            conn.Open();
            Cmd = new SqlCommand(" select count(*) from tbl_FIRDetails where Status=4", conn);
            Int32 count = (Int32)Cmd.ExecuteScalar();

            conn.Close();

            lblAdminApproval.Text = count.ToString();
        }
        protected void Binddata3()
        {
            SqlConnection conn = new SqlConnection(cs);
            SqlCommand Cmd = new SqlCommand();
            conn.Open();
            Cmd = new SqlCommand(" select count(*) from tbl_FIRDetails where Status=14", conn);
            Int32 count = (Int32)Cmd.ExecuteScalar();

            conn.Close();

            lblCreditNote.Text = count.ToString();

        }
        protected void Binddata4()
        {
            SqlConnection conn = new SqlConnection(cs);
            SqlCommand Cmd = new SqlCommand();
            conn.Open();
            Cmd = new SqlCommand(" select count(*) from tbl_FIRDetails where Status=15", conn);
            Int32 count = (Int32)Cmd.ExecuteScalar();

            conn.Close();

            lblComplaintCompleted.Text = count.ToString();
        }
        protected void Binddata5()
        {
            SqlConnection conn = new SqlConnection(cs);
            SqlCommand Cmd = new SqlCommand();
            conn.Open();
            Cmd = new SqlCommand(" select count(*) from tbl_FIRDetails where Status=9", conn);
            Int32 count = (Int32)Cmd.ExecuteScalar();

            conn.Close();

            lblServiceApproved.Text = count.ToString();

        }
        protected void Binddata6()
        {
            SqlConnection conn = new SqlConnection(cs);
            SqlCommand Cmd = new SqlCommand();
            conn.Open();
            Cmd = new SqlCommand(" select count(*) from tbl_FIRDetails where Status=10", conn);
            Int32 count = (Int32)Cmd.ExecuteScalar();

            conn.Close();

            lblServiceRejected.Text = count.ToString();
        }
        protected void Binddata7()
        {
            SqlConnection conn = new SqlConnection(cs);
            SqlCommand Cmd = new SqlCommand();
            conn.Open();
            Cmd = new SqlCommand(" select count(*) from tbl_FIRDetails where Status=12", conn);
            Int32 count = (Int32)Cmd.ExecuteScalar();

            conn.Close();

            lblDispatchWarehouse.Text = count.ToString();
        }
        protected void Binddata8()
        {
            SqlConnection conn = new SqlConnection(cs);
            SqlCommand Cmd = new SqlCommand();
            conn.Open();
            Cmd = new SqlCommand(" select count(*) from tbl_FIRDetails where Status=13", conn);
            Int32 count = (Int32)Cmd.ExecuteScalar();

            conn.Close();

            lblReceivedWarehouse.Text = count.ToString();
        }
        protected void BindValue()
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand("select (Select FIRStatus from tbl_FIRstatus b where b.id=a.Status) as StatusName,count(Id) as Total from tbl_FIRDetails a Group by Status", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            dt.Columns.ToString();

            string[] x = new string[dt.Rows.Count];
            int[] y = new int[dt.Rows.Count];


            chart = "[";
            for (int i = 0; i < dt.Rows.Count; i++)
            {

                x[i] = dt.Rows[i][0].ToString();
                // Console.WriteLine(dt.Rows[i].ItemArray[i] + "");
                y[i] = Convert.ToInt32(dt.Rows[i][1]);

                chart += "{ y: " + y[i] + ", label : '" + x[i] + "'},";

                //chartContainer += string.Concat(x, y);

                //.Points.DataBindXY(x, y);

                //Chart1.Series[0].Points.DataBindXY(x, y);
                //Chart1.Series[0].ChartType = SeriesChartType.Pie;
                //Chart1.Series["Series1"].Label = "#PERCENT{P2}";
                //Chart1.Series["Series1"].LegendText = "#VALX";
                //Chart1.Legends[0].LegendStyle = LegendStyle.Column;
                //Chart1.Legends[0].Docking = Docking.Right;
                //Chart1.Legends[0].Alignment = System.Drawing.StringAlignment.Center;

                //Chart1.ChartAreas[0].AxisX.Title = "Registered Details";
                //Chart1.ChartAreas[0].AxisY.Title = "User Status";
                //Chart1.ChartAreas["ChartArea1"].AxisX.IsLabelAutoFit = true;

                //Chart1.ChartAreas["ChartArea1"].AxisX.Interval = 1;
                //Chart1.ChartAreas["ChartArea1"].AxisY.IsLabelAutoFit = true;

                //Chart1.ChartAreas["ChartArea1"].AxisY.Interval = 1;
                //Chart1.ChartAreas[0].AxisX.LabelStyle.Format = "Itemname";
            }
            chart = chart.TrimEnd(',') + "]";
        }
       
       
    }
}