﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Rane_WSD
{
    public partial class ViewAccount : System.Web.UI.Page
    {
        string cs = ConfigurationManager.ConnectionStrings["conn"].ConnectionString;
        public string usertype = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            usertype = Request.QueryString["usertype"].ToString();
            if (!IsPostBack)
            {
                string editacc = MyCrypto.GetDecryptedQueryString(Request.QueryString["Id"].ToString());

                SqlConnection con = new SqlConnection(cs);
                SqlCommand cmd = new SqlCommand("sp_b_user", con);

                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                cmd.Parameters.AddWithValue("@Id", editacc);
                cmd.Parameters.AddWithValue("@qtype", "viewuser");
                SqlDataReader sdr = cmd.ExecuteReader();
                if (sdr.Read())
                {
                    lblname.Text = sdr["Name"].ToString();
                    lblemail.Text = sdr["EmailId"].ToString();
                    lblmobile.Text = sdr["Mobile"].ToString();
                    lblstate.Text = sdr["State"].ToString();
                    lbllocation.Text = sdr["BranchorLocation"].ToString();
                    lblusertype.Text= sdr["UserType"].ToString();
                    lblstatus.Text = sdr["Status"].ToString();
                    lblcity.Text = sdr["City"].ToString();
                    lblcode.Text = sdr["Code"].ToString();
                    lblbranch.Text = sdr["BranchName"].ToString();

                }
                con.Close();
            }
        }
    }
}