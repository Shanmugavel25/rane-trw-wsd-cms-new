﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Rane_WSD.Admins
{
    public partial class ViewServiceEng : System.Web.UI.Page
    {
        string cs = ConfigurationManager.ConnectionStrings["conn"].ConnectionString;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string editacc = MyCrypto.GetDecryptedQueryString(Request.QueryString["SEID"].ToString());

                SqlConnection con = new SqlConnection(cs);
                SqlCommand cmd = new SqlCommand("[sp_b_servieceng]", con);

                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                cmd.Parameters.AddWithValue("@SEID", editacc);
                cmd.Parameters.AddWithValue("@qtype", "viewService");
                SqlDataReader sdr = cmd.ExecuteReader();
                if (sdr.Read())
                {
                    lblname.Text = sdr["Name"].ToString();
                    lblsecode.Text = sdr["secode"].ToString();
                    lbladdress.Text = sdr["Address"].ToString();
                    lblcontact.Text = sdr["contactNo"].ToString();
                    lblmobile.Text = sdr["mobileNo"].ToString();
                    lblemail.Text = sdr["emailId"].ToString();
                    lblstate.Text = sdr["state"].ToString();
                    lblauthorizedServiceCenter.Text= sdr["authorizedServiceCenter"].ToString();
                    Region.Text = sdr["region"].ToString();
                    lblDestination.Text = sdr["regcode"].ToString();
                    lblflag.Text = sdr["flag"].ToString();
                    
                }
                con.Close();
            }
        }
    }
}