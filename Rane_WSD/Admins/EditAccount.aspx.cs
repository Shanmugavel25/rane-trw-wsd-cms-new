﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Rane_WSD
{
    public partial class EditAccount : System.Web.UI.Page
    {
        string cs = ConfigurationManager.ConnectionStrings["conn"].ConnectionString;
       public string usertype = "";
       public bool noError = true;

        protected void Page_Load(object sender, EventArgs e)
        {
            usertype = Request.QueryString["usertype"].ToString();
            if (!IsPostBack)
            {
                GetState();
                string editacc = MyCrypto.GetDecryptedQueryString(Request.QueryString["Id"].ToString());

                SqlConnection con = new SqlConnection(cs);
                SqlCommand cmd = new SqlCommand("sp_b_user", con);

                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                cmd.Parameters.AddWithValue("@Id", editacc);
                cmd.Parameters.AddWithValue("@qtype", "updateaccount"); 
                SqlDataReader sdr = cmd.ExecuteReader();
                if (sdr.Read())
                {
                    txtusername.Text = sdr["Name"].ToString();
                    txtemail.Text = sdr["EmailId"].ToString();
                    txtmobile.Text = sdr["Mobile"].ToString();
                    txtcode.Text=sdr["Code"].ToString();
                    txtBranchName.Text = sdr["BranchName"].ToString();
                    ddlstate.SelectedValue = sdr["State"].ToString();
                    txtlocation.Text = sdr["BranchorLocation"].ToString();
                    ddlUsertype.SelectedValue = sdr["UserType"].ToString();
                    ddlStatus.SelectedValue = sdr["Status"].ToString();

                    SqlConnection conn = new SqlConnection(cs);
                    conn.Open();
                    string com = "Select distinct city_id,city_name from cities  where city_state='" + sdr["State"].ToString() + "' order by city_name asc";
                    SqlDataAdapter adpt = new SqlDataAdapter(com, conn);
                    DataTable dt = new DataTable();
                    adpt.Fill(dt);
                    ddlcity.DataSource = dt;
                    ddlcity.DataTextField = "city_name";
                    ddlcity.DataValueField = "city_name";
                    ddlcity.DataBind();
                    ddlcity.Items.Insert(0, new ListItem("-- Select City --", ""));

                    ddlcity.SelectedValue = sdr["City"].ToString();

                }
                con.Close();
            }
            if (!IsPostBack)
            {
                GetData();
            }
        }
        protected void GetData()
        {
            SqlConnection conn = new SqlConnection(cs);
            conn.Open();
            string com = "Select distinct UserType,ShortName from [tbl_usertype] order by UserType asc";
            SqlDataAdapter adpt = new SqlDataAdapter(com, conn);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            ddlUsertype.DataSource = dt;
            ddlUsertype.DataTextField = "UserType";
            ddlUsertype.DataValueField = "ShortName";
            ddlUsertype.DataBind();
            ddlUsertype.Items.Insert(0, new ListItem("-- Select UserType --", ""));
        }
        protected void GetState()
        {
            SqlConnection conn = new SqlConnection(cs);
            conn.Open();
            string com = "Select distinct id,state from tbl_state order by state asc";
            SqlDataAdapter adpt = new SqlDataAdapter(com, conn);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            ddlstate.DataSource = dt;
            ddlstate.DataTextField = "state";
            ddlstate.DataValueField = "state";
            ddlstate.DataBind();
            ddlstate.Items.Insert(0, new ListItem("-- Select State --", ""));
        }
        protected void btnsubmit_Click(object sender, EventArgs e)
        {

            UpdateAccount();

            if (noError)
            {
                Response.Write("<script language='javascript'>alert('Edit Account Successfully');window.location=('listaccount.aspx?usertype=" + usertype + "')</script>");
            }
            else
            {
                //pnlerror.Visible = true;
                lblError.Text = "Error Occured please Contact Administrator";
            }
        }
        protected void UpdateAccount()
        {
            string editacc = MyCrypto.GetDecryptedQueryString(Request.QueryString["Id"].ToString());

            SqlConnection con = new SqlConnection(cs);
            try
            {
                SqlCommand cmd = new SqlCommand("sp_b_user", con);

                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                cmd.Parameters.AddWithValue("@Id", editacc);
                cmd.Parameters.AddWithValue("@qtype", "updateuser");
                cmd.Parameters.AddWithValue("@Name", txtusername.Text.Trim());
                cmd.Parameters.AddWithValue("@Mobile", txtmobile.Text.Trim());
                cmd.Parameters.AddWithValue("@EmailId", txtemail.Text.Trim());
                cmd.Parameters.AddWithValue("@State", ddlstate.SelectedValue);
                cmd.Parameters.AddWithValue("@City", ddlcity.SelectedValue);
                cmd.Parameters.AddWithValue("@BranchorLocation", txtlocation.Text.Trim());
                cmd.Parameters.AddWithValue("@UserType", ddlUsertype.SelectedValue);
                cmd.Parameters.AddWithValue("@Status", ddlStatus.SelectedValue);
                cmd.Parameters.AddWithValue("@UpdatedBy", Session["UserName"].ToString());

                cmd.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                Response.Write(ex);
                lblError.Text = "Error Occured, Please Contact Administrator";
                noError = false;
            }
            finally
            {
                con.Close();
                
                
            }
        }
        protected void ddlstate_SelectedIndexChanged(object sender, EventArgs e)
        {
            string State = ddlstate.SelectedValue;

            SqlConnection conn = new SqlConnection(cs);
            conn.Open();
            string com = "Select distinct city_name from cities  where city_state='" + State + "' order by city_name asc";
            SqlDataAdapter adpt = new SqlDataAdapter(com, conn);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            ddlcity.DataSource = dt;
            ddlcity.DataTextField = "city_name";
            ddlcity.DataValueField = "city_name";
            ddlcity.DataBind();
            ddlcity.Items.Insert(0, new ListItem("-- Select City --", ""));
        }
    }
}