﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="login.aspx.cs" Inherits="Rane_WSD.login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title>Login  | Rane TRW WSD</title>
    <link href="assets/css/icons.min.css" rel="stylesheet" />
    <link href="assets/libs/wave-effect/css/waves.min.css" rel="stylesheet" />
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="assets/css/styles.min.css" rel="stylesheet" />
<%--       <style>
            .btn-primary{
            background-color:#09294e !important;
        }
        .topbar-header{
            background-color:#09294e !important;
        }
        .page-title-box {
            background-color:#09294e !important;
        }
        .side-navbar .metismenu>li>a.active {
    border-left: 4px solid #09294e;
    color: #09294e !important;
    background-color: #f7f7ff;
    font-weight:700;
}
    </style>--%>
</head>
<body>
    <form id="form1" runat="server">
        <header id="page-topbar" class="topbar-header">
         <div class="navbar-header">
            <div class="left-bar">
               <div class="navbar-brand-box">
                  <a href="index.html" class="logo logo-dark">
                     <span class="logo-sm"><img src="../assets/images/logo.jpg" alt="Lettstart Admin"/></span>
                     <span class="logo-lg"><img src="../assets/images/logo.jpg" alt="Lettstart Admin"/></span>
                  </a>
                  <a href="index.html" class="logo logo-light">
                     <span class="logo-sm"><img src="../assets/images/logo.jpg" alt="Lettstart Admin"/></span>
                     <span class="logo-lg"><img src="../assets/images/logo.jpg" alt="Lettstart Admin"/></span>
                  </a>
               </div>
              <%-- <button type="button" id="vertical-menu-btn" class="btn hamburg-icon">
                  <i class="mdi mdi-menu"></i>
               </button>--%>
              
                
            </div>
              <div class="text-center "><h3 class="text-light" >Warranty Complaints and Service Tracking(WSD)</h3> </div>
            
         </div>
      </header>
        <div class="auth-pages">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-xl-11">
                        <div class="row justify-content-center">
                            <div class="col-md-6 pl-md-0">
                                <div class="card mb-0 p-md-3">
                                    <div class="card-body">
                                        <div class="clearfix text-center">
                                           <%-- <img src="assets/images/logo.jpg" height="75" alt="logo" />--%>
                                            <b style="font-weight: 900;
    font-size: 20px;"> Sign In</b>
                                        </div>
                                        <%--<h5 class="mt-4 font-weight-600">Welcome back!</h5>
                                        <p class="text-muted mb-4">Enter your email address and password to access admin panel.</p>--%>
                                        <div class="form-group ">
                                            <label for="">User Name <span class="text-danger">*</span></label>
                                            <asp:TextBox ID="txtusername" class="form-control" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="reqValUsername" runat="server" ControlToValidate="txtusername" ForeColor="Red" ErrorMessage="Enter Username"></asp:RequiredFieldValidator>
                                        </div>
                                        <div class="form-group">
                                            <label for="">Password<span class="text-danger">*</span></label>
                                            <asp:TextBox ID="txtpassword" TextMode="Password" class="form-control" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="reqValPassword" runat="server" ControlToValidate="txtpassword" ForeColor="Red" ErrorMessage="Enter Password"></asp:RequiredFieldValidator>
                                        </div>


                                        <asp:Label ID="lblErrorMsg" ForeColor="Red" runat="server" Text=""></asp:Label>
                                        <div class="form-group text-center">
                                            <asp:Button ID="txtLogin" class="btn btn-primary btn-block" runat="server" Text="Log In" OnClick="txtLogin_Click" />
                                        </div>
                                        <div class="clearfix text-center">
                                            <a href="ForgotPass.aspx" class="text-primary">Forgot your password ?</a>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end row -->
            </div>
            <!-- end container -->
        </div>
    </form>

    <script src="assets/js/vendor.min.js"></script>
    <script src="assets/js/app.min.js"></script>
</body>
</html>
