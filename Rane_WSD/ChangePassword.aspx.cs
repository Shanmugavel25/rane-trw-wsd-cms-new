﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Rane_WSD
{
    public partial class ChangePassword : System.Web.UI.Page
    {
        string cs = ConfigurationManager.ConnectionStrings["conn"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(cs);
            con.Open();
            SqlCommand cmd = new SqlCommand("select Password from tbl_Admin where UserName ='" + Session["UserName"].ToString() + "'", con);
            SqlDataReader sdr = cmd.ExecuteReader();
            if (sdr.Read())
            {
                Session["Password"] = sdr["Password"].ToString();
                //txtoldPassword.Text = sdr["Password"].ToString();
            }
            con.Close();

            SqlConnection con1 = new SqlConnection(cs);
            con1.Open();
            SqlCommand cmd1 = new SqlCommand("select Password from tbl_user where UserName='" + Session["UserName"].ToString() + "'", con1);
            SqlDataReader sdr1 = cmd1.ExecuteReader();
            if (sdr1.Read())
            {
                Session["Password"] = sdr1["Password"].ToString();
            }
            con1.Close();

            SqlConnection con2 = new SqlConnection(cs);
            con2.Open();
            SqlCommand cmd2 = new SqlCommand("select password from Authorized where userName='" + Session["UserName"].ToString() + "'", con2);
            SqlDataReader sdr2 = cmd2.ExecuteReader();
            if (sdr2.Read())
            {
                Session["Password"] = sdr2["password"].ToString();
            }
             con2.Close();

             SqlConnection con3 = new SqlConnection(cs);
             con3.Open();
             SqlCommand cmd3 = new SqlCommand("select password from ServiceEng where userName='" + Session["UserName"].ToString() + "'", con3);
             SqlDataReader sdr3 = cmd3.ExecuteReader();
            if (sdr3.Read())
            {
                Session["Password"] = sdr3["password"].ToString();
            }
            con3.Close();
        }

        protected void btnForgot_Click(object sender, EventArgs e)
        {
            string OldPass = Session["Password"].ToString();
            string NewPass = TextBox1.Text.Trim();
            string ConfirmPass = TextBox2.Text.Trim();

            if (OldPass == txtoldPassword.Text)
            {
                lblOldPassword.Text = "Old Password Match";

                if (NewPass == ConfirmPass)
                {
                    Label1.Text = "Password Match";

                    SqlConnection con1 = new SqlConnection(cs);
                    con1.Open();
                    SqlCommand cmd1 = new SqlCommand("Update tbl_Admin SET Password='" + NewPass + "' where UserName='" + Session["UserName"] + "'", con1);
                    SqlCommand cmd2 = new SqlCommand("Update tbl_user SET Password='" + NewPass + "' where UserName='" + Session["UserName"] + "'", con1);
                    SqlCommand cmd3 = new SqlCommand("Update Authorized SET password='" + NewPass + "' where userName='" + Session["UserName"] + "'", con1);
                    SqlCommand cmd4 = new SqlCommand("Update ServiceEng SET password='" + NewPass + "' where userName='" + Session["UserName"] + "'", con1);
                    cmd1.ExecuteNonQuery();
                    cmd2.ExecuteNonQuery();
                    cmd3.ExecuteNonQuery();
                    cmd4.ExecuteNonQuery();
                    con1.Close();

                    Response.Write("<script language=javascript>alert('Password Changed Sucessfully');window.location=('login.aspx');</script>");

                }
                else
                {
                    Label1.Text = "New Password Mismatch";
                }
            }
            else
            {
                lblOldPassword.Text = "Old Password Mismatch";

            }

        }
    }
}