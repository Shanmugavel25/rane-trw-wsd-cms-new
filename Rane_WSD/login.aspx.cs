﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Rane_WSD
{
    public partial class login : System.Web.UI.Page
    {
        string cs = ConfigurationManager.ConnectionStrings["conn"].ConnectionString;

        protected void Page_Load(object sender, EventArgs e)
        {
            txtusername.Focus();
        }

        protected void txtLogin_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(cs);
            try
            {
                SqlCommand cmd = new SqlCommand("sp_b_adminlogin", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                cmd.Parameters.AddWithValue("@qtype", "adminlogin");
                cmd.Parameters.AddWithValue("@UserName", txtusername.Text);
                cmd.Parameters.AddWithValue("@Password", txtpassword.Text);
                SqlDataReader sdr = cmd.ExecuteReader();
                if (sdr.Read())
                {
                    Session["UserName"] = sdr["UserName"].ToString();
                    Session["UserType"] = "Admin";
                    Response.Redirect("~/Admins/dashboard.aspx");
                }
                else if (txtusername.Text != "" && txtpassword.Text != "")
                {
                    this.chkUser();
                }
              

                else
                {
                    lblErrorMsg.Text = "Sorry Invalid User Name or Password";
                    txtusername.Text = "";
                    txtpassword.Text = "";
                }
            }
            catch (Exception ex)
            {
                 Response.Write(ex);
                 lblErrorMsg.Text = "Error Occurred Contact Administrator";
            }
            finally
            {
                con.Close();
            }
        }


        protected void chkUser()
        {
            SqlConnection con = new SqlConnection(cs);
            try
            {
                SqlCommand cmd = new SqlCommand("sp_b_user", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                cmd.Parameters.AddWithValue("@qtype", "userlogin");
                cmd.Parameters.AddWithValue("@UserName", txtusername.Text.Trim());
                cmd.Parameters.AddWithValue("@Password", txtpassword.Text.Trim());
                SqlDataReader sdr = cmd.ExecuteReader();
                if (sdr.Read())
                {
                    //Session["id"] = sdr["Id"].ToString();
                    Session["UserName"] = sdr["UserName"].ToString();
                    Session["BranchName"] = sdr["BranchName"].ToString();
                    Session["Code"] = sdr["Code"].ToString();
                    Session["UserType"] = sdr["UserType"].ToString();
                    Session["Password"] = sdr["Password"].ToString();
                    Session["Location"] = sdr["BranchorLocation"].ToString();
                    Session["EmailId"] = sdr["EmailId"].ToString();
                    Response.Redirect("~/User/Dashboard.aspx");
                }
                else
                {
                    this.ServiceEng();
                    //lblErrorMsg.Text = "Sorry Invalid User Name or Password";
                    //txtusername.Text = "";
                    //txtpassword.Text = "";
                }
            }
            catch (Exception ex)
            {
                Response.Write(ex);
                lblErrorMsg.Text = "Error Occur Contact Administrator";
            }
            finally
            {
                con.Close();
            }

        }
        protected void ServiceEng()
        {
            SqlConnection con = new SqlConnection(cs);
            try
            {
                SqlCommand cmd = new SqlCommand("sp_b_ServiceEng", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                cmd.Parameters.AddWithValue("@qtype", "servicelogin");
                cmd.Parameters.AddWithValue("@userName", txtusername.Text.Trim());
                cmd.Parameters.AddWithValue("@password", txtpassword.Text.Trim());
                SqlDataReader sdr = cmd.ExecuteReader();
                if (sdr.Read())
                {
                    Session["UserName"] = sdr["userName"].ToString();
                    //Session["BranchName"] = sdr["BranchName"].ToString();
                    Session["Code"] = sdr["secode"].ToString();
                    Session["UserType"] = "Service Engineer";
                    Session["Name"] = sdr["Name"].ToString();
                    Session["Password"] = sdr["password"].ToString();
                    Session["EmailId"] = sdr["emailId"].ToString();
                    Response.Redirect("~/User/Dashboard.aspx");
                }
                else
                {
                    
                    //lblErrorMsg.Text = "Sorry Invalid User Name or Password";
                    //txtusername.Text = "";
                    //txtpassword.Text = "";
                    this.chkASC();
                }
            }
            catch (Exception ex)
            {
                Response.Write(ex);
                lblErrorMsg.Text = "Error Occur Contact Administrator";
            }
            finally
            {
                con.Close();
            }

        }
        protected void chkASC()
        {
            SqlConnection con = new SqlConnection(cs);
            try
            {
                SqlCommand cmd = new SqlCommand("sp_b_authorized", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                cmd.Parameters.AddWithValue("@qtype", "ASClogin");
                cmd.Parameters.AddWithValue("@userName", txtusername.Text.Trim());
                cmd.Parameters.AddWithValue("@password", txtpassword.Text.Trim());
                SqlDataReader sdr = cmd.ExecuteReader();
                if (sdr.Read())
                {
                    Session["UserName"] = sdr["userName"].ToString();
                    //Session["BranchName"] = sdr["BranchName"].ToString();
                    Session["Code"] = sdr["serviceEng"].ToString();
                    Session["UserType"] = "ASC";
                    Session["InchargeName"] = sdr["InchargeName"].ToString();
                    Session["Name"] = sdr["Name"].ToString();
                    Session["Password"] = sdr["password"].ToString();
                    Session["EmailId"] = sdr["emailId"].ToString();
                    Response.Redirect("~/User/Dashboard.aspx");
                }
                else
                {

                    lblErrorMsg.Text = "Sorry Invalid User Name or Password";
                    txtusername.Text = "";
                    txtpassword.Text = "";
                   
                }
            }
            catch (Exception ex)
            {
                Response.Write(ex);
                lblErrorMsg.Text = "Error Occur Contact Administrator";
            }
            finally
            {
                con.Close();
            }

        }
    
    }
}