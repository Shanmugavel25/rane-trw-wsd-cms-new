USE [RaneComplaint_Tracking]
GO
/****** Object:  Table [dbo].[tbl_usertype]    Script Date: 9/22/2021 12:58:43 PM ******/
DROP TABLE [dbo].[tbl_usertype]
GO
/****** Object:  Table [dbo].[tbl_user]    Script Date: 9/22/2021 12:58:43 PM ******/
DROP TABLE [dbo].[tbl_user]
GO
/****** Object:  Table [dbo].[tbl_state]    Script Date: 9/22/2021 12:58:43 PM ******/
DROP TABLE [dbo].[tbl_state]
GO
/****** Object:  Table [dbo].[tbl_FIRstatus]    Script Date: 9/22/2021 12:58:43 PM ******/
DROP TABLE [dbo].[tbl_FIRstatus]
GO
/****** Object:  Table [dbo].[tbl_FIRRegister]    Script Date: 9/22/2021 12:58:43 PM ******/
DROP TABLE [dbo].[tbl_FIRRegister]
GO
/****** Object:  Table [dbo].[tbl_FIRDetails]    Script Date: 9/22/2021 12:58:43 PM ******/
DROP TABLE [dbo].[tbl_FIRDetails]
GO
/****** Object:  Table [dbo].[tbl_Admin]    Script Date: 9/22/2021 12:58:43 PM ******/
DROP TABLE [dbo].[tbl_Admin]
GO
/****** Object:  Table [dbo].[ServiceEng]    Script Date: 9/22/2021 12:58:43 PM ******/
DROP TABLE [dbo].[ServiceEng]
GO
/****** Object:  Table [dbo].[cities]    Script Date: 9/22/2021 12:58:43 PM ******/
DROP TABLE [dbo].[cities]
GO
/****** Object:  Table [dbo].[Authorized]    Script Date: 9/22/2021 12:58:43 PM ******/
DROP TABLE [dbo].[Authorized]
GO
/****** Object:  StoredProcedure [dbo].[sp_b_user]    Script Date: 9/22/2021 12:58:43 PM ******/
DROP PROCEDURE [dbo].[sp_b_user]
GO
/****** Object:  StoredProcedure [dbo].[sp_b_servieceng]    Script Date: 9/22/2021 12:58:43 PM ******/
DROP PROCEDURE [dbo].[sp_b_servieceng]
GO
/****** Object:  StoredProcedure [dbo].[sp_b_ServiceEng]    Script Date: 9/22/2021 12:58:43 PM ******/
DROP PROCEDURE [dbo].[sp_b_ServiceEng]
GO
/****** Object:  StoredProcedure [dbo].[sp_b_FIRstatus]    Script Date: 9/22/2021 12:58:43 PM ******/
DROP PROCEDURE [dbo].[sp_b_FIRstatus]
GO
/****** Object:  StoredProcedure [dbo].[sp_b_FIRRegister]    Script Date: 9/22/2021 12:58:43 PM ******/
DROP PROCEDURE [dbo].[sp_b_FIRRegister]
GO
/****** Object:  StoredProcedure [dbo].[sp_b_FIRDetails]    Script Date: 9/22/2021 12:58:43 PM ******/
DROP PROCEDURE [dbo].[sp_b_FIRDetails]
GO
/****** Object:  StoredProcedure [dbo].[sp_b_authorized]    Script Date: 9/22/2021 12:58:43 PM ******/
DROP PROCEDURE [dbo].[sp_b_authorized]
GO
/****** Object:  StoredProcedure [dbo].[sp_b_adminlogin]    Script Date: 9/22/2021 12:58:43 PM ******/
DROP PROCEDURE [dbo].[sp_b_adminlogin]
GO
USE [master]
GO
/****** Object:  Database [RaneComplaint_Tracking]    Script Date: 9/22/2021 12:58:43 PM ******/
DROP DATABASE [RaneComplaint_Tracking]
GO
/****** Object:  Database [RaneComplaint_Tracking]    Script Date: 9/22/2021 12:58:43 PM ******/
CREATE DATABASE [RaneComplaint_Tracking]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'RaneComplaint_Tracking', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\RaneComplaint_Tracking.mdf' , SIZE = 4160KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'RaneComplaint_Tracking_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\RaneComplaint_Tracking_log.ldf' , SIZE = 784KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [RaneComplaint_Tracking] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [RaneComplaint_Tracking].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [RaneComplaint_Tracking] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [RaneComplaint_Tracking] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [RaneComplaint_Tracking] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [RaneComplaint_Tracking] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [RaneComplaint_Tracking] SET ARITHABORT OFF 
GO
ALTER DATABASE [RaneComplaint_Tracking] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [RaneComplaint_Tracking] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [RaneComplaint_Tracking] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [RaneComplaint_Tracking] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [RaneComplaint_Tracking] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [RaneComplaint_Tracking] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [RaneComplaint_Tracking] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [RaneComplaint_Tracking] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [RaneComplaint_Tracking] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [RaneComplaint_Tracking] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [RaneComplaint_Tracking] SET  ENABLE_BROKER 
GO
ALTER DATABASE [RaneComplaint_Tracking] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [RaneComplaint_Tracking] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [RaneComplaint_Tracking] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [RaneComplaint_Tracking] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [RaneComplaint_Tracking] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [RaneComplaint_Tracking] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [RaneComplaint_Tracking] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [RaneComplaint_Tracking] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [RaneComplaint_Tracking] SET  MULTI_USER 
GO
ALTER DATABASE [RaneComplaint_Tracking] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [RaneComplaint_Tracking] SET DB_CHAINING OFF 
GO
ALTER DATABASE [RaneComplaint_Tracking] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [RaneComplaint_Tracking] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
USE [RaneComplaint_Tracking]
GO
/****** Object:  StoredProcedure [dbo].[sp_b_adminlogin]    Script Date: 9/22/2021 12:58:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_b_adminlogin]
(
@Id integer=null,
@UserName nvarchar(50)=null,
@Password nvarchar(50)=null,
@qtype nvarchar(50)
)
AS
BEGIN
	
	SET NOCOUNT ON;

	if(@qtype='adminlogin')
	Begin
	select Id,UserName,Password from tbl_Admin where UserName=@UserName and Password=@Password
	End
END

GO
/****** Object:  StoredProcedure [dbo].[sp_b_authorized]    Script Date: 9/22/2021 12:58:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_b_authorized]
(
@ASCID integer =null,
@Name nvarchar(max)=null,
@Address nvarchar(max)=null,
@InchargeName nvarchar(150)=null,
@contactNo nvarchar(50)=null,
@mobileNo nvarchar(50)=null,
@userName nvarchar(150)=null,
@password nvarchar(150)=null,
@emailId nvarchar(150)=null,
@Region nvarchar(150)=null,
@State nvarchar(150)=null,
@Place nvarchar(150)=null,
@CsiNo nvarchar(150)=null,
@GSTNo nvarchar(60)=null,
@TinNo nvarchar(150)=null,
@CertificateExpirydate smalldatetime=null,
@ReminderRenewalDate smalldatetime=null,
@serviceEng nvarchar(150)=null,
@flag integer=null,
@Destination nvarchar(150)=null,
@searchcode nvarchar(200)=null,
@qtype nvarchar(50)

)
AS
BEGIN

	SET NOCOUNT ON;

    if(@qtype='Selectlist')
	Begin
	SELECT ROW_NUMBER() 
	OVER(ORDER BY ASCID asc)AS slNo,ASCID,serviceEng,Name,Address,InchargeName,contactNo,mobileNo,emailId,State,Place 
	from Authorized 
	where  
	(@searchcode is null or serviceEng  like '%'+ @searchcode+'%')and
	(@Name is null or Name like '%'+ @Name +'%') and
				 (@InchargeName is null or InchargeName like '%'+ @InchargeName +'%') 
	End

	else if(@qtype='viewasc')
	Begin
	select ASCID,Name,Address,InchargeName,contactNo,mobileNo,emailId,State,Place,Region,CsiNo,GSTNo,TinNo,CertificateExpirydate,ReminderRenewalDate,serviceEng,flag,Destination from Authorized where ASCID=@ASCID
	End

	else if(@qtype='ASClogin')
	Begin
	select ASCID,Name,InchargeName,userName,emailId,password,serviceEng from Authorized where userName=@userName and password=@password
	End
END

GO
/****** Object:  StoredProcedure [dbo].[sp_b_FIRDetails]    Script Date: 9/22/2021 12:58:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_b_FIRDetails]
(
@Id integer=null,
@FIRId integer=null,
@FIRNo nvarchar(50)=null,
@ReasonForRejection nvarchar(250)=null,
@AllocateAsc nvarchar(50)=null,
@DispatchMethod nvarchar(50)=null,
@DocumentNo nvarchar(50)=null,
@DispatchDate DateTime=null,
@ASCRecommendation nvarchar(50)=null,
@ASCReportedDate DateTime=null,
@ObservationofProblem nvarchar(250)=null,
@FileUpload nvarchar(50)=null,
@LRNumber nvarchar(50)=null,
@LRDate nvarchar(50)=null,
@DCNo nvarchar(50)=null,
@ServiceEnggApproval nvarchar(50)=null,
@ReceivedDate nvarchar(50)=null,
@TransportationName nvarchar(50)=null,
@DeliveryNoteNo nvarchar(50)=null,
@InwardNumber nvarchar(50)=null,
@InwardDate nvarchar(50)=null,
@InwardQuantity nvarchar(50)=null,
@CreditNoteNo nvarchar(50)=null,
@CreditNoteDate nvarchar(50)=null,
@Value nvarchar(50)=null,
@PlantDocNo nvarchar(50)=null,
@PlantDocDate nvarchar(50)=null,
@PlantNotificationNo nvarchar(50)=null,
@Remarks nvarchar(250)=null,
@Status nvarchar(50)=null,
@UpdatedOn DateTime=null,
@UpdatedBy nvarchar(50)=null,
@AllocateServiceEngg nvarchar(250)=null,
@qtype nvarchar(50)=null
)
AS
BEGIN
	
	SET NOCOUNT ON;
	if(@qtype='insFIR')
	Begin
	insert into tbl_FIRDetails(FIRId,FIRNo,Status,UpdatedBy,UpdatedOn)Values
	((select Top 1 Id from tbl_FIRRegister where FIRNo=@FIRNo),@FIRNo,@Status,@UpdatedBy,getdate())
	End

	else if(@qtype='UpdateSaleFIR')
	Begin
	insert into tbl_FIRDetails (FIRId,FIRNo,Status,Remarks,ReasonForRejection,AllocateServiceEngg,UpdatedBy,UpdatedOn)Values((select Top 1 Id from tbl_FIRRegister where FIRNo=@FIRNo),@FIRNo,@Status,@Remarks,@ReasonForRejection,@AllocateServiceEngg,@UpdatedBy,getdate())
	--Update tbl_FIRDetails SET Status=@Status,Remarks=@Remarks,ReasonForRejection=@ReasonForRejection,AllocateServiceEngg=@AllocateServiceEngg where FIRId=@FIRId
	End

	else if(@qtype='UpdateServiceEngFIR')
	Begin
	insert into tbl_FIRDetails(FIRId,FIRNo,Status,Remarks,AllocateAsc,UpdatedBy,UpdatedOn)Values((select Top 1 Id from tbl_FIRRegister where FIRNo=@FIRNo),@FIRNo,@Status,@Remarks,@AllocateAsc,@UpdatedBy,getdate())
	End

	else if(@qtype='updASCName')
	Begin
	select AllocateAsc from tbl_FIRDetails where  FIRNo=@FIRNo and Status='5'
	End

	else if(@qtype='upApproveService')
	Begin
	select ASCRecommendation,ObservationofProblem,ASCReportedDate from tbl_FIRDetails where FIRNo=@FIRNo and Status='7'
	End

	else if(@qtype='updDispatchASC')
	Begin
	insert into tbl_FIRDetails(FIRId,FIRNo,Status,Remarks,AllocateAsc,UpdatedBy,UpdatedOn,DispatchMethod,DocumentNo,DispatchDate)Values((select Top 1 Id from tbl_FIRRegister where FIRNo=@FIRNo),@FIRNo,@Status,@Remarks,@AllocateAsc,@UpdatedBy,getdate(),@DispatchMethod,@DocumentNo,getdate())
	End

	else if(@qtype='MateReceiveWSD')
	Begin
	insert into tbl_FIRDetails(FIRId,FIRNo,Status,ASCReportedDate,ObservationofProblem,UpdatedBy,UpdatedOn,FileUpload,ReceivedDate,ASCRecommendation)Values((select Top 1 Id from tbl_FIRRegister where FIRNo=@FIRNo),@FIRNo,@Status,@ASCReportedDate,@ObservationofProblem,@UpdatedBy,getdate(),@FileUpload,getdate(),@ASCRecommendation)
	End

	else if(@qtype='MateDispatchWSD')
	Begin
	insert into tbl_FIRDetails(FIRId,FIRNo,Status,LRNumber,LRDate,DCNo,Remarks,UpdatedBy,UpdatedOn)values((select Top 1 Id from tbl_FIRRegister where FIRNo=@FIRNo),@FIRNo,@Status,@LRNumber,@LRDate,@DCNo,@Remarks,@UpdatedBy,getdate());
	End

	else if(@qtype='ServiceApproved')
	Begin
	insert into tbl_FIRDetails(FIRId,FIRNo,Status,Remarks,ASCReportedDate,ObservationofProblem,ASCRecommendation,UpdatedBy,UpdatedOn,ServiceEnggApproval)Values(@FIRId,@FIRNo,@Status,@Remarks,@ASCReportedDate,@ObservationofProblem,@ASCRecommendation,@UpdatedBy,getdate(),@Status)
	End

	else if(@qtype='UpdateWSDReceived')
	Begin
	insert into tbl_FIRDetails(FIRId,FIRNo,Status,FileUpload,ReceivedDate,UpdatedBy,UpdatedOn)Values((select Top 1 Id from tbl_FIRRegister where FIRNo=@FIRNo),@FIRNo,@Status,@FileUpload,getdate(),@UpdatedBy,getdate())
	End

	else if(@qtype='MateDispatchWarehouse')
	Begin
	insert into tbl_FIRDetails(FIRId,FIRNo,Status,LRNumber,LRDate,DeliveryNoteNo,TransportationName,Remarks,UpdatedBy,UpdatedOn)values((select Top 1 Id from tbl_FIRRegister where FIRNo=@FIRNo),@FIRNo,@Status,@LRNumber,@LRDate,@DeliveryNoteNo,@TransportationName,@Remarks,@UpdatedBy,getdate());
	End

	else if(@qtype='MateReceiveFromWSD')
	Begin
	insert into tbl_FIRDetails(FIRId,FIRNo,Status,InwardNumber,InwardDate,InwardQuantity,Remarks,UpdatedBy,UpdatedOn)values((select Top 1 Id from tbl_FIRRegister where FIRNo=@FIRNo),@FIRNo,@Status,@InwardNumber,@InwardDate,@InwardQuantity,@Remarks,@UpdatedBy,getdate());
	End

	else if(@qtype='CreditNoteNo')
	Begin
	insert into tbl_FIRDetails(FIRId,FIRNo,Status,CreditNoteNo,CreditNoteDate,Value,PlantDocNo,PlantDocDate,PlantNotificationNo,Remarks,UpdatedBy,UpdatedOn)values((select Top 1 Id from tbl_FIRRegister where FIRNo=@FIRNo),@FIRNo,@Status,@CreditNoteNo,@CreditNoteDate,@Value,@PlantDocNo,@PlantDocDate,@PlantNotificationNo,@Remarks,@UpdatedBy,getdate());
	End

	else if(@qtype='WSDClosed')
	Begin
	insert into tbl_FIRDetails(FIRId,FIRNo,Status,Remarks,UpdatedBy,UpdatedOn)values((select Top 1 Id from tbl_FIRRegister where FIRNo=@FIRNo),@FIRNo,@Status,@Remarks,@UpdatedBy,getdate());
	End

	else if(@qtype='RejectFeedBackToWSD')
	Begin
	insert into tbl_FIRDetails(FIRId,FIRNo,Status,Remarks,UpdatedBy,UpdatedOn,ServiceEnggApproval)values((select Top 1 Id from tbl_FIRRegister where FIRNo=@FIRNo),@FIRNo,@Status,@Remarks,@UpdatedBy,getdate(),@Status);
	
	End

	else if(@qtype='SendToRetailer')
	Begin
	insert into tbl_FIRDetails(FIRId,FIRNo,Status,Remarks,UpdatedBy,UpdatedOn)values((select Top 1 Id from tbl_FIRRegister where FIRNo=@FIRNo),@FIRNo,@Status,@Remarks,@UpdatedBy,getdate());
	
	End




	else if(@qtype='ViewFIRDetails')
	Begin
	select * from tbl_FIRDetails where FIRId=@FIRId and Status =2
	End

	else if(@qtype='ViewDetails')
	Begin
	select * from tbl_FIRDetails where FIRId=@FIRId and Status=5
	End

	else if(@qtype='ViewfirDet')
	Begin
	select * from tbl_FIRDetails where FIRId=@FIRId and Status=6
	End

	else if(@qtype='ViewDet')
	Begin
	select * from tbl_FIRDetails where FIRId=@FIRId and Status=7
	End

	else if(@qtype='ViewDetailsFIR')
	Begin
	select * from tbl_FIRDetails where FIRId=@FIRId and Status=8
	End

	else if(@qtype='ViewServiceApproval')
	Begin
	select * from tbl_FIRDetails where FIRId=@FIRId and Status=9
	End

	else if(@qtype='ViewReceiveWSD')
	Begin
	select * from tbl_FIRDetails where FIRId=@FIRId and Status=11
	End

	else if(@qtype='SendMaterialWarehouse')
	Begin
	select * from tbl_FIRDetails where FIRId=@FIRId and Status=12
	End

	else if(@qtype='ViewWarehouse')
	Begin
	select * from tbl_FIRDetails where FIRId=@FIRId and Status=13
	End

	else if(@qtype='SendWarehouse')
	Begin
	select * from tbl_FIRDetails where FIRId=@FIRId and Status=14
	End

	else if(@qtype='ClosedWSD')
	Begin
	select * from tbl_FIRDetails where FIRId=@FIRId and Status=15
	End
	
	else if(@qtype='MateSendBackRetailer')
	Begin
	select * from tbl_FIRDetails where FIRId=@FIRId and Status=10
	End

	else if(@qtype='WSDSendToRetailer')
	Begin
	select * from tbl_FIRDetails where FIRId=@FIRId and Status=16
	End

	else if(@qtype='RejectClosedWSD')
	Begin
	select * from tbl_FIRDetails where FIRId=@FIRId and Status=17
	End

  
END

GO
/****** Object:  StoredProcedure [dbo].[sp_b_FIRRegister]    Script Date: 9/22/2021 12:58:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_b_FIRRegister]
(
@Id integer=null,
@FIRNo nvarchar(50)=null,
@WSDName nvarchar(150)=null,
@WSDBranch nvarchar(150)=null,
@RetailerName nvarchar(150)=null,
@RetailerLocation nvarchar(150)=null,
@PartNo nvarchar(50)=null,
@PartType nvarchar(50)=null,
@WSDInvoiceNo nvarchar(50)=null,
@AggregateSerialNo nvarchar(50)=null,
@InvoiceDate DateTime =null,
@Quantity integer=null,
@DateofComplaint DateTime=null,
@UploadFile nvarchar(50)=null,
@Status nvarchar(50)=null,
@CreatedOn nvarchar(50)=null,
@CreatedBy nvarchar(50)=null,
@UpdatedOn nVarchar(50)=null,
@UpdatedBy nvarchar(50)=null,
@AllocateServiceEngg nvarchar(100)=null,
@AllocateAsc nvarchar(150)=null,
@search nvarchar(50)=null,
@Notes nvarchar(max)=null,
@qtype nvarchar(50)
)
AS
BEGIN
	
	SET NOCOUNT ON;

  if(@qtype='insertFIR')
  Begin
  insert into tbl_FIRRegister(FIRNo,WSDName,WSDBranch,RetailerName,RetailerLocation,PartNo,PartType,WSDInvoiceNo,AggregateSerialNo,InvoiceDate,Quantity,DateofComplaint,UploadFile,Status,CreatedOn,CreatedBy,Notes)values
  (@FIRNo,@WSDName,@WSDBranch,@RetailerName,@RetailerLocation,@PartNo,@PartType,@WSDInvoiceNo,@AggregateSerialNo,@InvoiceDate,@Quantity,getdate(),@UploadFile,@Status,getdate(),@CreatedBy,@Notes)
  End
  else if(@qtype='selectlist')
  Begin
		  SELECT ROW_NUMBER() OVER
		  (
			ORDER BY Id desc
		  )AS slNo,
			  Id,
			  FIRNo,
			  WSDName,
			  WSDBranch,
			  DateofComplaint,
			  Status,
			  PartType,
			  PartNo,
			  AggregateSerialNo,
			  CreatedBy,
			  (Select FIRStatus from tbl_FIRstatus b where b.id=a.Status)StatusName,(Select AllocateServiceEngg  from tbl_FIRDetails b where b.FIRId=a.Id and b.Status=2)AllocateServiceEngg,(Select AllocateAsc from tbl_FIRDetails b where b.FIRId=a.Id and b.Status=6)AllocateAsc,(Select UpdatedBy from tbl_FIRDetails b where b.FIRId=a.Id and b.Status=5)UpdatedBy,(Select UpdatedBy from tbl_FIRDetails b where b.FIRId=a.Id and b.Status=1)WSDUser from tbl_FIRRegister a
		   
		   where (@FIRNo is null or FIRNo like '%'+ @FIRNo +'%') and 
				 (@PartNo is null or PartNo like '%'+ @PartNo +'%') and
				 (@Status is null or Status like '%'+ @Status +'%') and CreatedBy=@CreatedBy
  End
  else if(@qtype='selectsales')
  Begin
		  SELECT ROW_NUMBER() OVER
		  (
			ORDER BY Id desc
		  )AS slNo,
			  Id,
			  FIRNo,
			  WSDName,
			  WSDBranch,
			  DateofComplaint,
			  Status,
			  PartType,
			  PartNo,
			  AggregateSerialNo,
			  CreatedBy,
			  (Select FIRStatus from tbl_FIRstatus b where b.id=a.Status)StatusName,(Select AllocateServiceEngg  from tbl_FIRDetails b where b.FIRId=a.Id and b.Status=2)AllocateServiceEngg,(Select AllocateAsc from tbl_FIRDetails b where b.FIRId=a.Id and b.Status=6)AllocateAsc,(Select UpdatedBy from tbl_FIRDetails b where b.FIRId=a.Id and b.Status=5)UpdatedBy,(Select UpdatedBy from tbl_FIRDetails b where b.FIRId=a.Id and b.Status=1)WSDUser from tbl_FIRRegister a
		   
		   where (@FIRNo is null or FIRNo like '%'+ @FIRNo +'%') and 
				 (@PartNo is null or PartNo like '%'+ @PartNo +'%') and
				 (@Status is null or Status like '%'+ @Status +'%') 
  End

  else if(@qtype='selectservice')
  Begin
		  SELECT ROW_NUMBER() OVER
		  (
			ORDER BY Id desc
		  )AS slNo,
			  Id,
			  FIRNo,
			  WSDName,
			  WSDBranch,
			  DateofComplaint,
			  Status,
			   PartType,
			  PartNo,
			  AggregateSerialNo,
			  CreatedBy,
			  (Select FIRStatus from tbl_FIRstatus b where b.id=a.Status)StatusName,(Select AllocateServiceEngg  from tbl_FIRDetails b where b.FIRId=a.Id and b.Status=2)AllocateServiceEngg,(Select AllocateAsc from tbl_FIRDetails b where b.FIRId=a.Id and b.Status=6)AllocateAsc,(Select UpdatedBy from tbl_FIRDetails b where b.FIRId=a.Id and b.Status=5)UpdatedBy,(Select UpdatedBy from tbl_FIRDetails b where b.FIRId=a.Id and b.Status=1)WSDUser from tbl_FIRRegister a
		   
		   where (@FIRNo is null or FIRNo like '%'+ @FIRNo +'%') and 
				 (@PartNo is null or PartNo like '%'+ @PartNo +'%') and
				 (@Status is null or Status like '%'+ @Status +'%') and Id in (select FIRId from tbl_FIRDetails where Status=2 and AllocateServiceEngg=@AllocateServiceEngg)
  End
  else if(@qtype='selectasc')
  Begin
		  SELECT ROW_NUMBER() OVER
		  (
			ORDER BY Id desc
		  )AS slNo,
			  Id,
			  FIRNo,
			  WSDName,
			  WSDBranch,
			  DateofComplaint,
			  Status,
			  PartType,
			  PartNo,
			  AggregateSerialNo,
			  CreatedBy,
			  (Select FIRStatus from tbl_FIRstatus b where b.id=a.Status)StatusName,(Select AllocateServiceEngg  from tbl_FIRDetails b where b.FIRId=a.Id and b.Status=2)AllocateServiceEngg,(Select AllocateAsc from tbl_FIRDetails b where b.FIRId=a.Id and b.Status=6)AllocateAsc,(Select UpdatedBy from tbl_FIRDetails b where b.FIRId=a.Id and b.Status=5)UpdatedBy,(Select UpdatedBy from tbl_FIRDetails b where b.FIRId=a.Id and b.Status=1)WSDUser from tbl_FIRRegister a
		   
		   where (@FIRNo is null or FIRNo like '%'+ @FIRNo +'%') and 
				(@PartNo is null or PartNo like '%'+ @PartNo +'%') and
				 (@Status is null or Status like '%'+ @Status +'%') and Id in (select FIRId from tbl_FIRDetails where Status=5 and AllocateAsc=@AllocateAsc)
  End

  else if(@qtype='search')
  Begin 
  SELECT ROW_NUMBER() OVER(ORDER BY Id desc)AS slNo,
  Id,
  FIRNo,
  WSDName,
  WSDBranch,
  DateofComplaint,
  Status,
   PartType,
			  PartNo,
			  AggregateSerialNo,
  CreatedBy,
   (Select FIRStatus from tbl_FIRstatus b where b.id=a.Status)StatusName,(Select AllocateServiceEngg  from tbl_FIRDetails b where b.FIRId=a.Id and b.Status=2)AllocateServiceEngg,(Select AllocateAsc from tbl_FIRDetails b where b.FIRId=a.Id and b.Status=6)AllocateAsc,(Select UpdatedBy from tbl_FIRDetails b where b.FIRId=a.Id and b.Status=5)UpdatedBy,(Select UpdatedBy from tbl_FIRDetails b where b.FIRId=a.Id and b.Status=1)WSDUser from tbl_FIRRegister a

   where

   (FIRNo like '%'+ @search + '%' or WSDName like '%'+ @search + '%'or WSDBranch like '%'+ @search + '%' or DateofComplaint like '%'+ @search + '%' or  Status like '%'+ @search + '%' )

  
  End

  else if(@qtype='viewFIR')
  Begin
  select Id,FIRNo,WSDName,WSDBranch,RetailerName,RetailerLocation,PartNo,PartType,WSDInvoiceNo,AggregateSerialNo,InvoiceDate,Quantity,DateofComplaint,UploadFile,Status,CreatedOn,CreatedBy,UpdatedBy,UpdatedOn,Notes,(Select FIRStatus from tbl_FIRstatus b where b.id=a.Status)StatusName  from tbl_FIRRegister a where Id=@Id
  End

  else if(@qtype='updateFIR')
  Begin
  Update tbl_FIRRegister SET FIRNo=@FIRNo,WSDName=@WSDName,WSDBranch=@WSDBranch,RetailerName=@RetailerName,RetailerLocation=@RetailerLocation,PartNo=@PartNo,PartType=@PartType,WSDInvoiceNo=@WSDInvoiceNo,AggregateSerialNo=@AggregateSerialNo,InvoiceDate=@InvoiceDate,Quantity=@Quantity,DateofComplaint=@DateofComplaint,UploadFile=@UploadFile,Status=@Status,UpdatedOn=getdate(),UpdatedBy=@UpdatedBy
  End

  else if(@qtype='deleteFIR')
  Begin
  delete from tbl_FIRRegister where Id=@Id
  End

  else if(@qtype='UpdateSalesEng')
  Begin
  Update tbl_FIRRegister SET Status=@Status,UpdatedBy=@UpdatedBy where Id=@Id
  End

  else if(@qtype='Selectdata')
  Begin
   Select Id,WSDName,FIRNo,WSDBranch,RetailerName,RetailerLocation,PartNo,WSDInvoiceNo,AggregateSerialNo,InvoiceDate,DateofComplaint,Notes,(Select AllocateServiceEngg  from tbl_FIRDetails b where b.FIRId=a.Id and b.Status=2)AllocateServiceEngg,(Select AllocateAsc from tbl_FIRDetails b where b.FIRId=a.Id and b.Status=6)AllocateAsc,(Select ASCRecommendation from tbl_FIRDetails b where b.FIRId=a.Id and b.Status=7)ASCRecommendation,(Select ASCReportedDate from tbl_FIRDetails b where b.FIRId=a.Id and b.Status=7)ASCReportedDate,(Select ObservationofProblem from tbl_FIRDetails b where b.FIRId=a.Id and b.Status=7)ObservationofProblem,(Select FIRStatus from tbl_FIRstatus b where b.id=a.Status)StatusName,(Select ASCRecommendation from tbl_FIRDetails b where b.FIRId=a.Id and b.Status=9)ASCRecommendationService,(Select Remarks from tbl_FIRDetails b where b.FIRId=a.Id and b.Status=9)ServiceEnggApproval,(Select Remarks from tbl_FIRDetails b where b.FIRId=a.Id and b.Status=10)ServiceEnggApproval from tbl_FIRRegister a where Id=@Id

  End

END

GO
/****** Object:  StoredProcedure [dbo].[sp_b_FIRstatus]    Script Date: 9/22/2021 12:58:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_b_FIRstatus]
(
	@Id integer=null,
    @FIRStatus nvarchar(50)=null,
    @Status nvarchar(50)=null,
    @CreatedOn DateTime=null,
    @CreatedBy nvarchar(50)=null,
	@UpdatedOn DateTime=null,
    @Updatedby  nvarchar(50)=null,
	@qtype nvarchar(50)
)
AS
BEGIN
	
	SET NOCOUNT ON;
	IF(@qtype='insertuser') BEGIN
		insert into tbl_FIRstatus(FIRStatus,Status,CreatedOn,CreatedBy)Values( @FIRStatus,@Status,getdate(),@CreatedBy)
	END
	ELSE IF (@qtype='selectlist') BEGIN
		SELECT ROW_NUMBER() OVER(ORDER BY Id asc)AS slNo,Id,FIRStatus,CreatedBy,Status from tbl_FIRstatus where (@FIRStatus is null or FIRStatus like '%'+ @FIRStatus +'%') 
	END
	ELSE IF(@qtype='deleteuser') BEGIN
		delete from tbl_FIRstatus where Id=@Id
	END
	ELSE IF(@qtype='updateaccount') BEGIN
		Select Id,FIRStatus,Status from tbl_FIRstatus where Id=@Id
	END
	ELSE IF(@qtype='updateuser') BEGIN
		Update tbl_FIRstatus SET FIRStatus=@FIRStatus,Status=@Status,UpdatedOn=getdate(),Updatedby=@Updatedby where Id=@Id
	END
	ELSE IF(@qtype='viewuser') BEGIN
		Select Id,FIRStatus,CreatedBy,Updatedby,Status from tbl_FIRstatus a where Id=@Id
	END
END


GO
/****** Object:  StoredProcedure [dbo].[sp_b_ServiceEng]    Script Date: 9/22/2021 12:58:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_b_ServiceEng]
(
@SEID integer=null,
@Name nvarchar(350)=null,
@Address nvarchar(550)=null,
@contactNo nvarchar(50)=null,
@mobileNo nvarchar(50)=null,
@emailId nvarchar(150)=null,
@secode nvarchar(50)=null,
@userName nvarchar(150)=null,
@password nvarchar(150)=null,
@authorizedServiceCenter nvarchar(Max)=null,
@state nvarchar(50)=null,
@regcode nvarchar(50)=null,
@qtype nvarchar(50)
)
AS
BEGIN
	
	SET NOCOUNT ON;

   if(@qtype='servicelogin')
   Begin
   select userName,password,emailId,Name,secode,authorizedServiceCenter from ServiceEng where userName=@userName and password=@password
   End
END

GO
/****** Object:  StoredProcedure [dbo].[sp_b_servieceng]    Script Date: 9/22/2021 12:58:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_b_servieceng]
(
@SEID integer=null,
@Name nvarchar(max)=null,
@Address nvarchar(max)=null,
@contactNo nvarchar(50)=null,
@mobileNo nvarchar(50)=null,
@emailId nvarchar(150)=null,
@secode nvarchar(50)=null,
@authorizedServiceCenter nvarchar(max)=null,
@region nvarchar(150)=null,
@state nvarchar(150)=null,
@regcode nvarchar(150)=null,
@flag nvarchar(150)=null,
@qtype nvarchar(50)

)
AS
BEGIN
	
	SET NOCOUNT ON;
	 if(@qtype='Selectlist')
	Begin
	SELECT ROW_NUMBER() OVER
	(ORDER BY SEID asc)AS slNo,SEID,Name,Address,secode,contactNo,
	mobileNo,emailId,State from ServiceEng
	 where

	  (@Name is null or Name like '%'+ @Name +'%') and 
				 (@secode is null or secode like '%'+ @secode +'%') and
				 (@state is null or State like '%'+ @state +'%') 

	End

	else if(@qtype='viewService')
	Begin
	select SEID,Name,Address,secode,contactNo,mobileNo,emailId,State,authorizedServiceCenter,region,state,regcode,flag from ServiceEng where SEID=@SEID
	End
 
END

GO
/****** Object:  StoredProcedure [dbo].[sp_b_user]    Script Date: 9/22/2021 12:58:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_b_user]
(
	@Id integer=null,
    @Name nvarchar(50)=null,
    @Mobile nvarchar(20)=null,
    @EmailId nvarchar(50)=null,
    @State nvarchar(50)=null,
    @City nvarchar(50)=null,
    @BranchorLocation nvarchar(50)=null,
    @UserType nvarchar(50)=null,
    @UserName nvarchar(50)=null,
    @Password nvarchar(50)=null,
    @Status nvarchar(50)=null,
    @CreatedOn DateTime=null,
    @CreatedBy nvarchar(50)=null,
	@UpdatedOn DateTime=null,
    @Updatedby  nvarchar(50)=null,
	@BranchName nvarchar(max)=null,
	@Code nvarchar(50)=null,
	@qtype nvarchar(50)
)
AS
BEGIN
	
	SET NOCOUNT ON;

	IF(@qtype='insertuser') BEGIN
		insert into tbl_user(Name,Mobile,EmailId,State,City,BranchorLocation,UserType,UserName,Password,Status,CreatedOn,CreatedBy,BranchName,Code)
		Values( @Name,@Mobile,@EmailId,@State,@City,@BranchorLocation,@UserType,@UserName,@Password,@Status,getdate(),@CreatedBy,@BranchName,@Code)
	END
	ELSE IF (@qtype='selectlist') BEGIN
		SELECT ROW_NUMBER() OVER(ORDER BY Id asc)AS slNo,
		       Id,
			   Name,
			   Mobile,
			   EmailId,
			   State,
			   City,
			   BranchorLocation,
			   UserType,
			   Status
		 from tbl_user where 

		 (@Name is null or Name like '%'+ @Name +'%') and 
				 (@Mobile is null or Mobile like '%'+ @Mobile +'%') and
				 (@EmailId is null or EmailId like '%'+ @EmailId +'%') and UserType=@UserType
	END
	ELSE IF(@qtype='deleteuser') BEGIN
		delete from tbl_user where Id=@Id
	END
	ELSE IF(@qtype='updateaccount') BEGIN
		Select Id,Name,Mobile,EmailId,State,City,BranchorLocation,UserType,Status,BranchName,Code from tbl_user where Id=@Id
	END
	ELSE IF(@qtype='updateuser') BEGIN
		Update tbl_user SET Name=@Name,Mobile=@Mobile,EmailId=@EmailId,State=@State,City=@City,BranchorLocation=@BranchorLocation,UserType=@UserType,Status=@Status,UpdatedOn=getdate(),Updatedby=@Updatedby where Id=@Id
	END
	ELSE IF(@qtype='viewuser') BEGIN
		Select Id,Name,Mobile,EmailId,State,City,BranchorLocation,UserType,Status,BranchName,Code from tbl_user where Id=@Id
	END
	--ELSE IF (@qtype='selectdata') BEGIN
	--	SELECT ROW_NUMBER() OVER(ORDER BY Id asc)AS slNo,Id,Name,Mobile,EmailId,State,City,BranchorLocation,UserType,Status from tbl_user where UserType='Sales Engineer'
	--END
	--ELSE IF (@qtype='selectdatalist') BEGIN
	--	SELECT ROW_NUMBER() OVER(ORDER BY Id asc)AS slNo,Id,Name,Mobile,EmailId,State,City,BranchorLocation,UserType,Status from tbl_user where UserType='Warehouse'
	--END
	else if(@qtype='userlogin')
	Begin
	select Id,UserName,Password,BranchName,UserType,Code,EmailId,BranchorLocation from tbl_user where UserName=@UserName and Password=@Password
	End
END

GO
/****** Object:  Table [dbo].[Authorized]    Script Date: 9/22/2021 12:58:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Authorized](
	[ASCID] [int] IDENTITY(1,1) NOT NULL,
	[ASCCode] [varchar](50) NULL,
	[Name] [nvarchar](max) NULL,
	[Address] [nvarchar](max) NULL,
	[InchargeName] [nvarchar](150) NULL,
	[contactNo] [nvarchar](50) NULL,
	[mobileNo] [nvarchar](50) NULL,
	[userName] [nvarchar](150) NULL,
	[password] [nvarchar](150) NULL,
	[emailId] [nvarchar](150) NULL,
	[Region] [nvarchar](150) NULL,
	[State] [nvarchar](150) NULL,
	[Place] [nvarchar](150) NULL,
	[CsiNo] [nvarchar](150) NULL,
	[GSTNo] [nvarchar](60) NULL,
	[TinNo] [nvarchar](150) NULL,
	[CertificateExpirydate] [smalldatetime] NULL,
	[ReminderRenewalDate] [smalldatetime] NULL,
	[serviceEng] [nvarchar](150) NULL,
	[flag] [int] NULL,
	[Destination] [nvarchar](150) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[cities]    Script Date: 9/22/2021 12:58:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cities](
	[city_id] [int] IDENTITY(1,1) NOT NULL,
	[city_name] [nvarchar](50) NULL,
	[city_state] [nvarchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[city_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ServiceEng]    Script Date: 9/22/2021 12:58:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ServiceEng](
	[SEID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](350) NULL,
	[Address] [nvarchar](550) NULL,
	[contactNo] [nvarchar](50) NULL,
	[mobileNo] [nvarchar](50) NULL,
	[emailId] [nvarchar](150) NULL,
	[secode] [nvarchar](50) NULL,
	[userName] [nvarchar](150) NULL,
	[password] [nvarchar](150) NULL,
	[authorizedServiceCenter] [nvarchar](max) NULL,
	[region] [nvarchar](150) NULL,
	[state] [nvarchar](150) NULL,
	[regcode] [nvarchar](150) NULL,
	[flag] [nvarchar](150) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_Admin]    Script Date: 9/22/2021 12:58:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Admin](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [nvarchar](50) NULL,
	[Password] [nvarchar](50) NULL,
	[Status] [nvarchar](50) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_FIRDetails]    Script Date: 9/22/2021 12:58:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_FIRDetails](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FIRId] [int] NULL,
	[FIRNo] [nvarchar](50) NULL,
	[ReasonForRejection] [nvarchar](250) NULL,
	[AllocateAsc] [nvarchar](50) NULL,
	[DispatchMethod] [nvarchar](50) NULL,
	[DocumentNo] [nvarchar](50) NULL,
	[DispatchDate] [datetime] NULL,
	[ASCRecommendation] [nvarchar](50) NULL,
	[ASCReportedDate] [datetime] NULL,
	[ObservationofProblem] [nvarchar](250) NULL,
	[FileUpload] [nvarchar](50) NULL,
	[LRNumber] [nvarchar](50) NULL,
	[LRDate] [nvarchar](50) NULL,
	[DCNo] [nvarchar](50) NULL,
	[ServiceEnggApproval] [nvarchar](50) NULL,
	[ReceivedDate] [nvarchar](50) NULL,
	[TransportationName] [nvarchar](50) NULL,
	[DeliveryNoteNo] [nvarchar](50) NULL,
	[InwardNumber] [nvarchar](50) NULL,
	[InwardDate] [nvarchar](50) NULL,
	[InwardQuantity] [nvarchar](50) NULL,
	[CreditNoteNo] [nvarchar](50) NULL,
	[CreditNoteDate] [nvarchar](50) NULL,
	[Value] [nvarchar](50) NULL,
	[PlantDocNo] [nvarchar](50) NULL,
	[PlantDocDate] [nvarchar](50) NULL,
	[PlantNotificationNo] [nvarchar](50) NULL,
	[Remarks] [nvarchar](250) NULL,
	[Status] [nvarchar](50) NULL,
	[AllocateServiceEngg] [nvarchar](250) NULL,
	[UpdatedOn] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
 CONSTRAINT [PK_tbl_FIRDetails] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_FIRRegister]    Script Date: 9/22/2021 12:58:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_FIRRegister](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FIRNo] [nvarchar](50) NULL,
	[WSDName] [nvarchar](150) NULL,
	[WSDBranch] [nvarchar](150) NULL,
	[RetailerName] [nvarchar](150) NULL,
	[RetailerLocation] [nvarchar](150) NULL,
	[PartNo] [nvarchar](50) NULL,
	[PartType] [nvarchar](50) NULL,
	[WSDInvoiceNo] [nvarchar](50) NULL,
	[AggregateSerialNo] [nvarchar](50) NULL,
	[InvoiceDate] [datetime] NULL,
	[Quantity] [int] NULL,
	[DateofComplaint] [datetime] NULL,
	[UploadFile] [nvarchar](50) NULL,
	[Status] [nvarchar](50) NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[UpdatedOn] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Notes] [nvarchar](max) NULL,
 CONSTRAINT [PK_tbl_FIRRegister] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_FIRstatus]    Script Date: 9/22/2021 12:58:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_FIRstatus](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FIRStatus] [nvarchar](50) NULL,
	[Status] [nvarchar](50) NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[UpdatedOn] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[FIRStatusName] [nvarchar](50) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_state]    Script Date: 9/22/2021 12:58:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_state](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[state] [nvarchar](150) NULL,
 CONSTRAINT [PK_tbl_state] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_user]    Script Date: 9/22/2021 12:58:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_user](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](150) NULL,
	[Mobile] [nvarchar](20) NULL,
	[EmailId] [nvarchar](50) NULL,
	[State] [nvarchar](250) NULL,
	[City] [nvarchar](250) NULL,
	[BranchorLocation] [nvarchar](250) NULL,
	[UserType] [nvarchar](50) NULL,
	[UserName] [nvarchar](50) NULL,
	[Password] [nvarchar](50) NULL,
	[Status] [nvarchar](50) NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[UpdatedOn] [datetime] NULL,
	[Updatedby] [nvarchar](50) NULL,
	[BranchName] [nvarchar](max) NULL,
	[Code] [nvarchar](50) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_usertype]    Script Date: 9/22/2021 12:58:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_usertype](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserType] [nvarchar](150) NULL,
	[ShortName] [nvarchar](50) NULL,
	[Status] [nvarchar](50) NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NULL
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[Authorized] ON 

INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (1, N'101', N'Triumph Auto Engineering Pvt. Ltd.', N'38 Km Milestone,  Delhi - Jaipur Highway, Khandsa,', N'Naresh Gupta', N'01246325044', N'9811052399', N'Triumph', N'nasc101', N'triumphauto@yahoo.com', N'North', N'Haryana', N'Delhi (Haryana)', N'1234', NULL, N'11', CAST(0x000B0000 AS SmallDateTime), CAST(0x000B0000 AS SmallDateTime), N'08548', 1, N'Owner')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (2, N'102', N'Shiva Motors', N'20/19, Nangli Poona,  G. T. Karnal Road,  Delhi - ', N'Gajinder Yadav', N'9212725724', N'9212725725', N'Shiva', N'nasc102', N'shivamotorsdelhi@rediffmail.com', N'North', N'Haryana', N'Delhi', N'07290184273', NULL, N'11', CAST(0x000B0000 AS SmallDateTime), CAST(0x000B0000 AS SmallDateTime), N'08548', 1, N'Partner')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (3, N'105', N'Surindera Autos (P) Limited', N'114, Industrial Area,  Phase - 1,  Panchkula.  Cha', N' Bahadur Singh', N' 0172576117', N' 9814123210', N'Surindera', N'SUKHOI1108', N'text.gmail.com', N'North', N'Chandigarh', N'Chandigarh', N'1234', NULL, N'11', CAST(0x000B0000 AS SmallDateTime), CAST(0x000B0000 AS SmallDateTime), N'10631', 0, N'Owner')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (4, N'106', N'Surindera Autos (P) Limited', N'114, Industrial Area,Phase - 1,  Panchkula.  Chand', N'Surinder Singh Sekhon', N'0172576117', N'9814123210', N'Surindera Autos', N'nasc106', N'dfsfsdfsdf@gmail.com', N'North', N'Chandigarh', N'Chandigarh', N'1000', NULL, N'1000', CAST(0x9E010000 AS SmallDateTime), CAST(0x9F6E0000 AS SmallDateTime), N'10631', 0, N'Owner')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (5, N'107', N'Cargo Motors Pvt. Limited', N'40, G. T. Road,  Jalandhar - 144 001.', N' S. K. Agarwal', N'01812243004', N'223997175', N'Cargo', N'nasc107', N' cargo@jla.vsnl.net.in', N'North', N'Punjab', N'Chandigarh', N'56451', NULL, N'159489', CAST(0x9E010000 AS SmallDateTime), CAST(0x9F6E0000 AS SmallDateTime), N'10631', 0, N'Owner')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (6, N'108', N'Energy Motors', N'No. 1-2, Gotewalon Ki Bageechi,  Agra Road, Transp', N'Nazar Mohammad', N'01412640861 ', N'9414071561', N'Energy', N'wajid9334', N'em_jpr@hotmail.com', N'North', N'Rajastan', N'Jaipur', N'1420/07169 dt. 18/01/03', NULL, N'11', CAST(0x000B0000 AS SmallDateTime), CAST(0x000B0000 AS SmallDateTime), N'08396', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (7, N'109', N'Mohan Motor Workshop', N'G. T. Road,  Kauri,  Khanna -141 401 (Pb).', N'AJIT SINGH', N'9914848755', N'9814580755', N'mohan', N'nasc109', N'gfgfufref@gmail.com mohankhannatass@rediffmail.com', N'North', N'Punjab', N'Chandigarh', N'3564', NULL, N'11', CAST(0x000B0000 AS SmallDateTime), CAST(0x000B0000 AS SmallDateTime), N'10631', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (8, N'110', N'Rajender Motors', N'At Near Chandra Petrol Pump,  Beechwal Industrial ', N'Rajender Singh', N'01512250548', N'09829174500', N'Rajender', N'nasc110', N'afsaftr@gmail.com', N'North', N'Rajastan', N'Bikaner', N'088213555331', NULL, N'446456', CAST(0x9E010000 AS SmallDateTime), CAST(0x9F6E0000 AS SmallDateTime), N'08396', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (9, N'111', N'Shree Ganga Sales & Service', N'73/3, Ist Floor,  Transport Nagar,  Agra - 2.', N'. Luv Agarwal', N'05622600641', N'09719119325', N'Shree Ganga', N'nasc111', N'jdbgfuerfjk@gmail.com', N'North', N'Uttatakhand', N'Agra', N'462384', NULL, N'32648723645', CAST(0x9E010000 AS SmallDateTime), CAST(0x9F6E0000 AS SmallDateTime), N'08548', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (10, N'112', N'K. Associate Agencies', N'Shop Nos. 457, Yard No. 6,  New Transport Nagar,
', N'Lalit  Virdi', N'2490073', N'9419181537', N'K', N'kassociate11', N'Email : kassociate@bahu.net', N'North', N'jammu and Kashmir', N'jammu', N'45235', NULL, N'11', CAST(0xA5230000 AS SmallDateTime), CAST(0xA5050000 AS SmallDateTime), N'10631', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (11, N'113', N'Anjali Motors', N'Plot No. 05, Meera Colony	Kailana Choraha	Jodhpur ', N'Mr. Surendra Kachchhawaha', N'09214056242', N'09214056245', N'Kachwaha', N'61216245', N'kachwahamotors@hotmail.com', N'North', N'Rajastan', N'Jodhpur', N'08BMKPK4671A', N'08BMKPK4671A', N'08BMKPK4671A', CAST(0xAAD80000 AS SmallDateTime), CAST(0xAABD0000 AS SmallDateTime), N'08396', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (12, N'114', N'Udaipur Auto Agency', N'Savina Road,  Rati Stand,  Udaipur - 313 001.', N'. Anil Nahar', N'02942584136', N'9829041573', N'Udaipur', N'nasc114', N'ewfggre@gmail.com', N'North', N'Rajastan', N'Udaipur', N'3253466', NULL, N'46578678456', CAST(0x9E010000 AS SmallDateTime), CAST(0x9F6E0000 AS SmallDateTime), N'08396', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (13, N'115', N'Raju Motor Works', N'62, Haryana Motor Market,G. T. Road,  Ambala City ', N'Raj Kumar', N'9896994444', N'9812121584', N'Raju', N'nasc115', N'dgwbduhsa7y@gmail.com', N'North', N'Haryana', N'Chandigarh', N'53465783165', NULL, N'390759834759', CAST(0x9E010000 AS SmallDateTime), CAST(0x9F6E0000 AS SmallDateTime), N'08548', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (14, N'116', N'Gurdeep Agencies', N'S-45, Guru Nanak Motor Market,  Gadarian purwa,  K', N' Pawan Deep Singh Bhatia', N'9793207775', N'9793207775', N'Gurdeep', N'hemkund', N'gaknp.pawan@gmail.com', N'North', N'Uttar Pradesh', N'Kanpur', N'5578796', NULL, N'11', CAST(0x000B0000 AS SmallDateTime), CAST(0x000B0000 AS SmallDateTime), N'12485', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (15, N'118', N'Shri Vishwakarma Aut', N'1, Jhalawar Road,  Opp. Airport,  Kota - 324 007 (', N'Aniruddh', N'0744364999', N'9414188817', N'Shri Vishwakarma', N'nasc118', N'egfdwfd@gmail.com', N'North', N'Rajastan', N'Kota', N'46567648', NULL, N'08602952078', CAST(0x9E010000 AS SmallDateTime), CAST(0x9F6E0000 AS SmallDateTime), N'08396', 0, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (16, N'144', N'Om Diesels Private Limited', N'198, Auto Market,  Hissar. Haryana.', N'Gulshan Kumar', N'01662286259', N'9416300198', N'Om', N'om198198', N'om.diesels@gmail.com', N'North', N'Haryana', N'Hissar', N'06711535220', NULL, N'11', CAST(0x000B0000 AS SmallDateTime), CAST(0x000B0000 AS SmallDateTime), N'08548', 1, N'Owner')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (17, N'201', N'Antony Garage', N'Waman Tukaram Patil Road,Off. Duke''s & Sons Limite', N'Mr. Edvin', N'0225561832', N'9987496500', N'antony', N'asc201', N'online@gmail.com', N'West', N'Maharastra', N'Mumbai', N'12345', NULL, N'12345', CAST(0x9E010000 AS SmallDateTime), CAST(0x9F6E0000 AS SmallDateTime), N'10574', 0, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (18, N'202', N'Power Zone', N'Bachoo Garage Compound,Safed Pool, Kurla Andheri,M', N'srinivas poojary', N'02228514759', N'9223377256', N'power', N'ranesteer', N'zonepwr@gmail.com', N'West', N'Maharastra', N'Mumbai', N'23456', NULL, N'11', CAST(0x000B0000 AS SmallDateTime), CAST(0x000B0000 AS SmallDateTime), N'12696', 1, N'Partner')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (19, N'119', N'Sagar Automobiles', N'Vishkarma Market,  Barnala Road,  Bathinda.  Punja', N'Parsant Sagar', N'09855223548', N'09855223548', N'Sagar Automobiles', N'nasc119', N'vefgwegruw@gmail.com', N'North', N'Punjab', N'Chandigarh', N'734781263', NULL, N'4378124236', CAST(0x9E010000 AS SmallDateTime), CAST(0x9F6E0000 AS SmallDateTime), N'10631', 0, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (20, N'120', N'Shree Vasu Automobiles Ltd.', N'C-1, Shatabdi Nagar,  Delhi Road,  Meerut.  Uttar ', N'.Anup Singh', N'9837086344', N'9837086344', N'Shree Vasu ', N'nasc120', N'shreevasu@hclinfinet.com', N'North', N'Uttar Pradesh', N'Meerut', N'0012608', NULL, N'5009975', CAST(0x9E010000 AS SmallDateTime), CAST(0x9F6E0000 AS SmallDateTime), N'08396', 0, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (21, N'121', N'Jaswinder Motors Repairing Works', N'Rubh Nagar,  Gulabi Bagh,  G. T. Road,  Moga.  Pun', N'Maghar Singh', N'9815338519', N'9781700519', N'Jaswinder', N'nasc121', N'dhwsytdfwy@gmail.com', N'North', N'Punjab', N'Chandigarh', N'378942136594', NULL, N'34879257394', CAST(0x9E010000 AS SmallDateTime), CAST(0x9F6E0000 AS SmallDateTime), N'10631', 0, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (22, N'122', N'Happy Motor Garage', N'Gobind Bagh,  Rajpura Road,Patiala - 147 001 (PB).', N'Jagmohan Singh', N' 2282937', N'911753101637', N'Happy', N'nasc122', N' happymotors2001@yahoo.com', N'North', N'Punjab', N'Chandigarh', N'23421423', NULL, N'64516110', CAST(0x9E010000 AS SmallDateTime), CAST(0x9F6E0000 AS SmallDateTime), N'10631', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (23, N'123', N'Bharat Motors', N'Opp. Indian Oil Depot,  Jalandhar Road,  Pathankot', N'Gurminder Singh', N'2240523', N'9417013977', N'Bharat Motors', N'nasc123', N'fwsedfywe@gmail.com', N'North', N'jammu and Kashmir', N'Pathankot', N'8202', NULL, N' 06102208202', CAST(0x9E010000 AS SmallDateTime), CAST(0x9F6E0000 AS SmallDateTime), N'10631', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (24, N'124', N'National Auto Store', N'611, Sector-4,  Transport Nagar,  Karnal - 132 001', N'Gurminder Singh', N'2275452', N'9253028203', N'National', N'nasc124', N'hsdagfy@gmail.com', N'North', N'Haryana', N'Karnal', N'8202', NULL, N'06102208202', CAST(0x9E010000 AS SmallDateTime), CAST(0x9F6E0000 AS SmallDateTime), N'08548', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (25, N'126', N'Raman Diesel Service', N'61-A, Guru Govind Singh Marg,  Lal Kaun,Lucknow - ', N'Pankaj Mital', N'05222638270', N'05222239546', N'Raman', N'nasc126', N'diesel@sancharnet.in; drpankajmital@hotmail.com', N'North', N'Uttar Pradesh', N'Kanpur', N'0229779', NULL, N'5128821', CAST(0x9E010000 AS SmallDateTime), CAST(0x9F6E0000 AS SmallDateTime), N'12485', 1, N'Partner')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (26, N'127', N'Shahdara Engineers', N'Plot No. 5, Jhilmil Industrial Area
G. T. Road
B', N' Agha Khan', N'9810321582', N'9810321582', N'Auto Engineers', N'nasc127', N'autoengineerstatamotors@yahoo.co.in', N'North', N'Haryana', N'Delhi', N'43164567', NULL, N'67486786798', CAST(0xA5230000 AS SmallDateTime), CAST(0xA5050000 AS SmallDateTime), N'08548', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (27, N'128', N'Jagir Motors', N'Truck Market,  Opp. Saini Dhaba,  Delhi Road, Sone', N'Jeevan Singh', N'01302218372', N'9813112128', N'Jagir', N'nanaksar', N'jagirmotors@rediffmail.com', N'North', N'Haryana', N'Sonepat', N'58236598305', NULL, N'11', CAST(0x000B0000 AS SmallDateTime), CAST(0x000B0000 AS SmallDateTime), N'08396', 0, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (28, N'129', N'Global Trading Company', N'J.15/65-C (Auto Sales Building),  G. T. Road,Alaip', N'Jeevan Singh', N'05426450934', N'05422213891', N'Global ', N'nasc129', N'c bzvcbznvchs@gmail.com', N'North', N'Uttatakhand', N'Kanpur', N'0516588', NULL, N'06863012399', CAST(0x9E010000 AS SmallDateTime), CAST(0x9F6E0000 AS SmallDateTime), N'12485', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (29, N'130', N'Yadav Diesels', N'1st Corner Shop,  Towards Jaipur Road,  Bye Pass C', N'VIMAL YADAV', N'01452695028', N'9829447649', N'Yadav', N'nasc130', N'yadavdiesels@gmail.com', N'North', N'Rajastan', N'Ajmer', N'315475678658', NULL, N'11', CAST(0x000B0000 AS SmallDateTime), CAST(0x000B0000 AS SmallDateTime), N'08396', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (30, N'132', N'D.S. Diesels', N'Motor Market Complex,  ,  Dist. Bilaspur.', N'Deepak Sharma', N'9816014072', N'9816014072', N'D.S.', N'nasc132', N'hdgfuysyfuye@gmail.com', N'North', N'Himachal Pradesh', N'Chandigarh', N'680978984857', NULL, N'6847984684', CAST(0x9E010000 AS SmallDateTime), CAST(0x9F6E0000 AS SmallDateTime), N'10631', 0, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (31, N'133', N'Paul Diesels & Electricals', N'Old Station Road,  Alwar - 301 001.', N'Sachin Sodi', N'01482324502', N'9352201006', N'Paul', N'nasc133', N'paldslelt@rediffmail.com', N'North', N'Rajastan', N'Alwar', N'0205', NULL, N'08310603140', CAST(0x9E010000 AS SmallDateTime), CAST(0x9F6E0000 AS SmallDateTime), N'08396', 1, N'Partner')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (32, N'134', N'Dashmesh Powers', N'C-5, Workshop Line,  Transport Nagar,Rampur Road, ', N'Satvinder Singh', N'9719238388', N'9719238388', N'Dashmesh', N'nasc134', N'dahsdagd@gmail.com', N'North', N'Uttatakhand', N'Haldwani', N'5068206', N'05AUNPS9502E2Z0', N'05006264479', CAST(0x9E010000 AS SmallDateTime), CAST(0x9F6E0000 AS SmallDateTime), N'08548', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (33, N'135', N'Chinar Motors', N'M. A. Road,  Sri Nagar - 190 001. Kashmir.', N'Ayub Mastafa Bacha', N'01942477411', N'09906032890', N'Chinar', N'nasc135', N'hgsdfsaghd@gmail.com', N'North', N'jammu and Kashmir', N'Chandigarh', N'76587356783', NULL, N'01272060751', CAST(0x9E010000 AS SmallDateTime), CAST(0x9F6E0000 AS SmallDateTime), N'10631', 0, N'Partner')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (36, N'000001', N'Gayathri', N'Chennai', N'Naveen', N'044321649684', N'654564541', N'G000001', N'000001', N'brainmagicbipl@gmail.com', N'South', N'Tamil Nadu', N'Chennai', N'565464534534', N'346567345', N'456', CAST(0xAA5F0000 AS SmallDateTime), CAST(0xAA600000 AS SmallDateTime), NULL, 0, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (79, N'432', N'Sunny Systems', N'Opp. Karnataka Bank  Manjunath Nagar Cross Gokul R', N'A. Arjunan', N' 2335331 ', N' 9343403476', N'ASCSOUTH23', N'11713', N'arthur_fern@rediffmail.com', N'South', N'Kerala', N'Hubli', N'7', NULL, N'7', CAST(0x9E010000 AS SmallDateTime), CAST(0x9F6E0000 AS SmallDateTime), N'089988', 0, N'Owner')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (80, N'419', N'Baskar Auto System', N'No. 24, Bye Pass Road Vellore - 632 004  ', N'K. Karthik', N' 2232360', N' 2232360', N'ASCSOUTH13', N'BLOCKED1', N'baskarsystem@hotmail.com', N'South', N'Tripura', N'Vellor', N'55', N'ABCB', N'6', CAST(0x9E010000 AS SmallDateTime), CAST(0x9F6E0000 AS SmallDateTime), N'06597', 1, N'Owner')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (34, N'137', N'Dass Motor Stores', N'10, Raja Road,  Near Laxmi Park,  Dehradun - 248 0', N'Mohan Dass', N'9456122232', N'9456122232', N'Dass', N'nasc137', N'dassmotorstores@gmail.com', N'North', N'Uttatakhand', N'Dehradun', N'5084105', NULL, N' 5001154519', CAST(0x9E010000 AS SmallDateTime), CAST(0x9F6E0000 AS SmallDateTime), N'08548', 1, N'Partner')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (35, N'138', N'Shrinath Sales & Service', N'54, Transport Nagar,  Bhilwara - 311 001 (Raj).', N'Sanjay Kogta', N'0148224356', N'9352110088', N'Shrinath', N'nasc138', N'skogta@yahoo.com', N'North', N'Rajastan', N'Bhilwara', N'324865638475', NULL, N' 08961001404', CAST(0x9E010000 AS SmallDateTime), CAST(0x9F6E0000 AS SmallDateTime), N'08396', 0, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (37, N'139', N'Punjab Diesels', N'Motor Market Complex,S.C.F - 450,Manimajra,  Chand', N'Manjit Singh', N'01722734818', N'9815495821', N'Punjab', N'msg139', N'punjabdiesels.chd@gmail.com', N'North', N'Chandigarh', N'Chandigarh', N'04BCNPS8981E1Z1', N'04BCNPS8981E1Z1', N'04BCNPS8981E1Z1', CAST(0xAAD80000 AS SmallDateTime), CAST(0xAABA0000 AS SmallDateTime), N'10631', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (38, N'140', N'Highway Motors', N'Kanpur Road,  Digara,  Jhansi  - 284 128 (U.P).', N'Mohd. Sajid Khan', N' 2321757', N'9415586882', N'Highway', N'nasc140', N'dsfgfhgeerf@gmail.com', N'North', N'Uttar Pradesh', N'Kanpur', N'0159843', NULL, N'5093065', CAST(0x9E010000 AS SmallDateTime), CAST(0x9F6E0000 AS SmallDateTime), N'06815', 0, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (39, N'141', N'Bala Diesel Motors', N'G-1-74, RIICO Industrial Area,  Opp. Midway, N. H.', N'Babulal Gurjar', N'01422273225', N'9414209570', N'Bala', N'nasc141', N'baladiesel_1234@yahoo.com', N'North', N'Rajastan', N'Shahpura', N'564645687', N'08AAVFB3709J1Z1', N'11', CAST(0xAAD90000 AS SmallDateTime), CAST(0xADB30000 AS SmallDateTime), N'08396', 1, N'Owner')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (40, N'142', N'R. M. Diesel & Electricals', N'10, Park Road,Gorakhpur - 273 001.', N'Ramesh Chandra Agrawal', N'05512334358', N'9455088369', N'R. M', N'nase142', N'bdscfseghfge@gmail.com', N'North', N'Uttar Pradesh', N'Kanpur', N'0192691', NULL, N'534563434807', CAST(0x9E010000 AS SmallDateTime), CAST(0x9F6E0000 AS SmallDateTime), N'06815', 0, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (41, N'143', N'Yash Automotive (P) Limited', N'NH-7, Chandaipur Village,Amoi Post,  Rewa Road,Mir', N'Sanjay Kumar', N'05442246001', N'05442246004', N'Yash', N'nasc143', N'egfrweugf@gmail.com', N'North', N'Uttar Pradesh', N'Kanpur', N'0097136', NULL, N'5059565', CAST(0x9E010000 AS SmallDateTime), CAST(0x9F6E0000 AS SmallDateTime), N'12485', 0, N'Owner')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (42, N'145', N'Shri Mahalaxmi Autotech Pvt Ltd', N'Shop No. 43, 1st Floor,Transport Nagar,Jaipur - 30', N'Kamlesh Kumar Agarwal', N'01415116126', N'9828160053', N'Mahalaxmi', N'nasc145', N'mss@shrimahalaxmi.co.in', N'North', N'Rajastan', N'Jaipur', N'584357846356', N'08AASCS1931M1ZR', N'08421606733', CAST(0x9E010000 AS SmallDateTime), CAST(0x9F6E0000 AS SmallDateTime), N'08396', 0, N'Partner')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (43, N'147', N'Gee Ess Auto Engg. Works', N'Outside Hall Gate,Inside Sarai Sant Ram,Amritsar. ', N'Kulwinder Singh Saggu', N'01832548050', N'9815366461', N'Gee ', N'preet241085', N'jimmy_ariesguy@yahoo.com', N'North', N'Punjab', N'Chandigarh', N'7856378456', NULL, N'11', CAST(0x000B0000 AS SmallDateTime), CAST(0x000B0000 AS SmallDateTime), N'10631', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (44, N'149', N'Suraksha Motors', N'Near R.S.E.B,National Highway,Opp. Tehsil,Swaroopg', N'Samrath Singh', N'02971224352', N'02971224352', N'Suraksha', N'nasc149', N'kamalmotorcompany@yahoo.co.in', N'North', N'Rajastan', N'Swaroopganj', N'758378455346', NULL, N'08553300481', CAST(0x9E010000 AS SmallDateTime), CAST(0x9F6E0000 AS SmallDateTime), N'08396', 1, N'Partner')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (45, N'150', N'Maanjeet Diesels', N'122/152, Kabari Market,Sarojini Nagar,Kanpur-12.', N'Bhalinder Singh/ Mr. Arju', N'05123295600', N' 09889820043', N'Maanjeet', N'nasc150', N'dfsdfhgsdu@gmail.com', N'North', N'Uttar Pradesh', N'Kanpur', N'0754913', NULL, N'5577619', CAST(0x9E010000 AS SmallDateTime), CAST(0x9F6E0000 AS SmallDateTime), N'12485', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (46, N'151', N'Shree Ram Automotive', N'B - 12, Gaushala Market,GT Road,Panipat - 132 103,', N'Sumeet Puri', N'9255299030', N'9466419010', N'Shree Ram', N'nasc151', N'wdghdfgywf@gmail.com', N'North', N'Haryana', N'Panipat', N'06142617908', NULL, N'3456789346', CAST(0x9E010000 AS SmallDateTime), CAST(0x9F6E0000 AS SmallDateTime), N'08396', 0, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (47, N'152', N'Vivek Motors', N'Doodh Ganga,Capri,Udhampur - 182 101. J&K.  ', N'Sumesh Magotra', N'019922274975', N'9419160753', N'Vivek', N'nasc152', N'vivek.motors@rediffmail.com', N'North', N'jammu and Kashmir', N'Chandigarh', N'01201080459', NULL, N'560348560', CAST(0x9E010000 AS SmallDateTime), CAST(0x9F6E0000 AS SmallDateTime), N'14606', 0, N'Owner')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (48, N'153', N'Jullundur Motor Agency (Delhi) Limited', N'Rane Authorised Service Centre,2E / 5, Jhandewalan', N' L. K. Nagpal', N'01123532985', N'9350001007', N'Jullundur', N'nasc153', N'sdhagyfty@gmail.com', N'North', N'Haryana', N'Delhi', N'07960038453', NULL, N'34785634856', CAST(0x9E010000 AS SmallDateTime), CAST(0x9F6E0000 AS SmallDateTime), N'08396', 0, N'Owner')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (49, N'154', N'Guru Gobind Singh Sales & Services', N'A/31, Kesari Market,Lal Kaun,M. B. Road,New Delhi ', N'Gautam Sibal', N'65056967', N'9818127999', N'Guru', N'nasc154', N'bhuvan.sibal@yahoo.com', N'North', N'Haryana', N'New Delhi', N'574565658403', NULL, N'453580676564', CAST(0x9E010000 AS SmallDateTime), CAST(0x9F6E0000 AS SmallDateTime), N'08548', 0, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (50, N'155', N'Shree Gopal Motors', N'B-195, Shastri Nagar,Bhilwara - 311 001,Rajasthan.', N'Sushil Kumar', N'01482242205', N'09414114205', N'sgm155', N'Welcome2u', N'sgm.bijoliya@gmail.com', N'North', N'Rajastan', N'Bijoliya', N'08261052811', NULL, N'08261052811', CAST(0x9E010000 AS SmallDateTime), CAST(0x9F6E0000 AS SmallDateTime), N'08396', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (51, N'156', N'Sachdeva Automobiles', N'72, Rewa Road,Dandi,Allahabad - 211001,Uttarprades', N'Mrs.Paramjeet Singh Sachd', N'05322684358', N'9415317585', N'Sachdeva ', N'nasc0156', N'vicky631@rediffmail.com', N'North', N'Uttar Pradesh', N'Kanpur', N'09412704503', NULL, N'11', CAST(0x000B0000 AS SmallDateTime), CAST(0x000B0000 AS SmallDateTime), N'12485', 1, N'Partner')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (52, N'157', N'Gagan Automobiles', N'Darla More (Darlaghat),Thesil.Arki,Distt.Solan (H.', N'Gagan Kanwar', N'9857760783', N'9857760783', N'Gagan', N'nasc157', N'gagan_skanwar@yahoo.com', N'North', N'Chandigarh', N'Chandigarh', N'02020400004', NULL, N'13594', CAST(0x9E010000 AS SmallDateTime), CAST(0x9F6E0000 AS SmallDateTime), N'10631', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (53, N'204', N'Bhoomi Motors', N'Opp. to Mony Hotel,Near Shakthi Estate,Isanpur,Ahm', N'Mr. G. Radhe Shyam', N' 07925712085', N'9712992276', N'bhoomi', N'asc204', N'online2@gmail.com', N'West', N'Gujarat', N'Ahmedabad', N'34256', NULL, N'34256', CAST(0x9E010000 AS SmallDateTime), CAST(0x9F6E0000 AS SmallDateTime), N'06608', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (54, N'205', N'Shinde Automotive Services', N'Hira Chambers,692 A/2, Pune - Satara Road,Pune - 4', N'Mr. Kedar Shinde', N'0204219001', N'9922459427', N'shinde', N'terminated', N'Email : kedars@vsnl.com', N'West', N'Maharastra', N'Pune', N'43526', NULL, N'43526', CAST(0x9E010000 AS SmallDateTime), CAST(0x9F6E0000 AS SmallDateTime), N'10574', 0, N'Partner')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (55, N'206', N'Mahin Motors Pvt. Lt', N'C/o. Bacha Motors Pvt. Ltd,Narol - Sarkhej Road,Sa', N'Mr. Saiyed Naushad', N'0796614353', N'0796614353', N'mahin', N'asc206', N'online3@gmail.com', N'West', N'Gujarat', N'Ahmedabad', N'43526', NULL, N'43526', CAST(0x9E010000 AS SmallDateTime), CAST(0x9F6E0000 AS SmallDateTime), N'06608', 0, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (56, N'208', N'Sagar Fuel Injection', N'61/1, Niranjanpur,Dewas Naka,A.  B. Road,Indore (M', N'Mr. Sagar Bhalla', N'07314021999', N'989322199', N'sagar', N'asc208', N'online4@gmail.com', N'West', N'Madhya Pradesh', N'Indore', N'34265', NULL, N'34265', CAST(0x9E010000 AS SmallDateTime), CAST(0x9F6E0000 AS SmallDateTime), N'10574', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (57, N'209', N'Bhoomi Motors', N'Plot No. 808, GIDC,Ranoli,Baroda - 391 350      ', N'Mr. G. Radhe Shyam', N'9712992276', N'9712992276', N'bhoomi2', N'asc209', N'online5@gmail.com', N'West', N'Gujarat', N'Baroda', N'76895', NULL, N'76895', CAST(0x9E010000 AS SmallDateTime), CAST(0x9F6E0000 AS SmallDateTime), N'06608', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (58, N'210', N'Sheth Auto Parts Pvt. Ltd.', N'19, Kamptee Road,Saibaba Complex,Gaddigodam,Nagpur', N'Mr. Devan Sheth', N'07122522066', N'9922412170', N'sheth', N'asc210', N'online6@gmail.com', N'West', N'Maharastra', N'Nagpur', N'98564', NULL, N'98564', CAST(0x9E010000 AS SmallDateTime), CAST(0x9F6E0000 AS SmallDateTime), N'06608', 0, N'Partner')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (59, N'212', N'RJT Auto Garage', N'Marketing Yard Road,Opp. Garden Restaurant,Rajkot ', N'Mr. Barath', N' 02812603176', N'9428346604', N'rjt', N'asc212', N'online7@gmail.com', N'West', N'Gujarat', N'Rajkot', N'54679', NULL, N'TIN No: 24591400623', CAST(0x9E010000 AS SmallDateTime), CAST(0x9F6E0000 AS SmallDateTime), N'10574', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (60, N'214', N'Balaji Automobiles', N'Plot No. P-81, D-11 Block,Near KSB Pumps Limited,M', N'Mr. Ajay', N' 02027459797', N' 9850970942', N'balaji', N'asc214', N'Email : tirupatiautomobiles@rediff.com', N'West', N'Maharastra', N'Pune', N'CST No. 411019/C/1172 dated 28', NULL, N'LST No. 411019/S/1446 dated 28', CAST(0x9E010000 AS SmallDateTime), CAST(0x9F6E0000 AS SmallDateTime), N'12696', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (61, N'217', N'Kolhapur Diesels', N'1229/K, Amble Complex,Bagal Chowk,Rajaram Road,Kol', N'Mr. Salim Imam Tamboli', N'2529367', N'9423040516', N'kolhapur', N'ASC217', N'Email : kopdiesels@dataone.in', N'West', N'Maharastra', N'Kolhapur', N'CST No. 416008/C/238 dated 1-4', NULL, N'11', CAST(0x000B0000 AS SmallDateTime), CAST(0x000B0000 AS SmallDateTime), N'12696', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (62, N'218', N'Rahul & Associates', N'Bethora Road,Ponda,Goa - 403 401      ', N'Mr. Rahul R.S Verenkar', N'08322315014', N'9422441514', N'rahul', N'asc218', N'verekarrahul@hotmail.com', N'West', N'Goa', N'Goa', N'CST No. PD/CST/1306 dated 22.1', NULL, N'LST No. PD/1582 dated 22.10.98', CAST(0x9E010000 AS SmallDateTime), CAST(0x9F6E0000 AS SmallDateTime), N'10574', 0, N'Partner')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (63, N'219', N'Shree New Ambica Auto Centre', N'Near Sanjivani Hospital,N. H. No.8,Chaltan - 394 3', N'Mr. Jugal Patel', N'02622281541', N'9825038641', N'ambica', N'asc219', N'online8@gmail.com', N'West', N'Maharastra', N'Chaltan (Surat)', N'CST No. 223900234', NULL, N'LST No. 47207177 dated 01.07.2', CAST(0x9E010000 AS SmallDateTime), CAST(0x9F6E0000 AS SmallDateTime), N'06608', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (64, N'220', N'S. S. Auto Electricals', N'Kansana Complex,Main Road,T. P. Nagar,Gwalior.  Ma', N'Mr. Sanjeev Saghel', N'560348', N'9425112636', N'ssauto', N'asc220', N'online11@gmail.com', N'West', N'Madhya Pradesh', N'Gwalior', N'45637', NULL, N'TIN NO. 2350530538', CAST(0x9E010000 AS SmallDateTime), CAST(0x9F6E0000 AS SmallDateTime), N'10574', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (65, N'221', N'City Car Care', N'Plot No. 79, Sector 11,Gandhidham - 370 201,Kutch ', N'Mr. AngurGupta', N'02836230775', N'9898033346', N'citycar', N'asc221', N'citycar@wilnetonline.net', N'West', N'Gujarat', N'Gandhidham', N'CST No. GUJ.7182012 -1', NULL, N'LST No. 0110011333-1', CAST(0x9E010000 AS SmallDateTime), CAST(0x9F6E0000 AS SmallDateTime), N'06608', 0, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (66, N'222', N'Automotive Seva Point', N'Above JMA Rane Marketing Limited,Plot No. X-357, T', N'Mr. Pravin', N'02402553966', N'9423028154', N'automotive', N'6164', N'autosevapoint@rediffmail.com', N'West', N'Maharastra', N'Aurangabad', N'CST No. 425003-C-540', NULL, N'11', CAST(0x000B0000 AS SmallDateTime), CAST(0x000B0000 AS SmallDateTime), N'06608', 1, N'Partner')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (67, N'223', N'Polestar Engg. Co.', N'B-88 , M I D C AREA, BEHIND ADOR WELDING 
AHMEDNA', N'Mr. Vijay G. Shembi', N'02412322221', N'09850507099', N'polestar223', N'password', N'polestar_powerol@yahoo.com', N'West', N'Maharastra', N'Ahmed Nagar', N'Cst No. 414111-C-511 Dated 28.', NULL, N'11', CAST(0x000B0000 AS SmallDateTime), CAST(0x000B0000 AS SmallDateTime), N'10574', 0, N'Partner')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (68, N'224', N'Genesis Technologies', N'Plot No -D-PAP -51
TTC MIDC Area	Opp Unitech Auto', N'Mr. P. Arul Das ', N'9322643383', N'9320126710', N'genesis', N'asc224', N'dinesh@genesisgroup.co.in', N'West', N'Maharastra', N'Navi Mumbai', N'27AHTPD6238M1ZQ', N'27AHTPD6238M1ZQ', N'27AHTPD6238M1ZQ', CAST(0xAAD80000 AS SmallDateTime), CAST(0xAABA0000 AS SmallDateTime), N'12696', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (69, N'226', N'Automotive Care Point', N'C/o. Jalgaon Auto Services,Highway Towers,N.H. No.', N'Mr.Mayur H Shah', N'02572233554', N'9326951905', N'carepoint', N'asc226', N'online12@gmail.com', N'West', N'Maharastra', N'Jalgaon', N'87906', NULL, N'87906', CAST(0x9E010000 AS SmallDateTime), CAST(0x9F6E0000 AS SmallDateTime), N'06608', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (70, N'227', N'Ghatge Patil Automobiles Limited', N'3/15, Bhavani Peth,Solapur - 413 002    ', N'Mr. Munshi', N'2625391', N'9158883503', N'ghatge', N'asc227', N'online13@gmail.com', N'West', N'Maharastra', N'Solapur', N'CST No. 416001/C/12 dated 01.0', NULL, N'MST No. 413001/S/009 dated 01.', CAST(0x9E010000 AS SmallDateTime), CAST(0x9F6E0000 AS SmallDateTime), N'12696', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (71, N'228', N'Premier Auto Works', N'Nanded Hyderabad Road,Nanded - 431 601    ', N'Mr.Laxmikant Keshetwar', N'02462235975', N'9370120159', N'premier', N'asc228', N'online13@gmail.com', N'West', N'Maharastra', N'Nanded', N'CST No. 431604/C/561 dated 01.', NULL, N'BST No. 431604/S/764 dated 01.', CAST(0x9E010000 AS SmallDateTime), CAST(0x9F6E0000 AS SmallDateTime), N'06608', 0, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (72, N'230', N'Sakshi Auto Sales & Service', N'H.No. 409, First Floor,Opp. Patrakar Bhawan,Baldeo', N'Mr. Virendra Patel', N'9893036655', N'9893036655', N'scservice', N'asc230', N'online14@gmail.com', N'West', N'Madhya Pradesh', N'Jabalpur', N'87960', NULL, N'TIN No. 23786105250', CAST(0x9E010000 AS SmallDateTime), CAST(0x9F6E0000 AS SmallDateTime), N'10574', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (73, N'231', N'Pramukh Diesels', N'C/9, Bass Complex,Near Ashopalav Restraurant,Silva', N'Mr. Mayur Khatra', N'9824344022', N'9824344022', N'pramukh', N'asc231', N'online15@gmail.com', N'West', N'Gujarat', N'Vapi', N'68594', NULL, N'TIN No. 23786105250', CAST(0x9E010000 AS SmallDateTime), CAST(0x9F6E0000 AS SmallDateTime), N'06608', 1, N'Partner')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (74, N'232', N'Capital Diesels', N'Hotel Mayur Complex,Berasia Road,Bhopal - 462 001 ', N'Mr. Nishant Sachdeva', N'07552745223', N'9826054544', N'capital', N'asc232', N'cdbp11@yahoo.co.in', N'West', N'Madhya Pradesh', N'Bhopal', N'65748', NULL, N'65748', CAST(0x9E010000 AS SmallDateTime), CAST(0x9F6E0000 AS SmallDateTime), N'10574', 1, N'Partner')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (75, N'332', N'Moulana Turbo Engineering', N'BRTF LANE,NAGATILA,SONAI ROAD,  SILCHAR,ASSAM', N'SAYED AHMED LASKAR', N'03842225355', N'9435071125', N'ascdemoeast', N'abc@123', N'asc.silcharassam@gmail.com', N'East', N'Assam', N'SILCHAR', N'2345', NULL, N'11', CAST(0x9E010000 AS SmallDateTime), CAST(0x9F6E0000 AS SmallDateTime), N'11737', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (76, N'233', N'Manish Hydraulics', N'Jam Nagar Dwarka Highway,Near MRF Tyres,Sikka Pati', N'Mr. Manish H. Patel', N'02882345668', N'9824299220', N'manish', N'asc233', N'online16@gmail.com', N'West', N'Gujarat', N'Jam Nagar', N'CST No. 2460010506 dt. 07.11.2', NULL, N'TIN No. 24100105606 dt. 29/09/', CAST(0x9E010000 AS SmallDateTime), CAST(0x9F6E0000 AS SmallDateTime), N'10574', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (77, N'234', N'Vijay Automobiles', N'Near Bus Stand,Mhow Road,Ratlam - 457 001 (M.P)   ', N'Mr. Rachet khandelwal', N'241161', N'9827714050', N'vijay', N'asc234', N'online17@gmail.com', N'West', N'Madhya Pradesh', N'Ratlam', N'87932', NULL, N'87932', CAST(0x9E010000 AS SmallDateTime), CAST(0x9F6E0000 AS SmallDateTime), N'10574', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (78, N'235', N'Saikrupa Automobiles', N'Near Vidyaniketan School,Nagpur Road,Chandrapur - ', N'Mr. Kishore Menon', N'07172250749', N'09225231422', N'saikrupa', N'asc235', N'online18@gmail.com', N'West', N'Maharastra', N'Chandrapur', N'No. 27540347396C dt.  1.4.06', NULL, N'TIN No. 27540347396C dt.  1.4.', CAST(0x9E010000 AS SmallDateTime), CAST(0x9F6E0000 AS SmallDateTime), N'06608', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (81, N'427', N'Auto Diesels', N'Indira Auto Nagar  Station Back Road  Bijapur - 58', N'A. Arjunan', N'08352250645', N'08352250645', N'ASCSOUTH20', N'BLOCKED2', N'autodiesels@yahoo.co.in', N'South', N'Karnataka', N'Bijapur', N'9', N'ABCB', N'9', CAST(0x9E010000 AS SmallDateTime), CAST(0x9F6E0000 AS SmallDateTime), N'08547', 0, N'Owner')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (82, N'236', N'Narayan Diesels', N'Lasudia Mori	Opp. Hindustan Lever	Dewas Naka	Indor', N'Mr. Prashant Mulchandani', N'09826023523', N'09826023523', N'narayan', N'ASC240', N'Narayandiesels@gmail.com', N'West', N'Madhya Pradesh', N'Indore', N'23AIUPM6659H1ZN', N'23AIUPM6659H1ZN', N'23AIUPM6659H1ZN', CAST(0xAAD80000 AS SmallDateTime), CAST(0xAABA0000 AS SmallDateTime), N'10574', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (83, N'237', N'Saikrupa Automobiles', N'Tawakal Layout,Katol Road,Wadi,Nagpur  - 442 401  ', N'Mr. Kishore Menon', N'07172250749', N'09225231422', N'saikrupa1', N'asc237', N'online18@gmail.com', N'West', N'Maharastra', N'Nagpur', N'MST No. 27540347396V', NULL, N'TIN No. 27540347396C dt.  1.4.', CAST(0x9E010000 AS SmallDateTime), CAST(0x9F6E0000 AS SmallDateTime), N'06608', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (84, N'238', N'Maharashtra Motors', N'Hasham Seth Market,Deepak Chowk,Akola - 444 001.  ', N'Mr. Prashant Rathi', N'2439759', N'9423127732', N'maharashtra', N'asc238', N'online19@gmail.com', N'West', N'Maharastra', N'Akola', N'CST No. 444001/C/149 dt. 24.12', NULL, N'LST No. 444001/S/189 dt. 1.4.9', CAST(0x9E010000 AS SmallDateTime), CAST(0x9F6E0000 AS SmallDateTime), N'06608', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (85, N'239', N'Jay Auto Care', N'Natvar Cotton Press Compound,Press Road,Bhav Nagar', N'Mr. S. G. Patel', N'02782516797', N'02782516797', N'jay', N'asc239', N'online20@gmail.com', N'West', N'Gujarat', N'Bhav Nagar', N'62310', NULL, N'62310', CAST(0x9E010000 AS SmallDateTime), CAST(0x9F6E0000 AS SmallDateTime), N'10574', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (86, N'240', N'Balaji Motors', N'G-22, Swastik Complex,Nr. Matangi Compound, NH No.', N'Mr. Nirankar Sharma', N'09904424998', N'09904424998', N'balaji1', N'asc240', N'online21@gmail.com', N'West', N'Gujarat', N'Ankleshwar', N'29873', NULL, N'29873', CAST(0x9E010000 AS SmallDateTime), CAST(0x9F6E0000 AS SmallDateTime), N'10574', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (87, N'241', N'Somnath Motor Repair', N'Junagadh Higfway  ,Opp. Indian Rayon,Behind Padam ', N'Mr.Bhudar Bhai Patel', N'09825285369', N'09825285369', N'somnath', N'asc241', N'online22@gmail.com', N'West', N'Gujarat', N'Veraval', N'45382', NULL, N'45382', CAST(0x9E010000 AS SmallDateTime), CAST(0x9F6E0000 AS SmallDateTime), N'10574', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (88, N'242', N'Automotive Seva Point(N)', N'Datar Appt, 506, Agra road,Near tractor Centre  ,N', N'Mr. Mayur.Harakhchand sha', N'9422202132', N'9422202132', N'automotive1', N'asc242', N'autosevapoint@rediffmail.com', N'West', N'Maharastra', N'Nashik', N'CST No: 27080259670', NULL, N'27080259670', CAST(0x9E010000 AS SmallDateTime), CAST(0x9F6E0000 AS SmallDateTime), N'12696', 1, N'Partner')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (89, N'243', N'Chhattisgarh Sales & Services', N'Shop No.13,	Sports Complex, T.P.Nagar, Korba (C.G.', N'Mr. Surendra Samad', N'07759245563', N'9424148384', N'chhattisgarh', N'asc243', N'cgsteering@rediffmail.com', N'West', N'Chhattisgarh', N'korba', N'22074604097', N'22BDNPS7190E2Z9', N'11', CAST(0x000B0000 AS SmallDateTime), CAST(0x000B0000 AS SmallDateTime), N'06608', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (90, N'244', N'Neemuch Diesels', N'MHOW Road,Neemuch - 458 441,  Madhya pradesh    ', N'Mr. Rakesh Chadha', N'07423222587', N'9827048870', N'neemuch', N'asc244', N'neemuch.diesels@rediffmail.com', N'West', N'Madhya Pradesh', N'Indore', N'23203200904', NULL, N'23203200904', CAST(0x9E010000 AS SmallDateTime), CAST(0x9F6E0000 AS SmallDateTime), N'10574', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (91, N'245', N'Vinayak Services', N'Near punjab Tyre Opp,Jain petrol pump,Tadibandh,Ra', N'Mr Kulab agrawal', N'09977106662', N'09977106662', N'vinayak', N'asc245', N'online24@gmail.com', N'West', N'Chandigarh', N'Nagpur', N'22091506488', NULL, N'22091506488', CAST(0x9E010000 AS SmallDateTime), CAST(0x9F6E0000 AS SmallDateTime), N'14606', 0, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (92, N'246', N'Genesis Technologies', N'Shop no 21 ,Plot No-87,C&D KWC,Kalamboli,Navi Mumb', N'Mr.P.Arul .Das ', N'9962752165', N'9962752165', N'genesis1', N'asc246', N'online27@gmail.com', N'West', N'Maharastra', N'Navi Mumbai', N'27940304302 C', NULL, N'27940304302 C', CAST(0x9E010000 AS SmallDateTime), CAST(0x9F6E0000 AS SmallDateTime), N'12696', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (93, N'247', N'Heritage Exports', N'Maini Towers,Near Gujarawadi Phata,No-61,Pune Sata', N'Virmeet Singh', N'02065703007', N'454546567', N'heritageexports', N'heritage', N'mainiautoagency@rediffmail.com', N'West', N'Maharastra', N'Khatrej', N'27630080354 C', NULL, N'27630080354 C', CAST(0x9E1F0000 AS SmallDateTime), CAST(0x9F8C0000 AS SmallDateTime), N'12696', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (94, N'301', N'Dey Motor Service', N'106, B. T. Road,Kolkata - 700 035    ', N'Mr. Dilip Kumar Dey', N' 0335770767', N'9831104767', N'dey', N'asc301', N'online.1@gmail.com', N'East', N'West Bengal', N'Kolkata', N'19320623252', NULL, N'19320623252', CAST(0x9E010000 AS SmallDateTime), CAST(0x9F6E0000 AS SmallDateTime), N'06147', 0, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (95, N'303', N'Himatsingka Auto Ent', N'Amingaon - 781 031,Dist. Kamrup,Assam      ', N'Mr. Rajesh Kumar Himatsin', N'0361804230', N'9957184829', N'Himatsingka', N'asc303', N'online.2@gmail.com', N'East', N'Assam', N'Amingaon', N'75439', NULL, N'75439', CAST(0x9E010000 AS SmallDateTime), CAST(0x9F6E0000 AS SmallDateTime), N'06147', 0, N'Partner')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (96, N'305', N'Jupiter Services', N'B. B. College More,  Ushagram,G. T. Road,Asansol –', N'Mr. B. S. Jayant', N'03416531379', N'03416531379', N'jupiter', N'asc305', N'online.3@gmail.com', N'East', N'Bihar', N'Asansol', N'43526', NULL, N'43526', CAST(0x9E010000 AS SmallDateTime), CAST(0x9F6E0000 AS SmallDateTime), N'06147', 0, N'Partner')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (97, N'306', N'Shree Jagannath Moto', N'Nirmal Market Complex,Power House Road  ,Rourkela ', N'Mr. Rajendra Narayan Mish', N'9884763276', N'9884763276', N'Jagannath', N'asc306', N'online.4@gmail.com', N'East', N'Jharkhand', N'Rourkela', N'14536', NULL, N'14536', CAST(0x9E010000 AS SmallDateTime), CAST(0x9F6E0000 AS SmallDateTime), N'1234', 0, N'Partner')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (98, N'307', N'Kartar Motors', N'37/A, Jeet Complex,R Road, Bistupur,  New Kalimati', N'Mr. Hardeep Singh', N'06572425267', N'9334805401', N'kartar', N'asc307', N'online.5@gmail.com', N'East', N'Jharkhand', N'Jamshedpur', N'54673', NULL, N'54673', CAST(0x9E010000 AS SmallDateTime), CAST(0x9F6E0000 AS SmallDateTime), N'14606', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (99, N'308', N'Surjeet Motor Garage', N'National Highway,Gandarpur,Cuttack - 753 003      ', N'Mr. Surjit Singh', N'06712440609', N'9437269922', N'surjeet ', N'asc308', N'online.6@gmail.com', N'East', N'Orissa', N'Cuttack', N'97654', NULL, N'97654', CAST(0x9E010000 AS SmallDateTime), CAST(0x9F6E0000 AS SmallDateTime), N'06808', 1, N'Propreiter')
GO
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (100, N'309', N'Diesel Centre', N'Mahatma Gandhi Setu Road,  Pahari,Patna - 800 008,', N'Mr. Himanshu Ranjan', N'06122367860', N'9934268597', N'diesel', N'asc309', N'online.7@gmail.com', N'East', N'Bihar', N'Patna', N'27645', NULL, N'27645', CAST(0x9E010000 AS SmallDateTime), CAST(0x9F6E0000 AS SmallDateTime), N'14606', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (101, N'310', N'G. D. Motors', N'Chatribari Road,Guwahati - 781 001    ', N'Mr. Savinder Singh Sethi', N'9864031944', N'9435104854', N'gdmotors', N'asc310', N'gdmotors@rediffmail.com', N'East', N'Assam', N'Guwahati', N'GAU-(D) C-198 Dt. 17.4.96', NULL, N'11', CAST(0x9E010000 AS SmallDateTime), CAST(0x9F6E0000 AS SmallDateTime), N'11737', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (102, N'311', N'Mahavir Auto Spare Pvt. Ltd.', N'Opp. N.I.D.C Building  Dharan Road  Mahendra Chowk', N'Mr. Binod Dalmia', N'0097721526469', N'0097721522837', N'mahabir', N'asc311', N'mtcbrt@cworld.com.np', N'East', N'Nepal', N'Birat Nagar - Nepal', N'9999999', NULL, N'9999999', CAST(0xA5230000 AS SmallDateTime), CAST(0xA5050000 AS SmallDateTime), N'11737', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (103, N'312', N'Arrkay Diesel', N'158, Jessore Road,  Sarat Colony,Kolkata - 700 081', N'Mr. Rishi Kr Barui', N'03325145271', N'9830284356', N'arrkay', N'asc312', N'online.9@gmail.com', N'East', N'West Bengal', N'Kolkata', N'Cst 19673405268', NULL, N'Tin: 19673405074', CAST(0xA5230000 AS SmallDateTime), CAST(0xA5050000 AS SmallDateTime), N'08509', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (104, N'313', N'Saharan Engineering Works', N'Sevoke Road  Siliguri - 734 401  West Bengal  ', N'Mr. Kamalesh Saharan', N'03532543860', N'9434021860', N'saharan', N'asc313', N'amargarage@sify.com', N'East', N'West Bengal', N'Siliguri', N'WBST No. 19894149138', NULL, N'CST No. 19894149235', CAST(0x9E010000 AS SmallDateTime), CAST(0x9F6E0000 AS SmallDateTime), N'08509', 1, N'Partner')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (105, N'314', N'Kailash Motors', N'Opp. St. Mary''s  School,  Station Road,  Barbill -', N'Mrs. Poonam Sachdev', N'06572437208', N'9431181265', N'kailash', N'asc314', N'kailash@gmail.com', N'East', N'West Bengal', N'Barbil', N'OST No. KJB-817 dt. 1.2.05', NULL, N'CST No. KJCB-631 dt. 1.2.05', CAST(0x9E010000 AS SmallDateTime), CAST(0x9F6E0000 AS SmallDateTime), N'06808', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (106, N'316', N'Shivam Sales', N'Beside bank of baroda  ratu road  Ranchi - 1    ', N'Mr. Indramani Singh', N'06512284724', N'09431115918', N'shivam', N'asc316', N'aks_sun29@yahoo.co.in', N'East', N'Jharkhand', N'Ranchi', N'CST No: 20170401938', NULL, N'11', CAST(0x000B0000 AS SmallDateTime), CAST(0x000B0000 AS SmallDateTime), N'14606', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (107, N'317', N'Medini Power Steering', N'Khudiram Nagore  Midnapore - 721 101  West Bengal ', N'Parimal Kanti Mondal', N'03222275152', N'9434654140', N'medini', N'asc317', N'medini@gmail.com', N'East', N'West Bengal', N'Midnapore', N'99999999', NULL, N'TIN No. 19842358079', CAST(0x9E010000 AS SmallDateTime), CAST(0x9F6E0000 AS SmallDateTime), N'06808', 0, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (108, N'318', N'Tarini Motors', N'N.H - 6, Sankarpur  Keonjhar - 758 001  Orissa  ', N'Mr. Bijay Kumar Rout', N'09237997508', N'09237997508', N'tarini', N'asc318', N'tariniautomotive@rediffmail.com', N'East', N'Orissa', N'Angul', N'9999999', NULL, N'TIN No. 21211306958', CAST(0x9E010000 AS SmallDateTime), CAST(0x9F6E0000 AS SmallDateTime), N'14606', 0, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (122, N'1004', N'Vikas Mechanical Works', N'Transport nagar,Jammu', N'Vikas Chand', N'NA', N'NA', N'Vikas1004', N'Welcome2u', N'vikasmechanicalworks@gmail.com', N'North', N'Jammu and Kashmir', N'Jammu', N'NA', N'01AWQPC8248J1ZD', N'NA', CAST(0xAA5F0000 AS SmallDateTime), CAST(0xADB30000 AS SmallDateTime), N'10631', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (151, N'397', N'Trinath Motors', N'Rourkela,Sundergarh,Odissa', N'Gangadhar Sahoo', N'NA', N'9937224460', N'Trinath', N'Welcome2u', N'', N'East', N'Odisha', N'Rourkela', N'NA', N'XYZ', N'NA', CAST(0xACC20000 AS SmallDateTime), CAST(0xADD10000 AS SmallDateTime), N'06808', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (163, N'1018', N'Bhumi Turbo', N'261, Pali Road,Jhakha Nagar, Sumerpur', N'Mr.Govind Kumar Suthar', N'NA', N'9950936202', N'Bhumi', N'Welcome2u', N'bhumiturbo@gmail.com', N'North', N'Rajasthan', N'Sumerpur', N'NA', N'08CAMPS4456E1ZB', N'NA', CAST(0xABCD0000 AS SmallDateTime), CAST(0xADD20000 AS SmallDateTime), N'08396', 1, N'Owner')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (109, N'319', N'Chandan Engineering', N'Near Hotel Prasanti  N.H - 42  Turanga  Angul - 75', N'Mr. Ullash Ch. Champatira', N'06764233555', N'06764233444', N'chandan', N'dikshya', N'chandan@gmail.com', N'East', N'Orissa', N'Angul', N'9999999', NULL, N'11', CAST(0x000B0000 AS SmallDateTime), CAST(0x000B0000 AS SmallDateTime), N'06808', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (110, N'320', N'Bharat Automobiles', N'5B/23, Seal''s Garden Lane  Kolkata - 700 002  ', N'Mr. Siddhartha Mondal', N'9331260405', N'9331260405', N'bharat', N'asc320', N'bharat@gmail.com', N'East', N'West Bengal', N'Kolkata', N'99999999', NULL, N'VAT No. 19322325020', CAST(0x9E010000 AS SmallDateTime), CAST(0x9F6E0000 AS SmallDateTime), N'08509', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (111, N'321', N'Malda Auto (P) Limited', N'Malda Auto Complex  Sisir Ganguly Enclave  Gabgach', N'Mr. Subhash Ganguly', N'03512266666', N'03512265666', N'malda', N'ASC321', N'newhonda@sancharnet.in', N'East', N'West Bengal', N'Malda', N'CST No. 19871732244', NULL, N'TIN No. 19871732050', CAST(0x9E010000 AS SmallDateTime), CAST(0x9F6E0000 AS SmallDateTime), N'06808', 0, N'Owner')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (112, N'322', N'Kedia Motors "N" Parts', N'S.H. 10, Kheruwal  Jharsuguda – 768 203  ', N'Mr. Praveen K. Kedia', N'9338820567', N'9437067493', N'kedia', N'asc322', N'kediamotors@yahoo.com', N'East', N'Orissa', N'Jharsuguda', N'9999999', NULL, N'11', CAST(0x9E010000 AS SmallDateTime), CAST(0x9F6E0000 AS SmallDateTime), N'06808', 1, N'Partner')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (113, N'323', N'Sahoo Motors Garage', N'NH-5, Remunai Gulai  Janugunj  Balasore - 756 019.', N'Mr. Mano Ranjan Sahoo', N'06782265008', N'09437058881', N'sahoo', N'asc323', N'sahoo@gmail.com', N'East', N'Orissa', N'Balasore', N'9999999', NULL, N'TIN No. 21441510289', CAST(0x9E010000 AS SmallDateTime), CAST(0x9F6E0000 AS SmallDateTime), N'06808', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (114, N'324', N'Advance Commerce & Engg. Co.', N'Plot No: 236, Laxmi Sagar Chowk,Opp. Andhra Bank, ', N'Mr.Dipak Thakur', N'09437048128', N'09438034467', N'Advance', N'asc324', N'ace_partsindia@rediffmail.com', N'East', N'Orissa', N'Bhubaneswar', N'CUCIE2356 dt 07.01.1998', NULL, N'CUIE-3691 dt 07.01.1998', CAST(0x9E010000 AS SmallDateTime), CAST(0x9F6E0000 AS SmallDateTime), N'06808', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (115, N'325', N'Ratna Motors', N'N.H.28, Bhagwanpur  Muzaffarpur - 842 001  Bihar  ', N'Mr. Rahul Ranjan', N'06212251215', N'06212251215', N'ratna', N'asc325', N'ratna@gmail.com', N'East', N'Bihar', N'Muzaffarpur', N'VAT  / LST No. 10303006030', NULL, N'CST No. 10303007094', CAST(0x9E010000 AS SmallDateTime), CAST(0x9F6E0000 AS SmallDateTime), N'14606', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (116, N'326', N'Anand Motor Sons', N'20, Mangoe Lane  Kolkata - 700 001  ', N'Mr. K.S. Anand', N'03326691888', N'22302399', N'anand', N'block326', N'anandmotorsons@rediffmail.com', N'East', N'West Bengal', N'Kolkata', N' 19470902248', NULL, N'19470902054', CAST(0x9E010000 AS SmallDateTime), CAST(0x9F6E0000 AS SmallDateTime), N'06808', 0, N'Partner')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (117, N'327', N'Khalsa Engineering Works', N'N.H - 2,G.T.ROAD,Near kishan Chowk,BARWADDA MORE ,', N'Mr. BALWINDER SINGH', N'9798003640', N'8092694231', N'Khalsa', N'BLOCKED', N'sukhjinder092@gmail.com', N'East', N'Jharkhand', N'DHANBAD', N'9999999', N'ABCD', N'11', CAST(0x000B0000 AS SmallDateTime), CAST(0x000B0000 AS SmallDateTime), N'06808', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (118, N'328', N'Jharkhand Motors Sales & Service Centre', N'Ranchi - Patna Road  Jhumri Telaiya,  Koderma - 82', N'Mr. shashikanth kumar ', N'09430346252', N'09430346252', N'jharkhand', N'asc328', N'bmservice.jmt@gmail.com', N'East', N'Jharkhand', N'Jhumri telaiya', N'CST No: 20732430191', NULL, N'11', CAST(0x000B0000 AS SmallDateTime), CAST(0x000B0000 AS SmallDateTime), N'14606', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (119, N'329', N'Shri Sidhi Vinayak Enterprises', N'The Felicity Building, Plot No. 567 / 2601	Khata N', N'Mr. Mananjay Choubey', N'09438063631', N'09438063631', N'SSVE329', N'Welcome2u', N'sidhivinayakenterprises942@gmail.com', N'East', N'Orissa', N'Rourkela', N'21AMNPC4757D1ZD', N'21AMNPC4757D1ZD', N'21AMNPC4757D1ZD', CAST(0x9E010000 AS SmallDateTime), CAST(0x9F6E0000 AS SmallDateTime), N'06808', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (120, N'330', N'Ananta Diesel', N'New bus stand road  Ganjam  Behrampur - 760 001  O', N'Mr. Arun kumar panigrahy', N'06802281471', N'9861008204', N'ananta', N'asc330', N'arun.panigrahy67@gmail.com', N'East', N'Orissa', N'Cuttack', N'CST No: 21391902730', NULL, N'9999999', CAST(0x9E010000 AS SmallDateTime), CAST(0x9F6E0000 AS SmallDateTime), N'06808', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (121, N'331', N'Debee Motors', N'N.H 37, Kotokipukhuri Byepass, Jorhat - 785 006  K', N'Mrs.Chinapro Saikia', N'09864097950', N'9864068752', N'debee', N'asc331', N'debeemotors@rediffmail.com', N'East', N'Assam', N'Kolkata', N'CST No: 18439926792', NULL, N'TIN: 18200993262', CAST(0x9E010000 AS SmallDateTime), CAST(0x9F6E0000 AS SmallDateTime), N'11737', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (123, N'401', N'M. K. S. Automotive Pvt. Ltd.', N'Emkeyes Centre  274/560, M. T. H. Road    Ambattur', N'M. K. S. Automotive', N'04426357548  ', N'04426357548 ', N'ASCSOUTH01', N'11520', N'mks@automotive.co.in    ', N'South', N'Tamil NAdu', N'chennai', N'56', NULL, N'55', CAST(0x9E010000 AS SmallDateTime), CAST(0x9F6E0000 AS SmallDateTime), N'03742', 0, N'Owner')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (124, N'402', N'VST Service Station (Salem)', N'114, Salem Road  Namakkal - 637 001   ', N'VST Service Station ', N'04286276894', N'04286276192', N'ASCSOUTH02', N'BLOCKED', N'nkl@vstmotors.co.in', N'South', N'Tamil NAdu', N'Namakkal', N'6', N'ABCD', N'66', CAST(0x9E010000 AS SmallDateTime), CAST(0x9F6E0000 AS SmallDateTime), N'1234', 0, N'Owner')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (125, N'407', N'Autozone Automobiles (P) Ltd.', N'SF No. 149  Mettupalayam Road  (Behind Lotus TVS S', N'Autozone Automobiles ', N'04224383601', N'04222436455', N'ASCSOUTH03', N'BLOCKED', N'spares@autozoneindia.com', N'South', N'Tamil NAdu', N'Coimbatore ', N'66', N'ABCD', N'6', CAST(0x9E010000 AS SmallDateTime), CAST(0x9F6E0000 AS SmallDateTime), N'11691', 0, N'Owner')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (126, N'409', N'TVS Automobile Solutions Private Limited', N'Plot No - B1 & B2, Kappalur Industrial Estate	Ucha', N'G. Srinivasa Raghavan', N'04525356370', N'04522339120  ', N'ASCSOUTH04', N'11563', N'tvsco@vsnl.com, r.kannan.tvsmdu@tvssons.com, k.sri', N'South', N'Tamil NAdu', N'Madurai', N'77', N'33AAGCM0329K1ZM', N'5', CAST(0x9E010000 AS SmallDateTime), CAST(0x9F6E0000 AS SmallDateTime), N'06597', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (127, N'411', N'Auto-Tech Engineering Service', N'5/306, Poonamalle Bye-Pass Road  Seneerkuppam  Che', N'Auto-Tech Engineering Ser', N'04426801532', N'9840157522 ', N'ASCSOUTH05', N'11476', N'atech.ramesh@yahoo.com', N'South', N'Tamil NAdu', N'CHENNAI', N'TNGST No. 1662770/2004-2005', NULL, N'11', CAST(0x000B0000 AS SmallDateTime), CAST(0x000B0000 AS SmallDateTime), N'06597', 1, N'Owner')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (128, N'412', N'Rayalaseema Motors', N'Auto Nagar  National Highways – 7  Hyderabad Road ', N'Rayalaseema Motors', N'0851853842', N'9848253374', N'ASCSOUTH06', N'12345', N'rayalseema@rediffmail.com', N'South', N'Andrapradesh', N'Kurnool', N'KNL/10/1/2389 DTD 2001', NULL, N'11', CAST(0x000B0000 AS SmallDateTime), CAST(0x000B0000 AS SmallDateTime), N'08547', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (129, N'413', N'Sundaram Motors', N'15th Km, Bangalore – Tumkur Road  Peenya Bangalore', N'S. Manigandan', N'984797941', N'98479741', N'ASCSOUTH07', N'11783', N' smwpeenya@sundarammotors.com', N'South', N'Karnataka', N'Bangalore', N'77', NULL, N'7', CAST(0x9E010000 AS SmallDateTime), CAST(0x9F6E0000 AS SmallDateTime), N'08547', 0, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (130, N'414', N'Spectra Cargo Engineers', N'3-9-190, N.H-7 Road  Beside Shabnams (Bharat Petro', N'T. Sivarama krishna', N'04024123366', N'9848023720', N'ASCSOUTH7', N'11550', N'spectracargoengineersl@yahoo.co.in', N'South', N'Andrapradesh', N'Hyderabad', N'APGST No. HYR/01/1/3692/02-03', NULL, N'CST No. HYR/01/1/2598/02-03', CAST(0x9E010000 AS SmallDateTime), CAST(0x9F6E0000 AS SmallDateTime), N'08547', 1, N'Owner')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (131, N'415', N'Hi-Tech Auto Services', N'No. 37, Cinema Nagar Road  Chinneri Vayakkadu  New', N'K. Karthik', N'04272336789', N'9843127219 ', N'ASCSOUTH09', N'11507', N'hitech6789@rediffmail.com', N'South', N'Tamil NAdu', N'Salem', N'LST No. 2842829', NULL, N'CST No. 781380 / 10.4.03', CAST(0x9E010000 AS SmallDateTime), CAST(0x9F6E0000 AS SmallDateTime), N'06597', 1, N'Owner')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (132, N'416', N'Modern Marketing Service', N'26-1-12, Bishan Complex Bowdara Road  Visakhapatna', N'K. Kolluri Naveen Babu', N'08912783624', N'9346717801', N'ASCSOUTH10', N'11518', N'jolly_mms@yahoo.com', N'South', N'Tamil NAdu', N'Visakhapatnam', N'77', NULL, N'TIN NO:28870169204', CAST(0x9E010000 AS SmallDateTime), CAST(0x9F6E0000 AS SmallDateTime), N'08547', 0, N'Owner')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (133, N'417', N'Abdul Khader Jeelani Motor Works', N'Plot No. 177,178,water tank road   Auto Nagar  Raj', N'K.Naveen Babu', N' 08832443323 ', N'9866678699', N'ASCSOUTH11', N'98666', N' akjmw.rjy@gmail.com', N'South', N'Andrapradesh', N'Rajahmundry', N'8', NULL, N'11', CAST(0x000B0000 AS SmallDateTime), CAST(0x000B0000 AS SmallDateTime), N'08547', 1, N'Owner')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (134, N'418', N'Mahindra Auto Sales & Services', N'32/1481-A, Kochappally Road  Near Palarivattom Bye', N'V.Antony Henson', N'04842336061', N' 04842336061 ', N'ASCSOUTH12', N'11525', N'saradyu@satyam.net.in, mahindramaison@yahoo.co.in', N'South', N'Kerala', N'Ernakulam', N'KGST No. 23242165 dt. 25.04.20', NULL, N'TIN No: 32071393732', CAST(0x9E010000 AS SmallDateTime), CAST(0x9F6E0000 AS SmallDateTime), N'11691', 0, N'Owner')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (135, N'420', N'Aramana Industries', N'Shop No. 5/454-D  PHED Road  Erangipalam  Calicut ', N'V.Antony Henson', N' 04953226263', N'9446533550', N'ASCSOUTH14', N'11480', N' aramanaindustries@gmail.com', N'South', N'Kerala', N'Calicut', N'7', NULL, N'7', CAST(0x9E010000 AS SmallDateTime), CAST(0x9F6E0000 AS SmallDateTime), N'10556', 1, N'Owner')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (136, N'421', N'Prasanthi Motors', N'NH-18, Masapeta  Rayachoty - 516 270  Cuddapah Dis', N'K. Kolluri Naveen Babu', N'08561256042', N'9848036648', N'ASCSOUTH15', N'block421', N'prasanti_motors@yahoo.co.in', N'South', N'Andrapradesh', N'Cuddapah Dist', N'APGST No. CTR/06/01/2413/01-02', NULL, N'TIN NO:28380107011', CAST(0x9E010000 AS SmallDateTime), CAST(0x9F6E0000 AS SmallDateTime), N'08547', 0, N'Owner')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (137, N'422', N'Vijayawada Auto Services', N'block no B-37,2nd street,2 nd cross ,autonagar  Au', N'K. Kolluri Naveen Babu', N'08666535202', N'9849186051', N'ASCSOUTH16', N'VASRANE', N'upsaccounts2008@gmail.com', N'South', N'Andrapradesh', N'Vijayawada', N'APGST No. VJA/11/07/2/2571 dt.', NULL, N'11', CAST(0x000B0000 AS SmallDateTime), CAST(0x000B0000 AS SmallDateTime), N'1234', 0, N'Owner')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (138, N'423', N'Rodricks Auto Diesel Service', N'Plot No. 53/3 & 53/4A Meenakalia Road Baikampady  ', N'S. Manigandan', N'08242407563', N'08242407563', N'ASCSOUTH17', N'11540', N'rodie@satyam.net.in', N'South', N'Karnataka', N'Mangalore', N'8', NULL, N'8', CAST(0x9E010000 AS SmallDateTime), CAST(0x9F6E0000 AS SmallDateTime), N'10556', 1, N'Owner')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (139, N'378', N'Ropar Diesel Service', N'NH-6, Bombay-Road, 	P.o-Argori Junglepur,	Howrah- ', N'Amritpal Singh', N'09433001762', N'09433001762', N'RDS378', N'Welcome2u', N'ropardieselservice@gmail.com', N'East', N'West Bengal', N'Howrah', N'19AAQFR6526G1ZP', N'19AAQFR6526G1ZP', N'19AAQFR6526G1ZP', CAST(0xAAD80000 AS SmallDateTime), CAST(0xAABA0000 AS SmallDateTime), N'08509', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (140, N'278', N'Shree Tapi Diesel', N'G-3, Ramkrishna Complex, 	Kholwad, NH-08	
Surat-3', N'Amit Kumar Rai', N'09824060996', N'09725798436', N'std278', N'Welcome2u', N'krishna12027@gmail.com', N'West', N'Gujarat', N'Surat', N'24APOPR4787A1ZL', N'24APOPR4787A1ZL', N'24APOPR4787A1ZL', CAST(0xAAD80000 AS SmallDateTime), CAST(0xADD20000 AS SmallDateTime), N'06608', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (141, N'1001', N'Satnam Singh & Brothers', N'Saharanpur Road	
Opp Power House	
Yamuna Nagar -', N'Mr. Satnam Singh', N'09355534503', N'09355534503', N'ssb1001', N'Welcome2u', N'satnamsinghbrothersynr@gmail.com', N'North', N'Haryana', N'Yamuna Nagar', N'06BHVPS9871E1ZG', N'06BHVPS9871E1ZG', N'06BHVPS9871E1ZG', CAST(0xAAD80000 AS SmallDateTime), CAST(0xAABA0000 AS SmallDateTime), N'08548', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (142, N'376', N'Chhotu Motor', N'Opposite State Bank Of India	
Demotand	Hazaribag ', N'Mr. Mohammad Neyar', N'07488995212', N'07488995212', N'cm376', N'Welcome2u', N'chhotumotor883@gmail.com', N'East', N'Jharkhand', N'Hazaribagh', N'20ALFPN1683A1ZP', N'20ALFPN1683A1ZP', N'20ALFPN1683A1ZP', CAST(0xAAD80000 AS SmallDateTime), CAST(0xAABA0000 AS SmallDateTime), N'14606', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (143, N'371', N'National Highway Diesel', N'Uttar Mechogram	Panskura	
Purba Medinipur - 72113', N'Mr. Partha Mondal', N'09732827942', N'09732827942', N'NHD371', N'Welcome2u', N'bapimondal@yahoomail.com', N'East', N'West Bengal', N'Uttar Mechogram', N'19AYLPM4592N1ZG', N'19AYLPM4592N1ZG', N'19AYLPM4592N1ZG', CAST(0xAAD80000 AS SmallDateTime), CAST(0xAABA0000 AS SmallDateTime), N'08509', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (144, N'377', N'Siddheswari Automobiles & Electrical', N'NH-06, Gopalpalli Chowk	P.O. Remed 	Sambalpur - 76', N'Mr. Anadi Saran Pattnaik', N'07008489528', N'07008489528', N'SAE377', N'Welcome2u', N'Sidheswariautoelectrical@gmail.com', N'East', N'Odisha', N'Sambalpur', N'21BEHPP5764G1ZF', N'21BEHPP5764G1ZF', N'21BEHPP5764G1ZF', CAST(0xAAD80000 AS SmallDateTime), CAST(0xAABA0000 AS SmallDateTime), N'06808', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (145, N'374', N'Vaishno Electronic & Repairing Center', N'NH-102 Mehiyan	Garkha Road	
Near 4 Lane	Chhapra -', N'Birendra Prasad', N'09631790862', N'07903618081', N'VERC374', N'Welcome2u', N'bpccpros@gmail.com', N'East', N'Bihar', N'Chhapra', N'10AXJPP1735D1ZX', N'10AXJPP1735D1ZX', N'10AXJPP1735D1ZX', CAST(0xAAD80000 AS SmallDateTime), CAST(0xAABA0000 AS SmallDateTime), N'14606', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (146, N'276', N'City Automobile', N'01, Anandeshwari Complex	
Doodhtalai 	Ujjiain- 45', N'Mohammed Shoyab', N'09907021608', N'09907021608', N'ca276', N'Welcome2u', N'nagori_aaqib@rediffmail.com', N'West', N'Madhya Pradesh', N'Ujjain', N'23BBYPN8974L1ZH', N'23BBYPN8974L1ZH', N'23BBYPN8974L1ZH', CAST(0xAAD80000 AS SmallDateTime), CAST(0xAABA0000 AS SmallDateTime), N'10574', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (147, N'SE016', N'Ritesh Kumar Singh', N'C/o. Diesel Centre 
Mahatma Gandhi Setu Road 
Pa', N'Ritesh Kumar Singh', N'07470988322', N'07470988322', N'SE016', N'Welcome2u', N's.riteshkumarsingh@ranegroup.com', N'East', N'Bihar', N'Patna', N'1234', N'1234', N'1234', CAST(0xAAD80000 AS SmallDateTime), CAST(0xAABA0000 AS SmallDateTime), N'SE016', 0, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (148, N'382', N'Rajdhani Diesel', N'N.H 37, Near Gurudwara	Kirandeep Building Shop No.', N'Dilbag Singh', N'09945707470', N'08486236447', N'rd382', N'Welcome2u', N'rajdhanid@gmail.com', N'East', N'Assam', N'Guwahati', N'18AFWPN2179Q1Z8', N'18AFWPN2179Q1Z8', N'18AFWPN2179Q1Z8', CAST(0xADB30000 AS SmallDateTime), CAST(0xAD950000 AS SmallDateTime), N'11737', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (149, N'281', N'Maa Banjari Power Steering', N'Before India Oil Petrol Pump,Dewagan Gorkha, JSPL,', N'Mr. Umesh Singh', N'NA', N'9981207875', N'Maa281', N'Welcome2u', N'maabanjaricorrier@gmail.com', N'East', N'Chhattisgarh', N'Raigarh', N'NA', N'22BZVPS6773D1ZS', N'NA', CAST(0xA7A30000 AS SmallDateTime), CAST(0xADB30000 AS SmallDateTime), N'06608', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (150, N'383', N'Super Rider', N'Golapbag,
Saraitikar Road,
PO- Rajbati ,
Dist- ', N'Mr. Shibabrata Mukherjee', N'NA', N'7797800661', N'Super383', N'Welcome2u', N'riderturbocare@gmail.com', N'East', N'West Bengal', N'Burdwan', N'NA', N'19AMAPM1190K1ZX', N'NA', CAST(0xAAD90000 AS SmallDateTime), CAST(0xADB30000 AS SmallDateTime), N'08509', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (152, N'1002', N'Verma and Sons', N'3912, Near Pul Bangush Metro Station	Rashanara Roa', N'Mr. Kulbhushan', N'09958606942', N'09958606942', N'VAS1002', N'Welcome2u', N'vermavrma623@gmail.com', N'North', N'Delhi', N'Delhi', N'07BNWPK5739C1ZL', N'07BNWPK5739C1ZL', N'07BNWPK5739C1ZL', CAST(0xAAD80000 AS SmallDateTime), CAST(0xAABA0000 AS SmallDateTime), N'08548', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (153, N'497', N'Sree Venkateswara Booster & Power Steering Works', N'No. 1/473, GST Road Opp. Vandaloor Railway Station', N'Mr. Jai Sankar', N'09841834777', N'9841834777', N'SVBPSW497', N'wELCOME2U', N'svbp4777@gmail.com', N'South', N'Tamil Nadu', N'Chennai', N'33AGOPJ8518L1ZX', N'33AGOPJ8518L1ZX', N'33AGOPJ8518L1ZX', CAST(0xAAD80000 AS SmallDateTime), CAST(0xAAC00000 AS SmallDateTime), N'06597', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (154, N'282', N'Shri G.S. Motors', N'Krishi Upaj Mandi road,Betul,Baroda- 460001,Madhya', N'Mr Ravi', N'NA', N'9925874060', N'GS282', N'Welcome2u', N'ravi.sagar1904@gmail.com', N'West', N'Madhya Pradesh', N'Betul', N'NA', N'23BJTPK9322G2ZY', N'NA', CAST(0xAABB0000 AS SmallDateTime), CAST(0xADF00000 AS SmallDateTime), N'10574', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (155, N'1009', N'Vikash Air Pressure', N'Bypass Road, Near Neelkanth Hotel,
Jhajjar - 1241', N'Mr. Vikash Kaushik', N'NA', N'9910338830 / 8930204488', N'Vikash', N'Welcome2u', N'Vikashass123@gmail.com', N'North', N'Haryana', N'Jhajjar', N'NA', N'06DJIPK6484A1Z7', N'NA', CAST(0xAAF80000 AS SmallDateTime), CAST(0xAF200000 AS SmallDateTime), N'08548', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (156, N'1010', N'Shri Sagun Tradings', N'Post.Chorsand, Jaunpur-Azamgarh Road,Jaunpur-22213', N'Mr. Amit Handa', N'NA', N'8601251073', N'Sagun', N'Welcome2u', N'swaraj.jais99@gmail.com', N'North', N'Uttar Pradesh', N'Jaunpur', N'NA', N'09BBLPJ8374F1Z7', N'NA', CAST(0xAB4A0000 AS SmallDateTime), CAST(0xADB30000 AS SmallDateTime), N'12485', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (157, N'1016', N'Balaji Automobiles', N'Khasra No-1778,Dhoom Manikpur-Village,Dadri,    
', N'Mr Jaipal Singh', N'NA', N'9354162382', N'Balajiauto', N'Welcome2u', N'automobilesdadri@gmail.com', N'North', N'Uttar Pradesh', N'Dadri', N'NA', N'09AAVFB1661P1Z0', N'NA', CAST(0xAB900000 AS SmallDateTime), CAST(0xADB30000 AS SmallDateTime), N'12696', 1, N'Partner')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (158, N'391', N'Kumar Automobiles', N'Near Mahesh Petrol Pump, Pantasang, Moratalab(Nala', N'Mr.Amit Kumar', N'NA', N'7870939106', N'Kumarauto', N'Welcome2u', N'kumarautomobiles2018@gmail.com', N'East', N'Bihar', N'Biharsharif', N'NA', N'10GAJPK7787M1Z0', N'NA', CAST(0xAB350000 AS SmallDateTime), CAST(0xADD20000 AS SmallDateTime), NULL, 0, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (159, N'279', N'Shivam Motors', N'Shop No.4, 5&6 Gourishankar Heights ,Satara-415 00', N'Mr. Madhav Digambar Jadha', N'NA', N'7350058100', N'Shivammotors', N'Welcome2u', N'rndjadhav27@gmail.com', N'West', N'Maharashtra', N'Satara', N'NA', N'27BFOPJ0948G1Z8', N'NA', CAST(0xAAD80000 AS SmallDateTime), CAST(0xADB30000 AS SmallDateTime), N'12696', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (160, N'395', N'Mahakali Automobiles Pvt.Ltd', N'Madhepura,NH-107 Road,NearVastu vihar,Saharsa', N'Mr Sanjeet Kumar Suman', N'NA', N'8709112284', N'Mahakali', N'Welcome2u', N'ss@mahakaligroup.in', N'East', N'Bihar', N'Saharsa', N'NA', N'10AAKCM4417G1ZU', N'NA', CAST(0xADB30000 AS SmallDateTime), CAST(0xADB40000 AS SmallDateTime), N'14606', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (161, N'1019', N'Sai Ancillaries', N'Hanuman Electronic Kanta,Near Balaji Temple, Makra', N'Mr. Samridhi Sharma', N'NA', N'9983348001', N'Sai1019', N'Welcome2u', N'saiancillaries@gmail.com', N'North', N'Rajasthan', N'Kishangarh', N'NA', N'08BSBPS4642E1ZQ', N'NA', CAST(0xABEB0000 AS SmallDateTime), CAST(0xADB30000 AS SmallDateTime), N'08396', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (162, N'280', N'Indore Diesels', N'296, 9,Transport Nagar Main Road		Indore - 452 001', N'Mohammed Sabir Sheikh', N'09827786786', N'09827786786', N'id280', N'Welcome2u', N'Indorediesel@gmail.com', N'West', N'Madhya Pradesh', N'Indore', N'23AEAPS5119L2ZF', N'23AEAPS5119L2ZF', N'23AEAPS5119L2ZF', CAST(0xAAD80000 AS SmallDateTime), CAST(0xAABA0000 AS SmallDateTime), N'10574', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (164, N'1006', N'Irfan Auto Garage', N'Ratanpur
Tedvi
Sultanpur - 228 001	Uttar Pradesh', N'Mohmmad Irfan', N'09918240196', N'09793098741', N'IAG1006', N'Welcome2u', N'imranautogaraj@gmail.com', N'North', N'Uttar Pradesh', N'Sultanpur', N'09AFOPI5121B1ZR', N'09AFOPI5121B1ZR', N'09AFOPI5121B1ZR', CAST(0xADB30000 AS SmallDateTime), CAST(0xAD950000 AS SmallDateTime), N'12485', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (165, N'184', N'R. K. Motor', N'Siroli Kalan,Opp.Daroga Market, Bareilly Road,Kich', N'Mr.Rakesh Kumar', N'NA', N'087550-49503', N'RK184', N'Welcome2u', N'rkmotorworks1986@gmail.com', N'North', N'Uttarakhand', N'Kitcha', N'NA', N'05CNYPK1961K1ZD', N'NA', CAST(0xA98B0000 AS SmallDateTime), CAST(0xADB30000 AS SmallDateTime), NULL, 0, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (166, N'1005', N'Shree Gee Industries', N'B-50, Bhopa Road,Transport Nagar, Muzaffarnagar -2', N'Mr. Sambhav Jain', N'NA', N'9997965556', N'Shree', N'Welcome2u', N'atinjain47@gmail.com', N'North', N'Uttar Pradesh', N'Muzaffarnagar', N'NA', N'09AQIPJ2576Q1Z1', N'NA', CAST(0xAA5F0000 AS SmallDateTime), CAST(0xADB30000 AS SmallDateTime), N'08548', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (167, N'4001', N'Hemant Trading Company', N'Auto Complex, Sagar Road,Shivamogga - 577 204', N'Mr. Amrit Baliga', N'NA', N' 08182222545', N'Hemanth', N'Welcome2u', N'hemanthtradingco@yahoo.co.in', N'South', N'Karnataka', N'Shivamogga', N'NA', N'29AAAFH6216K1ZA', N'NA', CAST(0xAB160000 AS SmallDateTime), CAST(0xADB30000 AS SmallDateTime), N'10556', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (168, N'1011', N'Shiva Diesel Service', N'40, Nehru Market, Near Central Bank of India, Bada', N'Mr. Amit Handa', N'NA', N'9911278006', N'Shiva1011', N'Welcome2u', N'shivadiesel199@gmail.com', N'North', N'Delhi', N'badarpur', N'NA', N'07ABAPH5729c1Z2', N'NA', CAST(0xABAE0000 AS SmallDateTime), CAST(0xADB30000 AS SmallDateTime), N'08548', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (169, N'368', N'Laxmi Diesel', N'Chanpur(Near Sherowali Himghar), P.o. Khayerpur, A', N'Mr. Litan Debnath', N'NA', N'Mobile : 098625-23836', N'Laxmi368', N'Welcome2u', N'mslaxmi009@gmail.com', N'East', N'Tripura', N'Agartala', N'NA', N'16AAHFL0388B1ZK', N'NA', CAST(0xABAE0000 AS SmallDateTime), CAST(0xADB30000 AS SmallDateTime), N'11737', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (170, N'384', N'New Bittu Enterprise', N'NH- 34,  Ranaghat, Nadia,Habibpur-741 402, West Be', N'Gosh', N'00', N'9997965556', N'Bittu', N'Welcome2u', N'newbittu.enterprise2015@gmail.com', N'East', N'West Bengal', N'Habibpur', N'NA', N'19AKTPG4678D1ZO', N'NA', CAST(0xAAD90000 AS SmallDateTime), CAST(0xADF00000 AS SmallDateTime), N'08509', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (171, N'480', N'Raja Auto Agency', N'209, S. P. Kannusamy Gounder Streeet	Sanganur Main', N'C. P. Rajaram', N'04226549767', N'9843019789', N'raa480', N'Welcome2u', N'cprcbe@yahoo.co.in', N'South', N'Tamil Nadu', N'Coimbatore', N'33AADFR1898L1ZS', N'33AADFR1898L1ZS', N'33AADFR1898L1ZS', CAST(0xAD950000 AS SmallDateTime), CAST(0xAD770000 AS SmallDateTime), NULL, 0, N'Partner')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (172, N'390', N'Arun Motors', N'Hasdiha,Near St. Teressa School, Bhagalpur - 81200', N'Arun Kumar', N'NA', N'9431609770', N'Arun', N'Welcome2u', N'akumar.vikramshila@gmail.com', N'East', N'Bihar', N'Bhagalpur', N'NA', N'10ELCPK9899E2ZT', N'NA', CAST(0xAB540000 AS SmallDateTime), CAST(0xADB30000 AS SmallDateTime), N'14606', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (173, N'1013', N'Shri Balaji Turbo Center', N'Village Bhuppur, Nahan Road,Paonta Sahib,Sirmour ', N'Mr. Triloki', N'NA', N'8077337311', N'NASC1013', N'Welcome2u', N'shribalajiturbocenter@gmail.com', N'North', N'Himachal Pradesh', N'Paonta Sahib', N'NA', N'02AJXPT6224H1ZU', N'NA', CAST(0xADB30000 AS SmallDateTime), CAST(0xADB40000 AS SmallDateTime), N'10631', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (174, N'392', N'Mani Motors', N'Bhairo Kothi,4lane Sonbarsa Road,Sitamarhi - 84330', N'Mr. Manish Ranjan', N'NA', N'7979065332', N'Mani', N'Welcome2u', N'ranjanmanish3123@gmail.com', N'East', N'Bihar', N'Sitamarhi', N'NA', N'10BWQPS4681A1ZJ', N'NA', CAST(0xAB540000 AS SmallDateTime), CAST(0xADB30000 AS SmallDateTime), N'14606', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (175, N'1015', N'Balaji Motors Jaipur', N'Shop No. 03, Near Badarna Bridge, Road No.14, Delh', N'Mr Jagdish Prasad Gurjar', N'NA', N'9928555807', N'ASC1015', N'Welcome2u', N'bmj.wabcorane@gmail.com', N'North', N'Rajasthan', N'Jaipur', N'NA', N'08CKBPG6493E1Z4', N'NA', CAST(0xAB710000 AS SmallDateTime), CAST(0xADB30000 AS SmallDateTime), N'08396', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (176, N'1012', N'Mohan Arts & Auto', N'Raghubeer Niwas,Near Central Bank of India, Civil ', N'Mrs. Swati Mohan', N'NA', N'9161722200', N'Mohanarts', N'Welcome2u', N'Mohanartsauto@gmail.com', N'North', N'Uttar Pradesh', N'Faizabad', N'NA', N'09ACEPM6997Q1ZE', N'NA', CAST(0xAB710000 AS SmallDateTime), CAST(0xADB30000 AS SmallDateTime), N'12485', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (177, N'424', N'Essarr Automotives', N'19/1-2, 1st Floor  H. Siddiah Road  Bangalore - 56', N'S. Manigandan', N'22224279', N'08041144809', N'ASCSOUTH18', N'11501', N' vijayadsls@yahoo.com', N'South', N'Karnataka', N'Bangalore', N'CST No. 92358883 dt. 15-5-2002', NULL, N'KST No. 92308880 dt. 15-5-2002', CAST(0x9E010000 AS SmallDateTime), CAST(0x9F6E0000 AS SmallDateTime), N'hjjjjh', 1, N'Owner')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (178, N'425', N'The Modern Auto Diesel Works', N'P. B. No. 29, M. S. R. Buildings  Chittoor - 517 0', N'K. Karthik', N'08572231185', N'9848148042', N'ASCSOUTH19', N'them', N'madwctr@gmail.com', N'South', N'Andrapradesh', N'Chittoor', N'APGST No. CTR/03/1/1034', NULL, N'11', CAST(0x000B0000 AS SmallDateTime), CAST(0x000B0000 AS SmallDateTime), N'08547', 1, N'Owner')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (179, N'428', N'Vijaya Motor Sales & Service', N'"Vijaya Complex" Bypass Road  Urgadur Shimoga - 57', N'A. Arjunan', N'08182242727', N'08182242747', N'ASCSOUTH21', N'11570', N'vijayams@sancharnet.in', N'South', N'Karnataka', N'Shimoga ', N'7', NULL, N'7', CAST(0x9E010000 AS SmallDateTime), CAST(0x9F6E0000 AS SmallDateTime), N'1234', 1, N'Owner')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (180, N'429', N'Srii Balaji Automobiles', N'6/5, NGL Bye-Pass Road Near Vignesh Car Alignments', N'V.S.Selvakumar', N' 0462 - 2586586', N'98421 - 64234', N'ASCSOUTH22', N'raneasc', N'sriibalajirasc@yahoo.com', N'South', N'Tamil NAdu', N'Tirunelveli', N'6', NULL, N'11', CAST(0x000B0000 AS SmallDateTime), CAST(0x000B0000 AS SmallDateTime), N'06597', 1, N'Partner')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (181, N'433', N'Janatha Auto Works', N'Harihara Road  Near RTO Office  Hospet - 583 201  ', N'A. Arjunan', N'09845601443', N'09845942596', N'ASCSOUTH24', N'01443', N'jaw.hpt@gmail.com', N'South', N'Karnataka', N'Hospet', N'7', NULL, N'11', CAST(0x000B0000 AS SmallDateTime), CAST(0x000B0000 AS SmallDateTime), N'10556', 1, N'Owner')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (182, N'434', N'P.Spectra Automobile', N'No. 69, Sony Business Complex  Prashanthi Nagar  I', N'T. Sivarama krishna', N'04023073970', N'9440050579', N'ASCSOUTH25', N'11693', N'spectra_auto@yahoo.com', N'South', N'Andrapradesh', N'Hyderabad', N'5', NULL, N'11', CAST(0x000B0000 AS SmallDateTime), CAST(0x000B0000 AS SmallDateTime), N'08547', 1, N'Owner')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (183, N'435', N'Vishaal Enterprises', N'30 & 30A, Jeeva Street,Chennai Bye Pass Road,TVS T', N'Mrs. Anita Giridharan', N'04312333502', N'546546546', N'Vishaal', N'vishaal', N'diesels@eth.net', N'South', N'Tamil NAdu', N'Trichy', N'33373563086', NULL, N'33373563086', CAST(0x9E1F0000 AS SmallDateTime), CAST(0x9F8C0000 AS SmallDateTime), N'06597', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (184, N'436', N'Akshay Motors', N'Mahalaxmi BuildingOpp. Unakal Bus Stop  Unakal Hub', N'RAJU', N'09980521815', N' 09980521815', N'ASCSOUTH26', N'RANETRWHBL', N' akshaymotors.hubli@gmail.com', N'South', N'Karnataka', N'Hubli', N'7', NULL, N'11', CAST(0x000B0000 AS SmallDateTime), CAST(0x000B0000 AS SmallDateTime), N'10556', 1, N'Partner')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (185, N'437', N'Akshay Motors', N'Plot No. 4158 / 7, Rukmini Vihar  5th Cross  Kaple', N'A. Arjunan', N' 9845205308', N' 9845205308', N'ASCSOUTH27', N'ranetrwbgm', N' akshaymotors.bgm@gmail.com', N'South', N'Karnataka', N'Belgaum', N'8', NULL, N'11', CAST(0x000B0000 AS SmallDateTime), CAST(0x000B0000 AS SmallDateTime), N'10556', 1, N'Owner')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (186, N'438', N'East Coast Auto Services', N'2/370, Bye Pass Road Red Hills  Chennai - 600 052 ', N'Mr. C. Suresh', N'04426310212', N'9841222654', N'East Coast Auto Services', N'abc123', N'Ponmozhiecas@Gmail.Com', N'South', N'Tamil NAdu', N'Chennai', N'Cst No. 691896 Dt. 26.09.96', NULL, N'TIN No.  33361081589', CAST(0xA2490000 AS SmallDateTime), CAST(0xA2490000 AS SmallDateTime), N'06597', 1, N'Owner')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (187, N'439', N'Lavanya Automobiles', N'Arasan Diesel Complex 2/301 E, Sipcot Bye - Pass R', N'S. Seetharaman', N' 9894623494', N' 9894623494', N'ASCSOUTH29', N'raneasc', N' lavanyaautorasc@yahoo.com', N'South', N'Tamil Nadu', N'Tuticorin', N'7', NULL, N'11', CAST(0x000B0000 AS SmallDateTime), CAST(0x000B0000 AS SmallDateTime), N'06597', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (188, N'440', N'Lucas Indian Service Limited', N'Shed No. 5, Plot No. 290, Rajiv Gandhi Nagar  Hosu', N'S. Manigandan', N'08041691666', N'08041691666', N'ASCSOUTH30', N'13703', N' lisblr@lissouth.com', N'South', N'Karnataka', N'Bangalore', N'CST No. 00250582, LST No. 2005', NULL, N'TIN No. 29390081229', CAST(0x9E010000 AS SmallDateTime), CAST(0x9F6E0000 AS SmallDateTime), N'08547', 0, N'Owner')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (189, N'441', N'Motor India Services', N'No. 1, Ayyanar Koil Street  Raja Nagar (Near Bus S', N'K. Kanaga Sudhkar', N'08608886644', N'08608886644', N'mis441', N'Welcome2u', N'test@gmail.com', N'South', N'Pondicherry', N'Pondicherry', N'34DKOPK7204R1ZH', N'34DKOPK7204R1ZH', N'34DKOPK7204R1ZH', CAST(0xAAD80000 AS SmallDateTime), CAST(0xAABA0000 AS SmallDateTime), N'06597', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (190, N'442', N'BMW Engineering Works', N'Plot No. 15, Auto Nagar  Throvagunta  Ongole - 523', N'T. Sivarama krishna', N'08592', N'9849498110', N'ASCSOUTH32', N'krishna', N' bmwongle@yahoo.co.in', N'South', N'Andrapradesh', N'Ongole', N'TIN No. 28746009352', NULL, N'APGST No. NRE/03/01/3706/2003-', CAST(0x9E010000 AS SmallDateTime), CAST(0x9F6E0000 AS SmallDateTime), N'08547', 1, N'Owner')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (191, N'444', N'Best Automotive', N'302 B, Trivandrum Main Road  Vettoornimadam Post, ', N'V.Antony Henson', N'96777-64234', N'96777-64234', N'ASCSOUTH33', N'raneasc', N'bestautorasc@yahoo.com', N'South', N'Tamil NAdu', N'Nagercoil', N'TN/CST No. 828865', NULL, N'11', CAST(0x000B0000 AS SmallDateTime), CAST(0x000B0000 AS SmallDateTime), N'06597', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (192, N'445', N'Arjuna Auto Care', N'Near SBT-TC-48- 4/10,11,12  NH - 47 By Pass Road, ', N'V.Antony Henson', N' 09443432789', N'09443432789', N'ASCSOUTH34', N'14540', N' arjunaautocare@yahoo.com', N'South', N'Kerala', N'Thirvandrum ', N'5', NULL, N'TIN NO: 33906183492', CAST(0x9E010000 AS SmallDateTime), CAST(0x9F6E0000 AS SmallDateTime), N'11691', 0, N'Owner')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (193, N'446', N'Sri Balaji Automobil', N'12.Thangam Motors Opp Side,Fathimanagar,Madurai-62', N'V.S.Selvakumar', N'09448439507', N'09448439507', N'sribalaji', N'sribalajimdu', N'tests@gm.com', N'South', N'Karnataka', N'Madurai', N'29070575911', NULL, N'11', CAST(0x000B0000 AS SmallDateTime), CAST(0x000B0000 AS SmallDateTime), N'06597', 1, N'Owner')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (194, N'447', N'New Karnataka Diesels', N'Challakere Road,Chitradurga.', N'Muzammil Nawaz', N'09448439507', N'09448439507', N'newkarnataka', N'newkarnataka', N'testkarnataka@rane.in', N'South', N'Karnataka', N'Chitradurga', N'29070575911', NULL, N'29070575911', CAST(0x9E1F0000 AS SmallDateTime), CAST(0x9F8C0000 AS SmallDateTime), N'08566', 0, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (195, N'448', N'Abdul Khadar Jilani Motor Works', N'DR No.39T26-1, NH-5 Highway,Punjab Hotel Junction,', N'Mr.Shaik Afrose', N'08916565656', N'9600854823', N'abdulkhadar', N'abdulkhadar', N'test@gmail.cpm', N'South', N'Andrapradesh', N'Visakhapatnam', N'29070575911', NULL, N'29070575911', CAST(0x9E1F0000 AS SmallDateTime), CAST(0x9F8C0000 AS SmallDateTime), N'08547', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (196, N'449', N'W.Spectra Automobile', N'Plot No B-33(Part)  Kakatiya Auto nagar  Warangal ', N'T. Sivarama krishna', N'04023073970', N'9440050579', N'Spectrawarangal', N'99999', N'spectra_auto@yahoo.com', N'South', N'Andrapradesh', N'Warrangal', N'29720490048', NULL, N'11', CAST(0x000B0000 AS SmallDateTime), CAST(0x000B0000 AS SmallDateTime), N'08547', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (197, N'333', N'Kusum Engineering Works', N'Chandrakona Road, Satbankura
Paschim, Medinipiur
', N'Mrs.Jamuna Lodha', N'9434389659', N'9434389659', N'KEW333', N'kusum', N'kgp.munnal@yahoo.co.in', N'East', N'West Bengal', N'Medinipur', N'19843005015', NULL, N'19843005015', CAST(0xA2490000 AS SmallDateTime), CAST(0xA24A0000 AS SmallDateTime), N'08509', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (198, N'451', N'B.Spectra Automobile', N'Dr.B.R.Ambedkar Street,
8th main hosur road,
Bom', N'K.Srinivas', N'9440050579', N'9440050579', N'spectra', N'spectra', N'spectra_auto@yahoo.com', N'South', N'Karnataka', N'Bangalore', N'AAMFM9710HSD002', NULL, N'29080625230', CAST(0xA2490000 AS SmallDateTime), CAST(0xA24A0000 AS SmallDateTime), N'10556', 1, N'Partner')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (199, N'158', N'Janta Sales & Service Centre', N'614, Harbans Nagar
Delhi Meerut Road
Ghaziabad
', N'Gaurav Sharma', N'9891078281', N'9891078281', N'Janta', N'123456', N'sharmas86@yahoo.com', N'North', N'Uttar Pradesh', N'Ghaziabad', N'''09288815803', NULL, N'''09288815803', CAST(0xA2490000 AS SmallDateTime), CAST(0xA2490000 AS SmallDateTime), N'08548', 1, N'Partner')
GO
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (200, N'334', N'R.S.Turbo Charger', N'N.H.31, Opp. Gyan Bharti School,
Begusarai - 851 ', N'Om prakash kumar', N'9386463401', N'9386463401', N'RSTC', N'15301', N'omprakash@rediffmail.com', N'East', N'Bihar', N'Begusarai', N'10364029185', NULL, N'10364029185', CAST(0xA2490000 AS SmallDateTime), CAST(0xA2490000 AS SmallDateTime), N'14606', 1, N'Partner')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (201, N'335', N'Rohtas Car Care', N'G.T.Road
Dillian Sasaram Rohtas
Bihar - 821 115', N'SUNIL KUMAR', N'7631990867', N'7631990867', N'Rohtas', N'rdiesels', N'rohtascarcare@gmail.com', N'East', N'Bihar', N'Rohtas', N'10248670026', NULL, N'11', CAST(0x000B0000 AS SmallDateTime), CAST(0x000B0000 AS SmallDateTime), N'14606', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (202, N'452', N'V.Spectra Automobile', N'C/o.Madhu Nillayam, Plot No.71&72
, Door No: 54-8', N'Mr.Kathi Srinivas', N'9440050579', N'9440050579', N'V.Spectra', N'Spectra@123', N'spectra_auto@yahoo.com', N'South', N'Andrapradesh', N'Vijayawada', N' AAMFM9710HSD002', NULL, N'Tin no: 29080625230', CAST(0xA2490000 AS SmallDateTime), CAST(0xA2490000 AS SmallDateTime), N'08547', 1, N'Partner')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (203, N'450', N'Mahindra Auto Service and Hydraulics', N'Door No.VIII/177, Dinkha Bhavan,
Anchagadi, East ', N'M.J.Maison', N'04842424011', N'04842424011', N'mahindra', N'mahindra@123', N'mahindraautoservicetcr@gmail.com', N'South', N'Kerala', N'Thrissur', N'32080597694C', NULL, N'32080597694', CAST(0xA2490000 AS SmallDateTime), CAST(0xA2490000 AS SmallDateTime), N'10556', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (204, N'159', N'Shri Ram Air Brakes', N'B-2/8, Near Purani Saria Mill,Old Bhiwani Road,Roh', N'Bhim Singh', N'9812577088', N'9812577088', N'Shri Ram Air Brakes', N'abc@123', N'bhimwabco@yahoo.com', N'North', N'Haryana', N'Rohtak', N'123456', NULL, N'123456', CAST(0xA2490000 AS SmallDateTime), CAST(0xA2490000 AS SmallDateTime), N'08548', 0, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (205, N'160', N'Shree Sainath Motors', N'460, Transport Nagar,

Gobaria Bavdi,

Kota (R', N'Ramesh Rajbhardwaj', N'9784611697', N'9784611697', N'SSMotors', N'abc123', N'rameshbhardwaj8@gmail.com ', N'North', N'Rajastan', N'Kota', N'08782958866', N'08ETIPS9350C1ZB', N'08782958866', CAST(0xA2490000 AS SmallDateTime), CAST(0xA2490000 AS SmallDateTime), N'08396', 1, N'Owner')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (206, N'418.', N'Swan Engineering & Hydraulics', N'Door No. 49/1157-A , Raghavan Pillai Road,Edapilly', N'Sherine Jose', N'04842346474', N'04842346474', N'SHERINE JOSE', N'9387', N'sehekm@gmail.com', N'South', N'Kerala', N'Edapilly', N'32071221292C', NULL, N'32071221292', CAST(0xA2490000 AS SmallDateTime), CAST(0xA24A0000 AS SmallDateTime), N'11691', 0, N'Owner')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (207, N'332', N'Moulana Turbo Engine', N'Sonai Road Nagatila BRTF Lane, Silchar -788006, As', N'Mr.Sayed Ahmed Laskar', N'9435071125', N'9435071125', N'Moulana Turbo Engineering', N'PASSWORD', N'asc.silcharassam@gmail.com', N'East', N'Assam', N'Silchar', N'18799928665', NULL, N'11', CAST(0x000B0000 AS SmallDateTime), CAST(0x000B0000 AS SmallDateTime), N'11737', 1, N'Owner')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (208, N'165', N'Neelkanth Turbo', N'Maruti Factory Gate No.2,Near Shani Temple,Katarpu', N'Mr.Ravi Chand', N'9313983599', N'9313983599', N'Neelkanth Turbo', N'123abc', N'vipinkumar.shukla@rane.co.in', N'North', N'Haryana', N'Gurgaon', N'14', NULL, N'14', CAST(0xA2490000 AS SmallDateTime), CAST(0xA24A0000 AS SmallDateTime), N'08548', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (209, N'167', N'K.L.Motor Works', N'A-121, Transport Nagar,Bareilly,
UttarPradesh - 2', N'Mr.Khunni Lal', N'9412293720', N'9412293720', N'K.L.Motor Works', N'abc123', N'vipinkumar.shukla@rane.co.in', N'North', N'Uttar Pradesh', N'Bareilly', N'09207304756', NULL, N'09207304756', CAST(0xA2490000 AS SmallDateTime), CAST(0xA24A0000 AS SmallDateTime), N'12485', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (210, N'454', N'Steeright', N'1ST Floor, Apollo Engineering,N.H.66, Ambalpady,Ud', N'Mr.Amit Padukone', N' 9986020110', N' 9986020110', N'Steeright', N'myth2412', N'amit.padukone86@gmail.com', N'South', N'Karnataka', N'Karnataka', N'Tin No: 29610598021', NULL, N'11', CAST(0x000B0000 AS SmallDateTime), CAST(0x000B0000 AS SmallDateTime), N'10556', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (211, N'166', N'Transport Diesels & Electricals', N'S-93, Transport Nagar,Kanpur Road, ( Near RTO)Luck', N'Mr.Abdul Mazid', N'052224331155', N'9335052435', N'Abdul Mazid', N'15846', N'tde_mazid@yahoo.co.in', N'North', N'Uttar Pradesh', N'Lucknow', N'Tin No: 09152002300', NULL, N'Tin No: 09152002300', CAST(0xA2490000 AS SmallDateTime), CAST(0xA2490000 AS SmallDateTime), N'12485', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (212, N'249', N'Shri Auto Care', N'Shop No.105/25, Ameya Apartments, Agalli - Fatoda,', N'Shri auto care', N'083222741824', N'08322741811', N'Shri Auto Care', N'abc123', N'sacgoa@yahoo.com', N'West', N'Goa', N'Mumbai', N'M/6599 Dt: 01.04.99', NULL, N'30611106333', CAST(0xA2490000 AS SmallDateTime), CAST(0xA2490000 AS SmallDateTime), N'12696', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (213, N'250', N'CHHATTISGARH AUTO CENTRE', N'P B Complex	Maharana Pratap Chowk, Raipur Road,	Bi', N'Mr.G.Murali Dhar Reddy', N'07752644478', N'9826758186', N'cac250', N'Reddy', N'cgautocentre@gmail.com', N'West', N'Chhattisgarh', N'Bilaspur', N'22164104740 ', N'22AXEPR6447H1ZC', N'11', CAST(0x000B0000 AS SmallDateTime), CAST(0x000B0000 AS SmallDateTime), N'06608', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (214, N'168', N'S.G.Associates Pvt.Ltd.,', N'C/o. Sandeep Motors,
Putholi, Chanderia, NH.79,
', N'Mr.Sandeep Rathi', N'9413357393', N'9413314416', N'S.G.Associates', N'abc123', N'moh_420@rediffmail.com', N'North', N'Rajastan', N'Chittorgarh', N'08361007166', NULL, N'08361007166', CAST(0xA2490000 AS SmallDateTime), CAST(0xA2490000 AS SmallDateTime), N'08396', 1, N'Owner')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (215, N'163', N'Anuradha Sales & Service Centre', N'N.H.-8, Main Market, Opp.Hanuman Mandir,Near Major', N'Mr.Sham Sunder Vats', N'9812674488', N'9812674488', N'Anuradha163', N'Welcome2u', N'Wabcoassc.Salesservice@Gmail.Com', N'North', N'Haryana', N'Gurgaon', N'Cst: 06452708190', NULL, N'Cst: 06452708190', CAST(0xA7FE0000 AS SmallDateTime), CAST(0xA7E00000 AS SmallDateTime), N'08548', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (216, N'455', N'M.S.Auto Tech Service', N'No. 5914/3, TVS Corner, 
Kalif Nagar	Near PNC Aut', N'C.Murugesan', N'9942433999', N'9942433999', N'M.S.Auto Tech Service', N'abc123', N'Msautotechservice@Gmail.Com', N'South', N'Tamil NAdu', N'Pudukottai', N'33ARSPM5892P1ZO', N'33ARSPM5892P1ZO', N'33ARSPM5892P1ZO', CAST(0xAAD80000 AS SmallDateTime), CAST(0xAABA0000 AS SmallDateTime), N'06597', 1, N'Partner')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (217, N'336', N'Balaji Sales Corporation', N'Jayashree Building
M.G.Road
Jaigaon
Dist. Jalpa', N'Mr.Ashok Jain', N'9679140186', N'9679140186', N'Ashok Jain', N'abc123', N'Bsc.Ashokjain@Gmail.Com', N'East', N'West Bengal', N'Jalpaiguri', N'19822343239', NULL, N'19822343045', CAST(0xA5230000 AS SmallDateTime), CAST(0xA5240000 AS SmallDateTime), N'11737', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (218, N'456', N'Burma Automotives Pvt .Ltd.', N'39/A 1st Main Road, 2nd Stage
Yashavanthapura, DD', N'Mr.Rajen.K.Bhatia', N'08041216176', N'08041216176', N'Burma Automitives', N'abc123', N'Burmacorp@Gmail.Com', N'South', N'Karnataka', N'Bangalore', N'Tin No :29740618465', NULL, N'Tin No :29740618465', CAST(0xA5230000 AS SmallDateTime), CAST(0xA5050000 AS SmallDateTime), N'10556', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (219, N'453', N' Spectra Koda', N'No. 4-81/A,Gudibanda Road	Suryapet,Kodad, 	Dist. T', N'Mr. S. Balasubrahmanyam', N'09885295118', N'9246833902', N'spectra453', N'Welcome2u', N'spectrakdd@gmail.com', N'South', N'Andrapradesh', N'Kodad', N'NIL', N'36BAPPK2459B1ZP', N'NIL', CAST(0xA7FE0000 AS SmallDateTime), CAST(0xA7E00000 AS SmallDateTime), N'08547', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (220, N'170', N'National Automobiles', N'Opp. Ashok Auto Sales Ltd
Soot Mill, G.T.Road
Al', N'Mr. Rajendra Singh', N'9837145135', N'9045679404', N'national170', N'Welcome2u', N'wabcoasc.aligarh@gmail.com', N'North', N'Uttar Pradesh', N'Aligarh', N'09525701496', NULL, N'09525701496', CAST(0xA5230000 AS SmallDateTime), CAST(0xA5060000 AS SmallDateTime), N'12485', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (221, N'169', N'Paras Auto Agency', N'Shop No. 12/A, Sanjay Complex
Near Kanpur Bareill', N'Anil Jain', N'09358022168', N'08476000674', N'paras', N'aniljainwabco99', N'aniljain.wabco11@gmail.com', N'North', N'Uttar Pradesh', N'Meerut', N'09876513004', NULL, N'11', CAST(0x000B0000 AS SmallDateTime), CAST(0x000B0000 AS SmallDateTime), N'08548', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (222, N'339', N'Bisal Diesel', N'Malpahari Road
Piyadpur, 
Pakur - 816107
Jharkh', N'Bishal Kumar Jaiswal', N'9431194315', N'8877773999', N'Bisal', N'Welcome2u', N'msbishaldiesel34190@gmail.com', N'East', N'Jharkhand', N'Pakur', N'Tin No: 20401305781', NULL, N'11', CAST(0x000B0000 AS SmallDateTime), CAST(0x000B0000 AS SmallDateTime), N'14606', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (223, N'458', N'Sri Venkateswara Engineering Works', N'Beside Eicher Works, Mydukur Road, 
Taj Nagar,
K', N'M.Venkata Narayana', N'08143547291', N'9652095161', N'SVEW', N'Welcome2u', N'm.venkatnarayana910@gmail.com', N'South', N'Andrapradesh', N'Kadapa', N'Tin No: 28518114712', NULL, N'Tin No: 28518114712', CAST(0xA5230000 AS SmallDateTime), CAST(0xA5050000 AS SmallDateTime), N'08547', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (224, N'460', N'Sri Venkateswara Auto Equipments', N'Plot No.A-33, Phase II
Nayagra Complex Road 
Aut', N'V. V. Narendra', N'086123660099', N'9849779922', N'SVAE', N'Welcome2u', N'svae.nlr@gmail.com', N'South', N'Andrapradesh', N'Nellore', N'CST:28330118016', NULL, N'TIN No. 28330118016', CAST(0xA5420000 AS SmallDateTime), CAST(0xA5050000 AS SmallDateTime), N'08547', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (225, N'174', N'Arora Diesels & Turbo Charger', N'Akashdeep Building, A5,
Transport Nagar, Nigdi
J', N'Smt. Raj Rami Arora', N'01412640471', N'09414048008', N'Arora', N'Welcome2u', N'aroradiesels@gmail.com', N'North', N'Rajastan', N'Jaipur', N'Tin No: 08621612885', NULL, N'Tin No: 08621612885', CAST(0xA5230000 AS SmallDateTime), CAST(0xA5050000 AS SmallDateTime), N'08396', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (226, N'338', N'Santosh Diesels', N'68, G.T.Road, 
P.O. Searsole
Rajbari - 713 358
', N'Aditya Khaitan', N'03412445209', N'09332980688', N'Santosh338', N'Welcome2u', N'Ompkhaitan1@Dataone.In', N'East', N'West Bengal', N'Rajbari', N'CST 19740880214', NULL, N'LST 19740880020', CAST(0xA5230000 AS SmallDateTime), CAST(0xA5050000 AS SmallDateTime), N'08509', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (227, N'461', N'A. L. Motors', N'Shop No. 30/B (Part), Beside New KSRTC Stand
Sir ', N'Mr.Sultan Khan', N'9343434370', N'9740117545', N'ALM461', N'Arifayub', N'almotors01@yahoo.in', N'South', N'Karnataka', N'Bellary', N'Tin: 29290652032', NULL, N'11', CAST(0x000B0000 AS SmallDateTime), CAST(0x000B0000 AS SmallDateTime), N'10556', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (228, N'254', N'Kalsi Air Brake Clinic', N'M4-91, Parking No. 6
Transport Nagar
Gwalior - 4', N'Mr. Dara Singh Kalsi', N'07514050278', N'09926754864', N'Kalsi254', N'Welcome2u', N'Kalsiabc@Rediffmail.Com', N'West', N'Madhya Pradesh', N'Gwalior', N'Tin : 23955307865', NULL, N'Tin : 23955307865', CAST(0xA5230000 AS SmallDateTime), CAST(0xA5050000 AS SmallDateTime), N'10574', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (229, N'257', N'Batra Sales & Service', N'Gole Park, Old Bus Stand
Rewa - 486 001 (M.P)
', N'Mr.Kamla Batra', N'07662250229', N'9425824509', N'Batra257', N'Welcome2u', N'ranju292002@gmail.com', N'West', N'Madhya Pradesh', N'Rewa', N'CST: 23959073862', NULL, N'CST: 23959073862', CAST(0xA5230000 AS SmallDateTime), CAST(0xA5050000 AS SmallDateTime), N'10574', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (230, N'256', N'Heritage Auto Experts', N'Plot No.76, Sector No.23
Transport Nagar, Nigdi
', N'Mr.Kohli Jasmeen Singh', N'8237011011', N'08237011011', N'Heritage256', N'Welcome2u', N'heritageautoexperts@gmail.com', N'West', N'Maharastra', N'Nigdi, Pune', N'CST: 27200988268C', NULL, N'TIN: 27200988268V', CAST(0xA5230000 AS SmallDateTime), CAST(0xA5050000 AS SmallDateTime), N'12696', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (231, N'173', N'Ganpati Sales & Service', N'158, NH-8, Molaheda, Kotputli
Jaipur - 303 108
', N'Gopal Agarwal', N'09928027274', N'09928027274', N'ganpati173', N'Welcome2u', N'shriganpatidiesels@rediffmail.com', N'North', N'Rajastan', N'Kotputli, Jaipur', N'Tin : 08062193059', NULL, N'Tin : 08062193059', CAST(0xA5230000 AS SmallDateTime), CAST(0xA5050000 AS SmallDateTime), N'08396', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (232, N'176', N'Pooja Earth Movers', N'S-26, RIICO Industrial area
Opp. P.N.B. Bank
Bar', N'Ms. Pooja Khare', N'09771488315', N'9950224662', N'pooja176', N'Welcome2u', N'poojaearthmoverjpr@gmail.com', N'North', N'Rajastan', N'Bamer', N'TIN No. 08354055736', NULL, N'TIN No. 08354055736', CAST(0xA5230000 AS SmallDateTime), CAST(0xA5050000 AS SmallDateTime), N'08396', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (233, N'260', N'Gurumeher Vehicle Dynamics', N'Survey No :- 336,  Village Mauje - Pelhar, Near Pa', N'Manjit Singh Anand', N'02222909414', N'9821976329', N'Gurumeher260', N'Welcome2u', N'gurumeher3009@yahoo.co.in', N'West', N'Maharastra', N'Vasai, Mumbai', N'27741059557 V. W.E.F 16/04/201', NULL, N'27741059557C. W.E.F. 16/04/201', CAST(0xA5230000 AS SmallDateTime), CAST(0xA5050000 AS SmallDateTime), N'12696', 1, N'Partner')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (234, N'171', N'Uttam Sales & Services', N'G.T Road, Sherpur Chowk
Opp. Apollo Hospital
Lud', N'Mr. Gurmeet Singh', N'9872341300', N'09876641200', N'Uttam171', N'Mnreet3155', N'gsuttamsale@yahoo.co.in', N'North', N'Punjab', N'Ludhiana', N'TIN No. 03731070968', NULL, N'11', CAST(0x000B0000 AS SmallDateTime), CAST(0x000B0000 AS SmallDateTime), N'10631', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (235, N'253', N'Riya Hydro Steering System', N'Shop No. 1, Plot No. 88
Sector 9/C, N.H.8A
Gandh', N'Mr. Atul. L. Messurani', N'02836222674', N'09825227604', N'riya253', N'rhss2013', N'rhssgandhidham@yahoo.in', N'West', N'Gujarat', N'Gandhidham (Kachch)', N'CST No. 24511006742', NULL, N'11', CAST(0x000B0000 AS SmallDateTime), CAST(0x000B0000 AS SmallDateTime), N'10574', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (236, N'259', N'Mahalaxmi Turbo', N'Near Parking No. 6
Transport Nagar
Rawabhata
Ra', N'Raghvendra Shanbag', N'07712227262 ', N'09300093969', N'maha259', N'Welcome2u', N'gkdiesels@gmail.com', N'West', N'Chhattisgarh', N'Raipur (C.G)', N'TIN No. 2267140422', NULL, N'TIN No. 2267140422', CAST(0xA5230000 AS SmallDateTime), CAST(0xA5050000 AS SmallDateTime), N'06608', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (237, N'464', N'Sri Poonkaliamman Engineering', N'131-F, Sundaram Complex
Abirami Theatre (Opp), Co', N'Mr. V. Singara Vadivelan', N'04254227739', N'09442187785', N'sripoo464', N'Welcome2u', N'poonkaliammanengineering@gmail.com', N'South', N'Tamil NAdu', N'Mettupalayam', N'TIN No.33872044179', NULL, N'TIN No.33872044179', CAST(0xA5230000 AS SmallDateTime), CAST(0xA5050000 AS SmallDateTime), N'06597', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (238, N'177', N'Roto Diesel Services', N'W-4, Transport Nagar
Saharanpur Road
Dehradun - ', N'Mr. Major Singh', N'01352640538', N'01352640538', N'roto177', N'denso*123', N'rotodiesel@yahoo.in', N'North', N'Uttatakhand', N'Dehradun', N'TIN No. 05000752939', NULL, N'11', CAST(0x000B0000 AS SmallDateTime), CAST(0x000B0000 AS SmallDateTime), N'08548', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (239, N'459', N'Sai Auto Services', N'Plot No.  21K,  Baigampalli Building Basement
Aut', N'Praveen M. Hiremetri', N'7406349182', N'7406349182', N'SAS459', N'Welcome2u', N'praveeninremetri0004@gmail.com', N'South', N'Karnataka', N'Bagalkot', N'Tin No: 29181152952', NULL, N'Tin No: 29181152952', CAST(0xA5230000 AS SmallDateTime), CAST(0xA5050000 AS SmallDateTime), N'10556', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (240, N'462', N'Perfect Automotive Engineering Works', N'Vinayak Nagar
Near Pooja International Hotel
P. ', N'Mr. Riyaz Ahmed Hubballi', N'9448288203', N'9632461271', N'perfect462', N'9036901015', N'paewhbl@gmail.com', N'South', N'Karnataka', N'Davangere', N'TIN No. 29351119733', NULL, N'11', CAST(0x000B0000 AS SmallDateTime), CAST(0x000B0000 AS SmallDateTime), N'10556', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (241, N'180', N'Ratan Motors', N'II Floor, Ratan Automobile
20, Thakkar Bappa Colo', N'Mr. Arpit Mehta', N'02942417338', N'09414157164', N'ratan180', N'Welcome2u', N'mehtaarpit2@gmail.com', N'North', N'Rajastan', N'Udaipur', N'TIN No. 08253965484', NULL, N'TIN No. 08253965484', CAST(0xA5230000 AS SmallDateTime), CAST(0xA5050000 AS SmallDateTime), N'08396', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (242, N'467', N'Siva Auto Service Centre', N'No. 3, New Bus Stand Road	
Erode - 638 003	
Tami', N'Mrs. S. Anitha', N'09965567757', N'09965567757', N'siva467', N'Welcome2u', N'sivaasc2012@gmail.com', N'South', N'Tamil NAdu', N'Erode', N'CST No. 1044875 dated 24.02.20', NULL, N'TIN No. 33802877205', CAST(0xA5230000 AS SmallDateTime), CAST(0xA5050000 AS SmallDateTime), N'06597', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (243, N'466', N'M&M Hydraulics Service and Sales', N'32/1379A, Pudussery Tower	Near Pipe Line Bye Pass ', N'Mr. Jomesh', N'09847566323', N'09847566323', N'M&M466', N'Welcome2u', N'kurianjomesh@gmail.com', N'South', N'Kerala', N'Cochin', N'TIN No. 32071302483', NULL, N'TIN No. 32071302483', CAST(0xA5230000 AS SmallDateTime), CAST(0xA5050000 AS SmallDateTime), N'10556', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (244, N'465', N'Ganapathy Diesel Service', N'65/62, M. G. Pudur, 3rd Street	Opp. Collector Offi', N'Mr. J. Prabhu', N'04214243624', N'8608804348', N'ganapathy465', N'KALA892MANI', N'sgabs.brake@gmail.com', N'South', N'Tamil NAdu', N'Tirupur', N'TIN No. 33632425715', NULL, N'11', CAST(0x000B0000 AS SmallDateTime), CAST(0x000B0000 AS SmallDateTime), N'06597', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (245, N'262', N'Harsh Enterprises', N'A-159, Kokta Transport Nagar
Near Bansal College
', N'Mr. Inder Ram Rakhyani', N'09926672727', N'9827076777', N'harsh262', N'Welcome2u', N'harshenterprisesbhopal@rediffmail.com', N'West', N'Madhya Pradesh', N'Bhopal', N'TIN No. 23569010172', NULL, N'TIN No. 23569010172', CAST(0xA5230000 AS SmallDateTime), CAST(0xA51E0000 AS SmallDateTime), N'10574', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (246, N'345', N'CK Hydraulics', N'Bamundanga (Bhangarmath)
Samabayapally Post
Howr', N'Chandan Gupta', N'03326710070', N'09330828559', N'CKH345', N'wELCOME2U', N'ckhydraulics@gmail.com', N'East', N'West Bengal', N'Howrah', N'TIN No. 19713781033', NULL, N'TIN No. 19713781033', CAST(0xA5230000 AS SmallDateTime), CAST(0xA51E0000 AS SmallDateTime), N'08509', 1, N'Partner')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (247, N'469', N'National Power Steering Works', N'5-7-24, Bye Pass Road	Opp. Reliance Petrol Bunk	Ka', N'Mr. SK. Khajavali', N'09849742505', N'09849742505', N'national469', N'national007', N'nationalpowersteering@gmail.com', N'South', N'Andrapradesh', N'Karim Nagar', N'TIN No. 36479936486', NULL, N'11', CAST(0x000B0000 AS SmallDateTime), CAST(0x000B0000 AS SmallDateTime), N'08547', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (248, N'474', N'Melge Motors', N'Survey No. 1059, Beside Aegis Autogas Pump	NH-4, G', N'Mr. Tanaji Melge', N'9741262099', N'9741262099', N'melge474', N'Welcome2u', N'melge_motors@rediffmail.com', N'South', N'Karnataka', N'Belgaum', N'TIN No. 29240580037', NULL, N'TIN No. 29240580037', CAST(0xA7FE0000 AS SmallDateTime), CAST(0xA7E00000 AS SmallDateTime), N'10556', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (249, N'340', N'Rohit Diesel', N'ITKI Road, 	Ranchi-834005, Jharkhand
', N'Mr.Rohit Arora', N'9835323445', N'9430376520', N'rohit340', N'Welcome2u', N'rohitdiesels@yahoo.com', N'East', N'Jharkhand', N'Ranchi', N'Tin No: 20350402033', NULL, N'Tin No: 20350402033', CAST(0xA7FE0000 AS SmallDateTime), CAST(0xA7E00000 AS SmallDateTime), N'14606', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (250, N'470', N'Janatha Auto Works', N'Near Bapuji High School	NH-13, Pillekeranahalli	Ch', N'Mr. G. Saifulla', N'09742153614', N'09845601443', N'Janatha470', N'Welcome2u', N'jaw.durga@gmail.com', N'South', N'Karnataka', N'Chitradurga', N'CST No. 65992570', NULL, N'TIN No. 29760712738', CAST(0xA7FE0000 AS SmallDateTime), CAST(0xA7E00000 AS SmallDateTime), N'10556', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (251, N'457', N'Rayalaseema Motors', N'MRSM Complex, #6-11-E20	NH-7, Bangalore Road	Anant', N'Mr.Syed Dadapeer', N'09951946048', N'09951946048', N'Rayala457', N'Welcome2u', N'rlsm786@gmail.com', N'South', N'Andrapradesh', N'Anantapur', N'TIN No.28930185304', NULL, N'TIN No.28930185304', CAST(0xA5230000 AS SmallDateTime), CAST(0xA7E00000 AS SmallDateTime), N'08547', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (252, N'255', N'Shree Ganesh Auto', N'4-5, Kusum Chandra Park
Opp. Swagat Complex, ONGC', N'Mr. Anand Chawla', N'9327952525', N'09898047417', N'shree255', N'Welcome2u', N'shreeganeshauto@gmail.com', N'West', N'Gujarat', N'Hazira - Surat', N'GST: 24223801364', NULL, N'CST: 24723801364', CAST(0xA5230000 AS SmallDateTime), CAST(0xA5050000 AS SmallDateTime), N'06608', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (253, N'475', N'M. Y. Battery Centre', N'#334, 1st Main, 2nd Cross
Dargah Mohalla, K.R. Pu', N'Mr. Abdul Lathief', N'09620514262', N'09620514262', N'myb475', N'abdul_123', N'mybattery17@gmail.com', N'South', N'Karnataka', N'Bangalore', N'TIN No. 29520313424', NULL, N'11', CAST(0x000B0000 AS SmallDateTime), CAST(0x000B0000 AS SmallDateTime), N'10556', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (254, N'261', N'Grace Motors', N'Near Sagar Automobiles
Opp. Duke''s & Co. Ltd., Wa', N'Mr. S. Edwin Selvakumar', N'9987496500', N'9987496500', N'grace261', N'Welcome2u', N'g.gracemotors@gmail.com', N'West', N'Maharastra', N'Chembur - Mumbai', N'CST No. 27770970640V', NULL, N'LST No. 27770970640C', CAST(0xA7FE0000 AS SmallDateTime), CAST(0xA7E00000 AS SmallDateTime), N'12696', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (255, N'179', N'Bansal Motor Parts', N'NH-64, Bhatinda - Dabwali Road	VPO - Gheributer	Ba', N'Mr. Surinder Pal Bansal', N'01642426999', N'09464700008', N'bansal179', N'rane@123', N'bansalmotorparts@gmail.com', N'North', N'Punjab', N'Bathinda', N'TIN No. 03532156799', NULL, N'11', CAST(0x000B0000 AS SmallDateTime), CAST(0x000B0000 AS SmallDateTime), N'10631', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (256, N'175', N'Lala Bashir & Sons', N'Meer Complex, Opp. General Bus Stand,
Panth Chowk', N'Mr.Anwar Bashir', N'09419001762', N'09419001762', N'Lala175', N'Welcome2u', N'lalabashirsons26@yahoo.com', N'North', N'jammu and Kashmir', N'Sri Nagar', N'TIN No. 01982061670 (Central)', NULL, N'TIN No. 01982061670 (Central)', CAST(0xA5230000 AS SmallDateTime), CAST(0xA51E0000 AS SmallDateTime), N'10631', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (257, N'463', N'Hi-Tech Auto Serviicesz', N'Pon Nagar   Salem Main Road   Namakkal – 637 001', N'Mr. C. S. Elangovan', N'09843127219', N'09843127219', N'hitech463', N'BLOCKED', N'hitech6789@rediffmail.com', N'South', N'Tamil NAdu', N'Namakkal', N'TIN No.33192846355 dated 24-12', N'ABCD', N'TIN No.33192846355 dated 24-12', CAST(0xA7FE0000 AS SmallDateTime), CAST(0xA7E00000 AS SmallDateTime), N'06597', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (258, N'182', N'Vitexo System & Solutions', N'Sagar Enclave  Adj. Transport Nagar  Jalandhar - 1', N'Mr. Atin Aggarwal', N'01812604747', N'08054711945', N'vitexo182', N'NARINDER1962', N'vitexosystems@gmail.com', N'North', N'Punjab', N'Jalandhar', N'03262163883', NULL, N'11', CAST(0x000B0000 AS SmallDateTime), CAST(0x000B0000 AS SmallDateTime), N'10631', 1, N'Partner')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (259, N'468', N'P. K. Engineering', N'Indira Nagar 1st Street  Near Stadium Bus Stand', N'Mr. Prasad Kumar. B', N'9847090707', N'9847090707', N'pkengineering468', N'Welcome2u', N'smartpkumar@gmail.com', N'South', N'Kerala', N'Palakkad', N'CST No. 32090575612C', NULL, N'TIN No. 32090575612', CAST(0xA7FE0000 AS SmallDateTime), CAST(0xA7E00000 AS SmallDateTime), N'10556', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (260, N'472', N'Sri Arunachala Air Brake Systems', N'440, Thadicombu Road  Near Collectorate  Dindigul ', N'Mr. S. Nanthagopal', N'09894795275', N'09894795275', N'sri472', N'Welcome2u', N'arunachalabrakes@gmail.com', N'South', N'Tamil NAdu', N'Dindigul', N'CST No. 168138 I.A No. 270', NULL, N'TIN No. 33395263582', CAST(0xA7FE0000 AS SmallDateTime), CAST(0xA7E00000 AS SmallDateTime), N'06597', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (261, N'181', N'Shaheed Kumar Singh (Veer Chakra) Motors', N'NH-11, Village-Ghasola  Agra Road  Bharatpur - 321', N'Mrs. Rena Singh', N'08875015476', N'08875015476', N'shaheed181', N'Welcome2u', N'shaheedmotors@rediffmail.com', N'North', N'Rajastan', N'Bharatpur', N'TIN No. 08370801957', NULL, N'TIN No. 08370801957', CAST(0xA7FE0000 AS SmallDateTime), CAST(0xA7E00000 AS SmallDateTime), N'08396', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (262, N'473', N'Taurus Turbo', N'SBV-1399/A-16  South Plaza Complex, South Bazar  K', N'Mr. Jineesh Kumar K. C.', N'09446756528', N'09447012995', N'taurus473', N'Welcome2u', N'taurusturbo@gmail.com', N'South', N'Kerala', N'Kannur', N'TIN No. 32110718106', N'32AAHFT2064L1Z4', N'TIN No. 32110718106C', CAST(0xA7FE0000 AS SmallDateTime), CAST(0xA7E00000 AS SmallDateTime), N'10556', 1, N'Partner')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (263, N'478', N'Famous Auto Service', N'No- II-70/3,Kanjiracode, Kalluthotti,
Kanjiracode', N'Mr. C.Ravi Kumar', N'9443388132', N'9443388132', N'famous478', N'Welcome2u', N'ravikumar.sfaew@gmail.com', N'South', N'Tamil NAdu', N'Marthandam', N'TIN No. 33066334207', NULL, N'TIN No. 33066334207', CAST(0xA7E00000 AS SmallDateTime), CAST(0xA7FE0000 AS SmallDateTime), N'06597', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (264, N'358', N'Vishwakarma Diesels', N'Samundai colony
Buritala Road, Mangalbari
Malda ', N'Mr. Sanjit Kumar Roy', N'9933410207', N'9933410207', N'vd358', N'Welcome2u', N'vkd.mld2009@gmail.com', N'East', N'West Bengal', N'Malda', N'GST 19AAHFV7469H1ZG', NULL, N'GST 19AAHFV7469H1ZG', CAST(0xAAD80000 AS SmallDateTime), CAST(0xAABA0000 AS SmallDateTime), N'08509', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (265, N'172', N'Kanpur Steering & Turbo', N'New Road, Tyre Market 
Purnan  Bus Stand
Jhansi ', N'Mr. Sandeep Kumar', N'9936113366', N'9936113366', N'kst172', N'Welcome2u', N'kanpursteering@rediffmail.com', N'North', N'Uttar Pradesh', N'Jhansi', N'09732410715C', NULL, N'09732410715C', CAST(0xA7FE0000 AS SmallDateTime), CAST(0xA7E00000 AS SmallDateTime), N'12485', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (266, N'341', N'Ideal Diesel', N'Demthring
Sawlad
Shillong - 793 021
Meghalaya
', N'Mr.Anand Sureka', N'03642230291', N'9863065802', N'ideal341', N'Welcome2u', N'idealdiesel_sfg@yahoo.co.in', N'East', N'Meghalaya', N'Shillong', N'17060031268', NULL, N'17060031054', CAST(0xA7FE0000 AS SmallDateTime), CAST(0xA7E00000 AS SmallDateTime), N'11737', 1, N'Partner')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (267, N'482', N'M. Y. Battery Centre', N'#334, 1st Main, 2nd Cross
Dargah Mohalla, K.R. Pu', N'Mr. Abdul Lathief', N'9986691784', N'9986691784', N'myb482', N'terminated', N'mybattery17@gmail.com', N'South', N'Karnataka', N'Bangalore', N'TIN No. 29520313424', NULL, N'TIN No. 29520313424', CAST(0xA7FE0000 AS SmallDateTime), CAST(0xA7E00000 AS SmallDateTime), N'10556', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (268, N'189', N'JBD Sales Corporation', N'1st Floor, Shop No. 37
Gurunanak Market, Transpor', N'Mr.Deepak Kalia', N'9815556989', N'9815556989', N'jbd123', N'65756989jbd', N'jbdsalescorporation@rediffmail.com', N'North', N'Punjab', N'Ludhiana', N'TIN No. 03362193122', NULL, N'11', CAST(0x000B0000 AS SmallDateTime), CAST(0x000B0000 AS SmallDateTime), N'10631', 1, N'Partner')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (269, N'479', N'Sri Engineers', N'143-1, Nava India Road	Near Ambal Driving School P', N'Ranjith Kumar', N'04222314855', N'9965565620', N'sri479', N'Welcome2u', N'sriengineerscbe@gmail.com', N'South', N'Tamil NAdu', N'Coimbatore', N'TIN No 33476365773', NULL, N'TIN No 33476365773', CAST(0xA7FE0000 AS SmallDateTime), CAST(0xA7E00000 AS SmallDateTime), N'06597', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (270, N'264', N'J. C. Enterprises', N'W-4, MIDC
Baramati - 413 102
Maharashtra
', N'Mr. Ajay Amrutlal Chankes', N'09823707514', N'09823707514', N'jce264', N'Welcome2u', N'jayeshchankeshwara@gmail.com', N'West', N'Maharastra', N'Baramati', N'TIN 27170033077C', NULL, N'TIN 27170033077C', CAST(0xA7FE0000 AS SmallDateTime), CAST(0xA7E00000 AS SmallDateTime), N'12696', 1, N'Partner')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (271, N'185', N'Aabha Motors', N'Parvati Nagar
Sangariya Fanta
Jodhpur - 342 005
', N'Mr. Saawan K. Raj', N'9529832647', N'9982321165', N'aabha185', N'Welcome2u', N'savankraj76@gmail.com', N'North', N'Rajastan', N'Jodhpur ', N'08632615714', NULL, N'08632615714', CAST(0xA7FE0000 AS SmallDateTime), CAST(0xA7E00000 AS SmallDateTime), N'08396', 1, N'Partner')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (272, N'183', N'Chopra Sales & Service', N'BG-229, Sanjaj Gandhi Transport Nagar
Delhi - 110', N'Mr. Yogi Chopra', N'01127832095', N'9873266000', N'css183', N'Welcome2u', N'yogichopra2@gmail.com', N'North', N'National Capital Territor', N'Delhi', N'TIN 07580335094', NULL, N'TIN 07580335094', CAST(0xA6910000 AS SmallDateTime), CAST(0xA6730000 AS SmallDateTime), N'08548', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (273, N'485', N'Quilon Brake System', N'NH-47, Mylakkad Post,
Kottiyam
Kollam - 691571
', N'Mr. P. Babu  ', N'04742593016', N'9447071826', N'qbs485', N'Welcome2u', N'quilonbrakesystem@gmail.com', N'South', N'Kerala', N'Kollam', N'CST No : 32021379538C', NULL, N'CST No : 32021379538C', CAST(0xA6910000 AS SmallDateTime), CAST(0xA6730000 AS SmallDateTime), N'10556', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (274, N'349', N'Papu Automobiles Repairing Centre', N'A. T. Road
Hijuguri
Opp. Hotel Highway
Tinsukia', N'Mr. Umesh Sah', N'03742334009', N'09954458475', N'par349', N'Welcome2u', N'papuautomobilestsk@gmail.com', N'East', N'Assam', N'Tinsukia', N'CST No. 18329949245', NULL, N'TIN No. 18880221888', CAST(0xA7FE0000 AS SmallDateTime), CAST(0xA7E00000 AS SmallDateTime), N'11737', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (275, N'265', N'Bhurke Automobiles', N'Shop No- 3, Golden Inox Appt.
Near Old Maruthi Sh', N'Mr. Abhijit D.Bhurke', N'09372944055', N'9970319333', N'ba265', N'tanishkaos2', N'abhijit.bhurke2310@gmail.com', N'West', N'Maharastra', N'Sangli', N'TIN 27850054043V', NULL, N'11', CAST(0x000B0000 AS SmallDateTime), CAST(0x000B0000 AS SmallDateTime), N'12696', 1, N'Partner')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (276, N'188', N'Amitdeep Automobiles', N'Nausarh Chowk to Kauriram Road
N.H. 29 (Near Kris', N'Mrs. Kusum Lata Tripathi', N'9452110520 ', N'9452110520 ', N'aa188', N'Welcome2u', N'amitdeepautomobilesgkp@gmail.com', N'North', N'Uttar Pradesh', N'Gorakhpur', N'TIN No. 09518625956', NULL, N'TIN No. 09518625956', CAST(0xA7FE0000 AS SmallDateTime), CAST(0xA7E00000 AS SmallDateTime), N'12485', 1, N'Partner')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (277, N'263', N'ZN Motors', N'C-21, 22, 23, Raj Chamber
Near Kothla Stand
Ahme', N'Mr. Sayyed Salim', N'02412329881', N'9271209450', N'gts263', N'Welcome2u', N'gulffuel.ang@gmail.com', N'West', N'Maharastra', N'Ahmed Nagar', N'TIN No. 27350055191', N'27AQZPS1066M3Z3', N'TIN No. 27350055191', CAST(0xA7FE0000 AS SmallDateTime), CAST(0xA7E00000 AS SmallDateTime), N'12696', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (278, N'192', N'Vikas Automobiles', N'Delhi Mathura Road, 39.6 km stone,
Jhajru Mor, Ba', N'Mr. Ashish Yadav', N'9971919228', N'9971919228', N'va192', N'Welcome2u', N'vikasservicecentre@hotmail.com', N'North', N'Haryana', N'Faridabad', N'TIN No. 06961345956', NULL, N'TIN No. 06961345956', CAST(0xA7FE0000 AS SmallDateTime), CAST(0xA7E00000 AS SmallDateTime), N'08548', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (279, N'350', N'Perfect Engineering Works', N'Mirzapur
Near Maleksh Mardini Temple
Darbhanga -', N'Mr. Islam', N'9905913595', N'9905913595', N'pew350', N'Welcome2u', N'perfectengineeringworks04@gmail.com', N'East', N'Bihar', N'Darbhanga', N'10389797068', NULL, N'10389797068', CAST(0xA7FE0000 AS SmallDateTime), CAST(0xA7E00000 AS SmallDateTime), N'14606', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (280, N'353', N'Jamuna Sales & Service', N'Kishaan Chowk (Barwadda More)
G.T. Road
Dhanbad ', N'Mr. Ashok. Kr. Singh', N'7654162070', N'7654162070', N'jss353', N'Welcome2u', N'ashoksinghdhanbad@gmail.com', N'East', N'Jharkhand', N'Dhanbad', N'CST No. 20521706643-101', NULL, N'CST No. 20521706643-101', CAST(0xA7FE0000 AS SmallDateTime), CAST(0xA7E00000 AS SmallDateTime), N'14606', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (281, N'352', N'Manalisha Diesel', N'Balipara Bomdila Road
Tezpur
Sonitpur - 784 101
', N'Mr. Prahlad Sarkar', N'9435563270', N'9435563270', N'md352', N'Welcome2u', N'manalishadiesel157@gmail.com', N'East', N'Assam', N'Tezpur', N'CST No : 18350082257', NULL, N'CST No : 18350082257', CAST(0xA7FE0000 AS SmallDateTime), CAST(0xA7E00000 AS SmallDateTime), N'11737', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (282, N'SE014', N'Shaik Nazeerbaba', N'RTSSL Engineer - Service
C/o. Spectra Automobile ', N'Shaik Nazeerbaba', N'09542462114', N'8143409260', N'SE014', N'WELCOME2U', N'shaik.nazeerbaba@ranegroup.com', N'South', N'Andrapradesh', N'Hyderabad', N'123456', N'ABCB', N'11', CAST(0x000B0000 AS SmallDateTime), CAST(0x000B0000 AS SmallDateTime), N'08547', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (283, N'484', N'PRM Auto Service', N'No. 208/B5, SKU Complex, Avvaiyar Street, Ariyalur', N'Mr. S. Panchapakesan', N'9944440045', N'9944440045', N'prm484', N'Welcome2u', N'universalautomech@gmail.com', N'South', N'Tamil NAdu', N'Perambalur', N'TIN No. 33326390814', NULL, N'GST No. 33AASFP5298F1ZQ', CAST(0xA7FE0000 AS SmallDateTime), CAST(0xA7E00000 AS SmallDateTime), N'06597', 1, N'Partner')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (284, N'SE008', N'Mistry Hemantkumar', N'RTSSL Engineer - Service
C/o. Bhoomi Motors
Near', N'Vacant', N'12345', N'12345', N'SE008', N'BLOCKED1', N'mistry.hemantkumar@ranegroup.com', N'West', N'Gujarat', N'Ahmedabad', N'12345', N'ABCB', N'12345', CAST(0xA7C20000 AS SmallDateTime), CAST(0xA7C30000 AS SmallDateTime), N'08566', 0, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (285, N'186', N'Sufi Motors', N'Plot No. 8A Industrial Area
Basni Road
Nagaur - ', N'Mr. Sakil Ahmad', N'9461290001', N'9461290001', N'sm186', N'sufimotor@2013', N'sufimotorsnagaur@gmail.com', N'North', N'Rajastan', N'Nagaur', N'TIN No. 08600248106', NULL, N'11', CAST(0x000B0000 AS SmallDateTime), CAST(0x000B0000 AS SmallDateTime), N'08396', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (286, N'266', N'Shree Ram Motors', N'New Nanded Road
Garud Chowk
Latur - 413 512
Mah', N'Mr. Avinash.M.Poduval', N'8087422240', N'8087422240', N'srm266', N'Welcome2u', N'vipin.poduval@yahoo.com', N'West', N'Maharastra', N'Latur', N'CST No. 27220988936 ', NULL, N'LST No. 27220988936-C', CAST(0xA7FE0000 AS SmallDateTime), CAST(0xA7E00000 AS SmallDateTime), N'12696', 1, N'Partner')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (287, N'190', N'Salasar Automobiles', N'Near Guddha Mod Railway Phatak
Vigyan Nagar
Jhun', N'Mr. Sharvan Kumar', N'9413683017', N'9413683017', N'sa190', N'Welcome2u', N'sharvannehra17@gmail.com', N'North', N'Rajastan', N'Jhunjhunu', N'TIN No. 08381508257', NULL, N'TIN No. 08381508257', CAST(0xA7FE0000 AS SmallDateTime), CAST(0xA7E00000 AS SmallDateTime), N'08396', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (288, N'354', N'Shakti Trading Company', N'Link Road
Jaigaon - 736 182
West Bengal
', N'Mr. Dilip Kumar Agarwal', N'03566263241', N'9933064241', N'stc354', N'Welcome2u', N'shakti@shaktitradingco.com', N'East', N'West Bengal', N'Jaigaon', N'GST : 19ACQPA7651R1ZQ', N'19ACQPA7651R1ZQ', N'GST : 19ACQPA7651R1ZQ', CAST(0xA7FE0000 AS SmallDateTime), CAST(0xA7E00000 AS SmallDateTime), N'08509', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (289, N'347', N'B.K. Diesels & Engineerings', N'Plot No. 977/2308, Chorda Bypass Road
NH - 215
J', N'Mr. Sanjay Kumar Biswal', N'06726223334', N'9861383998', N'bkd347', N'DLS2@pst', N'bkdls_enggs@yahoo.co.in', N'East', N'Orissa', N'Jajpur', N'TIN No  21651405656', NULL, N'11', CAST(0x000B0000 AS SmallDateTime), CAST(0x000B0000 AS SmallDateTime), N'06808', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (290, N'SE002', N'Nitish C', N'RTSSL Engineer - Service
Punjab Diesels
S.C.F - ', N'Nitish C', N'08059171213', N'08059171213', N'SE002', N'Welcome2u', N'h.nitish@ranegroup.com', N'North', N'Chandigarh', N'Chandigarh', N'12345', NULL, N'12345', CAST(0xA7C20000 AS SmallDateTime), CAST(0xA7C30000 AS SmallDateTime), N'10631', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (291, N'SE003', N'Ravi Prakash', N'RTSSL Engineer - Service 
C/o. Energy Motors
Sho', N'Ravi Prakash', N'08385948651', N'08385948651', N'SE003', N'Welcome3u', N's.raviprakash@ranegroup.com', N'North', N'Rajastan', N'Jaipur', N'12345', NULL, N'12345', CAST(0xA7C20000 AS SmallDateTime), CAST(0xA7C30000 AS SmallDateTime), N'08396', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (292, N'SE004', N'C. Prakash Shukla', N'RTSSL Engineer - Service
C/o. Gurdeep Sales & Ser', N'Prakash Shukla', N'09935373072', N'09935373072', N'SE004', N'Welcome2u', N'c.prakashshukla@ranegroup.com', N'North', N'Uttar Pradesh', N'Kanpur', N'12345', N'12345', N'12345', CAST(0xA7C20000 AS SmallDateTime), CAST(0xA7C30000 AS SmallDateTime), N'12485', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (293, N'SE005', N'I. Athul Chappadi', N'RTSSL Engineer - Service
C/o. Alpha Autotech
Plo', N'I. Athul Chappadi', N'09037094390', N'09037094390', N'SE005', N'BLOCKED1', N'l.athulchappadi@ranegroup.com', N'West', N'Mumbai', N'Navi Mumbai', N'12345', N'ABCB', N'12345', CAST(0xA7C20000 AS SmallDateTime), CAST(0xA7C30000 AS SmallDateTime), N'12696', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (294, N'SE006', N'Pramod Kumar Bairagi', N'RTSSL Engineer - Service
C/o.  Jullundur Motor Ag', N'Pramod Kumar Bairagi', N'07898989842', N'07898989842', N'SE006', N'mp478579', N'k.pramodkumarbairagi@ranegroup.com', N'West', N'Madhya Pradesh', N'Indore', N'12345', NULL, N'11', CAST(0x000B0000 AS SmallDateTime), CAST(0x000B0000 AS SmallDateTime), N'10574', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (295, N'SE007', N'Chetan Gajanan Kalambe', N'RTSSL Engineer - Service
C/o. JMA Rane Marketing ', N'Chetan Gajanan Kalambe', N'08600998445', N'08600998445', N'SE007', N'Welcome2u', N'chetangajanan.kalambe@ranegroup.com', N'West', N'Maharastra', N'Nagpur', N'12345', NULL, N'12345', CAST(0xA7C20000 AS SmallDateTime), CAST(0xA7C30000 AS SmallDateTime), N'06608', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (296, N'SE009', N'Abhik Bhakta', N'RTSS Engineer - Service 
Arrkay Diesel
158, Jess', N'Abhik Bhakta', N'09038502603', N'09038502603', N'SE009', N'Welcome2u', N's.abhikbhakta@ranegroup.com', N'East', N'West Bengal', N'Kolkata', N'12345', NULL, N'12345', CAST(0xA7C20000 AS SmallDateTime), CAST(0xA7C30000 AS SmallDateTime), N'08509', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (297, N'SE010', N'Prasanta Kumar Sahoo', N'RTSSL Engineer - Service 
C/o Sundaram Motors
Pl', N'Prasanta Kumar Sahoo', N'09937099556', N'09937099556', N'SE010', N'shreemaa', N'm.prasantakumarsahoo@ranegroup.com', N'East', N'Orissa', N'Cuttack', N'12345', NULL, N'11', CAST(0x000B0000 AS SmallDateTime), CAST(0x000B0000 AS SmallDateTime), N'06808', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (298, N'SE015', N'R. Dinesh Babu', N'RTSSL Engineer - Service
C/o. M&M Hydraulics Serv', N'R. Dinesh Babu', N'09167441911', N'09167441911', N'SE015', N'kuttydinesh', N'r.dineshbabu@ranegroup.com', N'South', N'Kerala', N'Cochin', N'12345', NULL, N'11', CAST(0x000B0000 AS SmallDateTime), CAST(0x000B0000 AS SmallDateTime), N'11691', 0, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (299, N'476', N'M&M Hydraulics Sales and Service', N'Bldg.No. 595/A, Valliara Bldg.
Opp. Royal Bajaj S', N'Mr.Soni.E.A', N'9847955874', N'9497701513', N'm&m476', N'Welcome2u', N'itssonyea@gmail.com', N'South', N'Kerala', N'Kottayam', N'32050601673C', NULL, N'32050601673C', CAST(0xA7FE0000 AS SmallDateTime), CAST(0xA7E00000 AS SmallDateTime), N'10556', 1, N'Propreiter')
GO
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (300, N'344', N'Mukherjee Automobiles & Boring', N'N.H.34, Panchanantala
Near Bhagirathi Milk Mill, ', N'Mr. Prabir Kumar Mukherje', N'9775261104', N'9434363065', N'mab344', N'BLOCKED', N'koustav.babay963@gmail.com', N'East', N'West Bengal', N'Berhampore', N'TIN No. 19761018578', N'AAAA', N'11', CAST(0xA7FE0000 AS SmallDateTime), CAST(0xA7E00000 AS SmallDateTime), N'08509', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (301, N'SE001', N'Om Prakash Kumar', N'RTSS Engineer - Service
C/o.  Jullundur Motor Age', N'Om Prakash', N'09910423993', N'09910423993', N'SE001', N'Welcome2u', N'c.omprakashkumar@ranegroup.com', N'North', N'National Capital Territor', N'New Delhi', N'123456', N'123455', N'123456', CAST(0xA7FE0000 AS SmallDateTime), CAST(0xA7E00000 AS SmallDateTime), N'08548', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (302, N'187', N'S.K. Diesel Sales & Service Pvt. Ltd.', N'Meghanagla, 
Bareilly Road
Rampur - 244 901
Utt', N'Mr. Sidharth Agarwal', N'8126097466', N'8126097466', N'skd187', N'Welcome2u', N'sales@skdiesel.in', N'North', N'Uttar Pradesh', N'Rampur', N'TIN No. 09960700337', NULL, N'TIN No. 09960700337', CAST(0xAAD80000 AS SmallDateTime), CAST(0xAABA0000 AS SmallDateTime), N'12485', 1, N'Partner')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (303, N'490', N'SS Air Brakes', N'Plot No. 228, 6th Cross Road
Autonagar
Kakinada ', N'Mr. MD.ADIL', N'9618166786', N'9618166786', N'ssa490', N'19021978', N'ssairbrakes@gmail.com', N'South', N'Andrapradesh', N'Kakinada', N'GST No. 37FCAPS7563F1Z7', NULL, N'11', CAST(0x000B0000 AS SmallDateTime), CAST(0x000B0000 AS SmallDateTime), N'08547', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (304, N'364', N'Das Diesel', N'Moram Rasta
Debhog, East Medinipur
Haldia - 721 ', N'Mr. Subhasis Das', N'9434973673', N'9434973673', N'dd364', N'Welcome2u', N'dasdiesel.das@gmail.com', N'East', N'West Bengal', N'Haldia', N'GST : 19AGZPD6466E1ZU', NULL, N'GST : 19AGZPD6466E1ZU', CAST(0xAAD80000 AS SmallDateTime), CAST(0xAABA0000 AS SmallDateTime), N'08509', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (305, N'357', N'Sethy Auto Traders', N'Bibigunj N.H - 28 (Near Durga Asthan)	Bhagwanpur C', N'Mr. Palwinder Singh', N'9939934947', N'9939934947', N'sat357', N'Welcome2u', N'palwinder555@gmail.com', N'East', N'Bihar', N'Muzaffarpur', N'10ATFPS5796P1ZW', NULL, N'10ATFPS5796P1ZW', CAST(0xAABB0000 AS SmallDateTime), CAST(0xAA9C0000 AS SmallDateTime), N'14606', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (306, N'366', N'Maa Tarini Auto Works', N'Plot No. 3349,	New Puri Bypass Road	Bhubaneswar - ', N'Mr. Surendranath Sahu', N'09437284617', N'09437284617', N'mtaw366', N'Welcome2u', N'surendranathasahu6975@gmail.com', N'East', N'Orissa', N'Bhubaneswar', N'21BEJPS9134M1Z1', NULL, N'21BEJPS9134M1Z1', CAST(0xAAD80000 AS SmallDateTime), CAST(0xAABA0000 AS SmallDateTime), N'06808', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (307, N'248', N'Heritage Exports', N'Shop No.10, Jadhav Complex
Saisatyam Park 
Ganes', N'Mr.Virmeet Singh Maini', N'09860878889', N'09860878889', N'he248', N'Welcome2u', N'Mainiautoagency@Yahoo.Com', N'West', N'Maharastra', N'Pune', N'27ABLPM1250A1Z4', NULL, N'27ABLPM1250A1Z4', CAST(0xAAD80000 AS SmallDateTime), CAST(0xAABA0000 AS SmallDateTime), N'12696', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (308, N'346', N'Paradeep Diesels', N'Dochhaki 
(Near Paradeep Inn Hotel)	
Udaya Vihar', N'Mr. Ranjit Kumar Biswal', N'09438304285', N'08594893911', N'pd346', N'Welcome2u', N'biswalranjit11@gmail.com', N'East', N'Orissa', N'Paradeep', N'21AAQFP8536N1ZL', NULL, N'21AAQFP8536N1ZL', CAST(0xAAD80000 AS SmallDateTime), CAST(0xAABA0000 AS SmallDateTime), N'06808', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (309, N'492', N'Mangalore Motors', N'Shop No. 2 & 3
A.R.K Complex 
Abdul Kalam Azad (', N'Mr. N.K. Hegde', N'08216553320', N'08216553320', N'mm492', N'Welcome2u', N'nphegde33@yahoo.com', N'South', N'Karnataka', N'Mysore', N'29ABQPH0648N1Z1', NULL, N'29ABQPH0648N1Z1', CAST(0xAAD80000 AS SmallDateTime), CAST(0xAABA0000 AS SmallDateTime), N'10556', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (310, N'197', N'Tirupati Automobiles', N'N. H. 8, Bhagwanda Khurd	Behind Om Motors (Man Tru', N'Bhanwar Singh Choudhary', N'09829044820', N'09829044820', N'TA197', N'wELCOME2U', N'tirupatiautomobiles@yahoo.com', N'North', N'Rajastan', N'Rajsamand', N'08ADLPC9757A1ZI', NULL, N'08ADLPC9757A1ZI', CAST(0xAAD80000 AS SmallDateTime), CAST(0xAABA0000 AS SmallDateTime), N'08396', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (311, N'362', N'Overdrive Automobiles', N'NH-60, Pratappur	Rampurhat - 731224	West Bengal
', N'Mr. Rakesh Mishra', N'07501330512', N'07501330512', N'OA362', N'Welcome2u', N'rakesh@overdriveautomobiles.com', N'East', N'West Bengal', N'Rampurhat', N'NA', N'19AADFO5925B1ZE', N'NA', CAST(0xAABA0000 AS SmallDateTime), CAST(0xADD10000 AS SmallDateTime), N'08509', 1, N'Partner')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (312, N'486', N'Asuwathika Motors', N'No. 184H/14, Sadayal Bypass Main Road	Near Sillara', N'Mr. R. N. Murugesan', N'9994336369', N'9994336369', N'am486', N'Welcome2u', N'asuwathikaturbo.in@gmail.com', N'South', N'Tamil NAdu', N'Theni', N'33ATQPM7730G1ZG', NULL, N'33ATQPM7730G1ZG', CAST(0xAABA0000 AS SmallDateTime), CAST(0xAA9C0000 AS SmallDateTime), N'06597', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (313, N'489', N'SLV Motors Service Centre', N'Khaneshumari No. 17/8, Hemantha Complex, Madhavara', N'Mr. Govindaraju M', N'07996127113', N'08884035417', N'slv489', N'Welcome2u', N'slv.motors.ac.servicecenter@gmail.com', N'South', N'Karnataka', N'Bengaluru', N'29BHKPG8873M1ZE', N'29BHKPG8873M1ZE', N'29BHKPG8873M1ZE', CAST(0xA7FE0000 AS SmallDateTime), CAST(0xA7E00000 AS SmallDateTime), N'10556', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (314, N'348', N'Mona Diesel Service', N'N.H - 31, Bypass Road/Subhash Chowk
Begusarai - 8', N'Mr. Gajendranath Choudhry', N'7547070161', N'7547070161', N'mds348', N'Welcome2u', N'amitkumar_delphitvs@yahoomail.com', N'East', N'Bihar', N'Begusarai', N'CST No:10362713119', NULL, N'GST No: 10ABUPC5672M1ZD', CAST(0xA7FE0000 AS SmallDateTime), CAST(0xA7E00000 AS SmallDateTime), N'14606', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (315, N'SE011', N'Chandan Chetry', N'RTSSL Engineer - Service 
C/o.  G.D Motors AT Roa', N'Chandan Chetry', N'09127752724', N'09127752724', N'SE011', N'east@ranetrw', N'c.chandanchetry@ranegroup.com', N'East', N'Assam', N'Guwahati', N'12345', NULL, N'11', CAST(0x000B0000 AS SmallDateTime), CAST(0x000B0000 AS SmallDateTime), N'11737', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (316, N'SE012', N'G.Sathianarayanan', N'RTSSL Engineer - Service
C/o. Hi-Tech Auto Servii', N'G.Sathianarayanan', N'8056012874', N'6381971446', N'SE012', N'Welcome2u', N'g.sathianarayanan@ranegroup.com', N'South', N'Tamil NAdu', N'Namakkal', N'12345', N'ABCB', N'11', CAST(0x000B0000 AS SmallDateTime), CAST(0x000B0000 AS SmallDateTime), N'06597', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (317, N'SE013', N'M. Chethan Kumar', N'RTSSL Engineer - Service
C/o. Spectra Automobile ', N'M. Chethan Kumar', N'09686773967', N'09686773967', N'SE013', N'Welcome2u', N'm.chethankumar@ranegroup.com', N'South', N'Karnataka', N'Bangalore', N'12345', NULL, N'12345', CAST(0xA7C20000 AS SmallDateTime), CAST(0xA7C30000 AS SmallDateTime), N'10556', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (318, N'342', N'Shiv Shankar Diesel', N'Swarajyapuri Road, Rai Kashinath more,
Gandhi Mai', N'Mr. Shri Chamaru Prasad', N'09234761317', N'09234761317', N'ssd342', N'Welcome2u', N'shivshankardiesel@gmail.com', N'East', N'Bihar', N'Gaya', N'TIN No. 10200814008', NULL, N'GST Prov. AA100617042665C', CAST(0xA7FE0000 AS SmallDateTime), CAST(0xA7E00000 AS SmallDateTime), N'14606', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (319, N'271', N'Standard Diesels', N'Basement No. 1
Satyam Chambers 
Sr. No. 414/4/4E', N'Mr. Rakhi Girijashankar S', N'09545505201', N'09545505201', N'sd271', N'Welcome2u', N'kushalmepl@yahoo.co.in', N'West', N'Maharastra', N'Pune', N'27AHYPS4790G1ZE', NULL, N'27AHYPS4790G1ZE', CAST(0xAAD80000 AS SmallDateTime), CAST(0xAABA0000 AS SmallDateTime), N'12696', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (320, N'493', N'Unitech Turbo & Power Steering Services', N'5/172, LNS Post
Covai Road, 	
Karur - 639 002	Ta', N'Mr. J.Gopal', N'09524722322', N'09524722322', N'UTPSS493', N'Welcome2u', N'unitechturbo@gmail.com', N'South', N'Tamil NAdu', N'Karur', N'GST No. 33BKLPG1489B1ZE', NULL, N'GST No. 33BKLPG1489B1ZE', CAST(0xAABB0000 AS SmallDateTime), CAST(0xAA9C0000 AS SmallDateTime), N'06597', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (321, N'258', N'Noori Motors', N'Savarkundla Road	Behind Dhanani Oil Mill	
Amreli ', N'Mr. Aminbhai Bhatti', N'02792220524', N'09925874060', N'NM258', N'Welcome2u', N'noorimotors@gmail.com', N'West', N'Gujarat', N'Amreli', N'CST NO: 24630101935', NULL, N'24AHRPB2732Q1ZI', CAST(0xAAD80000 AS SmallDateTime), CAST(0xAABA0000 AS SmallDateTime), N'10574', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (322, N'365', N'Imperial Agro Pvt. Ltd.', N'New Bypass Road, Khemni Chowk	
New Jaganpura
Nea', N'Mr. Uday Shankar Ojha', N'09771480717', N'09771480717', N'IAPL365', N'Welcome2u', N'ranepatna@gmail.com', N'East', N'Bihar', N'Patna', N'10AABCI9694J1ZB', NULL, N'10AABCI9694J1ZB', CAST(0xAAD80000 AS SmallDateTime), CAST(0xAABA0000 AS SmallDateTime), N'14606', 1, N'Owner')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (323, N'494', N'SN Auto Hydraulics', N'SN Auto Hydraulics
CH Bye Pass
Pandikkad Road
M', N'Mr. Nishan. C. & Mr. Salm', N'09995234111', N'09746943121', N'SNA494', N'Welcome2u', N'nishanc05@gmail.com', N'South', N'Kerala', N'Manjeri', N'32ADQFS4591P1Z6', NULL, N'32ADQFS4591P1Z6', CAST(0xAABA0000 AS SmallDateTime), CAST(0xAAD80000 AS SmallDateTime), N'10556', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (324, N'483', N'Manian Engineering Unit Centre', N'No: 10-K Raja Mill Road	
Madurai - 625 001	Tamil ', N'M. Prakash', N'09442029142', N'09442029142', N'MEUC483', N'Welcome2u', N'manian.engg.mdu@gmail.com', N'South', N'Tamil NAdu', N'Madurai', N'33AKMPP8624P1ZD', NULL, N'33AKMPP8624P1ZD', CAST(0xAAD80000 AS SmallDateTime), CAST(0xA94D0000 AS SmallDateTime), N'06597', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (325, N'196', N'Saurav Motor Workshop', N'Ambala Hisar Road	Near Payre Mohan Dharam Kata	Ism', N'Mr. Jai Ram', N'09996243037', N'09996243037', N'SMW196', N'Welcome2u', N'sauravmotorworkshop@gmail.com', N'North', N'Haryana', N'Ismailabad', N'06AKPPR6728P1Z4', NULL, N'06AKPPR6728P1Z4', CAST(0xAAD80000 AS SmallDateTime), CAST(0xA94E0000 AS SmallDateTime), N'08548', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (326, N'369', N'Gupta Diesel Service', N'Chapaguri Road	
North Bongaigaon - 783 380
Assam', N'Gaurav Gupta', N'09706894475', N'09706894475', N'gds369', N'pg1957pg', N'guptadiesel@gmail.com', N'East', N'Assam', N'North Bongaigaon', N'18ACUPG2204J1ZJ', NULL, N'11', CAST(0x000B0000 AS SmallDateTime), CAST(0x000B0000 AS SmallDateTime), N'11737', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (327, N'195', N'SRA Auto Agencies', N'Ganga Nagar	Narayanpur	Mirzapur - 231 305	Uttar Pr', N'Mr. Chitresh Srivastava', N'09721454820', N'09721454820', N'sra195', N'Welcome2u', N'sraauto14@gmail.com', N'North', N'Uttar Pradesh', N'Mirzapur', N'09DMOPS4252P1Z0', NULL, N'09DMOPS4252P1Z0', CAST(0xAAD80000 AS SmallDateTime), CAST(0xAABA0000 AS SmallDateTime), N'12485', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (328, N'270', N'Moniji Motors', N'Gut  No. Hyderabad Road	
Chandasingh Corner M.I.D', N'Amrik Singh', N'09881155134', N'09881155134', N'mm270', N'wELCOME2U', N'monijimotors@gmail.com', N'West', N'Maharastra', N'Nanded', N'27AJEPR6655F1ZW', NULL, N'27AJEPR6655F1ZW', CAST(0xAAD80000 AS SmallDateTime), CAST(0xAABB0000 AS SmallDateTime), N'06608', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (329, N'198', N'Shaan-E-Awadh Services', N'5, Kabir Mandir Market	Sitapur Road	Lucknow - 226 ', N'Mrs. Farooq Jahan Khan', N'9956562950', N'9415519502', N'seas198', N'farhanoor', N'shaaneawadhservices@gmail.com', N'North', N'Uttar Pradesh', N'Lucknow', N'09CWBPK7291L1ZX', NULL, N'11', CAST(0x000B0000 AS SmallDateTime), CAST(0x000B0000 AS SmallDateTime), N'12485', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (330, N'199', N'R. K. Sales & Service', N'Dandi Rewa Road	Opp. Jagwanti Hospital	Allahabad -', N'Rajesh Kumar Tiwari', N'09874918899', N'09874918899', N'rkss199', N'Welcome2u', N'raj.26jan1947@gmail.com', N'North', N'Uttar Pradesh', N'Allahabad', N'09AGPPT8998P1ZL', NULL, N'09AGPPT8998P1ZL', CAST(0xAAD80000 AS SmallDateTime), CAST(0xAABB0000 AS SmallDateTime), N'12485', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (331, N'496', N'Spectra Hosur', N'CP/DP, RCC Shop No.12 Ground Floor, 
Sri Sathya S', N'Ramprasad Kunkala', N'09845111577', N'09845111577', N'sh496', N'Welcome2u', N'spectrahos@gmail.com', N'South', N'Tamil Nadu', N'Hosur', N'33BNGPK5407F1ZB', N'33BNGPK5407F1ZB', N'33BNGPK5407F1ZB', CAST(0xAAD80000 AS SmallDateTime), CAST(0xAABB0000 AS SmallDateTime), N'06597', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (332, N'367', N'Arnav Turbo Services', N'NH-39, Near Hume Pipe	Khatkhati - 782 480	Karbi An', N'Mr. Surojit Sarkar', N'08876054191', N'08876054191', N'ATS367', N'Welcome2u', N'arnavturboservices@gmail.com', N'East', N'Assam', N'Khatkhati', N'18DRFPS4262H1ZF', N'18DRFPS4262H1ZF', N'18DRFPS4262H1ZF', CAST(0xAAD80000 AS SmallDateTime), CAST(0xAABA0000 AS SmallDateTime), N'11737', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (333, N'193', N'Kapoor Garage', N'NH-86, Samad Nagar	Kanpur Road  	
Mahoba - 210 42', N'Mr. Kapil Kapoor', N'09415145175', N'09415145175', N'kg193', N'Welcome2u', N'kkashokleyland@gmail.com', N'North', N'Uttar Pradesh', N'Mahoba', N'09AEIPK9695M1ZF', N'09AEIPK9695M1ZF', N'09AEIPK9695M1ZF', CAST(0xAAD80000 AS SmallDateTime), CAST(0xAABB0000 AS SmallDateTime), N'12485', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (334, N'373', N'Kisan Diesels', N'Near Ram Janki Mandir Lakhisariai By Pass Road	
B', N'Ranjit Kumar', N'09973486920', N'09973486920', N'KD373', N'Welcome2u', N'kisandiesel.jamui@gmail.com', N'East', N'Bihar', N'Jamui', N'10ASKPK9420A1ZB', N'10ASKPK9420A1ZB', N'10ASKPK9420A1ZB', CAST(0xAAD80000 AS SmallDateTime), CAST(0xAABA0000 AS SmallDateTime), N'14606', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (335, N'375', N'S. N. Motors', N'Mafa Petrol Pump	Near Dharam Kanta, 
NH 31	
Purn', N'Mohd Badrul Haque', N'09650141597', N'09716209564', N'SNM375', N'Welcome2u', N'snmotorsnoida1211@gmail.com', N'East', N'Bihar', N'Purnea', N'10AHOPH3068G1ZL', N'10AHOPH3068G1ZL', N'10AHOPH3068G1ZL', CAST(0xAAD80000 AS SmallDateTime), CAST(0xAABA0000 AS SmallDateTime), N'14606', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (336, N'289', N'Power Zone', N'Bachoo Compound,Opp.Fedex,Behind Gaurav Indian Pet', N'Mr Vanita Carvalho', N'NA', N'9223377256', N'Power3', N'Welcome2u', N'zonepower@gmail.com', N'West', N'Maharashtra', N'Bhiwandi', N'NA', N'27AAEPC3248E1Z5', N'NA', CAST(0xACA30000 AS SmallDateTime), CAST(0xADD20000 AS SmallDateTime), N'12696', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (337, N'386', N'Abhis Solution', N'C/O Paras Mistree RLSY College road,Jhumri Telaiya', N'Mr. Abhiraj Sharma', N'N/A ', N'9576183916', N'Abhis386', N'Welcome2u', N'mail2abhi800@gmail.com', N'East', N'Jharkhand', N'Jhumri Telaiya', N'NA', N'20FJUPS0282Q1ZE', N'NA', CAST(0xAB350000 AS SmallDateTime), CAST(0xADB30000 AS SmallDateTime), N'14606', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (340, N'273', N'Kohinoor Diesel', N'Nigahi Mode, Main Road Waidhan,Near Raj Fauji Rest', N'Ms. Priyanka Pramod Jadha', N'NA', N'094075-68698', N'Kohinoor273', N'Welcome2u', N'pramodjadhav19@gmail.com', N'West', N'Madhya Pradesh', N'Singrauli', N'NA', N'23AATFK4972A1ZA', N'NA', CAST(0xACA30000 AS SmallDateTime), CAST(0xADB30000 AS SmallDateTime), N'10574', 1, N'Partner')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (341, N'292', N'Aradhana Enterprises', N'H-405,Shop No:1/2, Tipl Colony,Jasai Naka,Jasai Vi', N'9320126710', N'NA', N'9320126710', N'Aradhana292', N'Welcome2u', N'aradhanaenterprises08@gmail.com', N'West', N'Maharashtra', N'Jasai,Uran', N'NA  ', N'27ASVPD7030N1Z7', N'NA', CAST(0xACA30000 AS SmallDateTime), CAST(0xADB30000 AS SmallDateTime), N'12696', 1, N'<-- Select -->')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (342, N' 355', N'New Diesel Injection', N'Rangirkhari,N.S. Avenue
opp-SBI New Silchar Branc', N'Mr. Sukesh Mohan Roy', N'NA', N'99574 - 82454', N'Newdi355', N'Welcome2u', N'newdiesel82@gmail.com', N'West', N'Assam', N'Silchar', N'NA', N'newdiesel82@gmail.com', N'NA', CAST(0xACA30000 AS SmallDateTime), CAST(0xADB30000 AS SmallDateTime), N'11737', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (343, N'399', N'Bhai Bhai Automobiles', N'vill-Bhabanipur,P.O-Bhatra	Rajgram Road,Murarai,	B', N'Mr.Nuralam Mallik', N'Nll', N'7134317877', N'Bhai399', N'Welcome2u', N'bhaibhaiautomobiles34@gmail.com', N'East', N'West Bengal', N'Bhabanipur', N'Nill', N'19AAVFB8870AIZ1', N'Nill', CAST(0xACA30000 AS SmallDateTime), CAST(0xADB30000 AS SmallDateTime), N'08509', 1, N'Partner')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (344, N'4006', N'Salem Tech Service', N'3/299-9,Circle Thottam,Kovai Bye Pass Road,	Salem-', N'Mr.T. Venkatesan', N'Nll', N'9952262846', N'Salem4006', N'Welcome2u', N'venkitvsfrds@gmail.com', N'South', N'Tamil Nadu', N'Salem', N'Nill', N'33AEBFS0796L1ZQ', N'Nill', CAST(0xACA30000 AS SmallDateTime), CAST(0xADB30000 AS SmallDateTime), N'06597', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (345, N'1017', N'Super Auto Sales', N'2/56 ,  Transport Nagar		Agra-282002
', N'Mr Pradeep Kumar Hura', N'Nll', N'9760022181', N'Super1017', N'Welcome2u', N'newturboyogesh2008@gmail.com', N'North', N'Uttar Pradesh', N'Agra', N'Nill', N'09AABPH8463K1ZD', N'Nill', CAST(0xACA30000 AS SmallDateTime), CAST(0xADB30000 AS SmallDateTime), NULL, 0, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (346, N'388', N'Motor Village', N'RGP/R02/J/1,I Beam, Rangpo, East Sikkim- 737 132	R', N'Mr. Hari shankar Sharma', N'Nil', N' 6296258738', N'Motor388', N'Welcome2u', N'motorvillagerangpo@gmail.com', N'East', N'Sikkim', N'Rangpo', N'Nill', N'11BYLPS2230J1ZI', N'Nill', CAST(0xACA30000 AS SmallDateTime), CAST(0xADD20000 AS SmallDateTime), N'08509', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (338, N'396', N'Rajeev Tradecomm Pvt.Ltd', N'N.H-28,Danapur,
Near Shree Gopal Auto
Gopalgunj-', N'Mr. Rajeev Kumar', N'NA', N'7360077141', N'Rajeev396', N'Welcome2u', N'gmservicerajeevtradecomm@gmail.com', N'East', N'Bihar', N'Gopalganj', N'NA  ', N'10AAKCS8871Q1ZL', N'NA', CAST(0xACA30000 AS SmallDateTime), CAST(0xADB30000 AS SmallDateTime), N'14606', 1, N'Owner')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (339, N'277', N'Power Zone', N'Satguru Elegance, Shop No.3, Behind Rutu Park, 
M', N'Mr Vanita Carvalho', N'NA', N'09223377284', N'Power2', N'Welcome2u', N'zonepower@gmail.com', N'West', N'Maharashtra', N'Thane', N'NA', N'27AAEPC3248E1Z5', N'NA', CAST(0xAC840000 AS SmallDateTime), CAST(0xADB30000 AS SmallDateTime), N'12696', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (347, N'1022', N'GNA Brakes & Streering Solutions', N'Shop no.21-22, Transport Nagar,	Ropar,	punjab-140-', N'Mr.Nakul Mehta', N'Nill   ', N'Mobile:98780-44507', N'GNA1022', N'Welcome2u', N'gsingh_1977@rediffmail.com', N'North', N'Punjab', N'Ropar', N'03AAVFG4010D1ZV', N'03AAVFG4010D1ZV', N'Nill', CAST(0xACA30000 AS SmallDateTime), CAST(0xADB30000 AS SmallDateTime), N'10631', 1, N'Partner')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (348, N'4008', N'Sun Turbo & Power Steering', N'No.8/1-1 D.R.Complex,	Old Bye-pass Road,	Vellore-6', N'Mr.S. Devendiran', N'Nil', N'9894761233', N'Sun4008', N'Welcome2u', N'dmv.vlr@gmail.com', N'South', N'Tamil Nadu', N'Vellore', N'Nill', N'33AYOPD5269N1ZW', N'Nill', CAST(0xACA30000 AS SmallDateTime), CAST(0xADB30000 AS SmallDateTime), N'06597', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (349, N'291', N'M.S. Automobiles', N'Lalpuliya, Near Hp Petrol Pump,	Yavatmal Road,Wani', N'Mr. Mohd yunus mohd yusuf', N'Nll', N'9322065981', N'Msauto291', N'Welcome2u', N'msautomobiles14@gmail.com', N'West', N'Maharashtra', N'Yawatmal', N'Nill', N'27BQEPS8576E1ZA', N'Nill', CAST(0xACA30000 AS SmallDateTime), CAST(0xADB30000 AS SmallDateTime), N'06608', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (350, N'3000', N'New Allied Power', N'Amar Complex,	Opp. Hijuguri Police out Post,Hijugu', N'Mr. P. Radhakrishnan', N'Nll', N'Phone : 600199291', N'Newallied3000', N'Welcome2u', N'Pradhakrishnan898@gmail.com', N'East', N'Assam', N'Tinsukia', N'Nill', N'18AFHPR6571F1Z2', N'Nill', CAST(0xACA30000 AS SmallDateTime), CAST(0xADB30000 AS SmallDateTime), N'11737', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (1349, N'4007', N'Sri Sabari Auto Agency', N'No.1/420-H,Salem to Karur main road,	Mudhalaipatty', N'Mr.K. Sabarinathan', N'nil', N'Mobile:6383119968', N'Srisabari4007', N'Welcome2u', N'sabarinkl1996@gmail.com', N'South', N'Tamil Nadu', N'Namakkal', N'NIL', N'33GZUPS4247C1ZY', N'NIL', CAST(0xACA30000 AS SmallDateTime), CAST(0xADB30000 AS SmallDateTime), N'06597', 1, N'Propreiter')
INSERT [dbo].[Authorized] ([ASCID], [ASCCode], [Name], [Address], [InchargeName], [contactNo], [mobileNo], [userName], [password], [emailId], [Region], [State], [Place], [CsiNo], [GSTNo], [TinNo], [CertificateExpirydate], [ReminderRenewalDate], [serviceEng], [flag], [Destination]) VALUES (1350, N'290', N'Shivansh Motors', N'Survey No.83/02,Shop No.03,Sai Prayag Complex	Kopa', N'Mr. Kotade Bhushan', N'Nil', N'Mobile: 7263091234', N'Shivansh290', N'Welcome2u', N'shivudyog.motors1234@gmail.com', N'West', N'Maharashtra', N'Kopargaon', N'Nill', N'27BNPPK1926A1Z5', N'Nill', CAST(0xACA30000 AS SmallDateTime), CAST(0xADB30000 AS SmallDateTime), N'12696', 1, N'Propreiter')
SET IDENTITY_INSERT [dbo].[Authorized] OFF
SET IDENTITY_INSERT [dbo].[cities] ON 

INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1, N'Kolhapur', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (2, N'Port Blair', N'Andaman & Nicobar Islands')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (3, N'Adilabad', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (4, N'Adoni', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (5, N'Amadalavalasa', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (6, N'Amalapuram', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (7, N'Anakapalle', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (8, N'Anantapur', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (9, N'Badepalle', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (10, N'Banganapalle', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (11, N'Bapatla', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (12, N'Bellampalle', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (13, N'Bethamcherla', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (14, N'Bhadrachalam', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (15, N'Bhainsa', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (16, N'Bheemunipatnam', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (17, N'Bhimavaram', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (18, N'Bhongir', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (19, N'Bobbili', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (20, N'Bodhan', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (21, N'Chilakaluripet', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (22, N'Chirala', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (23, N'Chittoor', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (24, N'Cuddapah', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (25, N'Devarakonda', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (26, N'Dharmavaram', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (27, N'Eluru', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (28, N'Farooqnagar', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (29, N'Gadwal', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (30, N'Gooty', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (31, N'Gudivada', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (32, N'Gudur', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (33, N'Guntakal', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (34, N'Guntur', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (35, N'Hanuman Junction', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (36, N'Hindupur', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (37, N'Hyderabad', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (38, N'Ichchapuram', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (39, N'Jaggaiahpet', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (40, N'Jagtial', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (41, N'Jammalamadugu', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (42, N'Jangaon', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (43, N'Kadapa', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (44, N'Kadiri', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (45, N'Kagaznagar', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (46, N'Kakinada', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (47, N'Kalyandurg', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (48, N'Kamareddy', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (49, N'Kandukur', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (50, N'Karimnagar', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (51, N'Kavali', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (52, N'Khammam', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (53, N'Koratla', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (54, N'Kothagudem', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (55, N'Kothapeta', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (56, N'Kovvur', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (57, N'Kurnool', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (58, N'Kyathampalle', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (59, N'Macherla', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (60, N'Machilipatnam', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (61, N'Madanapalle', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (62, N'Mahbubnagar', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (63, N'Mancherial', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (64, N'Mandamarri', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (65, N'Mandapeta', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (66, N'Manuguru', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (67, N'Markapur', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (68, N'Medak', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (69, N'Miryalaguda', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (70, N'Mogalthur', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (71, N'Nagari', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (72, N'Nagarkurnool', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (73, N'Nandyal', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (74, N'Narasapur', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (75, N'Narasaraopet', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (76, N'Narayanpet', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (77, N'Narsipatnam', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (78, N'Nellore', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (79, N'Nidadavole', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (80, N'Nirmal', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (81, N'Nizamabad', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (82, N'Nuzvid', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (83, N'Ongole', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (84, N'Palacole', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (85, N'Palasa Kasibugga', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (86, N'Palwancha', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (87, N'Parvathipuram', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (88, N'Pedana', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (89, N'Peddapuram', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (90, N'Pithapuram', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (91, N'Pondur', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (92, N'Ponnur', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (93, N'Proddatur', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (94, N'Punganur', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (95, N'Puttur', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (96, N'Rajahmundry', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (97, N'Rajam', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (98, N'Ramachandrapuram', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (99, N'Ramagundam', N'Andhra Pradesh')
GO
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (100, N'Rayachoti', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (101, N'Rayadurg', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (102, N'Renigunta', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (103, N'Repalle', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (104, N'Sadasivpet', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (105, N'Salur', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (106, N'Samalkot', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (107, N'Sangareddy', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (108, N'Sattenapalle', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (109, N'Siddipet', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (110, N'Singapur', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (111, N'Sircilla', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (112, N'Srikakulam', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (113, N'Srikalahasti', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (115, N'Suryapet', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (116, N'Tadepalligudem', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (117, N'Tadpatri', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (118, N'Tandur', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (119, N'Tanuku', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (120, N'Tenali', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (121, N'Tirupati', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (122, N'Tuni', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (123, N'Uravakonda', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (124, N'Venkatagiri', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (125, N'Vicarabad', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (126, N'Vijayawada', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (127, N'Vinukonda', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (128, N'Visakhapatnam', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (129, N'Vizianagaram', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (130, N'Wanaparthy', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (131, N'Warangal', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (132, N'Yellandu', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (133, N'Yemmiganur', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (134, N'Yerraguntla', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (135, N'Zahirabad', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (136, N'Rajampet', N'Andhra Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (137, N'Along', N'Arunachal Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (138, N'Bomdila', N'Arunachal Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (139, N'Itanagar', N'Arunachal Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (140, N'Naharlagun', N'Arunachal Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (141, N'Pasighat', N'Arunachal Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (142, N'Abhayapuri', N'Assam')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (143, N'Amguri', N'Assam')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (144, N'Anandnagaar', N'Assam')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (145, N'Barpeta', N'Assam')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (146, N'Barpeta Road', N'Assam')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (147, N'Bilasipara', N'Assam')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (148, N'Bongaigaon', N'Assam')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (149, N'Dhekiajuli', N'Assam')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (150, N'Dhubri', N'Assam')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (151, N'Dibrugarh', N'Assam')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (152, N'Digboi', N'Assam')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (153, N'Diphu', N'Assam')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (154, N'Dispur', N'Assam')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (156, N'Gauripur', N'Assam')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (157, N'Goalpara', N'Assam')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (158, N'Golaghat', N'Assam')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (159, N'Guwahati', N'Assam')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (160, N'Haflong', N'Assam')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (161, N'Hailakandi', N'Assam')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (162, N'Hojai', N'Assam')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (163, N'Jorhat', N'Assam')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (164, N'Karimganj', N'Assam')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (165, N'Kokrajhar', N'Assam')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (166, N'Lanka', N'Assam')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (167, N'Lumding', N'Assam')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (168, N'Mangaldoi', N'Assam')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (169, N'Mankachar', N'Assam')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (170, N'Margherita', N'Assam')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (171, N'Mariani', N'Assam')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (172, N'Marigaon', N'Assam')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (173, N'Nagaon', N'Assam')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (174, N'Nalbari', N'Assam')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (175, N'North Lakhimpur', N'Assam')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (176, N'Rangia', N'Assam')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (177, N'Sibsagar', N'Assam')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (178, N'Silapathar', N'Assam')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (179, N'Silchar', N'Assam')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (180, N'Tezpur', N'Assam')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (181, N'Tinsukia', N'Assam')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (182, N'Amarpur', N'Bihar')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (183, N'Araria', N'Bihar')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (184, N'Areraj', N'Bihar')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (185, N'Arrah', N'Bihar')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (186, N'Asarganj', N'Bihar')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (187, N'Aurangabad', N'Bihar')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (188, N'Bagaha', N'Bihar')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (189, N'Bahadurganj', N'Bihar')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (190, N'Bairgania', N'Bihar')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (191, N'Bakhtiarpur', N'Bihar')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (192, N'Banka', N'Bihar')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (193, N'Banmankhi Bazar', N'Bihar')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (194, N'Barahiya', N'Bihar')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (195, N'Barauli', N'Bihar')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (196, N'Barbigha', N'Bihar')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (197, N'Barh', N'Bihar')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (198, N'Begusarai', N'Bihar')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (199, N'Behea', N'Bihar')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (200, N'Bettiah', N'Bihar')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (201, N'Bhabua', N'Bihar')
GO
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (202, N'Bhagalpur', N'Bihar')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (203, N'Bihar Sharif', N'Bihar')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (204, N'Bikramganj', N'Bihar')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (205, N'Bodh Gaya', N'Bihar')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (206, N'Buxar', N'Bihar')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (207, N'Chandan Bara', N'Bihar')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (208, N'Chanpatia', N'Bihar')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (209, N'Chhapra', N'Bihar')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (210, N'Colgong', N'Bihar')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (211, N'Dalsinghsarai', N'Bihar')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (212, N'Darbhanga', N'Bihar')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (213, N'Daudnagar', N'Bihar')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (214, N'Dehri-on-Sone', N'Bihar')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (215, N'Dhaka', N'Bihar')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (216, N'Dighwara', N'Bihar')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (217, N'Dumraon', N'Bihar')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (218, N'Fatwah', N'Bihar')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (219, N'Forbesganj', N'Bihar')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (220, N'Gaya', N'Bihar')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (221, N'Gogri Jamalpur', N'Bihar')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (222, N'Gopalganj', N'Bihar')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (223, N'Hajipur', N'Bihar')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (224, N'Hilsa', N'Bihar')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (225, N'Hisua', N'Bihar')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (226, N'Islampur', N'Bihar')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (227, N'Jagdispur', N'Bihar')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (228, N'Jamalpur', N'Bihar')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (229, N'Jamui', N'Bihar')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (230, N'Jehanabad', N'Bihar')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (231, N'Jhajha', N'Bihar')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (232, N'Jhanjharpur', N'Bihar')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (233, N'Jogabani', N'Bihar')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (234, N'Kanti', N'Bihar')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (235, N'Katihar', N'Bihar')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (236, N'Khagaria', N'Bihar')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (237, N'Kharagpur', N'Bihar')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (238, N'Kishanganj', N'Bihar')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (239, N'Lakhisarai', N'Bihar')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (240, N'Lalganj', N'Bihar')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (241, N'Madhepura', N'Bihar')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (242, N'Madhubani', N'Bihar')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (243, N'Maharajganj', N'Bihar')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (244, N'Mahnar Bazar', N'Bihar')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (245, N'Makhdumpur', N'Bihar')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (246, N'Maner', N'Bihar')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (247, N'Manihari', N'Bihar')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (248, N'Marhaura', N'Bihar')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (249, N'Masaurhi', N'Bihar')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (250, N'Mirganj', N'Bihar')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (251, N'Mokameh', N'Bihar')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (252, N'Motihari', N'Bihar')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (253, N'Motipur', N'Bihar')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (254, N'Munger', N'Bihar')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (255, N'Murliganj', N'Bihar')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (256, N'Muzaffarpur', N'Bihar')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (257, N'Narkatiaganj', N'Bihar')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (258, N'Naugachhia', N'Bihar')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (259, N'Nawada', N'Bihar')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (260, N'Nokha', N'Bihar')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (261, N'Patna', N'Bihar')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (262, N'Piro', N'Bihar')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (263, N'Purnia', N'Bihar')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (264, N'Rafiganj', N'Bihar')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (265, N'Rajgir', N'Bihar')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (266, N'Ramnagar', N'Bihar')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (267, N'Raxaul Bazar', N'Bihar')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (268, N'Revelganj', N'Bihar')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (269, N'Rosera', N'Bihar')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (270, N'Saharsa', N'Bihar')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (271, N'Samastipur', N'Bihar')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (272, N'Sasaram', N'Bihar')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (273, N'Sheikhpura', N'Bihar')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (274, N'Sheohar', N'Bihar')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (275, N'Sherghati', N'Bihar')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (276, N'Silao', N'Bihar')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (277, N'Sitamarhi', N'Bihar')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (278, N'Siwan', N'Bihar')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (279, N'Sonepur', N'Bihar')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (280, N'Sugauli', N'Bihar')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (281, N'Sultanganj', N'Bihar')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (282, N'Supaul', N'Bihar')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (283, N'Warisaliganj', N'Bihar')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (284, N'Ahiwara', N'Chhattisgarh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (285, N'Akaltara', N'Chhattisgarh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (286, N'Ambagarh Chowki', N'Chhattisgarh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (287, N'Ambikapur', N'Chhattisgarh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (288, N'Arang', N'Chhattisgarh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (289, N'Bade Bacheli', N'Chhattisgarh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (290, N'Balod', N'Chhattisgarh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (291, N'Baloda Bazar', N'Chhattisgarh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (292, N'Bemetra', N'Chhattisgarh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (293, N'Bhatapara', N'Chhattisgarh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (294, N'Bilaspur', N'Chhattisgarh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (295, N'Birgaon', N'Chhattisgarh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (296, N'Champa', N'Chhattisgarh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (297, N'Chirmiri', N'Chhattisgarh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (298, N'Dalli-Rajhara', N'Chhattisgarh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (299, N'Dhamtari', N'Chhattisgarh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (300, N'Dipka', N'Chhattisgarh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (301, N'Dongargarh', N'Chhattisgarh')
GO
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (302, N'Durg-Bhilai Nagar', N'Chhattisgarh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (303, N'Gobranawapara', N'Chhattisgarh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (304, N'Jagdalpur', N'Chhattisgarh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (305, N'Janjgir', N'Chhattisgarh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (306, N'Jashpurnagar', N'Chhattisgarh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (307, N'Kanker', N'Chhattisgarh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (308, N'Kawardha', N'Chhattisgarh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (309, N'Kondagaon', N'Chhattisgarh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (310, N'Korba', N'Chhattisgarh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (311, N'Mahasamund', N'Chhattisgarh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (312, N'Mahendragarh', N'Chhattisgarh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (313, N'Mungeli', N'Chhattisgarh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (314, N'Naila Janjgir', N'Chhattisgarh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (315, N'Raigarh', N'Chhattisgarh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (316, N'Raipur', N'Chhattisgarh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (317, N'Rajnandgaon', N'Chhattisgarh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (318, N'Sakti', N'Chhattisgarh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (319, N'Tilda Newra', N'Chhattisgarh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (320, N'Amli', N'Dadra & Nagar Haveli')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (321, N'Silvassa', N'Dadra & Nagar Haveli')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (322, N'Daman and Diu', N'Daman & Diu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (323, N'Daman and Diu', N'Daman & Diu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (324, N'Asola', N'Delhi')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (325, N'Delhi', N'Delhi')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (326, N'Aldona', N'Goa')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (327, N'Curchorem Cacora', N'Goa')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (328, N'Madgaon', N'Goa')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (329, N'Mapusa', N'Goa')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (330, N'Margao', N'Goa')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (331, N'Marmagao', N'Goa')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (332, N'Panaji', N'Goa')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (333, N'Ahmedabad', N'Gujarat')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (334, N'Amreli', N'Gujarat')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (335, N'Anand', N'Gujarat')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (336, N'Ankleshwar', N'Gujarat')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (337, N'Bharuch', N'Gujarat')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (338, N'Bhavnagar', N'Gujarat')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (339, N'Bhuj', N'Gujarat')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (340, N'Cambay', N'Gujarat')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (341, N'Dahod', N'Gujarat')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (342, N'Deesa', N'Gujarat')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (343, N'Dharampur', N' India')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (344, N'Dholka', N'Gujarat')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (345, N'Gandhinagar', N'Gujarat')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (346, N'Godhra', N'Gujarat')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (347, N'Himatnagar', N'Gujarat')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (348, N'Idar', N'Gujarat')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (349, N'Jamnagar', N'Gujarat')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (350, N'Junagadh', N'Gujarat')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (351, N'Kadi', N'Gujarat')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (352, N'Kalavad', N'Gujarat')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (353, N'Kalol', N'Gujarat')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (354, N'Kapadvanj', N'Gujarat')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (355, N'Karjan', N'Gujarat')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (356, N'Keshod', N'Gujarat')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (357, N'Khambhalia', N'Gujarat')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (358, N'Khambhat', N'Gujarat')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (359, N'Kheda', N'Gujarat')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (360, N'Khedbrahma', N'Gujarat')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (361, N'Kheralu', N'Gujarat')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (362, N'Kodinar', N'Gujarat')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (363, N'Lathi', N'Gujarat')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (364, N'Limbdi', N'Gujarat')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (365, N'Lunawada', N'Gujarat')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (366, N'Mahesana', N'Gujarat')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (367, N'Mahuva', N'Gujarat')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (368, N'Manavadar', N'Gujarat')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (369, N'Mandvi', N'Gujarat')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (370, N'Mangrol', N'Gujarat')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (371, N'Mansa', N'Gujarat')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (372, N'Mehmedabad', N'Gujarat')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (373, N'Modasa', N'Gujarat')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (374, N'Morvi', N'Gujarat')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (375, N'Nadiad', N'Gujarat')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (376, N'Navsari', N'Gujarat')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (377, N'Padra', N'Gujarat')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (378, N'Palanpur', N'Gujarat')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (379, N'Palitana', N'Gujarat')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (380, N'Pardi', N'Gujarat')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (381, N'Patan', N'Gujarat')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (382, N'Petlad', N'Gujarat')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (383, N'Porbandar', N'Gujarat')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (384, N'Radhanpur', N'Gujarat')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (385, N'Rajkot', N'Gujarat')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (386, N'Rajpipla', N'Gujarat')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (387, N'Rajula', N'Gujarat')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (388, N'Ranavav', N'Gujarat')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (389, N'Rapar', N'Gujarat')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (390, N'Salaya', N'Gujarat')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (391, N'Sanand', N'Gujarat')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (392, N'Savarkundla', N'Gujarat')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (393, N'Sidhpur', N'Gujarat')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (394, N'Sihor', N'Gujarat')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (395, N'Songadh', N'Gujarat')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (396, N'Surat', N'Gujarat')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (397, N'Talaja', N'Gujarat')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (398, N'Thangadh', N'Gujarat')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (399, N'Tharad', N'Gujarat')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (400, N'Umbergaon', N'Gujarat')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (401, N'Umreth', N'Gujarat')
GO
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (402, N'Una', N'Gujarat')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (403, N'Unjha', N'Gujarat')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (404, N'Upleta', N'Gujarat')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (405, N'Vadnagar', N'Gujarat')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (406, N'Vadodara', N'Gujarat')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (407, N'Valsad', N'Gujarat')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (408, N'Vapi', N'Gujarat')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (409, N'Vapi', N'Gujarat')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (410, N'Veraval', N'Gujarat')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (411, N'Vijapur', N'Gujarat')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (412, N'Viramgam', N'Gujarat')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (413, N'Visnagar', N'Gujarat')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (414, N'Vyara', N'Gujarat')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (415, N'Wadhwan', N'Gujarat')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (416, N'Wankaner', N'Gujarat')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (417, N'Adalaj', N'Gujarat')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (418, N'Adityana', N'Gujarat')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (419, N'Alang', N'Gujarat')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (420, N'Ambaji', N'Gujarat')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (421, N'Ambaliyasan', N'Gujarat')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (422, N'Andada', N'Gujarat')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (423, N'Anjar', N'Gujarat')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (424, N'Anklav', N'Gujarat')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (425, N'Antaliya', N'Gujarat')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (426, N'Arambhada', N'Gujarat')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (427, N'Atul', N'Gujarat')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (428, N'Ballabhgarh', N'Haryana')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (429, N'Ambala', N'Haryana')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (430, N'Ambala', N'Haryana')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (431, N'Asankhurd', N'Haryana')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (432, N'Assandh', N'Haryana')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (433, N'Ateli', N'Haryana')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (434, N'Babiyal', N'Haryana')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (435, N'Bahadurgarh', N'Haryana')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (436, N'Barwala', N'Haryana')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (437, N'Bhiwani', N'Haryana')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (438, N'Charkhi Dadri', N'Haryana')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (439, N'Cheeka', N'Haryana')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (440, N'Ellenabad 2', N'Haryana')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (441, N'Faridabad', N'Haryana')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (442, N'Fatehabad', N'Haryana')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (443, N'Ganaur', N'Haryana')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (444, N'Gharaunda', N'Haryana')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (445, N'Gohana', N'Haryana')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (446, N'Gurgaon', N'Haryana')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (447, N'Haibat(Yamuna Nagar)', N'Haryana')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (448, N'Hansi', N'Haryana')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (449, N'Hisar', N'Haryana')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (450, N'Hodal', N'Haryana')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (451, N'Jhajjar', N'Haryana')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (452, N'Jind', N'Haryana')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (453, N'Kaithal', N'Haryana')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (454, N'Kalan Wali', N'Haryana')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (455, N'Kalka', N'Haryana')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (456, N'Karnal', N'Haryana')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (457, N'Ladwa', N'Haryana')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (458, N'Mahendragarh', N'Haryana')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (459, N'Mandi Dabwali', N'Haryana')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (460, N'Narnaul', N'Haryana')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (461, N'Narwana', N'Haryana')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (462, N'Palwal', N'Haryana')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (463, N'Panchkula', N'Haryana')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (464, N'Panipat', N'Haryana')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (465, N'Pehowa', N'Haryana')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (466, N'Pinjore', N'Haryana')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (467, N'Rania', N'Haryana')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (468, N'Ratia', N'Haryana')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (469, N'Rewari', N'Haryana')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (470, N'Rohtak', N'Haryana')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (471, N'Safidon', N'Haryana')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (472, N'Samalkha', N'Haryana')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (473, N'Shahbad', N'Haryana')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (474, N'Sirsa', N'Haryana')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (475, N'Sohna', N'Haryana')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (476, N'Sonipat', N'Haryana')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (477, N'Taraori', N'Haryana')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (478, N'Thanesar', N'Haryana')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (479, N'Tohana', N'Haryana')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (480, N'Yamunanagar', N'Haryana')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (481, N'Arki', N'Himachal Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (482, N'Baddi', N'Himachal Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (483, N'Bilaspur', N'Himachal Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (484, N'Chamba', N'Himachal Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (485, N'Dalhousie', N'Himachal Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (486, N'Dharamsala', N'Himachal Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (487, N'Hamirpur', N'Himachal Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (488, N'Mandi', N'Himachal Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (489, N'Nahan', N'Himachal Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (490, N'Shimla', N'Himachal Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (491, N'Solan', N'Himachal Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (492, N'Sundarnagar', N'Himachal Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (493, N'Jammu', N'Jammu & Kashmir')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (494, N'Achabbal', N'Jammu & Kashmir')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (495, N'Akhnoor', N'Jammu & Kashmir')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (496, N'Anantnag', N'Jammu & Kashmir')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (497, N'Arnia', N'Jammu & Kashmir')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (498, N'Awantipora', N'Jammu & Kashmir')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (499, N'Bandipore', N'Jammu & Kashmir')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (500, N'Baramula', N'Jammu & Kashmir')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (501, N'Kathua', N'Jammu & Kashmir')
GO
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (502, N'Leh', N'Jammu & Kashmir')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (503, N'Punch', N'Jammu & Kashmir')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (504, N'Rajauri', N'Jammu & Kashmir')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (505, N'Sopore', N'Jammu & Kashmir')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (506, N'Srinagar', N'Jammu & Kashmir')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (507, N'Udhampur', N'Jammu & Kashmir')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (508, N'Amlabad', N'Jharkhand')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (509, N'Ara', N'Jharkhand')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (510, N'Barughutu', N'Jharkhand')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (511, N'Bokaro Steel City', N'Jharkhand')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (512, N'Chaibasa', N'Jharkhand')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (513, N'Chakradharpur', N'Jharkhand')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (514, N'Chandrapura', N'Jharkhand')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (515, N'Chatra', N'Jharkhand')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (516, N'Chirkunda', N'Jharkhand')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (517, N'Churi', N'Jharkhand')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (518, N'Daltonganj', N'Jharkhand')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (519, N'Deoghar', N'Jharkhand')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (520, N'Dhanbad', N'Jharkhand')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (521, N'Dumka', N'Jharkhand')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (522, N'Garhwa', N'Jharkhand')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (523, N'Ghatshila', N'Jharkhand')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (524, N'Giridih', N'Jharkhand')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (525, N'Godda', N'Jharkhand')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (526, N'Gomoh', N'Jharkhand')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (527, N'Gumia', N'Jharkhand')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (528, N'Gumla', N'Jharkhand')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (529, N'Hazaribag', N'Jharkhand')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (530, N'Hussainabad', N'Jharkhand')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (531, N'Jamshedpur', N'Jharkhand')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (532, N'Jamtara', N'Jharkhand')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (533, N'Jhumri Tilaiya', N'Jharkhand')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (534, N'Khunti', N'Jharkhand')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (535, N'Lohardaga', N'Jharkhand')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (536, N'Madhupur', N'Jharkhand')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (537, N'Mihijam', N'Jharkhand')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (538, N'Musabani', N'Jharkhand')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (539, N'Pakaur', N'Jharkhand')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (540, N'Patratu', N'Jharkhand')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (541, N'Phusro', N'Jharkhand')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (542, N'Ramngarh', N'Jharkhand')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (543, N'Ranchi', N'Jharkhand')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (544, N'Sahibganj', N'Jharkhand')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (545, N'Saunda', N'Jharkhand')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (546, N'Simdega', N'Jharkhand')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (547, N'Tenu Dam-cum- Kathhara', N'Jharkhand')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (548, N'Arasikere', N'Karnataka')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (549, N'Bangalore', N'Karnataka')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (550, N'Belgaum', N'Karnataka')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (551, N'Bellary', N'Karnataka')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (552, N'Chamrajnagar', N'Karnataka')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (553, N'Chikkaballapur', N'Karnataka')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (554, N'Chintamani', N'Karnataka')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (555, N'Chitradurga', N'Karnataka')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (556, N'Gulbarga', N'Karnataka')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (557, N'Gundlupet', N'Karnataka')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (558, N'Hassan', N'Karnataka')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (559, N'Hospet', N'Karnataka')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (560, N'Hubli', N'Karnataka')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (561, N'Karkala', N'Karnataka')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (562, N'Karwar', N'Karnataka')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (563, N'Kolar', N'Karnataka')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (564, N'Kota', N'Karnataka')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (565, N'Lakshmeshwar', N'Karnataka')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (566, N'Lingsugur', N'Karnataka')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (567, N'Maddur', N'Karnataka')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (568, N'Madhugiri', N'Karnataka')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (569, N'Madikeri', N'Karnataka')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (570, N'Magadi', N'Karnataka')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (571, N'Mahalingpur', N'Karnataka')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (572, N'Malavalli', N'Karnataka')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (573, N'Malur', N'Karnataka')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (574, N'Mandya', N'Karnataka')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (575, N'Mangalore', N'Karnataka')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (576, N'Manvi', N'Karnataka')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (577, N'Mudalgi', N'Karnataka')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (578, N'Mudbidri', N'Karnataka')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (579, N'Muddebihal', N'Karnataka')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (580, N'Mudhol', N'Karnataka')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (581, N'Mulbagal', N'Karnataka')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (582, N'Mundargi', N'Karnataka')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (583, N'Mysore', N'Karnataka')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (584, N'Nanjangud', N'Karnataka')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (585, N'Pavagada', N'Karnataka')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (586, N'Puttur', N'Karnataka')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (587, N'Rabkavi Banhatti', N'Karnataka')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (588, N'Raichur', N'Karnataka')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (589, N'Ramanagaram', N'Karnataka')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (590, N'Ramdurg', N'Karnataka')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (591, N'Ranibennur', N'Karnataka')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (592, N'Robertson Pet', N'Karnataka')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (593, N'Ron', N'Karnataka')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (594, N'Sadalgi', N'Karnataka')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (595, N'Sagar', N'Karnataka')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (596, N'Sakleshpur', N'Karnataka')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (597, N'Sandur', N'Karnataka')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (598, N'Sankeshwar', N'Karnataka')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (599, N'Saundatti-Yellamma', N'Karnataka')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (600, N'Savanur', N'Karnataka')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (601, N'Sedam', N'Karnataka')
GO
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (602, N'Shahabad', N'Karnataka')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (603, N'Shahpur', N'Karnataka')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (604, N'Shiggaon', N'Karnataka')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (605, N'Shikapur', N'Karnataka')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (606, N'Shimoga', N'Karnataka')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (607, N'Shorapur', N'Karnataka')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (608, N'Shrirangapattana', N'Karnataka')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (609, N'Sidlaghatta', N'Karnataka')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (610, N'Sindgi', N'Karnataka')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (611, N'Sindhnur', N'Karnataka')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (612, N'Sira', N'Karnataka')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (613, N'Sirsi', N'Karnataka')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (614, N'Siruguppa', N'Karnataka')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (615, N'Srinivaspur', N'Karnataka')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (616, N'Talikota', N'Karnataka')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (617, N'Tarikere', N'Karnataka')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (618, N'Tekkalakota', N'Karnataka')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (619, N'Terdal', N'Karnataka')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (620, N'Tiptur', N'Karnataka')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (621, N'Tumkur', N'Karnataka')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (622, N'Udupi', N'Karnataka')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (623, N'Vijayapura', N'Karnataka')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (624, N'Wadi', N'Karnataka')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (625, N'Yadgir', N'Karnataka')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (626, N'Adoor', N'Kerala')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (627, N'Akathiyoor', N'Kerala')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (628, N'Alappuzha', N'Kerala')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (629, N'Ancharakandy', N'Kerala')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (630, N'Aroor', N'Kerala')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (631, N'Ashtamichira', N'Kerala')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (632, N'Attingal', N'Kerala')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (633, N'Avinissery', N'Kerala')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (634, N'Chalakudy', N'Kerala')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (635, N'Changanassery', N'Kerala')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (636, N'Chendamangalam', N'Kerala')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (637, N'Chengannur', N'Kerala')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (638, N'Cherthala', N'Kerala')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (639, N'Cheruthazham', N'Kerala')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (640, N'Chittur-Thathamangalam', N'Kerala')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (641, N'Chockli', N'Kerala')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (642, N'Erattupetta', N'Kerala')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (643, N'Guruvayoor', N'Kerala')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (644, N'Irinjalakuda', N'Kerala')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (645, N'Kadirur', N'Kerala')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (646, N'Kalliasseri', N'Kerala')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (647, N'Kalpetta', N'Kerala')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (648, N'Kanhangad', N'Kerala')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (649, N'Kanjikkuzhi', N'Kerala')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (650, N'Kannur', N'Kerala')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (651, N'Kasaragod', N'Kerala')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (652, N'Kayamkulam', N'Kerala')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (653, N'Kochi', N'Kerala')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (654, N'Kodungallur', N'Kerala')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (655, N'Kollam', N'Kerala')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (656, N'Koothuparamba', N'Kerala')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (657, N'Kothamangalam', N'Kerala')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (658, N'Kottayam', N'Kerala')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (659, N'Kozhikode', N'Kerala')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (660, N'Kunnamkulam', N'Kerala')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (661, N'Malappuram', N'Kerala')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (662, N'Mattannur', N'Kerala')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (663, N'Mavelikkara', N'Kerala')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (664, N'Mavoor', N'Kerala')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (665, N'Muvattupuzha', N'Kerala')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (666, N'Nedumangad', N'Kerala')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (667, N'Neyyattinkara', N'Kerala')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (668, N'Ottappalam', N'Kerala')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (669, N'Palai', N'Kerala')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (670, N'Palakkad', N'Kerala')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (671, N'Panniyannur', N'Kerala')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (672, N'Pappinisseri', N'Kerala')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (673, N'Paravoor', N'Kerala')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (674, N'Pathanamthitta', N'Kerala')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (675, N'Payyannur', N'Kerala')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (676, N'Peringathur', N'Kerala')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (677, N'Perinthalmanna', N'Kerala')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (678, N'Perumbavoor', N'Kerala')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (679, N'Ponnani', N'Kerala')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (680, N'Punalur', N'Kerala')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (681, N'Quilandy', N'Kerala')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (682, N'Shoranur', N'Kerala')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (683, N'Taliparamba', N'Kerala')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (684, N'Thiruvalla', N'Kerala')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (685, N'Thiruvananthapuram', N'Kerala')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (686, N'Thodupuzha', N'Kerala')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (687, N'Thrissur', N'Kerala')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (688, N'Tirur', N'Kerala')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (689, N'Vadakara', N'Kerala')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (690, N'Vaikom', N'Kerala')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (691, N'Varkala', N'Kerala')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (692, N'Kavaratti', N'Lakshadweep')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (693, N'Ashok Nagar', N'Madhya Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (694, N'Balaghat', N'Madhya Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (695, N'Betul', N'Madhya Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (696, N'Bhopal', N'Madhya Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (697, N'Burhanpur', N'Madhya Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (698, N'Chhatarpur', N'Madhya Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (699, N'Dabra', N'Madhya Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (700, N'Datia', N'Madhya Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (701, N'Dewas', N'Madhya Pradesh')
GO
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (702, N'Dhar', N'Madhya Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (703, N'Fatehabad', N'Madhya Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (704, N'Gwalior', N'Madhya Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (705, N'Indore', N'Madhya Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (706, N'Itarsi', N'Madhya Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (707, N'Jabalpur', N'Madhya Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (708, N'Katni', N'Madhya Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (709, N'Kotma', N'Madhya Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (710, N'Lahar', N'Madhya Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (711, N'Lundi', N'Madhya Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (712, N'Maharajpur', N'Madhya Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (713, N'Mahidpur', N'Madhya Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (714, N'Maihar', N'Madhya Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (715, N'Malajkhand', N'Madhya Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (716, N'Manasa', N'Madhya Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (717, N'Manawar', N'Madhya Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (718, N'Mandideep', N'Madhya Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (719, N'Mandla', N'Madhya Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (720, N'Mandsaur', N'Madhya Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (721, N'Mauganj', N'Madhya Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (722, N'Mhow Cantonment', N'Madhya Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (723, N'Mhowgaon', N'Madhya Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (724, N'Morena', N'Madhya Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (725, N'Multai', N'Madhya Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (726, N'Murwara', N'Madhya Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (727, N'Nagda', N'Madhya Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (728, N'Nainpur', N'Madhya Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (729, N'Narsinghgarh', N'Madhya Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (730, N'Narsinghgarh', N'Madhya Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (731, N'Neemuch', N'Madhya Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (732, N'Nepanagar', N'Madhya Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (733, N'Niwari', N'Madhya Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (734, N'Nowgong', N'Madhya Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (735, N'Nowrozabad', N'Madhya Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (736, N'Pachore', N'Madhya Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (737, N'Pali', N'Madhya Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (738, N'Panagar', N'Madhya Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (739, N'Pandhurna', N'Madhya Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (740, N'Panna', N'Madhya Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (741, N'Pasan', N'Madhya Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (742, N'Pipariya', N'Madhya Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (743, N'Pithampur', N'Madhya Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (744, N'Porsa', N'Madhya Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (745, N'Prithvipur', N'Madhya Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (746, N'Raghogarh-Vijaypur', N'Madhya Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (747, N'Rahatgarh', N'Madhya Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (748, N'Raisen', N'Madhya Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (749, N'Rajgarh', N'Madhya Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (750, N'Ratlam', N'Madhya Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (751, N'Rau', N'Madhya Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (752, N'Rehli', N'Madhya Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (753, N'Rewa', N'Madhya Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (754, N'Sabalgarh', N'Madhya Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (755, N'Sagar', N'Madhya Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (756, N'Sanawad', N'Madhya Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (757, N'Sarangpur', N'Madhya Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (758, N'Sarni', N'Madhya Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (759, N'Satna', N'Madhya Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (760, N'Sausar', N'Madhya Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (761, N'Sehore', N'Madhya Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (762, N'Sendhwa', N'Madhya Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (763, N'Seoni', N'Madhya Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (764, N'Seoni-Malwa', N'Madhya Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (765, N'Shahdol', N'Madhya Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (766, N'Shajapur', N'Madhya Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (767, N'Shamgarh', N'Madhya Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (768, N'Sheopur', N'Madhya Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (769, N'Shivpuri', N'Madhya Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (770, N'Shujalpur', N'Madhya Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (771, N'Sidhi', N'Madhya Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (772, N'Sihora', N'Madhya Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (773, N'Singrauli', N'Madhya Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (774, N'Sironj', N'Madhya Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (775, N'Sohagpur', N'Madhya Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (776, N'Tarana', N'Madhya Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (777, N'Tikamgarh', N'Madhya Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (778, N'Ujhani', N'Madhya Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (779, N'Ujjain', N'Madhya Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (780, N'Umaria', N'Madhya Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (781, N'Vidisha', N'Madhya Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (782, N'Wara Seoni', N'Madhya Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (783, N'Ahmednagar', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (784, N'Akola', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (785, N'Amravati', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (786, N'Aurangabad', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (787, N'Baramati', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (788, N'Chalisgaon', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (789, N'Chinchani', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (790, N'Devgarh', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (791, N'Dhule', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (792, N'Dombivli', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (793, N'Durgapur', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (794, N'Ichalkaranji', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (795, N'Jalna', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (796, N'Kalyan', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (797, N'Latur', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (798, N'Loha', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (799, N'Lonar', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (800, N'Lonavla', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (801, N'Mahad', N'Maharashtra')
GO
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (802, N'Mahuli', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (803, N'Malegaon', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (804, N'Malkapur', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (805, N'Manchar', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (806, N'Mangalvedhe', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (807, N'Mangrulpir', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (808, N'Manjlegaon', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (809, N'Manmad', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (810, N'Manwath', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (811, N'Mehkar', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (812, N'Mhaswad', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (813, N'Miraj', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (814, N'Morshi', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (815, N'Mukhed', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (816, N'Mul', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (817, N'Mumbai', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (818, N'Murtijapur', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (819, N'Nagpur', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (820, N'Nalasopara', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (821, N'Nanded-Waghala', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (822, N'Nandgaon', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (823, N'Nandura', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (824, N'Nandurbar', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (825, N'Narkhed', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (826, N'Nashik', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (827, N'Navi Mumbai', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (828, N'Nawapur', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (829, N'Nilanga', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (830, N'Osmanabad', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (831, N'Ozar', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (832, N'Pachora', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (833, N'Paithan', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (834, N'Palghar', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (835, N'Pandharkaoda', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (836, N'Pandharpur', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (837, N'Panvel', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (838, N'Parbhani', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (839, N'Parli', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (840, N'Parola', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (841, N'Partur', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (842, N'Pathardi', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (843, N'Pathri', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (844, N'Patur', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (845, N'Pauni', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (846, N'Pen', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (847, N'Phaltan', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (848, N'Pulgaon', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (849, N'Pune', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (850, N'Purna', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (851, N'Pusad', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (852, N'Rahuri', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (853, N'Rajura', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (854, N'Ramtek', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (855, N'Ratnagiri', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (856, N'Raver', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (857, N'Risod', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (858, N'Sailu', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (859, N'Sangamner', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (860, N'Sangli', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (861, N'Sangole', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (862, N'Sasvad', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (863, N'Satana', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (864, N'Satara', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (865, N'Savner', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (866, N'Sawantwadi', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (867, N'Shahade', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (868, N'Shegaon', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (869, N'Shendurjana', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (870, N'Shirdi', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (871, N'Shirpur-Warwade', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (872, N'Shirur', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (873, N'Shrigonda', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (874, N'Shrirampur', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (875, N'Sillod', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (876, N'Sinnar', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (877, N'Solapur', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (878, N'Soyagaon', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (879, N'Talegaon Dabhade', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (880, N'Talode', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (881, N'Tasgaon', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (882, N'Tirora', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (883, N'Tuljapur', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (884, N'Tumsar', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (885, N'Uran', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (886, N'Uran Islampur', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (887, N'Wadgaon Road', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (888, N'Wai', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (889, N'Wani', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (890, N'Wardha', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (891, N'Warora', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (892, N'Warud', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (893, N'Washim', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (894, N'Yevla', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (895, N'Uchgaon', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (896, N'Udgir', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (897, N'Umarga', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (898, N'Umarkhed', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (899, N'Umred', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (900, N'Vadgaon Kasba', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (901, N'Vaijapur', N'Maharashtra')
GO
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (902, N'Vasai', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (903, N'Virar', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (904, N'Vita', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (905, N'Yavatmal', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (906, N'Yawal', N'Maharashtra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (907, N'Imphal', N'Manipur')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (908, N'Kakching', N'Manipur')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (909, N'Lilong', N'Manipur')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (910, N'Mayang Imphal', N'Manipur')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (911, N'Thoubal', N'Manipur')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (912, N'Jowai', N'Meghalaya')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (913, N'Nongstoin', N'Meghalaya')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (914, N'Shillong', N'Meghalaya')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (915, N'Tura', N'Meghalaya')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (916, N'Aizawl', N'Mizoram')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (917, N'Champhai', N'Mizoram')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (918, N'Lunglei', N'Mizoram')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (919, N'Saiha', N'Mizoram')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (920, N'Dimapur', N'Nagaland')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (921, N'Kohima', N'Nagaland')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (922, N'Mokokchung', N'Nagaland')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (923, N'Tuensang', N'Nagaland')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (924, N'Wokha', N'Nagaland')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (925, N'Zunheboto', N'Nagaland')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (950, N'Anandapur', N'Odisha')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (951, N'Anugul', N'Odisha')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (952, N'Asika', N'Odisha')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (953, N'Balangir', N'Odisha')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (954, N'Balasore', N'Odisha')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (955, N'Baleshwar', N'Odisha')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (956, N'Bamra', N'Odisha')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (957, N'Barbil', N'Odisha')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (958, N'Bargarh', N'Odisha')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (959, N'Bargarh', N'Odisha')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (960, N'Baripada', N'Odisha')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (961, N'Basudebpur', N'Odisha')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (962, N'Belpahar', N'Odisha')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (963, N'Bhadrak', N'Odisha')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (964, N'Bhawanipatna', N'Odisha')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (965, N'Bhuban', N'Odisha')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (966, N'Bhubaneswar', N'Odisha')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (967, N'Biramitrapur', N'Odisha')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (968, N'Brahmapur', N'Odisha')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (969, N'Brajrajnagar', N'Odisha')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (970, N'Byasanagar', N'Odisha')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (971, N'Cuttack', N'Odisha')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (972, N'Debagarh', N'Odisha')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (973, N'Dhenkanal', N'Odisha')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (974, N'Gunupur', N'Odisha')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (975, N'Hinjilicut', N'Odisha')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (976, N'Jagatsinghapur', N'Odisha')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (977, N'Jajapur', N'Odisha')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (978, N'Jaleswar', N'Odisha')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (979, N'Jatani', N'Odisha')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (980, N'Jeypur', N'Odisha')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (981, N'Jharsuguda', N'Odisha')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (982, N'Joda', N'Odisha')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (983, N'Kantabanji', N'Odisha')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (984, N'Karanjia', N'Odisha')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (985, N'Kendrapara', N'Odisha')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (986, N'Kendujhar', N'Odisha')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (987, N'Khordha', N'Odisha')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (988, N'Koraput', N'Odisha')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (989, N'Malkangiri', N'Odisha')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (990, N'Nabarangapur', N'Odisha')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (991, N'Paradip', N'Odisha')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (992, N'Parlakhemundi', N'Odisha')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (993, N'Pattamundai', N'Odisha')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (994, N'Phulabani', N'Odisha')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (995, N'Puri', N'Odisha')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (996, N'Rairangpur', N'Odisha')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (997, N'Rajagangapur', N'Odisha')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (998, N'Raurkela', N'Odisha')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (999, N'Rayagada', N'Odisha')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1000, N'Sambalpur', N'Odisha')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1001, N'Soro', N'Odisha')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1002, N'Sunabeda', N'Odisha')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1003, N'Sundargarh', N'Odisha')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1004, N'Talcher', N'Odisha')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1005, N'Titlagarh', N'Odisha')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1006, N'Umarkote', N'Odisha')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1007, N'Karaikal', N'Puducherry')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1008, N'Mahe', N'Puducherry')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1009, N'Puducherry', N'Puducherry')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1010, N'Yanam', N'Puducherry')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1011, N'Ahmedgarh', N'Punjab')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1012, N'Amritsar', N'Punjab')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1013, N'Barnala', N'Punjab')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1014, N'Batala', N'Punjab')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1015, N'Bathinda', N'Punjab')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1016, N'Bhagha Purana', N'Punjab')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1017, N'Budhlada', N'Punjab')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1018, N'Chandigarh', N'Punjab')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1019, N'Dasua', N'Punjab')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1020, N'Dhuri', N'Punjab')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1021, N'Dinanagar', N'Punjab')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1022, N'Faridkot', N'Punjab')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1023, N'Fazilka', N'Punjab')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1024, N'Firozpur', N'Punjab')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1025, N'Firozpur Cantt.', N'Punjab')
GO
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1026, N'Giddarbaha', N'Punjab')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1027, N'Gobindgarh', N'Punjab')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1028, N'Gurdaspur', N'Punjab')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1029, N'Hoshiarpur', N'Punjab')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1030, N'Jagraon', N'Punjab')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1031, N'Jaitu', N'Punjab')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1032, N'Jalalabad', N'Punjab')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1033, N'Jalandhar', N'Punjab')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1034, N'Jalandhar Cantt.', N'Punjab')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1035, N'Jandiala', N'Punjab')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1036, N'Kapurthala', N'Punjab')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1037, N'Karoran', N'Punjab')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1038, N'Kartarpur', N'Punjab')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1039, N'Khanna', N'Punjab')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1040, N'Kharar', N'Punjab')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1041, N'Kot Kapura', N'Punjab')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1042, N'Kurali', N'Punjab')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1043, N'Longowal', N'Punjab')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1044, N'Ludhiana', N'Punjab')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1045, N'Malerkotla', N'Punjab')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1046, N'Malout', N'Punjab')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1047, N'Mansa', N'Punjab')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1048, N'Maur', N'Punjab')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1049, N'Moga', N'Punjab')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1050, N'Mohali', N'Punjab')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1051, N'Morinda', N'Punjab')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1052, N'Mukerian', N'Punjab')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1053, N'Muktsar', N'Punjab')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1054, N'Nabha', N'Punjab')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1055, N'Nakodar', N'Punjab')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1056, N'Nangal', N'Punjab')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1057, N'Nawanshahr', N'Punjab')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1058, N'Pathankot', N'Punjab')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1059, N'Patiala', N'Punjab')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1060, N'Patran', N'Punjab')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1061, N'Patti', N'Punjab')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1062, N'Phagwara', N'Punjab')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1063, N'Phillaur', N'Punjab')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1064, N'Qadian', N'Punjab')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1065, N'Raikot', N'Punjab')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1066, N'Rajpura', N'Punjab')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1067, N'Rampura Phul', N'Punjab')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1068, N'Rupnagar', N'Punjab')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1069, N'Samana', N'Punjab')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1070, N'Sangrur', N'Punjab')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1071, N'Sirhind Fatehgarh Sahib', N'Punjab')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1072, N'Sujanpur', N'Punjab')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1073, N'Sunam', N'Punjab')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1074, N'Talwara', N'Punjab')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1075, N'Tarn Taran', N'Punjab')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1076, N'Urmar Tanda', N'Punjab')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1077, N'Zira', N'Punjab')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1078, N'Zirakpur', N'Punjab')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1079, N'Bali', N'Rajasthan')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1080, N'Banswara', N'Rajasthan')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1081, N'Ajmer', N'Rajasthan')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1082, N'Alwar', N'Rajasthan')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1083, N'Bandikui', N'Rajasthan')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1084, N'Baran', N'Rajasthan')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1085, N'Barmer', N'Rajasthan')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1086, N'Bikaner', N'Rajasthan')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1087, N'Fatehpur', N'Rajasthan')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1088, N'Jaipur', N'Rajasthan')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1089, N'Jaisalmer', N'Rajasthan')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1090, N'Jodhpur', N'Rajasthan')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1091, N'Kota', N'Rajasthan')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1092, N'Lachhmangarh', N'Rajasthan')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1093, N'Ladnu', N'Rajasthan')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1094, N'Lakheri', N'Rajasthan')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1095, N'Lalsot', N'Rajasthan')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1096, N'Losal', N'Rajasthan')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1097, N'Makrana', N'Rajasthan')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1098, N'Malpura', N'Rajasthan')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1099, N'Mandalgarh', N'Rajasthan')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1100, N'Mandawa', N'Rajasthan')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1101, N'Mangrol', N'Rajasthan')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1102, N'Merta City', N'Rajasthan')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1103, N'Mount Abu', N'Rajasthan')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1104, N'Nadbai', N'Rajasthan')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1105, N'Nagar', N'Rajasthan')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1106, N'Nagaur', N'Rajasthan')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1107, N'Nargund', N'Rajasthan')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1108, N'Nasirabad', N'Rajasthan')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1109, N'Nathdwara', N'Rajasthan')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1110, N'Navalgund', N'Rajasthan')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1111, N'Nawalgarh', N'Rajasthan')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1112, N'Neem-Ka-Thana', N'Rajasthan')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1113, N'Nelamangala', N'Rajasthan')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1114, N'Nimbahera', N'Rajasthan')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1115, N'Nipani', N'Rajasthan')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1116, N'Niwai', N'Rajasthan')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1117, N'Nohar', N'Rajasthan')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1118, N'Nokha', N'Rajasthan')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1119, N'Pali', N'Rajasthan')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1120, N'Phalodi', N'Rajasthan')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1121, N'Phulera', N'Rajasthan')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1122, N'Pilani', N'Rajasthan')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1123, N'Pilibanga', N'Rajasthan')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1124, N'Pindwara', N'Rajasthan')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1125, N'Pipar City', N'Rajasthan')
GO
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1126, N'Prantij', N'Rajasthan')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1127, N'Pratapgarh', N'Rajasthan')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1128, N'Raisinghnagar', N'Rajasthan')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1129, N'Rajakhera', N'Rajasthan')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1130, N'Rajaldesar', N'Rajasthan')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1131, N'Rajgarh (Alwar)', N'Rajasthan')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1132, N'Rajgarh (Churu', N'Rajasthan')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1133, N'Rajsamand', N'Rajasthan')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1134, N'Ramganj Mandi', N'Rajasthan')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1135, N'Ramngarh', N'Rajasthan')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1136, N'Ratangarh', N'Rajasthan')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1137, N'Rawatbhata', N'Rajasthan')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1138, N'Rawatsar', N'Rajasthan')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1139, N'Reengus', N'Rajasthan')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1140, N'Sadri', N'Rajasthan')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1141, N'Sadulshahar', N'Rajasthan')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1142, N'Sagwara', N'Rajasthan')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1143, N'Sambhar', N'Rajasthan')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1144, N'Sanchore', N'Rajasthan')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1145, N'Sangaria', N'Rajasthan')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1146, N'Sardarshahar', N'Rajasthan')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1147, N'Sawai Madhopur', N'Rajasthan')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1148, N'Shahpura', N'Rajasthan')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1149, N'Shahpura', N'Rajasthan')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1150, N'Sheoganj', N'Rajasthan')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1151, N'Sikar', N'Rajasthan')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1152, N'Sirohi', N'Rajasthan')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1153, N'Sojat', N'Rajasthan')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1154, N'Sri Madhopur', N'Rajasthan')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1155, N'Sujangarh', N'Rajasthan')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1156, N'Sumerpur', N'Rajasthan')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1157, N'Suratgarh', N'Rajasthan')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1158, N'Taranagar', N'Rajasthan')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1159, N'Todabhim', N'Rajasthan')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1160, N'Todaraisingh', N'Rajasthan')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1161, N'Tonk', N'Rajasthan')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1162, N'Udaipur', N'Rajasthan')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1163, N'Udaipurwati', N'Rajasthan')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1164, N'Vijainagar', N'Rajasthan')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1165, N'Gangtok', N'Sikkim')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1166, N'Calcutta', N'West Bengal')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1167, N'Arakkonam', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1168, N'Arcot', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1169, N'Aruppukkottai', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1170, N'Bhavani', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1171, N'Chengalpattu', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1172, N'Chennai', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1173, N'Chinna salem', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1174, N'Coimbatore', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1175, N'Coonoor', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1176, N'Cuddalore', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1177, N'Dharmapuri', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1178, N'Dindigul', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1179, N'Erode', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1180, N'Gudalur', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1181, N'Gudalur', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1182, N'Gudalur', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1183, N'Kanchipuram', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1184, N'Karaikudi', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1185, N'Karungal', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1186, N'Karur', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1187, N'Kollankodu', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1188, N'Lalgudi', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1189, N'Madurai', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1190, N'Nagapattinam', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1191, N'Nagercoil', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1192, N'Namagiripettai', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1193, N'Namakkal', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1194, N'Nandivaram-Guduvancheri', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1195, N'Nanjikottai', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1196, N'Natham', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1197, N'Nellikuppam', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1198, N'Neyveli', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1199, N'O'' Valley', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1200, N'Oddanchatram', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1201, N'P.N.Patti', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1202, N'Pacode', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1203, N'Padmanabhapuram', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1204, N'Palani', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1205, N'Palladam', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1206, N'Pallapatti', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1207, N'Pallikonda', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1208, N'Panagudi', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1209, N'Panruti', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1210, N'Paramakudi', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1211, N'Parangipettai', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1212, N'Pattukkottai', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1213, N'Perambalur', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1214, N'Peravurani', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1215, N'Periyakulam', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1216, N'Periyasemur', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1217, N'Pernampattu', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1218, N'Pollachi', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1219, N'Polur', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1220, N'Ponneri', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1221, N'Pudukkottai', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1222, N'Pudupattinam', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1223, N'Puliyankudi', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1224, N'Punjaipugalur', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1225, N'Rajapalayam', N'Tamil Nadu')
GO
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1226, N'Ramanathapuram', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1227, N'Rameshwaram', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1228, N'Rasipuram', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1229, N'Salem', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1230, N'Sankarankoil', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1231, N'Sankari', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1232, N'Sathyamangalam', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1233, N'Sattur', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1234, N'Shenkottai', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1235, N'Sholavandan', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1236, N'Sholingur', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1237, N'Sirkali', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1238, N'Sivaganga', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1239, N'Sivagiri', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1240, N'Sivakasi', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1241, N'Srivilliputhur', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1242, N'Surandai', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1243, N'Suriyampalayam', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1244, N'Tenkasi', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1245, N'Thammampatti', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1246, N'Thanjavur', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1247, N'Tharamangalam', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1248, N'Tharangambadi', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1249, N'Theni Allinagaram', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1250, N'Thirumangalam', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1251, N'Thirunindravur', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1252, N'Thiruparappu', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1253, N'Thirupuvanam', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1254, N'Thiruthuraipoondi', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1255, N'Thiruvallur', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1256, N'Thiruvarur', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1257, N'Thoothukudi', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1258, N'Thuraiyur', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1259, N'Tindivanam', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1260, N'Tiruchendur', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1261, N'Tiruchengode', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1262, N'Tiruchirappalli', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1263, N'Tirukalukundram', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1264, N'Tirukkoyilur', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1265, N'Tirunelveli', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1266, N'Tirupathur', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1267, N'Tirupathur', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1268, N'Tiruppur', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1269, N'Tiruttani', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1270, N'Tiruvannamalai', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1271, N'Tiruvethipuram', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1272, N'Tittakudi', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1273, N'Udhagamandalam', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1274, N'Udumalaipettai', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1275, N'Unnamalaikadai', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1276, N'Usilampatti', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1277, N'Uthamapalayam', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1278, N'Uthiramerur', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1279, N'Vadakkuvalliyur', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1280, N'Vadalur', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1281, N'Vadipatti', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1282, N'Valparai', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1283, N'Vandavasi', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1284, N'Vaniyambadi', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1285, N'Vedaranyam', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1286, N'Vellakoil', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1287, N'Vellore', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1288, N'Vikramasingapuram', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1289, N'Viluppuram', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1290, N'Virudhachalam', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1291, N'Virudhunagar', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1292, N'Viswanatham', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1293, N'Agartala', N'Tripura')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1294, N'Badharghat', N'Tripura')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1295, N'Dharmanagar', N'Tripura')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1296, N'Indranagar', N'Tripura')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1297, N'Jogendranagar', N'Tripura')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1298, N'Kailasahar', N'Tripura')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1299, N'Khowai', N'Tripura')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1300, N'Pratapgarh', N'Tripura')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1301, N'Udaipur', N'Tripura')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1302, N'Achhnera', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1303, N'Adari', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1304, N'Agra', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1305, N'Aligarh', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1306, N'Allahabad', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1307, N'Amroha', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1308, N'Azamgarh', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1309, N'Bahraich', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1310, N'Ballia', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1311, N'Balrampur', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1312, N'Banda', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1313, N'Bareilly', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1314, N'Chandausi', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1315, N'Dadri', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1316, N'Deoria', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1317, N'Etawah', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1318, N'Fatehabad', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1319, N'Fatehpur', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1320, N'Fatehpur', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1321, N'Greater Noida', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1322, N'Hamirpur', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1323, N'Hardoi', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1324, N'Jajmau', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1325, N'Jaunpur', N'Uttar Pradesh')
GO
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1326, N'Jhansi', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1327, N'Kalpi', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1328, N'Kanpur', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1329, N'Kota', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1330, N'Laharpur', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1331, N'Lakhimpur', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1332, N'Lal Gopalganj Nindaura', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1333, N'Lalganj', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1334, N'Lalitpur', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1335, N'Lar', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1336, N'Loni', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1337, N'Lucknow', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1338, N'Mathura', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1339, N'Meerut', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1340, N'Modinagar', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1341, N'Muradnagar', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1342, N'Nagina', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1343, N'Najibabad', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1344, N'Nakur', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1345, N'Nanpara', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1346, N'Naraura', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1347, N'Naugawan Sadat', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1348, N'Nautanwa', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1349, N'Nawabganj', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1350, N'Nehtaur', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1351, N'NOIDA', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1352, N'Noorpur', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1353, N'Obra', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1354, N'Orai', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1355, N'Padrauna', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1356, N'Palia Kalan', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1357, N'Parasi', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1358, N'Phulpur', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1359, N'Pihani', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1360, N'Pilibhit', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1361, N'Pilkhuwa', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1362, N'Powayan', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1363, N'Pukhrayan', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1364, N'Puranpur', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1365, N'Purquazi', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1366, N'Purwa', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1367, N'Rae Bareli', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1368, N'Rampur', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1369, N'Rampur Maniharan', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1370, N'Rasra', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1371, N'Rath', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1372, N'Renukoot', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1373, N'Reoti', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1374, N'Robertsganj', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1375, N'Rudauli', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1376, N'Rudrapur', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1377, N'Sadabad', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1378, N'Safipur', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1379, N'Saharanpur', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1380, N'Sahaspur', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1381, N'Sahaswan', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1382, N'Sahawar', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1383, N'Sahjanwa', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1384, N'Saidpur', N' Ghazipur')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1385, N'Sambhal', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1386, N'Samdhan', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1387, N'Samthar', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1388, N'Sandi', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1389, N'Sandila', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1390, N'Sardhana', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1391, N'Seohara', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1392, N'Shahabad', N' Hardoi')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1393, N'Shahabad', N' Rampur')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1394, N'Shahganj', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1395, N'Shahjahanpur', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1396, N'Shamli', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1397, N'Shamsabad', N' Agra')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1398, N'Shamsabad', N' Farrukhabad')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1399, N'Sherkot', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1400, N'Shikarpur', N' Bulandshahr')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1401, N'Shikohabad', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1402, N'Shishgarh', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1403, N'Siana', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1404, N'Sikanderpur', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1405, N'Sikandra Rao', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1406, N'Sikandrabad', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1407, N'Sirsaganj', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1408, N'Sirsi', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1409, N'Sitapur', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1410, N'Soron', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1411, N'Suar', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1412, N'Sultanpur', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1413, N'Sumerpur', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1414, N'Tanda', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1415, N'Tanda', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1416, N'Tetri Bazar', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1417, N'Thakurdwara', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1418, N'Thana Bhawan', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1419, N'Tilhar', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1420, N'Tirwaganj', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1421, N'Tulsipur', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1422, N'Tundla', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1423, N'Unnao', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1424, N'Utraula', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1425, N'Varanasi', N'Uttar Pradesh')
GO
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1426, N'Vrindavan', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1427, N'Warhapur', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1428, N'Zaidpur', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1429, N'Zamania', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1430, N'Almora', N'Uttarakhand')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1431, N'Bazpur', N'Uttarakhand')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1432, N'Chamba', N'Uttarakhand')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1433, N'Dehradun', N'Uttarakhand')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1434, N'Haldwani', N'Uttarakhand')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1435, N'Haridwar', N'Uttarakhand')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1436, N'Jaspur', N'Uttarakhand')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1437, N'Kashipur', N'Uttarakhand')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1438, N'kichha', N'Uttarakhand')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1439, N'Kotdwara', N'Uttarakhand')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1440, N'Manglaur', N'Uttarakhand')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1441, N'Mussoorie', N'Uttarakhand')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1442, N'Nagla', N'Uttarakhand')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1443, N'Nainital', N'Uttarakhand')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1444, N'Pauri', N'Uttarakhand')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1445, N'Pithoragarh', N'Uttarakhand')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1446, N'Ramnagar', N'Uttarakhand')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1447, N'Rishikesh', N'Uttarakhand')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1448, N'Roorkee', N'Uttarakhand')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1449, N'Rudrapur', N'Uttarakhand')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1450, N'Sitarganj', N'Uttarakhand')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1451, N'Tehri', N'Uttarakhand')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1452, N'Muzaffarnagar', N'Uttar Pradesh')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1453, N'Adra', N' Purulia')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1454, N'Alipurduar', N'West Bengal')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1455, N'Arambagh', N'West Bengal')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1456, N'Asansol', N'West Bengal')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1457, N'Baharampur', N'West Bengal')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1458, N'Bally', N'West Bengal')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1459, N'Balurghat', N'West Bengal')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1460, N'Bankura', N'West Bengal')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1461, N'Barakar', N'West Bengal')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1462, N'Barasat', N'West Bengal')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1463, N'Bardhaman', N'West Bengal')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1464, N'Bidhan Nagar', N'West Bengal')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1465, N'Chinsura', N'West Bengal')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1466, N'Contai', N'West Bengal')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1467, N'Cooch Behar', N'West Bengal')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1468, N'Darjeeling', N'West Bengal')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1469, N'Durgapur', N'West Bengal')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1470, N'Haldia', N'West Bengal')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1471, N'Howrah', N'West Bengal')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1472, N'Islampur', N'West Bengal')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1473, N'Jhargram', N'West Bengal')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1474, N'Kharagpur', N'West Bengal')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1475, N'Kolkata', N'West Bengal')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1476, N'Mainaguri', N'West Bengal')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1477, N'Mal', N'West Bengal')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1478, N'Mathabhanga', N'West Bengal')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1479, N'Medinipur', N'West Bengal')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1480, N'Memari', N'West Bengal')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1481, N'Monoharpur', N'West Bengal')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1482, N'Murshidabad', N'West Bengal')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1483, N'Nabadwip', N'West Bengal')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1484, N'Naihati', N'West Bengal')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1485, N'Panchla', N'West Bengal')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1486, N'Pandua', N'West Bengal')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1487, N'Paschim Punropara', N'West Bengal')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1488, N'Purulia', N'West Bengal')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1489, N'Raghunathpur', N'West Bengal')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1490, N'Raiganj', N'West Bengal')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1491, N'Rampurhat', N'West Bengal')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1492, N'Ranaghat', N'West Bengal')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1493, N'Sainthia', N'West Bengal')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1494, N'Santipur', N'West Bengal')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1495, N'Siliguri', N'West Bengal')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1496, N'Sonamukhi', N'West Bengal')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1497, N'Srirampore', N'West Bengal')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1498, N'Suri', N'West Bengal')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1499, N'Taki', N'West Bengal')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1500, N'Tamluk', N'West Bengal')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1501, N'Tarakeswar', N'West Bengal')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1502, N'Chikmagalur', N'Karnataka')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1503, N'Davanagere', N'Karnataka')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1504, N'Dharwad', N'Karnataka')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1505, N'Gadag', N'Karnataka')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1506, N'Chennai', N'Tamil Nadu')
INSERT [dbo].[cities] ([city_id], [city_name], [city_state]) VALUES (1507, N'Coimbatore', N'Tamil Nadu')
SET IDENTITY_INSERT [dbo].[cities] OFF
SET IDENTITY_INSERT [dbo].[ServiceEng] ON 

INSERT [dbo].[ServiceEng] ([SEID], [Name], [Address], [contactNo], [mobileNo], [emailId], [secode], [userName], [password], [authorizedServiceCenter], [region], [state], [regcode], [flag]) VALUES (149, N'G.Sathianarayanan', N'Hi-Tech Auto Serviicesz	No. 777, Pon Nagar (Near V', N'8754404222', N'6381971446', N'g.sathianarayanan@ranegroup.com', N'06597', N'Sathiya', N'Welcome2u', N'Spectra Hosur', N'South', N'Tamil Nadu', N'01518', N'1')
INSERT [dbo].[ServiceEng] ([SEID], [Name], [Address], [contactNo], [mobileNo], [emailId], [secode], [userName], [password], [authorizedServiceCenter], [region], [state], [regcode], [flag]) VALUES (150, N'Hemant P. Mistry', N'Bhoomi Motors
Near Shreenath Transport Nagar
Opp', N'09428844469', N'09428844469', N'..', N'08566', N'hemant', N'BLOCKED', N'', N'West', N'Ahmedabad', N'05417', N'1')
INSERT [dbo].[ServiceEng] ([SEID], [Name], [Address], [contactNo], [mobileNo], [emailId], [secode], [userName], [password], [authorizedServiceCenter], [region], [state], [regcode], [flag]) VALUES (152, N'Chetan Gajanan Kalambe', N'C/o. JMA Rane Marketing Limited
Shriram Bhagwagha', N'07122272808', N'08600998445', N'chetangajanan.kalambe@rane.co.in', N'06608', N'chetangajanan', N'Nagpurmani', N'M.S. Automobiles', N'West', N'Nagpur', N'05417', N'1')
INSERT [dbo].[ServiceEng] ([SEID], [Name], [Address], [contactNo], [mobileNo], [emailId], [secode], [userName], [password], [authorizedServiceCenter], [region], [state], [regcode], [flag]) VALUES (153, N'M.PrasantaKumarSahoo', N'power house road rourkela', N'769001', N'9861193029', N'm.prasantakumarsahoo@rane.co.in', N'06808', N'Prasanta Kumar Sahoo', N'shreemaa', N'Abhis Solution', N'East', N'Orissa', N'08509', N'1')
INSERT [dbo].[ServiceEng] ([SEID], [Name], [Address], [contactNo], [mobileNo], [emailId], [secode], [userName], [password], [authorizedServiceCenter], [region], [state], [regcode], [flag]) VALUES (183, N'gayathri', N'chennai', N'5555', N'9944584959', N'mohan@brainmagic.info', N'1234', N'1234', N'1234', N'Vijaya Motor Sales & Service,0', N'South', N'Tamil Nadu', NULL, N'0')
INSERT [dbo].[ServiceEng] ([SEID], [Name], [Address], [contactNo], [mobileNo], [emailId], [secode], [userName], [password], [authorizedServiceCenter], [region], [state], [regcode], [flag]) VALUES (155, N'Chandan Chetry', N'Jullundur Motor Agency (Delhi)Pvt. Ltd.  Chattriba', N'919435584000', N'919435584000', N'c.chandanchetry@rane.co.in', N'11737', N'cchetry', N'east@ranetrw', N'New Allied Power', N'East', N'Assam', N'08509', N'1')
INSERT [dbo].[ServiceEng] ([SEID], [Name], [Address], [contactNo], [mobileNo], [emailId], [secode], [userName], [password], [authorizedServiceCenter], [region], [state], [regcode], [flag]) VALUES (156, N'Abhik Bhakta', N'RTSS Engineer - Service 
Arrkay Diesel
158, Jess', N'9038502603', N'9038502603', N's.abhikbhakta@ranegroup.com', N'08509', N'abhik', N'Welcome2u', N'Motor Village', N'East', N'West Bengal', N'003388', N'1')
INSERT [dbo].[ServiceEng] ([SEID], [Name], [Address], [contactNo], [mobileNo], [emailId], [secode], [userName], [password], [authorizedServiceCenter], [region], [state], [regcode], [flag]) VALUES (179, N'I. Athul Chappadi', N'M&M Hydraulics Service and Sales	
32/1379A, Pudus', N'09037094390', N'09037094390', N'...', N'11691', N'athul', N'BLOCKED', N'', N'South', N'Kerala', N'01518', N'1')
INSERT [dbo].[ServiceEng] ([SEID], [Name], [Address], [contactNo], [mobileNo], [emailId], [secode], [userName], [password], [authorizedServiceCenter], [region], [state], [regcode], [flag]) VALUES (158, N'Ravi Prakash', N'RTSSL Engineer - Service
C/o. Energy Motors
Shop', N'8385948651', N'8385948651', N's.raviprakash@rane.co.in', N'08396', N'ravi', N'9314462137', N'Shree Gopal Motors', N'North', N'Rajastan', N'003388', N'1')
INSERT [dbo].[ServiceEng] ([SEID], [Name], [Address], [contactNo], [mobileNo], [emailId], [secode], [userName], [password], [authorizedServiceCenter], [region], [state], [regcode], [flag]) VALUES (159, N'Om Prakash Kumar', N'RTSS Engineer - Service
C/o.  Jullundur Motor Age', N'09910423993', N'09910423993', N'c.omprakashkumar@ranegroup.com', N'08548', N'Omprakash', N'Welcome2u', N'Saurav Motor Workshop', N'North', N'National Capital Territor', N'003388', N'1')
INSERT [dbo].[ServiceEng] ([SEID], [Name], [Address], [contactNo], [mobileNo], [emailId], [secode], [userName], [password], [authorizedServiceCenter], [region], [state], [regcode], [flag]) VALUES (184, N'Ritesh Kumar Singh', N'RTSS Service Engineer
C/o. Diesel Centre
Patna', N'07470988322', N'07470988322', N's.riteshkumarsingh@ranegroup.com', N'14606', N'ritesh', N'Welcome2u', N'Abhis Solution', N'East', N'Bihar', NULL, N'0')
INSERT [dbo].[ServiceEng] ([SEID], [Name], [Address], [contactNo], [mobileNo], [emailId], [secode], [userName], [password], [authorizedServiceCenter], [region], [state], [regcode], [flag]) VALUES (180, N'C. Prakash Shukla', N'C/o.  Jullundur Motor Agency (Delhi) Ltd.
123/409', N'09166660344', N'09166660344', N'c.prakashshukla@ranegroup.com', N'12485', N'prakash', N'Welcome2u', N'Shri Sagun Tradings', N'North', N'Uttar Pradesh', N'003388', N'1')
INSERT [dbo].[ServiceEng] ([SEID], [Name], [Address], [contactNo], [mobileNo], [emailId], [secode], [userName], [password], [authorizedServiceCenter], [region], [state], [regcode], [flag]) VALUES (164, N'Shaik Nazeerbaba', N'RTSSL Engineer - Service & Spares
C/o. Spectra Au', N'09542462114', N'8143409260', N'shaik.nazeerbaba@ranegroup.com', N'08547', N'Nazeer', N'N@zeer123', N'SS Air Brakes', N'South', N'Andrapradesh', N'01518', N'1')
INSERT [dbo].[ServiceEng] ([SEID], [Name], [Address], [contactNo], [mobileNo], [emailId], [secode], [userName], [password], [authorizedServiceCenter], [region], [state], [regcode], [flag]) VALUES (168, N'R. Dinesh Babu', N'RTSSL Engineer - Service 	
C/o. Alpha Autotech	
', N'09788622093', N'09788622093', N'r.dineshbabu@ranegroup.com', N'12696', N'dinesh', N'kuttydinesh', N'Shivansh Motors', N'West', N'Mumbai', N'05417', N'1')
INSERT [dbo].[ServiceEng] ([SEID], [Name], [Address], [contactNo], [mobileNo], [emailId], [secode], [userName], [password], [authorizedServiceCenter], [region], [state], [regcode], [flag]) VALUES (169, N'A. Sivakumar', N'RTSSL - Service Engineer,C/o. V. S. T Motors Limit', N'04443949248', N'09176691024', N'a.sivakumar@rane.co.in', N'ASK12508', N'SIVA', N'Welcome2u', N'', N'South', N'Tamil Nadu', N'01518', N'1')
INSERT [dbo].[ServiceEng] ([SEID], [Name], [Address], [contactNo], [mobileNo], [emailId], [secode], [userName], [password], [authorizedServiceCenter], [region], [state], [regcode], [flag]) VALUES (174, N'Pramod Kumar Bairagi', N'RTSSL Engineer - Service & Spares
C/o.  Jullundur', N'07898989842', N'07898989842', N'pramodkumar.bairagi@rane.co.in', N'10574', N'pramod', N'mp478579', N'Kohinoor Diesel', N'West', N'Indore', N'05417', N'1')
INSERT [dbo].[ServiceEng] ([SEID], [Name], [Address], [contactNo], [mobileNo], [emailId], [secode], [userName], [password], [authorizedServiceCenter], [region], [state], [regcode], [flag]) VALUES (175, N'M. Chethan Kumar', N'RTSSL Engineer - Service
C/o. Spectra Automobile ', N'09686773967', N'09686773967', N'm.chethankumar@rane.co.in', N'10556', N'chethan', N'Welcome2u', N'SN Auto Hydraulics', N'South', N'Karnataka', N'01518', N'1')
INSERT [dbo].[ServiceEng] ([SEID], [Name], [Address], [contactNo], [mobileNo], [emailId], [secode], [userName], [password], [authorizedServiceCenter], [region], [state], [regcode], [flag]) VALUES (176, N'Nitish C', N'C/O Jullunder Motor Agency(Delhi) Ltd.
SCF - 394,', N'08059171213', N'08059171213', N'h.nitish@ranegroup.com', N'10631', N'nitish', N'Nitish1605@', N'GNA Brakes & Streering Solutions', N'North', N'Chandigarh', N'003388', N'1')
SET IDENTITY_INSERT [dbo].[ServiceEng] OFF
SET IDENTITY_INSERT [dbo].[tbl_Admin] ON 

INSERT [dbo].[tbl_Admin] ([Id], [UserName], [Password], [Status]) VALUES (1, N'Admin', N'Admin@123', N'Active')
SET IDENTITY_INSERT [dbo].[tbl_Admin] OFF
SET IDENTITY_INSERT [dbo].[tbl_FIRDetails] ON 

INSERT [dbo].[tbl_FIRDetails] ([Id], [FIRId], [FIRNo], [ReasonForRejection], [AllocateAsc], [DispatchMethod], [DocumentNo], [DispatchDate], [ASCRecommendation], [ASCReportedDate], [ObservationofProblem], [FileUpload], [LRNumber], [LRDate], [DCNo], [ServiceEnggApproval], [ReceivedDate], [TransportationName], [DeliveryNoteNo], [InwardNumber], [InwardDate], [InwardQuantity], [CreditNoteNo], [CreditNoteDate], [Value], [PlantDocNo], [PlantDocDate], [PlantNotificationNo], [Remarks], [Status], [AllocateServiceEngg], [UpdatedOn], [UpdatedBy]) VALUES (1, 1, N'FIR/2021/0001', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', NULL, CAST(0x0000ADAA01229B0B AS DateTime), N'RAJKUMAR')
INSERT [dbo].[tbl_FIRDetails] ([Id], [FIRId], [FIRNo], [ReasonForRejection], [AllocateAsc], [DispatchMethod], [DocumentNo], [DispatchDate], [ASCRecommendation], [ASCReportedDate], [ObservationofProblem], [FileUpload], [LRNumber], [LRDate], [DCNo], [ServiceEnggApproval], [ReceivedDate], [TransportationName], [DeliveryNoteNo], [InwardNumber], [InwardDate], [InwardQuantity], [CreditNoteNo], [CreditNoteDate], [Value], [PlantDocNo], [PlantDocDate], [PlantNotificationNo], [Remarks], [Status], [AllocateServiceEngg], [UpdatedOn], [UpdatedBy]) VALUES (2, 1, N'FIR/2021/0001', N'', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'Test', N'2', N'G.Sathianarayanan', CAST(0x0000ADAA0122C88C AS DateTime), N'Kishor')
INSERT [dbo].[tbl_FIRDetails] ([Id], [FIRId], [FIRNo], [ReasonForRejection], [AllocateAsc], [DispatchMethod], [DocumentNo], [DispatchDate], [ASCRecommendation], [ASCReportedDate], [ObservationofProblem], [FileUpload], [LRNumber], [LRDate], [DCNo], [ServiceEnggApproval], [ReceivedDate], [TransportationName], [DeliveryNoteNo], [InwardNumber], [InwardDate], [InwardQuantity], [CreditNoteNo], [CreditNoteDate], [Value], [PlantDocNo], [PlantDocDate], [PlantNotificationNo], [Remarks], [Status], [AllocateServiceEngg], [UpdatedOn], [UpdatedBy]) VALUES (3, 1, N'FIR/2021/0001', NULL, N'Auto-Tech Engineering Service', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'Test', N'5', NULL, CAST(0x0000ADAA0122F485 AS DateTime), N'Sathiya')
INSERT [dbo].[tbl_FIRDetails] ([Id], [FIRId], [FIRNo], [ReasonForRejection], [AllocateAsc], [DispatchMethod], [DocumentNo], [DispatchDate], [ASCRecommendation], [ASCReportedDate], [ObservationofProblem], [FileUpload], [LRNumber], [LRDate], [DCNo], [ServiceEnggApproval], [ReceivedDate], [TransportationName], [DeliveryNoteNo], [InwardNumber], [InwardDate], [InwardQuantity], [CreditNoteNo], [CreditNoteDate], [Value], [PlantDocNo], [PlantDocDate], [PlantNotificationNo], [Remarks], [Status], [AllocateServiceEngg], [UpdatedOn], [UpdatedBy]) VALUES (4, 1, N'FIR/2021/0001', NULL, N'Auto-Tech Engineering Service', N'Courier', N'67890', CAST(0x0000ADAA012338FD AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'Test', N'6', NULL, CAST(0x0000ADAA012338FD AS DateTime), N'RAJKUMAR')
INSERT [dbo].[tbl_FIRDetails] ([Id], [FIRId], [FIRNo], [ReasonForRejection], [AllocateAsc], [DispatchMethod], [DocumentNo], [DispatchDate], [ASCRecommendation], [ASCReportedDate], [ObservationofProblem], [FileUpload], [LRNumber], [LRDate], [DCNo], [ServiceEnggApproval], [ReceivedDate], [TransportationName], [DeliveryNoteNo], [InwardNumber], [InwardDate], [InwardQuantity], [CreditNoteNo], [CreditNoteDate], [Value], [PlantDocNo], [PlantDocDate], [PlantNotificationNo], [Remarks], [Status], [AllocateServiceEngg], [UpdatedOn], [UpdatedBy]) VALUES (5, 1, N'FIR/2021/0001', NULL, NULL, NULL, NULL, NULL, N'No Scope of Warranty', CAST(0x0000ADA900000000 AS DateTime), N'Test', N'Sunlandlogo.jpg,', NULL, NULL, NULL, NULL, N'Sep 21 2021  5:40PM', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'7', NULL, CAST(0x0000ADAA012363DB AS DateTime), N'ASCSOUTH05')
INSERT [dbo].[tbl_FIRDetails] ([Id], [FIRId], [FIRNo], [ReasonForRejection], [AllocateAsc], [DispatchMethod], [DocumentNo], [DispatchDate], [ASCRecommendation], [ASCReportedDate], [ObservationofProblem], [FileUpload], [LRNumber], [LRDate], [DCNo], [ServiceEnggApproval], [ReceivedDate], [TransportationName], [DeliveryNoteNo], [InwardNumber], [InwardDate], [InwardQuantity], [CreditNoteNo], [CreditNoteDate], [Value], [PlantDocNo], [PlantDocDate], [PlantNotificationNo], [Remarks], [Status], [AllocateServiceEngg], [UpdatedOn], [UpdatedBy]) VALUES (6, 1, N'FIR/2021/0001', NULL, NULL, NULL, NULL, NULL, N'No Scope of Warranty', CAST(0x0000ADA900000000 AS DateTime), N'Test', NULL, NULL, NULL, NULL, N'9', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'Test', N'9', NULL, CAST(0x0000ADAA01239362 AS DateTime), N'Sathiya')
INSERT [dbo].[tbl_FIRDetails] ([Id], [FIRId], [FIRNo], [ReasonForRejection], [AllocateAsc], [DispatchMethod], [DocumentNo], [DispatchDate], [ASCRecommendation], [ASCReportedDate], [ObservationofProblem], [FileUpload], [LRNumber], [LRDate], [DCNo], [ServiceEnggApproval], [ReceivedDate], [TransportationName], [DeliveryNoteNo], [InwardNumber], [InwardDate], [InwardQuantity], [CreditNoteNo], [CreditNoteDate], [Value], [PlantDocNo], [PlantDocDate], [PlantNotificationNo], [Remarks], [Status], [AllocateServiceEngg], [UpdatedOn], [UpdatedBy]) VALUES (7, 1, N'FIR/2021/0001', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'456', N'2021-09-20', N'678', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'Test', N'8', NULL, CAST(0x0000ADAA0123BEFA AS DateTime), N'ASCSOUTH05')
INSERT [dbo].[tbl_FIRDetails] ([Id], [FIRId], [FIRNo], [ReasonForRejection], [AllocateAsc], [DispatchMethod], [DocumentNo], [DispatchDate], [ASCRecommendation], [ASCReportedDate], [ObservationofProblem], [FileUpload], [LRNumber], [LRDate], [DCNo], [ServiceEnggApproval], [ReceivedDate], [TransportationName], [DeliveryNoteNo], [InwardNumber], [InwardDate], [InwardQuantity], [CreditNoteNo], [CreditNoteDate], [Value], [PlantDocNo], [PlantDocDate], [PlantNotificationNo], [Remarks], [Status], [AllocateServiceEngg], [UpdatedOn], [UpdatedBy]) VALUES (8, 1, N'FIR/2021/0001', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'Sunlandlogo.jpg,', NULL, NULL, NULL, NULL, N'Sep 21 2021  5:43PM', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'11', NULL, CAST(0x0000ADAA012434D7 AS DateTime), N'RAJKUMAR')
INSERT [dbo].[tbl_FIRDetails] ([Id], [FIRId], [FIRNo], [ReasonForRejection], [AllocateAsc], [DispatchMethod], [DocumentNo], [DispatchDate], [ASCRecommendation], [ASCReportedDate], [ObservationofProblem], [FileUpload], [LRNumber], [LRDate], [DCNo], [ServiceEnggApproval], [ReceivedDate], [TransportationName], [DeliveryNoteNo], [InwardNumber], [InwardDate], [InwardQuantity], [CreditNoteNo], [CreditNoteDate], [Value], [PlantDocNo], [PlantDocDate], [PlantNotificationNo], [Remarks], [Status], [AllocateServiceEngg], [UpdatedOn], [UpdatedBy]) VALUES (9, 1, N'FIR/2021/0001', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'456', N'2021-09-20', NULL, NULL, NULL, N'Car', N'657', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'Test', N'12', NULL, CAST(0x0000ADAA0125D4AC AS DateTime), N'RAJKUMAR')
INSERT [dbo].[tbl_FIRDetails] ([Id], [FIRId], [FIRNo], [ReasonForRejection], [AllocateAsc], [DispatchMethod], [DocumentNo], [DispatchDate], [ASCRecommendation], [ASCReportedDate], [ObservationofProblem], [FileUpload], [LRNumber], [LRDate], [DCNo], [ServiceEnggApproval], [ReceivedDate], [TransportationName], [DeliveryNoteNo], [InwardNumber], [InwardDate], [InwardQuantity], [CreditNoteNo], [CreditNoteDate], [Value], [PlantDocNo], [PlantDocDate], [PlantNotificationNo], [Remarks], [Status], [AllocateServiceEngg], [UpdatedOn], [UpdatedBy]) VALUES (10, 1, N'FIR/2021/0001', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'768', N'2021-09-20', N'3', NULL, NULL, NULL, NULL, NULL, NULL, N'Test', N'13', NULL, CAST(0x0000ADAA0125F801 AS DateTime), N'Kumar')
INSERT [dbo].[tbl_FIRDetails] ([Id], [FIRId], [FIRNo], [ReasonForRejection], [AllocateAsc], [DispatchMethod], [DocumentNo], [DispatchDate], [ASCRecommendation], [ASCReportedDate], [ObservationofProblem], [FileUpload], [LRNumber], [LRDate], [DCNo], [ServiceEnggApproval], [ReceivedDate], [TransportationName], [DeliveryNoteNo], [InwardNumber], [InwardDate], [InwardQuantity], [CreditNoteNo], [CreditNoteDate], [Value], [PlantDocNo], [PlantDocDate], [PlantNotificationNo], [Remarks], [Status], [AllocateServiceEngg], [UpdatedOn], [UpdatedBy]) VALUES (11, 1, N'FIR/2021/0001', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'8976', N'2021-09-20', N'3', N'789', N'2021-09-20', N'789', N'Test', N'14', NULL, CAST(0x0000ADAA01260EE8 AS DateTime), N'Kumar')
INSERT [dbo].[tbl_FIRDetails] ([Id], [FIRId], [FIRNo], [ReasonForRejection], [AllocateAsc], [DispatchMethod], [DocumentNo], [DispatchDate], [ASCRecommendation], [ASCReportedDate], [ObservationofProblem], [FileUpload], [LRNumber], [LRDate], [DCNo], [ServiceEnggApproval], [ReceivedDate], [TransportationName], [DeliveryNoteNo], [InwardNumber], [InwardDate], [InwardQuantity], [CreditNoteNo], [CreditNoteDate], [Value], [PlantDocNo], [PlantDocDate], [PlantNotificationNo], [Remarks], [Status], [AllocateServiceEngg], [UpdatedOn], [UpdatedBy]) VALUES (12, 1, N'FIR/2021/0001', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'Test', N'15', NULL, CAST(0x0000ADAA012631CB AS DateTime), N'RAJKUMAR')
INSERT [dbo].[tbl_FIRDetails] ([Id], [FIRId], [FIRNo], [ReasonForRejection], [AllocateAsc], [DispatchMethod], [DocumentNo], [DispatchDate], [ASCRecommendation], [ASCReportedDate], [ObservationofProblem], [FileUpload], [LRNumber], [LRDate], [DCNo], [ServiceEnggApproval], [ReceivedDate], [TransportationName], [DeliveryNoteNo], [InwardNumber], [InwardDate], [InwardQuantity], [CreditNoteNo], [CreditNoteDate], [Value], [PlantDocNo], [PlantDocDate], [PlantNotificationNo], [Remarks], [Status], [AllocateServiceEngg], [UpdatedOn], [UpdatedBy]) VALUES (13, 2, N'FIR/2021/0002', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', NULL, CAST(0x0000ADAA013CA0CC AS DateTime), N'Kishore')
INSERT [dbo].[tbl_FIRDetails] ([Id], [FIRId], [FIRNo], [ReasonForRejection], [AllocateAsc], [DispatchMethod], [DocumentNo], [DispatchDate], [ASCRecommendation], [ASCReportedDate], [ObservationofProblem], [FileUpload], [LRNumber], [LRDate], [DCNo], [ServiceEnggApproval], [ReceivedDate], [TransportationName], [DeliveryNoteNo], [InwardNumber], [InwardDate], [InwardQuantity], [CreditNoteNo], [CreditNoteDate], [Value], [PlantDocNo], [PlantDocDate], [PlantNotificationNo], [Remarks], [Status], [AllocateServiceEngg], [UpdatedOn], [UpdatedBy]) VALUES (14, 3, N'FIR/2021/0003', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1', NULL, CAST(0x0000ADAA013E0246 AS DateTime), N'Kishore')
INSERT [dbo].[tbl_FIRDetails] ([Id], [FIRId], [FIRNo], [ReasonForRejection], [AllocateAsc], [DispatchMethod], [DocumentNo], [DispatchDate], [ASCRecommendation], [ASCReportedDate], [ObservationofProblem], [FileUpload], [LRNumber], [LRDate], [DCNo], [ServiceEnggApproval], [ReceivedDate], [TransportationName], [DeliveryNoteNo], [InwardNumber], [InwardDate], [InwardQuantity], [CreditNoteNo], [CreditNoteDate], [Value], [PlantDocNo], [PlantDocDate], [PlantNotificationNo], [Remarks], [Status], [AllocateServiceEngg], [UpdatedOn], [UpdatedBy]) VALUES (15, 3, N'FIR/2021/0003', N'', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'test', N'2', N'G.Sathianarayanan', CAST(0x0000ADAB00A676F0 AS DateTime), N'Kishor')
INSERT [dbo].[tbl_FIRDetails] ([Id], [FIRId], [FIRNo], [ReasonForRejection], [AllocateAsc], [DispatchMethod], [DocumentNo], [DispatchDate], [ASCRecommendation], [ASCReportedDate], [ObservationofProblem], [FileUpload], [LRNumber], [LRDate], [DCNo], [ServiceEnggApproval], [ReceivedDate], [TransportationName], [DeliveryNoteNo], [InwardNumber], [InwardDate], [InwardQuantity], [CreditNoteNo], [CreditNoteDate], [Value], [PlantDocNo], [PlantDocDate], [PlantNotificationNo], [Remarks], [Status], [AllocateServiceEngg], [UpdatedOn], [UpdatedBy]) VALUES (16, 3, N'FIR/2021/0003', NULL, N'Auto-Tech Engineering Service', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'test', N'5', NULL, CAST(0x0000ADAB00A6A34E AS DateTime), N'Sathiya')
INSERT [dbo].[tbl_FIRDetails] ([Id], [FIRId], [FIRNo], [ReasonForRejection], [AllocateAsc], [DispatchMethod], [DocumentNo], [DispatchDate], [ASCRecommendation], [ASCReportedDate], [ObservationofProblem], [FileUpload], [LRNumber], [LRDate], [DCNo], [ServiceEnggApproval], [ReceivedDate], [TransportationName], [DeliveryNoteNo], [InwardNumber], [InwardDate], [InwardQuantity], [CreditNoteNo], [CreditNoteDate], [Value], [PlantDocNo], [PlantDocDate], [PlantNotificationNo], [Remarks], [Status], [AllocateServiceEngg], [UpdatedOn], [UpdatedBy]) VALUES (17, 3, N'FIR/2021/0003', NULL, N'Auto-Tech Engineering Service', N'Courier', N'67890', CAST(0x0000ADAB00A6C972 AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'test', N'6', NULL, CAST(0x0000ADAB00A6C972 AS DateTime), N'Kishore')
INSERT [dbo].[tbl_FIRDetails] ([Id], [FIRId], [FIRNo], [ReasonForRejection], [AllocateAsc], [DispatchMethod], [DocumentNo], [DispatchDate], [ASCRecommendation], [ASCReportedDate], [ObservationofProblem], [FileUpload], [LRNumber], [LRDate], [DCNo], [ServiceEnggApproval], [ReceivedDate], [TransportationName], [DeliveryNoteNo], [InwardNumber], [InwardDate], [InwardQuantity], [CreditNoteNo], [CreditNoteDate], [Value], [PlantDocNo], [PlantDocDate], [PlantNotificationNo], [Remarks], [Status], [AllocateServiceEngg], [UpdatedOn], [UpdatedBy]) VALUES (18, 3, N'FIR/2021/0003', NULL, NULL, NULL, NULL, NULL, N'No Scope of Warranty', CAST(0x0000ADAA00000000 AS DateTime), N'test', N'Sunland2.docx,Sunlandlogo.jpg,Sunland.docx,', NULL, NULL, NULL, NULL, N'Sep 22 2021 10:08AM', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'7', NULL, CAST(0x0000ADAB00A7070C AS DateTime), N'ASCSOUTH05')
INSERT [dbo].[tbl_FIRDetails] ([Id], [FIRId], [FIRNo], [ReasonForRejection], [AllocateAsc], [DispatchMethod], [DocumentNo], [DispatchDate], [ASCRecommendation], [ASCReportedDate], [ObservationofProblem], [FileUpload], [LRNumber], [LRDate], [DCNo], [ServiceEnggApproval], [ReceivedDate], [TransportationName], [DeliveryNoteNo], [InwardNumber], [InwardDate], [InwardQuantity], [CreditNoteNo], [CreditNoteDate], [Value], [PlantDocNo], [PlantDocDate], [PlantNotificationNo], [Remarks], [Status], [AllocateServiceEngg], [UpdatedOn], [UpdatedBy]) VALUES (19, 3, N'FIR/2021/0003', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'10', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'test', N'10', NULL, CAST(0x0000ADAB00A73D31 AS DateTime), N'Sathiya')
INSERT [dbo].[tbl_FIRDetails] ([Id], [FIRId], [FIRNo], [ReasonForRejection], [AllocateAsc], [DispatchMethod], [DocumentNo], [DispatchDate], [ASCRecommendation], [ASCReportedDate], [ObservationofProblem], [FileUpload], [LRNumber], [LRDate], [DCNo], [ServiceEnggApproval], [ReceivedDate], [TransportationName], [DeliveryNoteNo], [InwardNumber], [InwardDate], [InwardQuantity], [CreditNoteNo], [CreditNoteDate], [Value], [PlantDocNo], [PlantDocDate], [PlantNotificationNo], [Remarks], [Status], [AllocateServiceEngg], [UpdatedOn], [UpdatedBy]) VALUES (20, 3, N'FIR/2021/0003', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'456', N'2021-09-21', N'678', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'test', N'8', NULL, CAST(0x0000ADAB00A75CBA AS DateTime), N'ASCSOUTH05')
INSERT [dbo].[tbl_FIRDetails] ([Id], [FIRId], [FIRNo], [ReasonForRejection], [AllocateAsc], [DispatchMethod], [DocumentNo], [DispatchDate], [ASCRecommendation], [ASCReportedDate], [ObservationofProblem], [FileUpload], [LRNumber], [LRDate], [DCNo], [ServiceEnggApproval], [ReceivedDate], [TransportationName], [DeliveryNoteNo], [InwardNumber], [InwardDate], [InwardQuantity], [CreditNoteNo], [CreditNoteDate], [Value], [PlantDocNo], [PlantDocDate], [PlantNotificationNo], [Remarks], [Status], [AllocateServiceEngg], [UpdatedOn], [UpdatedBy]) VALUES (21, 3, N'FIR/2021/0003', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'Sunlandlogo.jpg,Sunland.docx,', NULL, NULL, NULL, NULL, N'Sep 22 2021 10:10AM', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'11', NULL, CAST(0x0000ADAB00A7A26A AS DateTime), N'Kishore')
INSERT [dbo].[tbl_FIRDetails] ([Id], [FIRId], [FIRNo], [ReasonForRejection], [AllocateAsc], [DispatchMethod], [DocumentNo], [DispatchDate], [ASCRecommendation], [ASCReportedDate], [ObservationofProblem], [FileUpload], [LRNumber], [LRDate], [DCNo], [ServiceEnggApproval], [ReceivedDate], [TransportationName], [DeliveryNoteNo], [InwardNumber], [InwardDate], [InwardQuantity], [CreditNoteNo], [CreditNoteDate], [Value], [PlantDocNo], [PlantDocDate], [PlantNotificationNo], [Remarks], [Status], [AllocateServiceEngg], [UpdatedOn], [UpdatedBy]) VALUES (22, 3, N'FIR/2021/0003', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'test', N'16', NULL, CAST(0x0000ADAB00A7B555 AS DateTime), N'Kishore')
INSERT [dbo].[tbl_FIRDetails] ([Id], [FIRId], [FIRNo], [ReasonForRejection], [AllocateAsc], [DispatchMethod], [DocumentNo], [DispatchDate], [ASCRecommendation], [ASCReportedDate], [ObservationofProblem], [FileUpload], [LRNumber], [LRDate], [DCNo], [ServiceEnggApproval], [ReceivedDate], [TransportationName], [DeliveryNoteNo], [InwardNumber], [InwardDate], [InwardQuantity], [CreditNoteNo], [CreditNoteDate], [Value], [PlantDocNo], [PlantDocDate], [PlantNotificationNo], [Remarks], [Status], [AllocateServiceEngg], [UpdatedOn], [UpdatedBy]) VALUES (23, 3, N'FIR/2021/0003', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'test', N'15', NULL, CAST(0x0000ADAB00B12072 AS DateTime), N'Kishore')
INSERT [dbo].[tbl_FIRDetails] ([Id], [FIRId], [FIRNo], [ReasonForRejection], [AllocateAsc], [DispatchMethod], [DocumentNo], [DispatchDate], [ASCRecommendation], [ASCReportedDate], [ObservationofProblem], [FileUpload], [LRNumber], [LRDate], [DCNo], [ServiceEnggApproval], [ReceivedDate], [TransportationName], [DeliveryNoteNo], [InwardNumber], [InwardDate], [InwardQuantity], [CreditNoteNo], [CreditNoteDate], [Value], [PlantDocNo], [PlantDocDate], [PlantNotificationNo], [Remarks], [Status], [AllocateServiceEngg], [UpdatedOn], [UpdatedBy]) VALUES (24, 2, N'FIR/2021/0002', N'', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'Test', N'2', N'G.Sathianarayanan', CAST(0x0000ADAB00CC10AB AS DateTime), N'Kishor')
INSERT [dbo].[tbl_FIRDetails] ([Id], [FIRId], [FIRNo], [ReasonForRejection], [AllocateAsc], [DispatchMethod], [DocumentNo], [DispatchDate], [ASCRecommendation], [ASCReportedDate], [ObservationofProblem], [FileUpload], [LRNumber], [LRDate], [DCNo], [ServiceEnggApproval], [ReceivedDate], [TransportationName], [DeliveryNoteNo], [InwardNumber], [InwardDate], [InwardQuantity], [CreditNoteNo], [CreditNoteDate], [Value], [PlantDocNo], [PlantDocDate], [PlantNotificationNo], [Remarks], [Status], [AllocateServiceEngg], [UpdatedOn], [UpdatedBy]) VALUES (25, 2, N'FIR/2021/0002', NULL, N'Auto-Tech Engineering Service', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'Test', N'5', NULL, CAST(0x0000ADAB00CC309C AS DateTime), N'Sathiya')
INSERT [dbo].[tbl_FIRDetails] ([Id], [FIRId], [FIRNo], [ReasonForRejection], [AllocateAsc], [DispatchMethod], [DocumentNo], [DispatchDate], [ASCRecommendation], [ASCReportedDate], [ObservationofProblem], [FileUpload], [LRNumber], [LRDate], [DCNo], [ServiceEnggApproval], [ReceivedDate], [TransportationName], [DeliveryNoteNo], [InwardNumber], [InwardDate], [InwardQuantity], [CreditNoteNo], [CreditNoteDate], [Value], [PlantDocNo], [PlantDocDate], [PlantNotificationNo], [Remarks], [Status], [AllocateServiceEngg], [UpdatedOn], [UpdatedBy]) VALUES (26, 2, N'FIR/2021/0002', NULL, N'Auto-Tech Engineering Service', N'Courier', N'67890', CAST(0x0000ADAB00CD1319 AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'Test', N'6', NULL, CAST(0x0000ADAB00CD1319 AS DateTime), N'Kishore')
INSERT [dbo].[tbl_FIRDetails] ([Id], [FIRId], [FIRNo], [ReasonForRejection], [AllocateAsc], [DispatchMethod], [DocumentNo], [DispatchDate], [ASCRecommendation], [ASCReportedDate], [ObservationofProblem], [FileUpload], [LRNumber], [LRDate], [DCNo], [ServiceEnggApproval], [ReceivedDate], [TransportationName], [DeliveryNoteNo], [InwardNumber], [InwardDate], [InwardQuantity], [CreditNoteNo], [CreditNoteDate], [Value], [PlantDocNo], [PlantDocDate], [PlantNotificationNo], [Remarks], [Status], [AllocateServiceEngg], [UpdatedOn], [UpdatedBy]) VALUES (27, 2, N'FIR/2021/0002', NULL, NULL, NULL, NULL, NULL, N'No Scope of Warranty', CAST(0x0000ADAA00000000 AS DateTime), N'Test', N'Sunland1.docx,Sunland2.docx,Sunlandlogo.jpg,Sunlan', NULL, NULL, NULL, NULL, N'Sep 22 2021 12:27PM', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'7', NULL, CAST(0x0000ADAB00CD55B9 AS DateTime), N'ASCSOUTH05')
INSERT [dbo].[tbl_FIRDetails] ([Id], [FIRId], [FIRNo], [ReasonForRejection], [AllocateAsc], [DispatchMethod], [DocumentNo], [DispatchDate], [ASCRecommendation], [ASCReportedDate], [ObservationofProblem], [FileUpload], [LRNumber], [LRDate], [DCNo], [ServiceEnggApproval], [ReceivedDate], [TransportationName], [DeliveryNoteNo], [InwardNumber], [InwardDate], [InwardQuantity], [CreditNoteNo], [CreditNoteDate], [Value], [PlantDocNo], [PlantDocDate], [PlantNotificationNo], [Remarks], [Status], [AllocateServiceEngg], [UpdatedOn], [UpdatedBy]) VALUES (28, 2, N'FIR/2021/0002', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'10', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'Test', N'10', NULL, CAST(0x0000ADAB00CDE523 AS DateTime), N'Sathiya')
INSERT [dbo].[tbl_FIRDetails] ([Id], [FIRId], [FIRNo], [ReasonForRejection], [AllocateAsc], [DispatchMethod], [DocumentNo], [DispatchDate], [ASCRecommendation], [ASCReportedDate], [ObservationofProblem], [FileUpload], [LRNumber], [LRDate], [DCNo], [ServiceEnggApproval], [ReceivedDate], [TransportationName], [DeliveryNoteNo], [InwardNumber], [InwardDate], [InwardQuantity], [CreditNoteNo], [CreditNoteDate], [Value], [PlantDocNo], [PlantDocDate], [PlantNotificationNo], [Remarks], [Status], [AllocateServiceEngg], [UpdatedOn], [UpdatedBy]) VALUES (29, 2, N'FIR/2021/0002', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'456', N'2021-09-21', N'678', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'Test', N'8', NULL, CAST(0x0000ADAB00CE3509 AS DateTime), N'ASCSOUTH05')
INSERT [dbo].[tbl_FIRDetails] ([Id], [FIRId], [FIRNo], [ReasonForRejection], [AllocateAsc], [DispatchMethod], [DocumentNo], [DispatchDate], [ASCRecommendation], [ASCReportedDate], [ObservationofProblem], [FileUpload], [LRNumber], [LRDate], [DCNo], [ServiceEnggApproval], [ReceivedDate], [TransportationName], [DeliveryNoteNo], [InwardNumber], [InwardDate], [InwardQuantity], [CreditNoteNo], [CreditNoteDate], [Value], [PlantDocNo], [PlantDocDate], [PlantNotificationNo], [Remarks], [Status], [AllocateServiceEngg], [UpdatedOn], [UpdatedBy]) VALUES (30, 2, N'FIR/2021/0002', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'Sunlandlogo.jpg,', NULL, NULL, NULL, NULL, N'Sep 22 2021 12:40PM', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'11', NULL, CAST(0x0000ADAB00D0E249 AS DateTime), N'Kishore')
INSERT [dbo].[tbl_FIRDetails] ([Id], [FIRId], [FIRNo], [ReasonForRejection], [AllocateAsc], [DispatchMethod], [DocumentNo], [DispatchDate], [ASCRecommendation], [ASCReportedDate], [ObservationofProblem], [FileUpload], [LRNumber], [LRDate], [DCNo], [ServiceEnggApproval], [ReceivedDate], [TransportationName], [DeliveryNoteNo], [InwardNumber], [InwardDate], [InwardQuantity], [CreditNoteNo], [CreditNoteDate], [Value], [PlantDocNo], [PlantDocDate], [PlantNotificationNo], [Remarks], [Status], [AllocateServiceEngg], [UpdatedOn], [UpdatedBy]) VALUES (31, 2, N'FIR/2021/0002', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'Test', N'16', NULL, CAST(0x0000ADAB00D10019 AS DateTime), N'Kishore')
INSERT [dbo].[tbl_FIRDetails] ([Id], [FIRId], [FIRNo], [ReasonForRejection], [AllocateAsc], [DispatchMethod], [DocumentNo], [DispatchDate], [ASCRecommendation], [ASCReportedDate], [ObservationofProblem], [FileUpload], [LRNumber], [LRDate], [DCNo], [ServiceEnggApproval], [ReceivedDate], [TransportationName], [DeliveryNoteNo], [InwardNumber], [InwardDate], [InwardQuantity], [CreditNoteNo], [CreditNoteDate], [Value], [PlantDocNo], [PlantDocDate], [PlantNotificationNo], [Remarks], [Status], [AllocateServiceEngg], [UpdatedOn], [UpdatedBy]) VALUES (32, 2, N'FIR/2021/0002', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'Test', N'15', NULL, CAST(0x0000ADAB00D110B6 AS DateTime), N'Kishore')
SET IDENTITY_INSERT [dbo].[tbl_FIRDetails] OFF
SET IDENTITY_INSERT [dbo].[tbl_FIRRegister] ON 

INSERT [dbo].[tbl_FIRRegister] ([Id], [FIRNo], [WSDName], [WSDBranch], [RetailerName], [RetailerLocation], [PartNo], [PartType], [WSDInvoiceNo], [AggregateSerialNo], [InvoiceDate], [Quantity], [DateofComplaint], [UploadFile], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [Notes]) VALUES (1, N'FIR/2021/0001', N'JMA', N'Ms.JULLUNDUR MOTOR AGENCY (DELHI) LTDNEW MOTOR MARKET,SCF-486,1st & 2nd FLOOR,,MANIMAJRA (U.T.),-160101', N'Sam', N'Guindy', N'987456100', N'Aggregate', N'9898978', N'787878', CAST(0x0000ADA900000000 AS DateTime), 1, CAST(0x0000ADAA01229AFC AS DateTime), N'Sunlandlogo.jpg,Sunlandlogo.jpg,', N'15', CAST(0x0000ADAA01229AFC AS DateTime), N'RAJKUMAR', NULL, N'RAJKUMAR', N'Test')
INSERT [dbo].[tbl_FIRRegister] ([Id], [FIRNo], [WSDName], [WSDBranch], [RetailerName], [RetailerLocation], [PartNo], [PartType], [WSDInvoiceNo], [AggregateSerialNo], [InvoiceDate], [Quantity], [DateofComplaint], [UploadFile], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [Notes]) VALUES (2, N'FIR/2021/0002', N'S.M Motors', N'Velacherry', N'Sam', N'Guindy', N'987456100', N'Aggregate', N'9898978', N'787878', CAST(0x0000ADA900000000 AS DateTime), 1, CAST(0x0000ADAA013CA046 AS DateTime), N'Sunlandlogo.jpg,Sunland1.docx,', N'15', CAST(0x0000ADAA013CA046 AS DateTime), N'Kishore', NULL, N'Kishore', N'Test')
INSERT [dbo].[tbl_FIRRegister] ([Id], [FIRNo], [WSDName], [WSDBranch], [RetailerName], [RetailerLocation], [PartNo], [PartType], [WSDInvoiceNo], [AggregateSerialNo], [InvoiceDate], [Quantity], [DateofComplaint], [UploadFile], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [Notes]) VALUES (3, N'FIR/2021/0003', N'S.M Motors', N'Velacherry', N'Sam', N'Guindy', N'987456100', N'Child Part', N'9898978', N'', CAST(0x0000ADAA00000000 AS DateTime), 4, CAST(0x0000ADAA013E0246 AS DateTime), N'Sunland1.docx,', N'15', CAST(0x0000ADAA013E0246 AS DateTime), N'Kishore', NULL, N'Kishore', N'747')
SET IDENTITY_INSERT [dbo].[tbl_FIRRegister] OFF
SET IDENTITY_INSERT [dbo].[tbl_FIRstatus] ON 

INSERT [dbo].[tbl_FIRstatus] ([Id], [FIRStatus], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [FIRStatusName]) VALUES (1, N'FIR Created', N'Active', CAST(0x0000AD8F0142EB42 AS DateTime), N'Admin', CAST(0x0000AD9000A904FD AS DateTime), N'Admin', N'FIR Created')
INSERT [dbo].[tbl_FIRstatus] ([Id], [FIRStatus], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [FIRStatusName]) VALUES (2, N'Approved By Sales Engineer', N'Active', NULL, N'Admin', NULL, NULL, N'Approve By Sales Engineer')
INSERT [dbo].[tbl_FIRstatus] ([Id], [FIRStatus], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [FIRStatusName]) VALUES (3, N'Rejected By Sales Engineer', N'Active', NULL, N'Admin', NULL, NULL, N'Reject By Sales Engineer')
INSERT [dbo].[tbl_FIRstatus] ([Id], [FIRStatus], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [FIRStatusName]) VALUES (4, N'Pending with Admin Approval', N'Active', NULL, N'Admin', NULL, NULL, N'Pending With Admin Approval')
INSERT [dbo].[tbl_FIRstatus] ([Id], [FIRStatus], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [FIRStatusName]) VALUES (5, N'Allocated to ASC', N'Active', NULL, N'Admin', NULL, NULL, N'Allocate to ASC')
INSERT [dbo].[tbl_FIRstatus] ([Id], [FIRStatus], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [FIRStatusName]) VALUES (6, N'Material Dispatched to ASC', N'Active', NULL, N'Admin', NULL, NULL, N'Material Dispatch to ASC')
INSERT [dbo].[tbl_FIRstatus] ([Id], [FIRStatus], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [FIRStatusName]) VALUES (7, N'Material Received From WSD/ASC Observation', N'Active', NULL, N'Admin', NULL, NULL, N'Material Receive From WSD/ASC Observation')
INSERT [dbo].[tbl_FIRstatus] ([Id], [FIRStatus], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [FIRStatusName]) VALUES (8, N'Material Dispatched to WSD', N'Active', NULL, N'Admin', NULL, NULL, N'Material Dispatch to WSD')
INSERT [dbo].[tbl_FIRstatus] ([Id], [FIRStatus], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [FIRStatusName]) VALUES (9, N'Approved ASC FeedBack', N'Active', NULL, N'Admin', NULL, NULL, N'Approve ASC FeedBack')
INSERT [dbo].[tbl_FIRstatus] ([Id], [FIRStatus], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [FIRStatusName]) VALUES (10, N'Rejected ASC FeedBack', N'Active', NULL, N'Admin', NULL, NULL, N'Reject ASC Feedback')
INSERT [dbo].[tbl_FIRstatus] ([Id], [FIRStatus], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [FIRStatusName]) VALUES (11, N'Material Received From ASC', N'Active', NULL, N'Admin', NULL, NULL, N'Material Receive From ASC')
INSERT [dbo].[tbl_FIRstatus] ([Id], [FIRStatus], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [FIRStatusName]) VALUES (12, N'Material Dispatched to Warehouse', N'Active', NULL, N'Admin', NULL, NULL, N'Material Dispatch to Warehouse')
INSERT [dbo].[tbl_FIRstatus] ([Id], [FIRStatus], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [FIRStatusName]) VALUES (13, N'Material Received From WSD by Warehouse', N'Active', NULL, N'Admin', NULL, NULL, N'Material Receive From WSD by Warehouse')
INSERT [dbo].[tbl_FIRstatus] ([Id], [FIRStatus], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [FIRStatusName]) VALUES (14, N'Credit Note Updated', N'Active', NULL, N'Admin', NULL, NULL, N'Credit Note Updation')
INSERT [dbo].[tbl_FIRstatus] ([Id], [FIRStatus], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [FIRStatusName]) VALUES (15, N'FIR Closed', N'Active', NULL, N'Admin', NULL, NULL, N'FIR Close')
INSERT [dbo].[tbl_FIRstatus] ([Id], [FIRStatus], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [UpdatedBy], [FIRStatusName]) VALUES (16, N'Material Sent Back To Retailer', N'Active', NULL, N'Admin', NULL, NULL, N'Material Send Back To Retailer')
SET IDENTITY_INSERT [dbo].[tbl_FIRstatus] OFF
SET IDENTITY_INSERT [dbo].[tbl_state] ON 

INSERT [dbo].[tbl_state] ([id], [state]) VALUES (1, N'ANDAMAN AND NICOBAR ISLANDS')
INSERT [dbo].[tbl_state] ([id], [state]) VALUES (2, N'ANDHRA PRADESH')
INSERT [dbo].[tbl_state] ([id], [state]) VALUES (3, N'ARUNACHAL PRADESH')
INSERT [dbo].[tbl_state] ([id], [state]) VALUES (4, N'ASSAM')
INSERT [dbo].[tbl_state] ([id], [state]) VALUES (5, N'BIHAR')
INSERT [dbo].[tbl_state] ([id], [state]) VALUES (6, N'CHATTISGARH')
INSERT [dbo].[tbl_state] ([id], [state]) VALUES (7, N'CHANDIGARH')
INSERT [dbo].[tbl_state] ([id], [state]) VALUES (8, N'DAMAN AND DIU')
INSERT [dbo].[tbl_state] ([id], [state]) VALUES (9, N'DELHI')
INSERT [dbo].[tbl_state] ([id], [state]) VALUES (10, N'DADRA AND NAGAR HAVELI')
INSERT [dbo].[tbl_state] ([id], [state]) VALUES (11, N'GOA')
INSERT [dbo].[tbl_state] ([id], [state]) VALUES (12, N'GUJARAT')
INSERT [dbo].[tbl_state] ([id], [state]) VALUES (13, N'HIMACHAL PRADESH')
INSERT [dbo].[tbl_state] ([id], [state]) VALUES (14, N'HARYANA')
INSERT [dbo].[tbl_state] ([id], [state]) VALUES (15, N'JAMMU AND KASHMIR')
INSERT [dbo].[tbl_state] ([id], [state]) VALUES (16, N'JHARKHAND')
INSERT [dbo].[tbl_state] ([id], [state]) VALUES (17, N'KERALA')
INSERT [dbo].[tbl_state] ([id], [state]) VALUES (18, N'KARNATAKA')
INSERT [dbo].[tbl_state] ([id], [state]) VALUES (19, N'LAKSHADWEEP')
INSERT [dbo].[tbl_state] ([id], [state]) VALUES (20, N'MEGHALAYA')
INSERT [dbo].[tbl_state] ([id], [state]) VALUES (21, N'MAHARASHTRA')
INSERT [dbo].[tbl_state] ([id], [state]) VALUES (22, N'MANIPUR')
INSERT [dbo].[tbl_state] ([id], [state]) VALUES (23, N'MADHYA PRADESH')
INSERT [dbo].[tbl_state] ([id], [state]) VALUES (24, N'MIZORAM')
INSERT [dbo].[tbl_state] ([id], [state]) VALUES (25, N'NAGALAND')
INSERT [dbo].[tbl_state] ([id], [state]) VALUES (26, N'ORISSA')
INSERT [dbo].[tbl_state] ([id], [state]) VALUES (27, N'PUNJAB')
INSERT [dbo].[tbl_state] ([id], [state]) VALUES (28, N'PONDICHERRY')
INSERT [dbo].[tbl_state] ([id], [state]) VALUES (29, N'RAJASTHAN')
INSERT [dbo].[tbl_state] ([id], [state]) VALUES (30, N'SIKKIM')
INSERT [dbo].[tbl_state] ([id], [state]) VALUES (31, N'TAMIL NADU')
INSERT [dbo].[tbl_state] ([id], [state]) VALUES (32, N'TRIPURA')
INSERT [dbo].[tbl_state] ([id], [state]) VALUES (33, N'UTTARAKHAND')
INSERT [dbo].[tbl_state] ([id], [state]) VALUES (34, N'UTTAR PRADESH')
INSERT [dbo].[tbl_state] ([id], [state]) VALUES (35, N'WEST BENGAL')
INSERT [dbo].[tbl_state] ([id], [state]) VALUES (36, N'TELANGANA')
SET IDENTITY_INSERT [dbo].[tbl_state] OFF
SET IDENTITY_INSERT [dbo].[tbl_user] ON 

INSERT [dbo].[tbl_user] ([Id], [Name], [Mobile], [EmailId], [State], [City], [BranchorLocation], [UserType], [UserName], [Password], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [Updatedby], [BranchName], [Code]) VALUES (1, N'shanmugavel', N'9791443819', N'shanmugavel@brainmagic.info', N'TAMIL NADU', N'Chennai', N'Velacherry', N'ASC', N'shanmugavel', N'9791443819', N'Active', CAST(0x0000AD8F00CD5250 AS DateTime), N'Admin', CAST(0x0000AD9000F4587E AS DateTime), N'Admin', NULL, NULL)
INSERT [dbo].[tbl_user] ([Id], [Name], [Mobile], [EmailId], [State], [City], [BranchorLocation], [UserType], [UserName], [Password], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [Updatedby], [BranchName], [Code]) VALUES (4, N'Kishore', N'8976577857', N'kishor@brainmagic.info', N'GUJARAT', N'Cambay', N'Velacherry', N'WSD', N'Kishore', N'12345678', N'Active', CAST(0x0000AD9001061CD7 AS DateTime), N'Admin', CAST(0x0000AD9B01000527 AS DateTime), N'Admin', N'S.M Motors', N'9090')
INSERT [dbo].[tbl_user] ([Id], [Name], [Mobile], [EmailId], [State], [City], [BranchorLocation], [UserType], [UserName], [Password], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [Updatedby], [BranchName], [Code]) VALUES (5, N'Kishore', N'123123123', N'kishor@brainmagic.info', N'TAMIL NADU', N'Chennai', N'Velacherry', N'Sales Engineer', N'Kishor', N'123123123', N'Active', CAST(0x0000AD9100B10A3F AS DateTime), N'Admin', CAST(0x0000AD9B01000FAC AS DateTime), N'Admin', N'', NULL)
INSERT [dbo].[tbl_user] ([Id], [Name], [Mobile], [EmailId], [State], [City], [BranchorLocation], [UserType], [UserName], [Password], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [Updatedby], [BranchName], [Code]) VALUES (6, N'Kishore', N'456456456', N'kishor@brainmagic.info', N'HARYANA', N'Ganaur', N'Velacherry', N'Warehouse', N'Kumar', N'456456456', N'Active', CAST(0x0000AD9100DB7305 AS DateTime), N'Admin', NULL, NULL, N'', NULL)
INSERT [dbo].[tbl_user] ([Id], [Name], [Mobile], [EmailId], [State], [City], [BranchorLocation], [UserType], [UserName], [Password], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [Updatedby], [BranchName], [Code]) VALUES (7, N'test', N'8438433851', N'test@gmail.com', N'TAMIL NADU', N'Chennai', N'test', N'ASC', N'test', N'8438433851', N'Active', CAST(0x0000AD9100F2D48D AS DateTime), N'Admin', NULL, NULL, N'S.M Motors', NULL)
INSERT [dbo].[tbl_user] ([Id], [Name], [Mobile], [EmailId], [State], [City], [BranchorLocation], [UserType], [UserName], [Password], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [Updatedby], [BranchName], [Code]) VALUES (11, N'shanmugavel', N'9791443819', N'shanmugavel@brainmagic.info', N'TAMIL NADU', N'Chennai', N'Velacherry', N'WSD', N'shanmugavel', N'9791443819', N'Active', CAST(0x0000AD9101324F27 AS DateTime), N'Admin', NULL, NULL, N'S.M Motors', N'9090')
INSERT [dbo].[tbl_user] ([Id], [Name], [Mobile], [EmailId], [State], [City], [BranchorLocation], [UserType], [UserName], [Password], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [Updatedby], [BranchName], [Code]) VALUES (12, N'RAJKUMAR
', N'9316004642
', N'jmachd@jmaindia.com
', N'Chandigarh
', N'CHANDIGARH
', N'Ms.JULLUNDUR MOTOR AGENCY (DELHI) LTDNEW MOTOR MARKET,SCF-486,1st & 2nd FLOOR,,MANIMAJRA (U.T.),-160101
', N'WSD', N'RAJKUMAR', N'9316004642', N'Active', CAST(0x0000AD9101319731 AS DateTime), N'Admin', CAST(0x0000AD9B01000FAC AS DateTime), N'Admin', N'JMA', N'10249
')
INSERT [dbo].[tbl_user] ([Id], [Name], [Mobile], [EmailId], [State], [City], [BranchorLocation], [UserType], [UserName], [Password], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [Updatedby], [BranchName], [Code]) VALUES (13, N'SUSHIL RANA', N'9888806284', N'mgr_chg@impal.net,chandigarh@impal.net', N'Chandigarh', N'CHANDIGARH', N'Ms.INDIA MOTOR PARTS AND ACC LTDSCO No.484,Motor Market&CommercialComplex,Manimajra UT-160101
', N'WSD', N'SUSHIL RANA', N'9888806284', N'Active', NULL, N'Admin', NULL, N'Admin', N'IMPAL', N'10184
')
INSERT [dbo].[tbl_user] ([Id], [Name], [Mobile], [EmailId], [State], [City], [BranchorLocation], [UserType], [UserName], [Password], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [Updatedby], [BranchName], [Code]) VALUES (14, N'PARAM JEET SINGH
', N'9781705269
', N'maschandigarh@mastvs.com', N'Chandigarh', N'CHANDIGARH', N'Ms.MADRAS AUTO SERVICEMANIMAJRASCO No.379 Motor Market,Chandigarh-160101
', N'WSD', N'PARAM JEET SINGH
', N'9781705269', N'Active', NULL, N'Admin', NULL, N'Admin', N'MAS
', N'17859
')
INSERT [dbo].[tbl_user] ([Id], [Name], [Mobile], [EmailId], [State], [City], [BranchorLocation], [UserType], [UserName], [Password], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [Updatedby], [BranchName], [Code]) VALUES (15, N'SUSHIL KUMAR
', N'8826898806
', N'jmakg1@jmaindia.com
', N'Delhi
', N'DELHI (kashmiri)
', N'Ms.JULLUNDUR MOTOR AGENCY (DELHI) LTD2769, RAMLAL CHANDOK MARG,,KASHMERE GATE-110006
', N'WSD', N'SUSHIL KUMAR
', N'8826898806
', N'Active', NULL, N'Admin', NULL, N'Admin', N'JMA
', N'10229
')
INSERT [dbo].[tbl_user] ([Id], [Name], [Mobile], [EmailId], [State], [City], [BranchorLocation], [UserType], [UserName], [Password], [Status], [CreatedOn], [CreatedBy], [UpdatedOn], [Updatedby], [BranchName], [Code]) VALUES (16, N'NAVEEN SIKRI
', N'9910669074
', N'jasco@jmaindia.com
', N'Delhi
', N'DELHI ', N'Ms.JULLUNDUR AUTO SALES CORPN LTDBehind Minerva Cinema, Kashmere Gate,2746, Mirza Umrao Street,,DELHI-110006
', N'WSD', N'NAVEEN SIKRI
', N'9910669074
', N'Active', NULL, N'Admin', NULL, N'Admin', N'JASCO
', N'10242
')
SET IDENTITY_INSERT [dbo].[tbl_user] OFF
SET IDENTITY_INSERT [dbo].[tbl_usertype] ON 

INSERT [dbo].[tbl_usertype] ([Id], [UserType], [ShortName], [Status], [CreatedOn], [CreatedBy]) VALUES (1, N'Whole Sale Distributor', N'WSD', N'Active', CAST(0x0000AD8E00000000 AS DateTime), N'Admin')
INSERT [dbo].[tbl_usertype] ([Id], [UserType], [ShortName], [Status], [CreatedOn], [CreatedBy]) VALUES (2, N'Sales Engineer', N'Sales Engineer', N'Active', CAST(0x0000AD8E00000000 AS DateTime), N'Admin')
INSERT [dbo].[tbl_usertype] ([Id], [UserType], [ShortName], [Status], [CreatedOn], [CreatedBy]) VALUES (3, N'Service Engineer', N'Service Engineer', N'Active', CAST(0x0000AD8E00000000 AS DateTime), N'Admin')
INSERT [dbo].[tbl_usertype] ([Id], [UserType], [ShortName], [Status], [CreatedOn], [CreatedBy]) VALUES (4, N'Authorized Service Centre', N'ASC', N'Active', CAST(0x0000AD8E00000000 AS DateTime), N'Admin')
INSERT [dbo].[tbl_usertype] ([Id], [UserType], [ShortName], [Status], [CreatedOn], [CreatedBy]) VALUES (5, N'Warehouse', N'Warehouse', N'Active', CAST(0x0000AD8E00000000 AS DateTime), N'Admin')
SET IDENTITY_INSERT [dbo].[tbl_usertype] OFF
USE [master]
GO
ALTER DATABASE [RaneComplaint_Tracking] SET  READ_WRITE 
GO
