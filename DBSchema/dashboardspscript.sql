USE [RaneComplaint_Tracking]
GO
/****** Object:  StoredProcedure [dbo].[sp_b_Dashboard]    Script Date: 28-09-2021 20:14:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_b_Dashboard]
(
@NAME varchar(100)=null,@qtype varchar(50)=null
)
AS
BEGIN
	
	SET NOCOUNT ON;
	--WSDName(RAJKUMAR)
	if(@qtype='WSD')
	Begin
	 select distinct(select count(Id) from tbl_FIRRegister where Status=1 and (WSDName=@NAME))New,
	 (select count(Id) from tbl_FIRRegister where Status not in (1,15,3) and WSDName=@NAME)Pending,
	 (select count(Id) from tbl_FIRRegister where Status=3 and WSDName=@NAME)Rejected,
	 (select count(Id) from tbl_FIRRegister where Status=15 and WSDName=@NAME)Completed,
	 (select count(Id) from tbl_FIRRegister where Status=4 and WSDName=@NAME)PendingWithAdminApproval,
	 (select count(Id) from tbl_FIRRegister where Status=14 and WSDName=@NAME)CreditNoteUpdated,
	 
     (select count(Id) from tbl_FIRRegister where Status=9 and WSDName=@NAME)ServiceEnggApprove,
	 (select count(Id) from tbl_FIRRegister where Status=10 and WSDName=@NAME)ServiceEnggRejected, 
     (select count(Id) from tbl_FIRRegister  where Status=12 and WSDName=@NAME)MaterialDisptachedtoWH,
	 (select count(Id) from tbl_FIRRegister where Status=13 and WSDName=@NAME)MaterialReceivedfromWSDbyWH
     from tbl_FIRRegister fr inner join tbl_FIRDetails fd on fr.Id=fd.FIRId
	End
	--Name(Kishor)
	else if(@qtype='Sales Engineer')
	begin
	  select distinct(select count(Id) from tbl_FIRRegister where Status=1)New,
	 (select count(Id) from tbl_FIRRegister where Status not in (1,15,3))Pending,
	 (select count(Id) from tbl_FIRRegister where Status=3)Rejected,
	 (select count(Id) from tbl_FIRRegister where Status=15)Completed,	 
      (select count(Id) from tbl_FIRRegister where Status=16)MaterialSendBacktoRetailer,
	  (select count(Id) from tbl_FIRRegister r where Status=4 and (select UpdatedBy from tbl_FIRDetails d where r.ID=d.FIRId and d.Status=2)=@NAME)PendingWithAdminApproval,
 (select count(Id) from tbl_FIRRegister r where Status=2 and (select UpdatedBy from tbl_FIRDetails d where r.ID=d.FIRId and d.Status=2)=@NAME)SalesEnggApprove,
  (select count(Id) from tbl_FIRRegister r where Status=3 and (select UpdatedBy from tbl_FIRDetails d where r.ID=d.FIRId and d.Status=2)=@NAME)SalesEnggRejected 
  from tbl_FIRRegister fr inner join tbl_FIRDetails fd on fr.Id=fd.FIRId
	End

	else if(@qtype='ASC')
	begin
	  --BranchName(Auto-Tech Engineering Service)
 select distinct(select count(Id) from tbl_FIRRegister r  where Status=5 and (select AllocateASC from tbl_FIRDetails d where r.ID=d.FIRId and d.Status=5)=(select Name from Authorized where USERNAME=@NAME))AllocatedFIR
  ,(select count(Id) from tbl_FIRRegister r  where Status=6 and (select AllocateASC from tbl_FIRDetails d where r.ID=d.FIRId and d.Status=5)=(select Name from Authorized where USERNAME=@NAME))MaterialDispatchedtoASC,
 (select count(Id) from tbl_FIRRegister r where Status=7 and (select AllocateASC from tbl_FIRDetails d where r.ID=d.FIRId and d.Status=5)=(select Name from Authorized where USERNAME=@NAME))MaterialReceivedFromWSDorASCObservation,
 (select count(Id) from tbl_FIRRegister r where Status=8 and (select AllocateASC from tbl_FIRDetails d where r.ID=d.FIRId and d.Status=5)=(select Name from Authorized where USERNAME=@NAME))MaterialDispatchedtoWSD,
  (select count(Id) from tbl_FIRRegister r where Status=9 and (select AllocateASC from tbl_FIRDetails d where r.ID=d.FIRId and d.Status=5)=(select Name from Authorized where USERNAME=@NAME))ASCFeedbackApproved,
   (select count(Id) from tbl_FIRRegister r where Status=10 and (select AllocateASC from tbl_FIRDetails d where r.ID=d.FIRId and d.Status=5)=(select Name from Authorized where USERNAME=@NAME))ASCFeedbackRejected,
      (select count(Id) from tbl_FIRRegister r where Status=15 and (select AllocateASC from tbl_FIRDetails d where r.ID=d.FIRId and d.Status=5)=(select Name from Authorized where userName=@NAME))Completed
  from tbl_FIRRegister fr inner join tbl_FIRDetails fd on fd.FIRId =fr.Id 
	End
	--InchargeName(G Sathianarayanan)
	else if(@qtype='Service Engineer')
	begin	
   
  select distinct(select count(Id) from tbl_FIRRegister r where Status=2 and (select AllocateServiceEngg from tbl_FIRDetails d where r.ID=d.FIRId and d.Status=2)=(select Name from ServiceEng where USERNAME=@NAME))AllocatedFIR,
  (select count(Id) from tbl_FIRRegister r where Status=9 and (select AllocateServiceEngg from tbl_FIRDetails d where r.ID=d.FIRId and d.Status=2)=(select Name from ServiceEng where USERNAME=@NAME))ASCFeedbackApproved,
  (select count(Id) from tbl_FIRRegister r where Status=10 and (select AllocateServiceEngg from tbl_FIRDetails d where r.ID=d.FIRId and d.Status=2)=(select Name from ServiceEng where USERNAME=@NAME))ASCFeedbackRejected,
  (select count(Id) from tbl_FIRRegister r where Status=15 and (select AllocateServiceEngg from tbl_FIRDetails d where r.ID=d.FIRId and d.Status=2)=(select Name from ServiceEng where USERNAME=@NAME))Completed
  from tbl_FIRRegister fr inner join tbl_FIRDetails fd on fr.Id=fd.FIRId
	End
	--Name(Kumar)
	else if(@qtype='Warehouse')
	begin
	  select distinct(select count(Id) from tbl_FIRRegister r where Status=12)MaterialDispatchedToWarehouse,
  (select count(Id) from tbl_FIRRegister r where Status=13 and (select UpdatedBy from tbl_FIRDetails d where r.ID=d.FIRId and d.Status=13)=@NAME)MaterialReceivedFromWSDbyWarehouse,
  (select count(Id) from tbl_FIRRegister r where Status=14 and (select UpdatedBy from tbl_FIRDetails d where r.ID=d.FIRId and d.Status=13)=@NAME)PendingwithWarehouseorCreditnoteupdated,
  (select count(Id) from tbl_FIRRegister r where Status=15 and (select UpdatedBy from tbl_FIRDetails d where r.ID=d.FIRId and d.Status=13)=@NAME)Completed
  from tbl_FIRRegister fr inner join tbl_FIRDetails fd on fd.FIRId =fr.Id  
	End
	else if(@qtype='Tracking')
	begin
	select distinct a.Id,a.FIRNo,a.CreatedBy,a.CreatedOn,a.AggregateSerialNo,a.PartNo as AggregatePartNo,a.AggregateModel,a.DateofComplaint,a.InvoiceDate,a.Quantity,a.Notes as CustomerComplaint,a.RetailerLocation,a.RetailerName,a.WSDBranch,a.WSDName,a.WSDInvoiceNo,
(select b.UpdatedOn from tbl_FIRDetails b where b.status in (2,3) and b.FIRId=a.Id)SalesEnggApprovedorRejectedDate,(select b.AllocateServiceEngg from tbl_FIRDetails b where b.status in (2) and b.FIRId=a.Id)AllocatedServiceEngg,(select b.UpdatedBy from tbl_FIRDetails b where b.status in (2,3) and b.FIRId=a.Id)HandledSalesEngg, 
(select b.AllocateAsc from tbl_FIRDetails b where b.status in (5) and b.FIRId=a.Id)AllocateASC,(select b.UpdatedOn from tbl_FIRDetails b where b.status in (5) and b.FIRId=a.Id)ASCAllocatedDate,
(select b.UpdatedOn from tbl_FIRDetails b where b.status in (6) and b.FIRId=a.Id)DispatchedTOASCDate,(select b.DispatchMethod from tbl_FIRDetails b where b.status in (6) and b.FIRId=a.Id)DispatchedMethod,(select b.DocumentNo from tbl_FIRDetails b where b.status in (6) and b.FIRId=a.Id)DocNo,
(select b.UpdatedOn from tbl_FIRDetails b where b.status in (7) and b.FIRId=a.Id)ReceivedtoASCDate,(select b.ASCRecommendation from tbl_FIRDetails b where b.status in (7) and b.FIRId=a.Id)ASCRecommendation,(select b.ASCReportedDate from tbl_FIRDetails b where b.status in (7) and b.FIRId=a.Id)ASCReportedDate,
(select b.ObservationofProblem from tbl_FIRDetails b where b.status in (7) and b.FIRId=a.Id)observationofproblem,(select b.UpdatedOn from tbl_FIRDetails b where b.status in (8) and b.FIRId=a.Id)DIspatchedtoWSDDate,
(select b.LRNumber from tbl_FIRDetails b where b.status in (8) and b.FIRId=a.Id)LRNo,(select b.LRDate from tbl_FIRDetails b where b.status in (8) and b.FIRId=a.Id)LRDate,(select b.DCNo from tbl_FIRDetails b where b.status in (8) and b.FIRId=a.Id)DCNO,
(select b.UpdatedOn from tbl_FIRDetails b where b.status in (9) and b.FIRId=a.Id)ASCFeedbackApprobedDate,(select b.ASCRecommendation from tbl_FIRDetails b where b.status in (9) and b.FIRId=a.Id)ServiceEnggRecommendation,
(select b.UpdatedOn from tbl_FIRDetails b where b.status in (11) and b.FIRId=a.Id)MaterialReceivedDate, (select b.UpdatedOn from tbl_FIRDetails b where b.status in (12) and b.FIRId=a.Id)DispatchedtowhDate,
(select b.LRNumber from tbl_FIRDetails b where b.status in (12) and b.FIRId=a.Id)whLRno,(select b.LRDate from tbl_FIRDetails b where b.status in (12) and b.FIRId=a.Id)whLRDATE,
(select b.TransportationName from tbl_FIRDetails b where b.status in (12) and b.FIRId=a.Id)TransportationName,(select b.DeliveryNoteNo from tbl_FIRDetails b where b.status in (12) and b.FIRId=a.Id)DeliveryNoteNo,
(select b.UpdatedOn from tbl_FIRDetails b where b.status in (13) and b.FIRId=a.Id)WHReceivedDate,(select b.InwardNumber from tbl_FIRDetails b where b.status in (13) and b.FIRId=a.Id)InwardNumber,
(select b.InwardQuantity from tbl_FIRDetails b where b.status in (13) and b.FIRId=a.Id)InwardQuantity,(select b.InwardDate from tbl_FIRDetails b where b.status in (13) and b.FIRId=a.Id)InwardDate,
(select b.CreditNoteNo from tbl_FIRDetails b where b.status in (14) and b.FIRId=a.Id)CreditNoteNo ,(select b.CreditNoteDate from tbl_FIRDetails b where b.status in (14) and b.FIRId=a.Id)CreditNoteDate,
(select b.Value from tbl_FIRDetails b where b.status in (14) and b.FIRId=a.Id)Value,(select b.PlantDocNo from tbl_FIRDetails b where b.status in (14) and b.FIRId=a.Id)PlantDocNo,
(select b.PlantDocDate from tbl_FIRDetails b where b.status in (14) and b.FIRId=a.Id)PlantDocDate,(select b.PlantNotificationNo from tbl_FIRDetails b where b.status in (14) and b.FIRId=a.Id)PlantNotificationNo,
(select b.UpdatedOn from tbl_FIRDetails b where b.status in (14) and b.FIRId=a.Id)CreditNoteUpdatedDate,(select b.UpdatedOn from tbl_FIRDetails b where b.status in (15) and b.FIRId=a.Id)CompletedDate,(select FIRStatus from tbl_FIRstatus f where f.Id=a.Status)StatusName
 from tbl_FIRRegister a inner join tbl_FIRDetails b on a.id=b.FIRId 
End
	END


GO
